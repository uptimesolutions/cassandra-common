/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Delete;
import com.datastax.oss.driver.api.mapper.annotations.Insert;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import com.datastax.oss.driver.api.mapper.annotations.Update;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.SET_TO_NULL;
import java.lang.reflect.Field;

/**
 *
 * @author kpati
 * @param <T>
 */
public interface BaseDAO<T> {

    @Insert
    void createLocal(T t) throws UnavailableException, WriteTimeoutException;

    @Update(nullSavingStrategy = SET_TO_NULL)
    void updateLocal(T t) throws UnavailableException, WriteTimeoutException;

    @Delete
    void deleteLocal(T t) throws UnavailableException, WriteTimeoutException;

    @Select
    PagingIterable<T> getAll() throws UnavailableException, ReadTimeoutException;

}
