/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteAssetPresetDAO extends BaseDAO<SiteAssetPreset> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteAssetPresetDAO.class);

    /**
     * Inserts the given entity into the site_device_preset table
     *
     * @param entity, SiteAssetPreset Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(SiteAssetPreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of SiteAssetPreset into the site_device_preset
     * table
     *
     * @param entities, List Object of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<SiteAssetPreset> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the site_device_preset table
     *
     * @param entity, SiteAssetPreset Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(SiteAssetPreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entities in the site_device_preset table
     *
     * @param entities, List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<SiteAssetPreset> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the site_device_preset table.
     *
     * @param entity, SiteAssetPreset Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(SiteAssetPreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the site_device_preset table.
     *
     * @param entities, List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<SiteAssetPreset> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteAssetPreset> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    default public List<SiteAssetPreset> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
         if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        return queryByCustomerSite(customerAccount, siteId).all();
    }
    
    
    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteAssetPreset> queryByCustomerSiteAssetType(String customerAccount, UUID siteId, String assetTypeName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    default public List<SiteAssetPreset> findByCustomerSiteAssetType(String customerAccount, UUID siteId, String assetTypeName) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
         if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (assetTypeName == null || assetTypeName.isEmpty()) {
            throw new IllegalArgumentException("Argument Asset Type Name cannot be null or empty.");
        }

        return queryByCustomerSiteAssetType(customerAccount, siteId, assetTypeName).all();
    }

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @param assetPresetId, UUID Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteAssetPreset> queryByCustomerSiteAssetTypePresetId(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @param assetPresetId, UUID Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    default public List<SiteAssetPreset> findByCustomerSiteAssetTypePresetId(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
         if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (assetTypeName == null || assetTypeName.isEmpty()) {
            throw new IllegalArgumentException("Argument Asset Type Name cannot be null or empty.");
        }
        if (assetPresetId == null) {
            throw new IllegalArgumentException("Argument assetPresetId cannot be null.");
        }

        return queryByCustomerSiteAssetTypePresetId(customerAccount, siteId, assetTypeName, assetPresetId).all();
    }

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @param assetPresetId, UUID Object
     * @param pointLocationName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteAssetPreset> queryByPK(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId, String pointLocationName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteAssetPreset data by customer account and siteId from
     * site_asset_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @param assetPresetId, UUID Object
     * @param pointLocationName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    default public List<SiteAssetPreset> findByPK(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId, String pointLocationName) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (assetTypeName == null || assetTypeName.isEmpty()) {
            throw new IllegalArgumentException("Argument Asset Type Name cannot be null or empty.");
        }
        if (assetPresetId == null) {
            throw new IllegalArgumentException("Argument assetPresetId cannot be null.");
        }
         if (pointLocationName == null || pointLocationName.isEmpty()) {
            throw new IllegalArgumentException("Argument Point Location Name cannot be null or empty.");
        }

        return queryByPK(customerAccount, siteId, assetTypeName, assetPresetId, pointLocationName).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and
     * statementType
     *
     * @param entity, SiteAssetPreset Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteAssetPreset entity, String statementType) {
        StringBuilder query, values;
        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getAssetTypeName() != null && !entity.getAssetTypeName().isEmpty()
                && entity.getAssetPresetId() != null
                && entity.getPointLocationName() != null && !entity.getPointLocationName().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_asset_preset(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",asset_type_name");
                    values.append(",'").append(entity.getAssetTypeName()).append("'");
                    query.append(",asset_preset_id");
                    values.append(",").append(entity.getAssetPresetId());
                    query.append(",point_location_name");
                    values.append(",'").append(entity.getPointLocationName()).append("'");
                    query.append(",asset_preset_name");
                    values.append(",").append(entity.getAssetPresetName());
                    query.append(",device_preset_id");
                    values.append(",").append(entity.getDevicePresetId());
                    query.append(",device_preset_type");
                    values.append(",").append(entity.getDevicePresetType());
                    query.append(",sample_interval");
                    values.append(",").append(entity.getSampleInterval());
                    query.append(",ff_set_ids");
                    values.append(",").append(entity.getFfSetIds());
                    query.append(",roll_diameter");
                    values.append(",").append(entity.getRollDiameter());
                    query.append(",roll_diameter_units");
                    values.append(",").append(entity.getRollDiameterUnits());
                    query.append(",speed_ratio");
                    values.append(",'").append(entity.getSpeedRatio()).append("'");
                    query.append(",tach_id");
                    values.append(",'").append(entity.getTachId()).append("'");
                    query.append(",description");
                    values.append(",'").append(entity.getDescription()).append("'");

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_device_preset WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND asset_type_name=").append(entity.getAssetTypeName()).append("'");
                    query.append(" AND asset_preset_id=").append(entity.getAssetPresetId());
                    query.append(" AND point_location_name='").append(entity.getPointLocationName()).append("'");
                }
                break;
            }
        }

        return query.toString();
    }

}
