/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteFaultFrequenciesDAO extends BaseDAO<SiteFaultFrequencies> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteFaultFrequenciesDAO.class);

    /**
     * Inserts the given entity into the site_fault_frequencies table
     *
     * @param entity, SiteFaultFrequencies Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(SiteFaultFrequencies entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of SiteFaultFrequencies into the site_fault_frequencies table
     *
     * @param entities, List Object of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<SiteFaultFrequencies> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the site_fault_frequencies table
     *
     * @param entity, SiteFaultFrequencies Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(SiteFaultFrequencies entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entities in the site_fault_frequencies table
     *
     * @param entities, List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<SiteFaultFrequencies> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the site_fault_frequencies table.
     *
     * @param entity, SiteFaultFrequencies Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(SiteFaultFrequencies entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the site_fault_frequencies table.
     *
     * @param entities, List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<SiteFaultFrequencies> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Returns SiteFaultFrequencies Objects for the given customerAccount and siteId from site_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteFaultFrequencies> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns SiteFaultFrequencies Objects for the given customerAccount and siteId from site_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteFaultFrequencies> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        return queryByCustomerSite(customerAccount, siteId).all();
    }

    /**
     * Returns SiteFaultFrequencies Objects for the given customerAccount, siteId and ffSetId from site_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @return PagingIterable of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteFaultFrequencies> queryByCustomerSiteSet(String customerAccount, UUID siteId, UUID ffSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns SiteFaultFrequencies Objects for the given customerAccount, siteId and ffSetId from site_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @return PagingIterable of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteFaultFrequencies> findByCustomerSiteSet(String customerAccount, UUID siteId, UUID ffSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (ffSetId == null) {
            throw new IllegalArgumentException("Argument ffSetId cannot be null or empty.");
        }

        return queryByCustomerSiteSet(customerAccount, siteId, ffSetId).all();
    }

    /**
     * Returns SiteFaultFrequencies Objects for the given customerAccount, siteId, ffSetId and ffId from site_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return PagingIterable of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteFaultFrequencies> queryByPK(String customerAccount, UUID siteId, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns SiteFaultFrequencies Objects for the given customerAccount, siteId, ffSetId and ffId from site_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return PagingIterable of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteFaultFrequencies> findByPK(String customerAccount, UUID siteId, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (ffSetId == null) {
            throw new IllegalArgumentException("Argument ffSetId cannot be null or empty.");
        }
        if (ffId == null) {
            throw new IllegalArgumentException("Argument ffId cannot be null or empty.");
        }
        return queryByPK(customerAccount, siteId, ffSetId, ffId).all();
    }

    /**
     * Truncate the site_fault_frequencies table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE site_fault_frequencies")
    public void truncateAllSiteFaultFrequencies() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the site_fault_frequencies table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllSiteFaultFrequencies();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new SiteFaultFrequencies(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new SiteFaultFrequencies(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, SiteFaultFrequencies Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteFaultFrequencies entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getFfSetId() != null
                && entity.getFfId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_fault_frequencies(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",ff_set_id");
                    values.append(",").append(entity.getFfSetId());
                    query.append(",ff_id");
                    values.append(",").append(entity.getFfId());
                    query.append(",ff_value");
                    values.append(",").append(entity.getFfValue());
                    if (entity.getFfSetName() != null) {
                        query.append(",ff_set_name");
                        values.append(",'").append(entity.getFfSetName()).append("'");
                    }
                    if (entity.getFfName() != null) {
                        query.append(",ff_name");
                        values.append(",'").append(entity.getFfName()).append("'");
                    }
                    if (entity.getFfSetDesc() != null) {
                        query.append(",ff_set_desc");
                        values.append(",'").append(entity.getFfSetDesc()).append("'");
                    }
                    if (entity.getFfUnit() != null) {
                        query.append(",ff_unit");
                        values.append(",'").append(entity.getFfUnit()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_fault_frequencies WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND ff_set_id=").append(entity.getFfSetId());
                    query.append(" AND ff_id=").append(entity.getFfId()).append(";");
                }
                break;
            }
        }
        return query.toString();
    }
}
