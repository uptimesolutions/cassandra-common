/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author Joseph
 */
public class PresetsMapperImpl {

    private static PresetsMapper instance = null;

    public static PresetsMapper getInstance() {
        if (instance == null) {
            new PresetsMapperImpl();
        }
        return instance;
    }

    private PresetsMapperImpl(){
        instance = new PresetsMapperBuilder(CassandraData.getSession()).build();
    }
}
