/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface CustomerApSetDAO extends BaseDAO<CustomerApSet> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(CustomerApSetDAO.class);

    /**
     * Inserts the given entity into the customer_ap_set table
     *
     * @param entity, CustomerApSet Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(CustomerApSet entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the customer_ap_set table
     *
     * @param entity, CustomerApSet Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(CustomerApSet entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the customer_ap_set table.
     *
     * @param entity, CustomerApSet Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(CustomerApSet entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Gets the CustomerApSet data by customer account from customer_ap_set table
     *
     * @param customerAccount, String Object
     * @return List of CustomerApSet objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<CustomerApSet> queryByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the CustomerApSet data by customer account from customer_ap_set table
     *
     * @param customerAccount, String Object
     * @return List of CustomerApSet objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<CustomerApSet> findByCustomer(String customerAccount) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        return queryByCustomer(customerAccount).all();
    }

    /**
     * Gets the CustomerApSet data by customer account and apSetId from customer_ap_set table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return List of CustomerApSet Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<CustomerApSet> queryByPK(String customerAccount, UUID apSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the CustomerApSet data by customer account and apSetId from customer_ap_set table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return List of CustomerApSet Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<CustomerApSet> findByPK(String customerAccount, UUID apSetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null or empty.");
        }

        return queryByPK(customerAccount, apSetId).all();
    }

    /**
     * Truncate the customer_ap_set table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE customer_ap_set")
    public void truncateAllCustomerApSet() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the customer_ap_set table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllCustomerApSet();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new CustomerApSet(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new CustomerApSet(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, CustomerApSet Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(CustomerApSet entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getApSetId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO customer_ap_set(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",ap_set_id");
                    values.append(",").append(entity.getApSetId());
                    query.append(",fmax");
                    values.append(",").append(entity.getFmax());
                    query.append(",resolution");
                    values.append(",").append(entity.getResolution());
                    query.append(",period");
                    values.append(",").append(entity.getPeriod());
                    query.append(",sample_rate");
                    values.append(",").append(entity.getSampleRate());
                    if (entity.getApSetName() != null) {
                        query.append(",ap_set_name");
                        values.append(",'").append(entity.getApSetName()).append("'");
                    }
                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type ");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }
                    if (entity.getSensorType() != null) {
                        query.append(",site_id");
                        values.append(",").append(entity.getSiteId());
                    }
                    query.append(") values (").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM customer_ap_set WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND ap_set_id=").append(entity.getApSetId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }
}
