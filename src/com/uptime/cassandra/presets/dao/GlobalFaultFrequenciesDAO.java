/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface GlobalFaultFrequenciesDAO extends BaseDAO<GlobalFaultFrequencies> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(GlobalFaultFrequenciesDAO.class);

    /**
     * Inserts the given entity into the global_fault_frequencies table
     *
     * @param entity, GlobalFaultFrequencies Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(GlobalFaultFrequencies entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of GlobalFaultFrequencies into the global_fault_frequencies table
     *
     * @param entities, List Object of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<GlobalFaultFrequencies> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the global_fault_frequencies table
     *
     * @param entity, GlobalFaultFrequencies Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(GlobalFaultFrequencies entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entities in the global_fault_frequencies table
     *
     * @param entities, List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<GlobalFaultFrequencies> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the global_fault_frequencies table.
     *
     * @param entity, GlobalFaultFrequencies Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(GlobalFaultFrequencies entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the global_fault_frequencies table.
     *
     * @param entities, List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<GlobalFaultFrequencies> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Returns GlobalFaultFrequencies Objects for the given setName from global_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @return PagingIterable of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalFaultFrequencies> queryByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of GlobalFaultFrequencies Objects for the given customerAccount from global_fault_frequencies table
     *
     * @param customerAccount
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<GlobalFaultFrequencies> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        return queryByCustomer(customerAccount).all();
    }

    /**
     * Returns GlobalFaultFrequencies Objects for the given customerAccount and ffSetId from global_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param ffSetId, UUID Object
     * @return PagingIterable of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalFaultFrequencies> queryByCustomerSetId(String customerAccount, UUID ffSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns GlobalFaultFrequencies Objects for the given customerAccount and ffSetId from global_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param ffSetId, UUID Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<GlobalFaultFrequencies> findByCustomerSetId(String customerAccount, UUID ffSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (ffSetId == null) {
            throw new IllegalArgumentException("Argument ffSetId cannot be null.");
        }

        return queryByCustomerSetId(customerAccount, ffSetId).all();
    }

    /**
     * Returns GlobalFaultFrequencies Objects for the given customerAccount, ffSetId and ffId from global_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return PagingIterable of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalFaultFrequencies> queryByPK(String customerAccount, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of GlobalFaultFrequencies Objects for the customerAccount, ffSetId and ffId from global_fault_frequencies table
     *
     * @param customerAccount, String Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<GlobalFaultFrequencies> findByPK(String customerAccount, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (ffSetId == null) {
            throw new IllegalArgumentException("Argument ffSetId cannot be null.");
        }
        if (ffId == null) {
            throw new IllegalArgumentException("Argument ffId cannot be null.");
        }
        return queryByPK(customerAccount, ffSetId, ffId).all();
    }

    /**
     * Truncate the global_fault_frequencies table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE global_fault_frequencies")
    public void truncateAllGlobalFaultFrequencies() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes all the entities from the global_fault_frequencies table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllGlobalFaultFrequencies();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new GlobalFaultFrequencies(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new GlobalFaultFrequencies(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, GlobalFaultFrequencies Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(GlobalFaultFrequencies entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getFfSetId() != null && entity.getFfId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO global_fault_frequencies(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",ff_set_id");
                    values.append(",").append(entity.getFfSetId());
                    query.append(",ff_id");
                    values.append(",").append(entity.getFfId());
                    query.append(",ff_value");
                    values.append(",").append(entity.getFfValue());
                    if (entity.getFfSetName() != null) {
                        query.append(",ff_set_name");
                        values.append(",'").append(entity.getFfSetName()).append("'");
                    }
                    if (entity.getFfName() != null) {
                        query.append(",ff_name");
                        values.append(",'").append(entity.getFfName()).append("'");
                    }
                    if (entity.getFfSetDesc() != null) {
                        query.append(",ff_set_desc");
                        values.append(",'").append(entity.getFfSetDesc()).append("'");
                    }
                    if (entity.getFfUnit() != null) {
                        query.append(",ff_unit");
                        values.append(",'").append(entity.getFfUnit()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM global_fault_frequencies WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND ff_set_id=").append(entity.getFfSetId());
                    query.append(" AND ff_id=").append(entity.getFfId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
