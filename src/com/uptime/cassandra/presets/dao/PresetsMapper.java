/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;

/**
 *
 * @author Joseph
 */
@Mapper
public interface PresetsMapper {

    @DaoFactory
    SiteFaultFrequenciesDAO siteFaultFrequenciesDAO();

    @DaoFactory
    SiteTachometersDAO siteTachometersDAO();

    @DaoFactory
    GlobalTachometersDAO globalTachometersDAO();

    @DaoFactory
    GlobalFaultFrequenciesDAO globalFaultFrequenciesDAO();

    @DaoFactory
    GlobalApAlSetsDAO globalApAlSetsDAO();

    @DaoFactory
    GlobalApAlSetsByCustomerDAO globalApAlSetsByCustomerDAO();

    @DaoFactory
    SiteApAlSetsDAO siteApAlSetsDAO();

    @DaoFactory
    SiteApAlSetsByCustomerDAO siteApAlSetsByCustomerDAO();

    @DaoFactory
    CustomerApSetDAO customerApSetDAO();

    @DaoFactory
    GlobalDevicePresetDAO globalDevicePresetDAO();

    @DaoFactory
    SiteDevicePresetDAO siteDevicePresetDAO();

    @DaoFactory
    GlobalPtLocationNamesDAO globalPtLocationNamesDAO();

    @DaoFactory
    SitePtLocationNamesDAO sitePtLocationNamesDAO();

    @DaoFactory
    BearingCatalogDAO bearingCatalogDAO();
    
    @DaoFactory
    SiteBearingFavoritesDAO siteBearingFavoritesDAO();
    
    @DaoFactory
    SiteFFSetFavoritesDAO siteFFSetFavoritesDAO();
    
    @DaoFactory
    SiteAssetPresetDAO siteAssetPresetDAO();
    
    @DaoFactory
    GlobalAssetTypesDAO globalAssetTypesDAO();
    
    @DaoFactory
    GlobalAssetPresetDAO globalAssetPresetDAO();
}
