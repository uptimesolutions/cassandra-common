/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteDevicePresetDAO extends BaseDAO<SiteDevicePreset> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteDevicePresetDAO.class);

    /**
     * Inserts the given entity into the site_device_preset table
     *
     * @param entity, SiteDevicePreset Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(SiteDevicePreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of SiteDevicePreset into the site_device_preset table
     *
     * @param entities, List Object of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<SiteDevicePreset> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the site_device_preset table
     *
     * @param entity, SiteDevicePreset Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(SiteDevicePreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entities in the site_device_preset table
     *
     * @param entities, List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<SiteDevicePreset> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the site_device_preset table.
     *
     * @param entity, SiteDevicePreset Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(SiteDevicePreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the site_device_preset table.
     *
     * @param entities, List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<SiteDevicePreset> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Gets the SiteDevicePreset data by customer account and siteId from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteDevicePreset> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteDevicePreset data by customer account and siteId from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteDevicePreset> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }

        return queryByCustomerSite(customerAccount, siteId).all();
    }

    /**
     * Gets the SiteDevicePreset data by customer account, siteId and device type from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteDevicePreset> queryByCustomerSiteDeviceType(String customerAccount, UUID siteId, String deviceType) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteDevicePreset data by customer account, siteId and device Type from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    default public List<SiteDevicePreset> findByCustomerSiteDeviceType(String customerAccount, UUID siteId, String deviceType) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (deviceType == null || deviceType.isEmpty()) {
            throw new IllegalArgumentException("Argument Device Type cannot be null or empty.");
        }

        return queryByCustomerSiteDeviceType(customerAccount, siteId, deviceType).all();
    }

    /**
     * Gets the SiteDevicePreset data by customer account, siteId, device type and presetId from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteDevicePreset> queryByCustomerSiteDeviceTypePreset(String customerAccount, UUID siteId, String deviceType, UUID presetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteDevicePreset data by customer account, siteId, device Type and presetId from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    default public List<SiteDevicePreset> findByCustomerSiteDeviceTypePreset(String customerAccount, UUID siteId, String deviceType, UUID presetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (deviceType == null || deviceType.isEmpty()) {
            throw new IllegalArgumentException("Argument Device Type cannot be null or empty.");
        }
        if (presetId == null) {
            throw new IllegalArgumentException("Argument presetId cannot be null.");
        }

        return queryByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId).all();
    }

    /**
     * Gets the SiteDevicePreset data by customer account, siteId, device Type, presetId, channel type and channel number from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @param channelType, String Object
     * @param channelNumber, byte
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteDevicePreset> queryByPK(String customerAccount, UUID siteId, String deviceType, UUID presetId, String channelType, byte channelNumber) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteDevicePreset data by customer account, siteId, deviceType, presetId, channelType and channelNumber from site_device_preset table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType,, String Object
     * @param presetId, UUID Object
     * @param channelType, String Object
     * @param channelNumber, byte
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteDevicePreset> findByPK(String customerAccount, UUID siteId, String deviceType, UUID presetId, String channelType, byte channelNumber) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (deviceType == null || deviceType.isEmpty()) {
            throw new IllegalArgumentException("Argument Device Type cannot be null or empty.");
        }
        if (presetId == null) {
            throw new IllegalArgumentException("Argument presetId cannot be null.");
        }
        if (channelType == null || channelType.isEmpty()) {
            throw new IllegalArgumentException("Argument Channel Type cannot be null or empty.");
        }
//        if (channelNumber == 0) {
//            throw new IllegalArgumentException("Argument Channel Number cannot be 0.");
//        }
        return queryByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber).all();
    }
    
    /**
     * Truncate the site_device_preset table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE site_device_preset")
    public void truncateAllSiteDevicePreset() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the site_device_preset table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllSiteDevicePreset();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new SiteDevicePreset(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new SiteDevicePreset(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, SiteDevicePreset Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteDevicePreset entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getDeviceType() != null && !entity.getDeviceType().isEmpty()
                && entity.getPresetId() != null
                && entity.getChannelType() != null && !entity.getChannelType().isEmpty()
                && entity.getChannelNumber() != 0) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_device_preset(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",device_type");
                    values.append(",'").append(entity.getDeviceType()).append("'");
                    query.append(",preset_id");
                    values.append(",").append(entity.getPresetId());
                    query.append(",channel_type");
                    values.append(",'").append(entity.getChannelType()).append("'");
                    query.append(",channel_number");
                    values.append(",").append(entity.getChannelNumber());
                    if (entity.getApSetId() != null) {
                        query.append(",ap_set_id");
                        values.append(",").append(entity.getApSetId());
                    }
                    if (entity.getAlSetId() != null) {
                        query.append(",al_set_id");
                        values.append(",").append(entity.getAlSetId());
                    }
                    query.append(",alarm_enabled");
                    values.append(",").append(entity.isAlarmEnabled());
                    query.append(",auto_acknowledge");
                    values.append(",").append(entity.isAutoAcknowledge());
                    query.append(",is_disabled ");
                    values.append(",").append(entity.isDisabled());
                    if (entity.getName() != null) {
                        query.append(",name");
                        values.append(",'").append(entity.getName()).append("'");
                    }
                    query.append(",sample_interval");
                    values.append(",").append(entity.getSampleInterval());
                    query.append(",sensor_offset");
                    values.append(",").append(entity.getSensorOffset());
                    query.append(",sensor_sensitivity");
                    values.append(",").append(entity.getSensorSensitivity());

                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }
                    if (entity.getSensorUnits() != null) {
                        query.append(",sensor_units");
                        values.append(",'").append(entity.getSensorUnits()).append("'");
                    }
                    
                    if (entity.getSensorSubType() != null) {
                        query.append(",sensor_sub_type");
                        values.append(",'").append(entity.getSensorSubType()).append("'");
                    }
                    
                    if (entity.getSensorLocalOrientation() != null) {
                        query.append(",sensor_local_orientation");
                        values.append(",'").append(entity.getSensorLocalOrientation()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_device_preset WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND device_type=").append(entity.getDeviceType()).append("'");
                    query.append(" AND preset_id=").append(entity.getPresetId());
                    query.append(" AND channel_type='").append(entity.getChannelType()).append("'");
                    query.append(" AND channel_number=").append(entity.getChannelNumber()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}