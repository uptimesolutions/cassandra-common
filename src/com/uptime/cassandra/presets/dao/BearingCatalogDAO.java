/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.PreparedStatement;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.presets.entity.BearingCatalog;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joseph
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface BearingCatalogDAO extends BaseDAO<BearingCatalog> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(BearingCatalogDAO.class);

    /**
     * Inserts the given entity into the bearing_catalog table
     *
     * @param entity, BearingCatalog Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(BearingCatalog entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the bearing_catalog table
     *
     * @param entity, BearingCatalog Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(BearingCatalog entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the bearing_catalog table.
     *
     * @param entity, BearingCatalog Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(BearingCatalog entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, BearingCatalog Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(BearingCatalog entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getBearingId() != null && !entity.getBearingId().isEmpty()
                && entity.getVendorId() != null && !entity.getVendorId().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO bearing_catalog(vendor_id");
                    values.append("'").append(entity.getVendorId()).append("'");
                    query.append(",bearing_id");
                    values.append(",'").append(entity.getBearingId()).append("'");
                    query.append(",bc");
                    values.append(",").append(entity.getBc());
                    query.append(",bpfi");
                    values.append(",").append(entity.getBpfi());
                    query.append(",bpfo");
                    values.append(",").append(entity.getBpfo());
                    query.append(",bsf");
                    values.append(",").append(entity.getBsf());
                    query.append(",ftf");
                    values.append(",").append(entity.getFtf());
                    query.append(") values (").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {                    
                    query.append("DELETE FROM bearing_catalog WHERE vendor_id='").append(entity.getVendorId()).append("'");                    
                    query.append(" AND bearing_id='").append(entity.getBearingId()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

    /**
     * Gets the BearingCatalog data by vendorId and bearingId from bearing_catalog table
     *
     * @param vendorId
     * @param bearingId
     * @return List of BearingCatalog Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<BearingCatalog> queryByPK(String vendorId, String bearingId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the BearingCatalog data by vendorId and bearingId from bearing_catalog table
     *
     * @param vendorId
     * @param bearingId
     * @return List of BearingCatalog Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<BearingCatalog> findByPK(String vendorId, String bearingId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (vendorId == null || vendorId.isEmpty()) {
            throw new IllegalArgumentException("Argument vendorId cannot be null or empty.");
        }
        if (bearingId == null || bearingId.isEmpty()) {
            throw new IllegalArgumentException("Argument bearingId cannot be null.");
        }

        return queryByPK(vendorId, bearingId).all();
    }

    /**
     * Gets the BearingCatalog data by vendorId from bearing_catalog table
     *
     * @param vendorId
     * @return List of BearingCatalog objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<BearingCatalog> queryByVendorId(String vendorId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the BearingCatalog data by vendorId from bearing_catalog table
     *
     * @param vendorId
     * @return List of BearingCatalog objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<BearingCatalog> findByVendorId(String vendorId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (vendorId == null || vendorId.isEmpty()) {
            throw new IllegalArgumentException("Argument vendorId cannot be null or empty.");
        }

        return queryByVendorId(vendorId).all();
    }

    /**
     * Fetches the all distinct vendor_id records from the bearing_catalog table.
     * @return List of String Objects
     * 
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<String> findDistinctVendors() throws UnavailableException, WriteTimeoutException {
        List<String> vendorList = new ArrayList<>();
        BoundStatement query_distinct_vendors = CassandraData.getSession().prepare("select distinct vendor_id from bearing_catalog").bind();
        ResultSet resultSet = CassandraData.getSession().execute(query_distinct_vendors);
        // Get the results of the query
        while (resultSet.iterator().hasNext()) {
            vendorList.add(resultSet.iterator().next().getString("vendor_id"));
        }
        return vendorList;
    }
}
