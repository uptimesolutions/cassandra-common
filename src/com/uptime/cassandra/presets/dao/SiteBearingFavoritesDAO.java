/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteBearingFavoritesDAO extends BaseDAO<SiteBearingFavorites> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteBearingFavoritesDAO.class);

    /**
     * Inserts the given entity into the site_bearing_favorites table
     *
     * @param entity, SiteBearingFavorites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(SiteBearingFavorites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of SiteBearingFavorites into the site_bearing_favorites table
     *
     * @param entities, List Object of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<SiteBearingFavorites> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * update the given entity in the site_bearing_favorites table
     *
     * @param entity, SiteBearingFavorites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(SiteBearingFavorites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

        /**
     * update the given entities in the site_bearing_favorites table
     *
     * @param entities, List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<SiteBearingFavorites> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }
    
    /**
     * Deletes the given entity from the site_bearing_favorites table.
     *
     * @param entity, SiteBearingFavorites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(SiteBearingFavorites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the site_bearing_favorites table.
     *
     * @param entities, List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<SiteBearingFavorites> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     * Gets the SiteBearingFavorites data by customer account and siteId from site_bearing_favorites table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     *
     */
    @Select
    PagingIterable<SiteBearingFavorites> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteBearingFavorites data by customer account and siteId from site_bearing_favorites table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteBearingFavorites> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }

        return queryByCustomerSite(customerAccount, siteId).all();
    }

    /**
     * Gets the SiteBearingFavorites data by customer account, siteId and vendorId from site_bearing_favorites table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param vendorId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteBearingFavorites> queryByCustomerSiteVendor(String customerAccount, UUID siteId, String vendorId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteBearingFavorites data by customer account, siteId and vendorId from site_bearing_favorites table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param vendorId, String Object
     * @return List of SiteBearingFavorites Objects
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteBearingFavorites> findByCustomerSiteVendor(String customerAccount, UUID siteId, String vendorId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (vendorId == null || vendorId.isEmpty()) {
            throw new IllegalArgumentException("Argument vendorId cannot be null or empty.");
        }
        return queryByCustomerSiteVendor(customerAccount, siteId, vendorId).all();
    }

    /**
     * Gets the SiteBearingFavorites data by customer account, siteId, vendorId and bearingId from site_bearing_favorites table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param vendorId, String Object
     * @param bearingId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteBearingFavorites> queryByPK(String customerAccount, UUID siteId, String vendorId, String bearingId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteBearingFavorites data by customer account, siteId, vendorId and bearingId from site_bearing_favorites table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param vendorId, String Object
     * @param bearingId, String Object
     * @return List of SiteBearingFavorites Objects
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteBearingFavorites> findByPK(String customerAccount, UUID siteId, String vendorId, String bearingId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (vendorId == null || vendorId.isEmpty()) {
            throw new IllegalArgumentException("Argument vendorId cannot be null or empty.");
        }
        if (bearingId == null || bearingId.isEmpty()) {
            throw new IllegalArgumentException("Argument bearingId cannot be null or empty.");
        }
        return queryByPK(customerAccount, siteId, vendorId, bearingId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, SiteBearingFavorites Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteBearingFavorites entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAcct() != null && !entity.getCustomerAcct().isEmpty()
                && entity.getSiteId() != null
                && entity.getVendorId() != null && !entity.getVendorId().isEmpty()
                && entity.getBearingId() != null && !entity.getBearingId().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_bearing_favorites(customer_acct");
                    values.append("'").append(entity.getCustomerAcct()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",vendor_id");
                    values.append(",'").append(entity.getVendorId()).append("'");
                    query.append(",bearing_id");
                    values.append(",'").append(entity.getBearingId()).append("'");
                    query.append(",ftf");
                    values.append(",").append(entity.getFtf());
                    query.append(",bsf");
                    values.append(",").append(entity.getBsf());
                    query.append(",bpfo");
                    values.append(",").append(entity.getBpfo());
                    query.append(",bpfi ");
                    values.append(",").append(entity.getBpfi());

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_bearing_favorites WHERE customer_acct='").append(entity.getCustomerAcct()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND vendor_id='").append(entity.getVendorId()).append("'");
                    query.append(" AND bearing_id='").append(entity.getBearingId()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

}
