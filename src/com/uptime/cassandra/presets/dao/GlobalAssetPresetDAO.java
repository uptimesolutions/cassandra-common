/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import static com.uptime.cassandra.presets.dao.GlobalAssetPresetDAO.SLF4J_CQL_LOG;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joseph
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface GlobalAssetPresetDAO extends BaseDAO<GlobalAssetPreset> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(GlobalAssetPresetDAO.class);

    /**
     * Inserts the given entity into the global_asset_preset table
     *
     * @param entity, GlobalAssetPreset Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(GlobalAssetPreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

      /**
     * Inserts the given List of GlobalAssetPreset into the global_asset_preset table
     *
     * @param entities, List Object of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<GlobalAssetPreset> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * update the given entity in the global_asset_preset table
     *
     * @param entity, GlobalAssetPreset Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(GlobalAssetPreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

     /**
     * update the given entities in the global_asset_preset table
     *
     * @param entities, List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<GlobalAssetPreset> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }
    
    /**
     * Deletes the given entity from the global_asset_preset table.
     *
     * @param entity, GlobalAssetPreset Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(GlobalAssetPreset entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the global_asset_preset table.
     *
     * @param entities, List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<GlobalAssetPreset> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, GlobalAssetPreset Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(GlobalAssetPreset entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getAssetTypeName() != null && entity.getAssetTypeName() != null
                && entity.getAssetPresetId() != null
                && entity.getPointLocationName() != null && !entity.getPointLocationName().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO global_asset_preset(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",asset_type_name");
                    values.append(",'").append(entity.getAssetTypeName()).append("'");
                    query.append(",asset_preset_id");
                    values.append(",").append(entity.getAssetPresetId());
                    query.append(",point_location_name");
                    values.append(",'").append(entity.getPointLocationName()).append("'");
                    if (entity.getAssetPresetName() != null) {
                        query.append(",asset_preset_name");
                        values.append(",'").append(entity.getAssetPresetName()).append("'");
                    }
                    query.append(",device_preset_id");
                    values.append(",").append(entity.getDevicePresetId());
                    query.append(",device_preset_type");
                    values.append(",").append(entity.getDevicePresetType());
                    query.append(",sample_interval");
                    values.append(",").append(entity.getSampleInterval());
                    if (entity.getFfSetIds() != null) {
                        query.append(",ff_set_ids ");
                        values.append(",'").append(entity.getFfSetIds()).append("'");
                    }
                    query.append(",roll_diameter");
                    values.append(",").append(entity.getRollDiameter());
                    if (entity.getRollDiameterUnits() != null) {
                        query.append(",roll_diameter_units");
                        values.append(",'").append(entity.getRollDiameterUnits()).append("'");
                    }
                    query.append(",speed_ratio");
                    values.append(",").append(entity.getSpeedRatio());
                    query.append(",tach_id");
                    values.append(",").append(entity.getTachId());

                    if (entity.getDescription() != null) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM global_asset_preset WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND asset_type_name='").append(entity.getAssetTypeName()).append("'");
                    query.append(" AND asset_preset_id=").append(entity.getAssetPresetId());
                    query.append(" AND point_location_name='").append(entity.getPointLocationName()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

    /**
     * Gets the GlobalAssetPreset data by customer  from global_asset_preset table
     *
     * @param customer
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalAssetPreset> queryByCustomer(String customer) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalAssetPreset data by customer and assetTypeName from global_asset_preset table
     *
     * @param customer
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalAssetPreset> findByCustomer(String customer) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customer == null || customer.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
       
        return queryByCustomer(customer).all();
    }

    /**
     * Gets the GlobalAssetPreset data by customer and assetTypeName from global_asset_preset table
     *
     * @param customer
     * @param assetTypeName
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalAssetPreset> queryByCustomerAssetType(String customer, String assetTypeName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalAssetPreset data by customer and assetTypeName from global_asset_preset table
     *
     * @param customer
     * @param assetTypeName
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalAssetPreset> findByCustomerAssetType(String customer, String assetTypeName) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customer == null || customer.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (assetTypeName == null || assetTypeName.isEmpty()) {
            throw new IllegalArgumentException("Argument AssetType Name cannot be null.");
        }

        return queryByCustomerAssetType(customer, assetTypeName).all();
    }

    /**
     * Gets the GlobalAssetPreset data by customer , assetTypeName and assetPresetId from global_asset_preset table
     *
     * @param customer
     * @param assetTypeName
     * @param assetPresetId
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalAssetPreset> queryByCustomerAssetTypePresetId(String customer, String assetTypeName, UUID assetPresetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalAssetPreset data by customer , assetTypeName and assetPresetId from global_asset_preset table
     *
     * @param customer
     * @param assetTypeName
     * @param assetPresetId
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalAssetPreset> findByCustomerAssetTypePresetId(String customer, String assetTypeName, UUID assetPresetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customer == null || customer.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (assetTypeName == null || assetTypeName.isEmpty()) {
            throw new IllegalArgumentException("Argument AssetType Name cannot be null.");
        }
        if (assetPresetId == null) {
            throw new IllegalArgumentException("Argument assetPresetId cannot be null.");
        }

        return queryByCustomerAssetTypePresetId(customer, assetTypeName, assetPresetId).all();
    }

    /**
     * Gets the GlobalAssetPreset data by customer , assetTypeName, assetPresetId and pointLocationName from global_asset_preset table
     *
     * @param customer
     * @param assetTypeName
     * @param assetPresetId
     * @param pointLocationName
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalAssetPreset> queryByPK(String customer, String assetTypeName, UUID assetPresetId, String pointLocationName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalAssetPreset data by customer , assetTypeName, assetPresetId and pointLocationName from global_asset_preset table
     *
     * @param customer
     * @param assetTypeName
     * @param assetPresetId
     * @param pointLocationName
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalAssetPreset> findByPK(String customer, String assetTypeName, UUID assetPresetId, String pointLocationName) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customer == null || customer.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (assetTypeName == null || assetTypeName.isEmpty()) {
            throw new IllegalArgumentException("Argument AssetType Name cannot be null or empty.");
        }
        if (assetPresetId == null) {
            throw new IllegalArgumentException("Argument assetPresetId cannot be null.");
        }
        if (pointLocationName == null || pointLocationName.isEmpty()) {
            throw new IllegalArgumentException("Argument PointLocation Name cannot be null or empty.");
        }
        return queryByPK(customer, assetTypeName, assetPresetId, pointLocationName).all();
    }

    /**
     * Gets the GlobalAssetPreset data by customer from global_asset_preset table
     *
     * @param customer
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
//    @Select
//    PagingIterable<GlobalAssetPreset> queryByCustomer(String customer) throws UnavailableException, ReadTimeoutException;
    /**
     * Gets the GlobalAssetPreset data by customer from global_asset_preset table
     *
     * @param customer
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
//    default public List<GlobalAssetPreset> findByCustomer(String customer) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
//        if (customer == null || customer.isEmpty()) {
//            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
//        }
//
//        return queryByCustomer(customer).all();
//    }
}
