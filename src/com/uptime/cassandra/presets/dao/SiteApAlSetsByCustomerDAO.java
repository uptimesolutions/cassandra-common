/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Delete;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteApAlSetsByCustomerDAO extends BaseDAO<SiteApAlSetsByCustomer> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteApAlSetsByCustomerDAO.class);

    /**
     * Inserts the given entity into the site_ap_al_sets_by_customer table
     *
     * @param entity, SiteApAlSetsByCustomer Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(SiteApAlSetsByCustomer entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the site_ap_al_sets_by_customer table
     *
     * @param entity, SiteApAlSetsByCustomer Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(SiteApAlSetsByCustomer entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the site_ap_al_sets_by_customer table.
     *
     * @param entity, SiteApAlSetsByCustomer Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(SiteApAlSetsByCustomer entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given partition from the site_ap_al_sets_by_customer table.
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @param alSetId
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Delete(entityClass = SiteApAlSetsByCustomer.class)
    void deleteByCustomerApSetId(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the given partition from the site_ap_al_sets_by_customer table.
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @param alSetId
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (sensorType == null || sensorType.isEmpty()) {
            throw new IllegalArgumentException("Argument sensorType cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null or empty.");
        }
        if (alSetId == null) {
            throw new IllegalArgumentException("Argument alSetId cannot be null or empty.");
        }
        try {
            deleteByCustomerApSetId(customerAccount, siteId, sensorType, apSetId, alSetId);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(customerAccount, siteId, sensorType, apSetId, alSetId));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(customerAccount, siteId, sensorType, apSetId, alSetId));
            }
            throw e;
        }
    }

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account and ap set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteApAlSetsByCustomer> queryByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account and ap set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteApAlSetsByCustomer> findByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }

        return queryByCustomerSiteId(customerAccount, siteId).all();
    }

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account and ap set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteApAlSetsByCustomer> queryByCustomerSiteIdSensorType(String customerAccount, UUID siteId, String sensorType) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account and ap set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteApAlSetsByCustomer> findByCustomerSensorType(String customerAccount, UUID siteId, String sensorType) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (sensorType == null || sensorType.isEmpty()) {
            throw new IllegalArgumentException("Argument sensorType cannot be null or empty.");
        }
        return queryByCustomerSiteIdSensorType(customerAccount, siteId, sensorType).all();
    }

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account and ap set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteApAlSetsByCustomer> queryByCustomerSiteIdSensorTypeApSetId(String customerAccount, UUID siteId, String sensorType, UUID apSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account and ap set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteApAlSetsByCustomer> findByCustomerSensorTypeApSetId(String customerAccount, UUID siteId, String sensorType, UUID apSetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (sensorType == null || sensorType.isEmpty()) {
            throw new IllegalArgumentException("Argument sensorType cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null or empty.");
        }
        return queryByCustomerSiteIdSensorTypeApSetId(customerAccount, siteId, sensorType, apSetId).all();
    }

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account, apSetId and alSetId from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteApAlSetsByCustomer> queryByCustomerSiteIdSensorTypeApSet(String customerAccount, UUID siteId, String sensorType, UUID apSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account, ap set name and al set name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    default public List<SiteApAlSetsByCustomer> findByCustomerSiteIdSensorTypeApSet(String customerAccount, UUID siteId, String sensorType, UUID apSetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (sensorType == null || sensorType.isEmpty()) {
            throw new IllegalArgumentException("Argument sensorType cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }

        return queryByCustomerSiteIdSensorTypeApSet(customerAccount, siteId, sensorType, apSetId).all();
    }

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account, apSetId, alSetId and param name from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteApAlSetsByCustomer> queryByPK(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteApAlSetsByCustomer data by customer account, apSetId, alSetId and paramName from site_ap_al_sets_by_customer table
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteApAlSetsByCustomer> findByPK(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (sensorType == null || sensorType.isEmpty()) {
            throw new IllegalArgumentException("Argument sensorType cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        if (alSetId == null) {
            throw new IllegalArgumentException("Argument alSetId cannot be null.");
        }
        return queryByPK(customerAccount, siteId, sensorType, apSetId, alSetId).all();
    }

    /**
     * Truncate the site_ap_al_sets_by_customer table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE site_ap_al_sets_by_customer")
    public void truncateAllSiteApAlSetsByCustomer() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the site_ap_al_sets_by_customer table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllSiteApAlSetsByCustomer();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new SiteApAlSetsByCustomer(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new SiteApAlSetsByCustomer(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, SiteApAlSetsByCustomer Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteApAlSetsByCustomer entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSensorType() != null && !entity.getSensorType().isEmpty()
                && entity.getApSetId() != null && entity.getAlSetId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_ap_al_sets_by_customer (customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }
                    query.append(",ap_set_id");
                    values.append(",").append(entity.getApSetId());
                    query.append(",al_set_id");
                    values.append(",").append(entity.getAlSetId());
                    if (entity.getApSetName() != null) {
                        query.append(",ap_set_name");
                        values.append(",'").append(entity.getApSetName()).append("'");
                    }
                    if (entity.getAlSetName() != null) {
                        query.append(",al_set_name");
                        values.append(",'").append(entity.getAlSetName()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_ap_al_sets_by_customer WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND sensor_type='").append(entity.getSensorType()).append("'");
                    query.append(" AND ap_set_id=").append(entity.getApSetId());
                    query.append(" AND al_set_id=").append(entity.getAlSetId()).append(";");

                }
                break;
            }
        }

        return query.toString();
    }

    /**
     * Return a String Object of the CQL Statement based on the given values
     *
     * @param customerAccount, String Object
     * @param siteId
     * @param sensorType
     * @param apSetId, UUID Object
     * @param alSetId
     * @return String Object
     */
    default public String printCQL(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) {
        StringBuilder query;

        query = new StringBuilder();
        query.append("DELETE FROM site_ap_al_sets_by_customer WHERE customer_acct='").append(customerAccount).append("'");
        query.append(" AND site_id=").append(siteId);
        query.append(" AND sensor_type='").append(sensorType).append("'");
        query.append(" AND ap_set_id=").append(apSetId);
        query.append(" AND al_set_id=").append(alSetId).append(";");

        return query.toString();
    }
}
