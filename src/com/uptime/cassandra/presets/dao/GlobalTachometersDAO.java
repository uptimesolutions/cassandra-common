/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface GlobalTachometersDAO extends BaseDAO<GlobalTachometers> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(GlobalTachometersDAO.class);

    /**
     * Inserts the given entity into the global_tachometers table
     *
     * @param entity, GlobalTachometers Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(GlobalTachometers entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the global_tachometers table
     *
     * @param entity, GlobalTachometers Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(GlobalTachometers entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the global_tachometers table.
     *
     * @param entity, GlobalTachometers Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(GlobalTachometers entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns GlobalTachometers Objects for the given customerAccount from global_tachometers table
     *
     * @param customerAccount, String Object
     * @return PagingIterable of GlobalTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalTachometers> queryByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of GlobalTachometers Objects for the given customerAccount from global_tachometers table
     *
     * @param customerAccount, String Object
     * @return List of GlobalTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<GlobalTachometers> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        return queryByCustomer(customerAccount).all();
    }

    /**
     * Returns GlobalTachometers Objects for the given customerAccount and tachName from global_tachometers table
     *
     * @param customerAccount, String Object
     * @param tachId, UUID Object
     * @return PagingIterable of GlobalTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalTachometers> queryByPK(String customerAccount, UUID tachId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of GlobalTachometers Objects for the given customerAccount and tachName from global_tachometers table
     *
     * @param customerAccount, String Object
     * @param tachId, UUID Object
     * @return List of GlobalTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<GlobalTachometers> findByPK(String customerAccount, UUID tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (tachId == null) {
            throw new IllegalArgumentException("Argument tachId cannot be null.");
        }

        return queryByPK(customerAccount, tachId).all();
    }

    /**
     * Truncate the global_tachometers table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE global_tachometers")
    public void truncateAllGlobalTachometers() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes all the entities from the global_tachometers table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllGlobalTachometers();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new GlobalTachometers(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new GlobalTachometers(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, GlobalTachometers Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(GlobalTachometers entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getTachId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO global_tachometers(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",tach_id");
                    values.append(",").append(entity.getTachId());
                    query.append(",tach_reference_units");
                    values.append(",").append(entity.getTachReferenceUnits());
                    query.append(",tach_reference_speed");
                    values.append(",").append(entity.getTachReferenceSpeed());

                    if (entity.getTachName() != null) {
                        query.append(",tach_name");
                        values.append(",'").append(entity.getTachName()).append("'");
                    }
                    if (entity.getTachType() != null) {
                        query.append(",tach_type");
                        values.append(",'").append(entity.getTachType()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_tachometers WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND tach_id=").append(entity.getTachId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
