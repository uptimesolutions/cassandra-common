/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.GlobalAssetTypes;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface GlobalAssetTypesDAO extends BaseDAO<GlobalAssetTypes> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(GlobalAssetTypesDAO.class);

    /**
     * Inserts the given entity into the global_asset_types table
     *
     * @param entity, GlobalAssetTypes Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(GlobalAssetTypes entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of GlobalAssetTypes into the global_asset_types table
     *
     * @param entities, List Object of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<GlobalAssetTypes> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the global_asset_types table
     *
     * @param entity, GlobalAssetTypes Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(GlobalAssetTypes entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entities in the global_asset_types table
     *
     * @param entities, List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<GlobalAssetTypes> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the global_asset_types table.
     *
     * @param entity, GlobalAssetTypes Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(GlobalAssetTypes entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the global_asset_types table.
     *
     * @param entities, List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<GlobalAssetTypes> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Gets the GlobalAssetTypes data by customer account from global_asset_types table
     *
     * @param customerAccount, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalAssetTypes> queryByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalAssetTypes data by customer account from global_asset_types table
     *
     * @param customerAccount, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalAssetTypes> findByCustomer(String customerAccount) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }

        return queryByCustomer(customerAccount).all();
    }

    /**
     * Gets the GlobalAssetTypes data by customer account and asset type from global_asset_types table
     *
     * @param customerAccount, String Object
     * @param assetType, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalAssetTypes> queryByPK(String customerAccount, String assetType) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalAssetTypes data by customer account and asset type from global_asset_types table
     *
     * @param customerAccount, String Object
     * @param assetType, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    default public List<GlobalAssetTypes> findByPK(String customerAccount, String assetType) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (assetType == null || assetType.isEmpty()) {
            throw new IllegalArgumentException("Argument Asset Type cannot be null.");
        }

        return queryByPK(customerAccount, assetType).all();
    }


    /**
     * Truncate the global_asset_types table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE global_asset_types")
    public void truncateAllGlobalAssetTypes() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes all the entities from the global_asset_types table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllGlobalAssetTypes();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new GlobalAssetTypes(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new GlobalAssetTypes(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, GlobalAssetTypes Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(GlobalAssetTypes entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getAssetTypeName() != null && !entity.getAssetTypeName().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO global_asset_types(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",asset_type_name");
                    values.append(",'").append(entity.getAssetTypeName()).append("'");
                    if (entity.getDescription() != null && !entity.getDescription().isEmpty()) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM global_asset_types WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND asset_type_name='").append(entity.getAssetTypeName()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

}
