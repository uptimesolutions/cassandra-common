/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joseph
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteFFSetFavoritesDAO extends BaseDAO<SiteFFSetFavorites> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteFFSetFavoritesDAO.class);

    /**
     * Inserts the given entity into the site_ff_set_favorites table
     *
     * @param entity, SiteFFSetFavorites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(SiteFFSetFavorites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of SiteFaultFrequencies into the site_fault_frequencies table
     *
     * @param entities, List Object of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<SiteFFSetFavorites> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the site_ff_set_favorites table
     *
     * @param entity, SiteFFSetFavorites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(SiteFFSetFavorites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entities in the site_ff_set_favorites table
     *
     * @param entities, List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<SiteFFSetFavorites> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the site_ff_set_favorites table.
     *
     * @param entity, SiteFFSetFavorites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(SiteFFSetFavorites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the site_ff_set_favorites table.
     *
     * @param entities, List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<SiteFFSetFavorites> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Deletes the given entity from the site_ff_set_favorites table.
     *
     * @param customerAccount
     * @param siteId
     * @param ffSetId
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(String customerAccount, UUID siteId, UUID ffSetId) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (ffSetId == null) {
            throw new IllegalArgumentException("Argument ffSetId cannot be null or empty.");
        }
        try {
            deleteBycustomerAcctSiteIdffSetId(customerAccount, siteId, ffSetId);
            SLF4J_CQL_LOG.info("DELETE FROM site_ff_set_favorites WHERE \n"
                    + " customer_acct=" + customerAccount
                    + " and site_id=" + siteId
                    + " and ff_set_id=" + ffSetId);
        } catch (WriteTimeoutException e) {
            SLF4J_CQL_LOG.error("DELETE FROM site_ff_set_favorites WHERE \n"
                    + " customer_acct=" + customerAccount
                    + " and site_id=" + siteId
                    + " and ff_set_id=" + ffSetId);
            throw e;
        }
    }

    @Query("DELETE FROM site_ff_set_favorites WHERE \n"
            + "customer_acct=:customerAcct\n"
            + "and site_id=:siteId \n"
            + "and ff_set_id=:ffSetId \n")
    void deleteBycustomerAcctSiteIdffSetId(String customerAcct, UUID siteId, UUID ffSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Deletes the given entity from the site_ff_set_favorites table.
     *
     * @param customerAccount
     * @param siteId
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(String customerAccount, UUID siteId) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }

        try {
            deleteByCustomerAccountSiteId(customerAccount, siteId);
            SLF4J_CQL_LOG.info("DELETE FROM site_ff_set_favorites WHERE \n"
                    + "customer_acct=" + customerAccount
                    + " and site_id=" + siteId);
        } catch (WriteTimeoutException e) {
            SLF4J_CQL_LOG.error("DELETE FROM site_ff_set_favorites WHERE \n"
                    + "customer_acct=" + customerAccount
                    + " and site_id=" + siteId);
            throw e;
        }
    }

    @Query("DELETE FROM site_ff_set_favorites WHERE \n"
            + "customer_acct=:customerAcct\n"
            + "and site_id=:siteId \n")
    void deleteByCustomerAccountSiteId(String customerAcct, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, SiteFFSetFavorites Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteFFSetFavorites entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getFfSetId() != null
                && entity.getFfId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_ff_set_favorites(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",ff_set_id");
                    values.append(",").append(entity.getFfSetId());
                    query.append(",ff_id ");
                    values.append(",").append(entity.getFfId());
                    query.append(",ff_set_name");
                    values.append(",'").append(entity.getFfSetName()).append("'");
                    query.append(",ff_set_desc ");
                    values.append(",'").append(entity.getFfSetDesc()).append("'");
                    query.append(",ff_name");
                    values.append(",'").append(entity.getFfName()).append("'");
                    query.append(",ff_value ");
                    values.append(",").append(entity.getFfValue());
                    query.append(",ff_unit");
                    values.append(",'").append(entity.getFfUnit()).append("'");
                    query.append(") values (").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_ff_set_favorites WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND ff_set_id=").append(entity.getFfSetId());
                    query.append(" AND ff_id=").append(entity.getFfId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

    /**
     * Gets the SiteFFSetFavorites data by vendorId and bearingId from site_ff_set_favorites table
     *
     * @param customerAccount
     * @param siteId
     * @param ffSetId
     * @param ffId
     * @return List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteFFSetFavorites> queryByPK(String customerAccount, UUID siteId, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteFFSetFavorites data by vendorId and bearingId from site_ff_set_favorites table
     *
     * @param customerAccount
     * @param siteId
     * @param ffSetId
     * @param ffId
     * @return List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteFFSetFavorites> findByPK(String customerAccount, UUID siteId, UUID ffSetId, UUID ffId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (ffSetId == null) {
            throw new IllegalArgumentException("Argument ffSetId cannot be null or empty.");
        }
        if (ffId == null) {
            throw new IllegalArgumentException("Argument ffId cannot be null or empty.");
        }

        return queryByPK(customerAccount, siteId, ffSetId, ffId).all();
    }

    /**
     * Gets the SiteFFSetFavorites data by vendorId from site_ff_set_favorites table
     *
     * @param customerAccount
     * @param siteId
     * @return List of SiteFFSetFavorites objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteFFSetFavorites> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SiteFFSetFavorites data by vendorId from site_ff_set_favorites table
     *
     * @param customerAccount
     * @param siteId
     * @return List of SiteFFSetFavorites objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SiteFFSetFavorites> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        return queryByCustomerSite(customerAccount, siteId).all();
    }
    
    /**
     * Truncate the site_ff_set_favorites table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE site_ff_set_favorites")
    public void truncateAllSiteFFSetFavorites() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the site_ff_set_favorites table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllSiteFFSetFavorites();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new SiteFFSetFavorites(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new SiteFFSetFavorites(), "DELETE"));
            }
            throw e;
        }
    }

}
