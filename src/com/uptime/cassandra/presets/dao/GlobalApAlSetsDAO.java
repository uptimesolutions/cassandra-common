/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Delete;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface GlobalApAlSetsDAO extends BaseDAO<GlobalApAlSets> {
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(GlobalApAlSetsDAO.class);

    /**
     * Inserts the given entity into the global_ap_al_sets table
     *
     * @param entity, GlobalApAlSets Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(GlobalApAlSets entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the global_ap_al_sets table
     *
     * @param entity, GlobalApAlSets Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(GlobalApAlSets entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the global_ap_al_sets table.
     *
     * @param entity, GlobalApAlSets Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(GlobalApAlSets entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given partition from the global_ap_al_sets table.
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Delete(entityClass = GlobalApAlSets.class)
    void deleteByCustomerApSetId(String customerAccount, UUID apSetId) throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the given partition from the global_ap_al_sets table.
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(String customerAccount, UUID apSetId) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null or empty.");
        }
        try {
            deleteByCustomerApSetId(customerAccount, apSetId);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(customerAccount, apSetId));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(customerAccount, apSetId));
            }
            throw e;
        }
    }

    /**
     * Gets the GlobalApAlSets data by customer account and ap set name from
     * global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalApAlSets> queryByCustomerApSet(String customerAccount, UUID apSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalApAlSets data by customer account and ap set name from
     * global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalApAlSets> findByCustomerApSet(String customerAccount, UUID apSetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        return queryByCustomerApSet(customerAccount, apSetId).all();
    }

    /**
     * Gets the GlobalApAlSets data by customer account and ap set name from
     * global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetIdList, List of UUID Objects
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalApAlSets> findByCustomerApSets(String customerAccount, List<UUID> apSetIdList) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (apSetIdList == null || apSetIdList.isEmpty()) {
            throw new IllegalArgumentException("Argument apSetIdList cannot be null.");
        }
        List<String> apSetList = new ArrayList();
        apSetIdList.stream().forEach(apSetId -> apSetList.add(apSetId.toString()));
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM global_ap_al_sets WHERE customer_acct = '");
        query.append(customerAccount).append("'");
        query.append(" and ap_set_id in (");
        for (int i = 0; i < apSetIdList.size(); i++) {
            query.append(apSetIdList.get(i)).append(i < apSetIdList.size() - 1 ? "," : "");
        }
        query.append(");");
        BoundStatement query_apSets = CassandraData.getSession().prepare(query.toString()).bind();
        ResultSet resultSet = CassandraData.getSession().execute(query_apSets);
        // Get the results of the query
        List<GlobalApAlSets> globalApAlSetsApAlSetsList = new ArrayList<>();
        while (resultSet.iterator().hasNext()) {
            GlobalApAlSets globalApAlSets = new GlobalApAlSets();
            Row row = resultSet.iterator().next();
            globalApAlSets.setCustomerAccount(row.getString("customer_acct"));
            globalApAlSets.setApSetId(row.getUuid("ap_set_id"));
            globalApAlSets.setAlSetId(row.getUuid("al_set_id"));
            globalApAlSets.setParamName(row.getString("param_name"));
            globalApAlSets.setAlSetName(row.getString("al_set_name"));
            globalApAlSets.setApSetName(row.getString("ap_set_name"));
            globalApAlSets.setDemodHighPass(row.getFloat("demod_high_pass"));
            globalApAlSets.setDemodLowPass(row.getFloat("demod_low_pass"));
            globalApAlSets.setDwell(row.getInt("dwell"));
            globalApAlSets.setFmax(row.getInt("fmax"));
            globalApAlSets.setFrequencyUnits(row.getString("freq_units"));
            globalApAlSets.setHighAlert(row.getFloat("high_alert"));
            globalApAlSets.setHighAlertActive(row.getBoolean("high_alert_active"));
            globalApAlSets.setHighFault(row.getFloat("high_fault"));
            globalApAlSets.setHighFaultActive(row.getBoolean("high_fault_active"));
            globalApAlSets.setDemod(row.getBoolean("is_demod"));
            globalApAlSets.setLowAlert(row.getFloat("low_alert"));
            globalApAlSets.setLowAlertActive(row.getBoolean("low_alert_active"));
            globalApAlSets.setLowFault(row.getFloat("low_fault"));
            globalApAlSets.setLowFaultActive(row.getBoolean("low_fault_active"));
            globalApAlSets.setMaxFrequency(row.getFloat("max_freq"));
            globalApAlSets.setMinFrequency(row.getFloat("min_freq"));
            globalApAlSets.setParamAmpFactor(row.getString("param_amp_factor"));
            globalApAlSets.setParamType(row.getString("param_type"));
            globalApAlSets.setParamUnits(row.getString("param_units"));
            globalApAlSets.setPeriod(row.getFloat("period"));
            globalApAlSets.setResolution(row.getInt("resolution"));
            globalApAlSets.setSampleRate(row.getFloat("sample_rate"));
            globalApAlSets.setSensorType(row.getString("sensor_type"));
            globalApAlSetsApAlSetsList.add(globalApAlSets);
        }
        return globalApAlSetsApAlSetsList;
    }

    /**
     * Gets the GlobalApAlSets data by customer account, apSetId and alSetId
     * from global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalApAlSets> queryByCustomerApSetAlSet(String customerAccount, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalApAlSets data by customer account, ap set name and al set
     * name from global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    default public List<GlobalApAlSets> findByCustomerApSetAlSet(String customerAccount, UUID apSetId, UUID alSetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        if (alSetId == null) {
            throw new IllegalArgumentException("Argument alSetId cannot be null.");
        }

        return queryByCustomerApSetAlSet(customerAccount, apSetId, alSetId).all();
    }

    /**
     * Gets the GlobalApAlSets data by customer account, apSetId, alSetId
     * and param name from global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<GlobalApAlSets> queryByPK(String customerAccount, UUID apSetId, UUID alSetId, String paramName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the GlobalApAlSets data by customer account, apSetId, alSetId
     * and paramName from global_ap_al_sets table
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<GlobalApAlSets> findByPK(String customerAccount, UUID apSetId, UUID alSetId, String paramName) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        if (alSetId == null) {
            throw new IllegalArgumentException("Argument alSetId cannot be null.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument Param name cannot be null or empty.");
        }
        return queryByPK(customerAccount, apSetId, alSetId, paramName).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and
     * statementType
     *
     * @param entity, GlobalApAlSets Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(GlobalApAlSets entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getApSetId() != null && entity.getAlSetId() != null
                && entity.getParamName() != null && !entity.getParamName().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO global_ap_al_sets(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",ap_set_id");
                    values.append(",").append(entity.getApSetId());
                    query.append(",al_set_id");
                    values.append(",").append(entity.getAlSetId());
                    query.append(",param_name");
                    values.append(",'").append(entity.getParamName()).append("'");
                    query.append(",fmax");
                    values.append(",").append(entity.getFmax());
                    query.append(",resolution");
                    values.append(",").append(entity.getResolution());
                    query.append(",period");
                    values.append(",").append(entity.getPeriod());
                    query.append(",sample_rate");
                    values.append(",").append(entity.getSampleRate());
                    query.append(",is_demod ");
                    values.append(",").append(entity.isDemod());
                    query.append(",demod_high_pass");
                    values.append(",").append(entity.getDemodHighPass());
                    query.append(",demod_low_pass");
                    values.append(",").append(entity.getDemodLowPass());
                    query.append(",min_freq");
                    values.append(",").append(entity.getMinFrequency());
                    query.append(",max_freq");
                    values.append(",").append(entity.getMaxFrequency());
                    query.append(",dwell");
                    values.append(",").append(entity.getDwell());
                    query.append(",low_fault_active");
                    values.append(",").append(entity.isLowFaultActive());
                    query.append(",low_alert_active");
                    values.append(",").append(entity.isLowAlertActive());
                    query.append(",high_alert_active");
                    values.append(",").append(entity.isHighAlertActive());
                    query.append(",high_fault_active");
                    values.append(",").append(entity.isHighFaultActive());
                    query.append(",low_fault");
                    values.append(",").append(entity.getLowFault());
                    query.append(",low_alert");
                    values.append(",").append(entity.getLowAlert());
                    query.append(",high_alert");
                    values.append(",").append(entity.getHighAlert());
                    query.append(",high_fault");
                    values.append(",").append(entity.getHighFault());
                    if (entity.getApSetName() != null) {
                        query.append(",ap_set_name");
                        values.append(",'").append(entity.getApSetName()).append("'");
                    }
                    if (entity.getAlSetName() != null) {
                        query.append(",al_set_name");
                        values.append(",'").append(entity.getAlSetName()).append("'");
                    }
                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }
                    if (entity.getParamType() != null) {
                        query.append(",param_type");
                        values.append(",'").append(entity.getParamType()).append("'");
                    }
                    if (entity.getFrequencyUnits() != null) {
                        query.append(",freq_units");
                        values.append(",'").append(entity.getFrequencyUnits()).append("'");
                    }
                    if (entity.getParamUnits() != null) {
                        query.append(",param_units");
                        values.append(",'").append(entity.getParamUnits()).append("'");
                    }
                    if (entity.getParamAmpFactor() != null) {
                        query.append(",param_amp_factor");
                        values.append(",'").append(entity.getParamAmpFactor()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM global_ap_al_sets WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND ap_set_id=").append(entity.getApSetId());
                    query.append(" AND al_set_id=").append(entity.getAlSetId());
                    query.append(" AND param_name='").append(entity.getParamName()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }
    
    /**
     * Truncate the global_ap_al_sets table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE global_ap_al_sets")
    public void truncateAllGlobalApAlSets() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes all the entities from the global_ap_al_sets table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllGlobalApAlSets();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new GlobalApAlSets(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new GlobalApAlSets(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given values
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return String Object
     */
    default public String printCQL(String customerAccount, UUID apSetId) {
        StringBuilder query;

        query = new StringBuilder();
        query.append("DELETE FROM global_ap_al_sets WHERE customer_acct='").append(customerAccount).append("'");
        query.append(" AND ap_set_id=").append(apSetId).append(";");
        
        return query.toString();
    }
}
