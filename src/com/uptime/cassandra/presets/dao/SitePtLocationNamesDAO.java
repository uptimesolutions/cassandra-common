/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Delete;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SitePtLocationNamesDAO extends BaseDAO<SitePtLocationNames> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SitePtLocationNamesDAO.class);

    /**
     * Inserts the given entity into the site_pt_location_names table
     *
     * @param entity, SitePtLocationNames Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void create(SitePtLocationNames entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the site_pt_location_names table
     *
     * @param entity, SitePtLocationNames Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void update(SitePtLocationNames entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the site_pt_location_names table.
     *
     * @param entity, SitePtLocationNames Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default void delete(SitePtLocationNames entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Gets the SitePtLocationNames data by customer account and siteId from site_pt_location_names table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SitePtLocationNames objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SitePtLocationNames> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SitePtLocationNames data by customer account and siteId from site_pt_location_names table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SitePtLocationNames objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SitePtLocationNames> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        return queryByCustomerSite(customerAccount, siteId).all();
    }

    /**
     * Gets the SitePtLocationNames data by customer account, siteId and pointLocationName from site_pt_location_names table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param pointLocationName, String Object
     * @return List of SitePtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SitePtLocationNames> queryByPK(String customerAccount, UUID siteId, String pointLocationName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the SitePtLocationNames data by customer account, siteId and pointLocationName from site_pt_location_names table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param pointLocationName, String Object
     * @return List of SitePtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<SitePtLocationNames> findByPK(String customerAccount, UUID siteId, String pointLocationName) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (pointLocationName == null || pointLocationName.isEmpty()) {
            throw new IllegalArgumentException("Argument pointLocationName cannot be null or empty.");
        }

        return queryByPK(customerAccount, siteId, pointLocationName).all();
    }

    /**
     * Delete the site_pt_location_names table By customer and siteÌd.
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    @Delete(entityClass = SitePtLocationNames.class)
    void deleteByCustomerSiteId(String customer, UUID siteÌd) throws UnavailableException, WriteTimeoutException, IllegalArgumentException;

    /**
     * Delete the site_pt_location_names table By customer and siteÌd.
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public void deleteAllByCustomerSiteId(String customer, UUID siteÌd) throws UnavailableException, WriteTimeoutException {
        try {
            deleteByCustomerSiteId(customer, siteÌd);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new SitePtLocationNames(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new SitePtLocationNames(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, SitePtLocationNames Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SitePtLocationNames entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getPointLocationName() != null && !entity.getPointLocationName().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_pt_location_names(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",point_location_name");
                    values.append(",'").append(entity.getPointLocationName()).append("'");
                    if (entity.getDescription() != null) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    query.append(") values (").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_pt_location_names WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND point_location_name='").append(entity.getPointLocationName()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }
}
