/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
@Entity
@CqlName(value = "global_fault_frequencies")
public class GlobalFaultFrequencies {

    @PartitionKey
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @ClusteringColumn(0)
    @CqlName(value = "ff_set_id")
    private UUID ffSetId;

    @ClusteringColumn(1)
    @CqlName(value = "ff_id")
    private UUID ffId;

    @CqlName(value = "ff_set_name")
    private String ffSetName;
    
    @CqlName(value = "ff_name")
    private String ffName;
    
    @CqlName(value = "ff_set_desc")
    private String ffSetDesc;

    @CqlName(value = "ff_value")
    private float ffValue;

    @CqlName(value = "ff_unit")
    private String ffUnit;

    public GlobalFaultFrequencies() {
    }

    public GlobalFaultFrequencies(GlobalFaultFrequencies entity) {
        customerAccount = entity.getCustomerAccount();
        ffSetId = entity.getFfSetId();
        ffId = entity.getFfId();
        ffSetName = entity.getFfSetName();
        ffName = entity.getFfName();
        ffSetDesc = entity.getFfSetDesc();
        ffValue = entity.getFfValue();
        ffUnit = entity.getFfUnit();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getFfSetId() {
        return ffSetId;
    }

    public void setFfSetId(UUID ffSetId) {
        this.ffSetId = ffSetId;
    }

    public UUID getFfId() {
        return ffId;
    }

    public void setFfId(UUID ffId) {
        this.ffId = ffId;
    }

    public String getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(String ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getFfName() {
        return ffName;
    }

    public void setFfName(String ffName) {
        this.ffName = ffName;
    }

    public String getFfSetDesc() {
        return ffSetDesc;
    }

    public void setFfSetDesc(String ffSetDesc) {
        this.ffSetDesc = ffSetDesc;
    }

    public float getFfValue() {
        return ffValue;
    }

    public void setFfValue(float ffValue) {
        this.ffValue = ffValue;
    }

    public String getFfUnit() {
        return ffUnit;
    }

    public void setFfUnit(String ffUnit) {
        this.ffUnit = ffUnit;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.ffSetId);
        hash = 97 * hash + Objects.hashCode(this.ffId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GlobalFaultFrequencies other = (GlobalFaultFrequencies) obj;
        if (Float.floatToIntBits(this.ffValue) != Float.floatToIntBits(other.ffValue)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        if (!Objects.equals(this.ffName, other.ffName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetDesc, other.ffSetDesc)) {
            return false;
        }
        if (!Objects.equals(this.ffUnit, other.ffUnit)) {
            return false;
        }
        if (!Objects.equals(this.ffSetId, other.ffSetId)) {
            return false;
        }
        if (!Objects.equals(this.ffId, other.ffId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GlobalFaultFrequencies{" + "customerAccount=" + customerAccount + ", ffSetId=" + ffSetId + ", ffId=" + ffId + ", ffSetName=" + ffSetName + ", ffName=" + ffName + ", ffSetDesc=" + ffSetDesc + ", ffValue=" + ffValue + ", ffUnit=" + ffUnit + '}';
    }

}
