/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "global_asset_types")
public class GlobalAssetTypes {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @ClusteringColumn(0)
    @CqlName(value = "asset_type_name")
    private String assetTypeName;
    
    @CqlName(value = "description")
    private String description;
    
    public GlobalAssetTypes() {
    }

    public GlobalAssetTypes(GlobalAssetTypes entity) {
        this.customerAccount = entity.getCustomerAccount();
        this.assetTypeName = entity.getAssetTypeName();
        this.description = entity.getDescription();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.assetTypeName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GlobalAssetTypes other = (GlobalAssetTypes) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeName, other.assetTypeName)) {
            return false;
        }
        return Objects.equals(this.description, other.description);
    }

    @Override
    public String toString() {
        return "GlobalAssetTypes{" + "customerAccount=" + customerAccount + ", assetTypeName=" + assetTypeName + ", description=" + description + '}';
    }

}
