/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;

/**
 *
 * @author joseph
 */
@Entity
@CqlName(value = "bearing_catalog")
public class BearingCatalog {

    @PartitionKey(0)
    @CqlName(value = "vendor_id")
    String vendorId;

    @ClusteringColumn(0)
    @CqlName(value = "bearing_id")
    String bearingId;

    @CqlName(value = "bc")
    short bc;

    @CqlName(value = "bpfi")
    float bpfi;

    @CqlName(value = "bpfo")
    float bpfo;

    @CqlName(value = "bsf")
    float bsf;

    @CqlName(value = "ftf")
    float ftf;

    public BearingCatalog() {
    }

    public BearingCatalog(BearingCatalog bearingCatalog) {
        this.vendorId = bearingCatalog.getVendorId();
        this.bearingId = bearingCatalog.getBearingId();
        this.bc = bearingCatalog.getBc();
        this.bpfi = bearingCatalog.getBpfi();
        this.bpfo = bearingCatalog.getBpfo();
        this.bsf = bearingCatalog.getBsf();
        this.ftf = bearingCatalog.getFtf();
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getBearingId() {
        return bearingId;
    }

    public void setBearingId(String bearingId) {
        this.bearingId = bearingId;
    }

    public short getBc() {
        return bc;
    }

    public void setBc(short bc) {
        this.bc = bc;
    }   

    public float getBpfi() {
        return bpfi;
    }

    public void setBpfi(float bpfi) {
        this.bpfi = bpfi;
    }

    public float getBpfo() {
        return bpfo;
    }

    public void setBpfo(float bpfo) {
        this.bpfo = bpfo;
    }

    public float getBsf() {
        return bsf;
    }

    public void setBsf(float bsf) {
        this.bsf = bsf;
    }

    public float getFtf() {
        return ftf;
    }

    public void setFtf(float ftf) {
        this.ftf = ftf;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.vendorId);
        hash = 19 * hash + Objects.hashCode(this.bearingId);
        hash = 19 * hash + this.bc;
        hash = 19 * hash + Float.floatToIntBits(this.bpfi);
        hash = 19 * hash + Float.floatToIntBits(this.bpfo);
        hash = 19 * hash + Float.floatToIntBits(this.bsf);
        hash = 19 * hash + Float.floatToIntBits(this.ftf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BearingCatalog other = (BearingCatalog) obj;
        if (this.bc != other.bc) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfi) != Float.floatToIntBits(other.bpfi)) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfo) != Float.floatToIntBits(other.bpfo)) {
            return false;
        }
        if (Float.floatToIntBits(this.bsf) != Float.floatToIntBits(other.bsf)) {
            return false;
        }
        if (Float.floatToIntBits(this.ftf) != Float.floatToIntBits(other.ftf)) {
            return false;
        }
        if (!Objects.equals(this.vendorId, other.vendorId)) {
            return false;
        }
        if (!Objects.equals(this.bearingId, other.bearingId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BearingCatalog{" + "vendorId=" + vendorId + ", bearingId=" + bearingId + ", bc=" + bc + ", bpfi=" + bpfi + ", bpfo=" + bpfo + ", bsf=" + bsf + ", ftf=" + ftf + '}';
    }

}
