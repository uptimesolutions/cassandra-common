/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
@Entity
@CqlName(value = "global_tachometers")
public class GlobalTachometers {
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;
    
    @ClusteringColumn
    @CqlName(value = "tach_id")
    private UUID tachId;
    
    @CqlName(value = "tach_name")
    private String tachName;
    
    @CqlName(value = "tach_reference_speed")
    private float tachReferenceSpeed;
    
    @CqlName(value = "tach_reference_units")
    private String tachReferenceUnits;
    
    @CqlName(value = "tach_type")
    private String tachType;
    
    public GlobalTachometers(){
        
    }

    public GlobalTachometers(GlobalTachometers entity) {
        customerAccount = entity.getCustomerAccount();
        tachId = entity.getTachId();
        tachName = entity.getTachName();
        tachType = entity.getTachType();
        tachReferenceSpeed = entity.getTachReferenceSpeed();
        tachReferenceUnits = entity.getTachReferenceUnits();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public String getTachType() {
        return tachType;
    }

    public void setTachType(String tachType) {
        this.tachType = tachType;
    }

    public float getTachReferenceSpeed() {
        return tachReferenceSpeed;
    }

    public void setTachReferenceSpeed(float tachReferenceSpeed) {
        this.tachReferenceSpeed = tachReferenceSpeed;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.customerAccount);
        hash = 61 * hash + Objects.hashCode(this.tachId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GlobalTachometers other = (GlobalTachometers) obj;
        if (Float.floatToIntBits(this.tachReferenceSpeed) != Float.floatToIntBits(other.tachReferenceSpeed)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.tachType, other.tachType)) {
            return false;
        }
        if (!Objects.equals(this.tachReferenceUnits, other.tachReferenceUnits)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GlobalTachometers{" + "customerAccount=" + customerAccount + ", tachId=" + tachId + ", tachName=" + tachName + ", tachType=" + tachType + ", tachReferenceSpeed=" + tachReferenceSpeed + ", tachReferenceUnits=" + tachReferenceUnits + '}';
    }
}
