/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_ap_al_sets")
public class SiteApAlSets {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;
    
    @PartitionKey(2)
    @CqlName(value = "ap_set_id")
    private UUID apSetId;

    @ClusteringColumn(0)
    @CqlName(value = "al_set_id")
    private UUID alSetId;

    @ClusteringColumn(1)
    @CqlName(value = "param_name")
    private String paramName;

    @CqlName(value = "ap_set_name")
    String apSetName;
    
    @CqlName(value = "al_set_name")
    String alSetName;

    @CqlName(value = "demod_high_pass")
    private float demodHighPass;
    
    @CqlName(value = "demod_low_pass")
    private float demodLowPass;
    
    @CqlName(value = "dwell")
    private int dwell;
    
    @CqlName(value = "fmax")
    private int fmax;

    @CqlName(value = "freq_units")
    private String frequencyUnits;

    @CqlName(value = "high_alert")
    private float highAlert;
    
    @CqlName(value = "high_alert_active")
    private boolean highAlertActive;
    
    @CqlName(value = "high_fault")
    private float highFault;
    
    @CqlName(value = "high_fault_active")
    private boolean highFaultActive;
    
    @CqlName(value = "is_demod")
    private boolean demod;
    
    @CqlName(value = "low_alert")
    private float lowAlert;
    
    @CqlName(value = "low_alert_active")
    private boolean lowAlertActive;
    
    @CqlName(value = "low_fault")
    private float lowFault;
    
    @CqlName(value = "low_fault_active")
    private boolean lowFaultActive;
    
    @CqlName(value = "max_freq")
    private float maxFrequency;
    
    @CqlName(value = "min_freq")
    private float minFrequency;
    
    @CqlName(value = "param_amp_factor")
    private String paramAmpFactor;
    
    @CqlName(value = "param_type")
    private String paramType;
    
    @CqlName(value = "param_units")
    private String paramUnits;

    @CqlName(value = "period")
    private float period;
    
    @CqlName(value = "resolution")
    private int resolution;

    @CqlName(value = "sample_rate")
    private float sampleRate;

    @CqlName(value = "sensor_type")
    private String sensorType;

    public SiteApAlSets() {
    }

    public SiteApAlSets(SiteApAlSets entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        apSetId = entity.getApSetId();
        alSetId = entity.getAlSetId();
        paramName = entity.getParamName();
        apSetName = entity.getApSetName();
        alSetName = entity.getAlSetName();
        demodHighPass = entity.getDemodHighPass();
        demodLowPass = entity.getDemodLowPass();
        dwell = entity.getDwell();
        fmax = entity.getFmax();
        frequencyUnits = entity.getFrequencyUnits();
        highAlert = entity.getHighAlert();
        highAlertActive = entity.isHighAlertActive();
        highFault = entity.getHighFault();
        highFaultActive = entity.isHighFaultActive();
        demod = entity.isDemod();
        lowAlert = entity.getLowAlert();
        lowAlertActive = entity.isLowAlertActive();
        lowFault = entity.getLowFault();
        lowFaultActive = entity.isLowFaultActive();
        maxFrequency = entity.getMaxFrequency();
        minFrequency = entity.getMinFrequency();
        paramAmpFactor = entity.getParamAmpFactor();
        paramType = entity.getParamType();
        paramUnits = entity.getParamUnits();
        period = entity.getPeriod();
        resolution = entity.getResolution();
        sampleRate = entity.getSampleRate();
        sensorType = entity.getSensorType();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public String getFrequencyUnits() {
        return frequencyUnits;
    }

    public void setFrequencyUnits(String frequencyUnits) {
        this.frequencyUnits = frequencyUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public boolean isDemod() {
        return demod;
    }

    public void setDemod(boolean demod) {
        this.demod = demod;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(float maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public float getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(float minFrequency) {
        this.minFrequency = minFrequency;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.apSetId);
        hash = 97 * hash + Objects.hashCode(this.alSetId);
        hash = 97 * hash + Objects.hashCode(this.paramName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteApAlSets other = (SiteApAlSets) obj;
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fmax != other.fmax) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (this.demod != other.demod) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFrequency) != Float.floatToIntBits(other.maxFrequency)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFrequency) != Float.floatToIntBits(other.minFrequency)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.frequencyUnits, other.frequencyUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteApAlSets{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", paramName=" + paramName + ", apSetName=" + apSetName + ", alSetName=" + alSetName + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fmax=" + fmax + ", frequencyUnits=" + frequencyUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", demod=" + demod + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFrequency=" + maxFrequency + ", minFrequency=" + minFrequency + ", paramAmpFactor=" + paramAmpFactor + ", paramType=" + paramType + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", sensorType=" + sensorType + '}';
    }
}
