/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "customer_ap_set")
public class CustomerApSet {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;

    @ClusteringColumn(0)
    @CqlName(value = "ap_set_id")
    UUID apSetId;
    
    @CqlName(value = "ap_set_name")
    String apSetName;

    @CqlName(value = "fmax")
    int fmax;

    @CqlName(value = "period")
    float period;
    
    @CqlName(value = "resolution")
    int resolution;
    
    @CqlName(value = "sample_rate")
    float sampleRate;
    
    @CqlName(value = "sensor_type")
    String sensorType;

    @CqlName(value = "site_id")
    UUID siteId;

    public CustomerApSet() {
    }

    public CustomerApSet(CustomerApSet entity) {
        customerAccount = entity.getCustomerAccount();
        apSetId = entity.getApSetId();
        apSetName = entity.getApSetName();
        fmax = entity.getFmax();
        period = entity.getPeriod();
        resolution = entity.getResolution();
        sampleRate = entity.getSampleRate();
        sensorType = entity.getSensorType();
        siteId = entity.getSiteId();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.apSetId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomerApSet other = (CustomerApSet) obj;
        if (this.fmax != other.fmax) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CustomerApSet{" + "customerAccount=" + customerAccount + ", apSetId=" + apSetId + ", apSetName=" + apSetName + ", fmax=" + fmax + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", sensorType=" + sensorType + ", siteId=" + siteId + '}';
    }
}
