/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_pt_location_names")
public class SitePtLocationNames {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;
    
    @PartitionKey(1)
    @CqlName(value = "site_id")
    UUID siteId ;

    @ClusteringColumn(0)
    @CqlName(value = "point_location_name")
    String pointLocationName;
    
    @CqlName(value = "description")
    String description;

    public SitePtLocationNames() {
    }

    public SitePtLocationNames(SitePtLocationNames entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        pointLocationName = entity.getPointLocationName();
        description = entity.getDescription();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        hash = 37 * hash + Objects.hashCode(this.pointLocationName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SitePtLocationNames other = (SitePtLocationNames) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SitePtLocationNames{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", pointLocationName=" + pointLocationName + ", description=" + description + '}';
    }

}
