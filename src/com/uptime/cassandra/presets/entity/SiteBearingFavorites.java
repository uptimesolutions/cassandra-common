/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_bearing_favorites")
public class SiteBearingFavorites {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAcct;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;
    
    @ClusteringColumn(0)
    @CqlName(value = "vendor_id")
    private String vendorId;

    @ClusteringColumn(1)
    @CqlName(value = "bearing_id")
    private String bearingId;

    @CqlName(value = "ftf")
    private float ftf;
    
    @CqlName(value = "bsf")
    private float bsf;

    @CqlName(value = "bpfo")
    private float bpfo;
    
    @CqlName(value = "bpfi")
    private float bpfi;
    

    public SiteBearingFavorites() {
    }

    public SiteBearingFavorites(SiteBearingFavorites entity) {
        customerAcct = entity.getCustomerAcct();
        siteId = entity.getSiteId();
        vendorId = entity.getVendorId();
        bearingId = entity.getBearingId();
        ftf = entity.getFtf();
        bsf = entity.getBsf();
        bpfo = entity.getBpfo();
        bpfi = entity.getBpfi();
    }

    public String getCustomerAcct() {
        return customerAcct;
    }

    public void setCustomerAcct(String customerAcct) {
        this.customerAcct = customerAcct;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getBearingId() {
        return bearingId;
    }

    public void setBearingId(String bearingId) {
        this.bearingId = bearingId;
    }

    public float getFtf() {
        return ftf;
    }

    public void setFtf(float ftf) {
        this.ftf = ftf;
    }

    public float getBsf() {
        return bsf;
    }

    public void setBsf(float bsf) {
        this.bsf = bsf;
    }

    public float getBpfo() {
        return bpfo;
    }

    public void setBpfo(float bpfo) {
        this.bpfo = bpfo;
    }

    public float getBpfi() {
        return bpfi;
    }

    public void setBpfi(float bpfi) {
        this.bpfi = bpfi;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.customerAcct);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        hash = 37 * hash + Objects.hashCode(this.vendorId);
        hash = 37 * hash + Objects.hashCode(this.bearingId);
        hash = 37 * hash + Float.floatToIntBits(this.ftf);
        hash = 37 * hash + Float.floatToIntBits(this.bsf);
        hash = 37 * hash + Float.floatToIntBits(this.bpfo);
        hash = 37 * hash + Float.floatToIntBits(this.bpfi);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteBearingFavorites other = (SiteBearingFavorites) obj;
        if (Float.floatToIntBits(this.ftf) != Float.floatToIntBits(other.ftf)) {
            return false;
        }
        if (Float.floatToIntBits(this.bsf) != Float.floatToIntBits(other.bsf)) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfo) != Float.floatToIntBits(other.bpfo)) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfi) != Float.floatToIntBits(other.bpfi)) {
            return false;
        }
        if (!Objects.equals(this.customerAcct, other.customerAcct)) {
            return false;
        }
        if (!Objects.equals(this.vendorId, other.vendorId)) {
            return false;
        }
        if (!Objects.equals(this.bearingId, other.bearingId)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

    @Override
    public String toString() {
        return "SiteBearingFavorites{" + "customerAcct=" + customerAcct + ", siteId=" + siteId + ", vendorId=" + vendorId + ", bearingId=" + bearingId + ", ftf=" + ftf + ", bsf=" + bsf + ", bpfo=" + bpfo + ", bpfi=" + bpfi + '}';
    }

}
