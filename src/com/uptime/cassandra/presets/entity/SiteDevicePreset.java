/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_device_preset")
public class SiteDevicePreset {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;
    
    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "device_type")
    private String deviceType;
    
    @ClusteringColumn(1)
    @CqlName(value = "preset_id")
    private UUID presetId;

    @ClusteringColumn(2)
    @CqlName(value = "channel_type")
    private String channelType;

    @ClusteringColumn(3)
    @CqlName(value = "channel_number")
    private byte channelNumber;
    
    @CqlName(value = "ap_set_id")
    UUID apSetId;
    
    @CqlName(value = "al_set_id")
    UUID alSetId;

    @CqlName(value = "alarm_enabled")
    private boolean alarmEnabled;
    
    @CqlName(value = "auto_acknowledge")
    boolean autoAcknowledge;
    
    @CqlName(value = "is_disabled")
    boolean disabled;
    
    @CqlName(value = "name")
    String name;
    
    @CqlName(value = "sample_interval")
    int sampleInterval;
    
    @CqlName(value = "sensor_offset")
    float sensorOffset;

    @CqlName(value = "sensor_sensitivity")
    float sensorSensitivity;
    
    @CqlName(value = "sensor_type")
    String sensorType;
    
    @CqlName(value = "sensor_units")
    String sensorUnits;
    
    @CqlName(value = "sensor_sub_type")
    String sensorSubType;
    
    @CqlName(value = "sensor_local_orientation")
    String sensorLocalOrientation;
    
    public SiteDevicePreset() {
    }

    public SiteDevicePreset(SiteDevicePreset entity) {
        customerAccount = entity.getCustomerAccount();
        channelNumber = entity.getChannelNumber();
        channelType = entity.getChannelType();
        presetId = entity.getPresetId();
        deviceType = entity.getDeviceType();
        apSetId = entity.getApSetId();
        alSetId = entity.getAlSetId();
        alarmEnabled = entity.isAlarmEnabled();
        autoAcknowledge = entity.isAutoAcknowledge();
        disabled = entity.isDisabled();
        name = entity.getName();
        sampleInterval = entity.getSampleInterval();
        sensorOffset = entity.getSensorOffset();
        sensorSensitivity = entity.getSensorSensitivity();
        sensorType = entity.getSensorType();
        sensorUnits = entity.getSensorUnits();
        sensorSubType = entity.getSensorSubType();
        sensorLocalOrientation = entity.getSensorLocalOrientation();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public UUID getPresetId() {
        return presetId;
    }

    public void setPresetId(UUID presetId) {
        this.presetId = presetId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(byte channelNumber) {
        this.channelNumber = channelNumber;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public boolean isAutoAcknowledge() {
        return autoAcknowledge;
    }

    public void setAutoAcknowledge(boolean autoAcknowledge) {
        this.autoAcknowledge = autoAcknowledge;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.deviceType);
        hash = 79 * hash + Objects.hashCode(this.presetId);
        hash = 79 * hash + Objects.hashCode(this.channelType);
        hash = 79 * hash + this.channelNumber;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteDevicePreset other = (SiteDevicePreset) obj;
        if (this.channelNumber != other.channelNumber) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (this.autoAcknowledge != other.autoAcknowledge) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceType, other.deviceType)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.presetId, other.presetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteDevicePreset{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceType=" + deviceType + ", presetId=" + presetId + ", channelType=" + channelType + ", channelNumber=" + channelNumber + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", alarmEnabled=" + alarmEnabled + ", autoAcknowledge=" + autoAcknowledge + ", disabled=" + disabled + ", name=" + name + ", sampleInterval=" + sampleInterval + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + '}';
    }

    
}
