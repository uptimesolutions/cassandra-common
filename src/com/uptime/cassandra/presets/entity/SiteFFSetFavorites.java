/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author joseph
 */
@Entity
@CqlName(value = "site_ff_set_favorites")
public class SiteFFSetFavorites {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "ff_set_id")
    private UUID ffSetId;

    @ClusteringColumn(1)
    @CqlName(value = "ff_id")
    private UUID ffId;

    @CqlName(value = "ff_name")
    private String ffName;

    @CqlName(value = "ff_set_desc")
    private String ffSetDesc;

    @CqlName(value = "ff_set_name")
    private String ffSetName;

    @CqlName(value = "ff_unit")
    private String ffUnit;

    @CqlName(value = "ff_value")
    private float ffValue;

    @CqlName(value = "is_global")
    private boolean global;

    public SiteFFSetFavorites() {
    }

    public SiteFFSetFavorites(SiteFFSetFavorites siteFFSetFavorites) {
        this.customerAccount = siteFFSetFavorites.getCustomerAccount();
        this.siteId = siteFFSetFavorites.getSiteId();
        this.ffSetId = siteFFSetFavorites.getFfSetId();
        this.ffId = siteFFSetFavorites.getFfId();
        this.ffName = siteFFSetFavorites.getFfName();
        this.ffSetDesc = siteFFSetFavorites.getFfSetDesc();
        this.ffSetName = siteFFSetFavorites.getFfSetName();
        this.ffUnit = siteFFSetFavorites.getFfUnit();
        this.ffValue = siteFFSetFavorites.getFfValue();
        this.global = siteFFSetFavorites.isGlobal();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getFfSetId() {
        return ffSetId;
    }

    public void setFfSetId(UUID ffSetId) {
        this.ffSetId = ffSetId;
    }

    public UUID getFfId() {
        return ffId;
    }

    public void setFfId(UUID ffId) {
        this.ffId = ffId;
    }

    public String getFfName() {
        return ffName;
    }

    public void setFfName(String ffName) {
        this.ffName = ffName;
    }

    public String getFfSetDesc() {
        return ffSetDesc;
    }

    public void setFfSetDesc(String ffSetDesc) {
        this.ffSetDesc = ffSetDesc;
    }

    public String getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(String ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getFfUnit() {
        return ffUnit;
    }

    public void setFfUnit(String ffUnit) {
        this.ffUnit = ffUnit;
    }

    public float getFfValue() {
        return ffValue;
    }

    public void setFfValue(float ffValue) {
        this.ffValue = ffValue;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.customerAccount);
        hash = 67 * hash + Objects.hashCode(this.siteId);
        hash = 67 * hash + Objects.hashCode(this.ffSetId);
        hash = 67 * hash + Objects.hashCode(this.ffId);
        hash = 67 * hash + Objects.hashCode(this.ffName);
        hash = 67 * hash + Objects.hashCode(this.ffSetDesc);
        hash = 67 * hash + Objects.hashCode(this.ffSetName);
        hash = 67 * hash + Objects.hashCode(this.ffUnit);
        hash = 67 * hash + Float.floatToIntBits(this.ffValue);
        hash = 67 * hash + (this.global ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteFFSetFavorites other = (SiteFFSetFavorites) obj;
        if (Float.floatToIntBits(this.ffValue) != Float.floatToIntBits(other.ffValue)) {
            return false;
        }
        if (this.global != other.global) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.ffName, other.ffName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetDesc, other.ffSetDesc)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        if (!Objects.equals(this.ffUnit, other.ffUnit)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.ffSetId, other.ffSetId)) {
            return false;
        }
        if (!Objects.equals(this.ffId, other.ffId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteFFSetFavorites{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", ffSetId=" + ffSetId + ", ffId=" + ffId + ", ffName=" + ffName + ", ffSetDesc=" + ffSetDesc + ", ffSetName=" + ffSetName + ", ffUnit=" + ffUnit + ", ffValue=" + ffValue + ", global=" + global + '}';
    }

}
