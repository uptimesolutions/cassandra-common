/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author joseph
 */
@Entity
@CqlName(value = "global_asset_preset")
public class GlobalAssetPreset {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @ClusteringColumn(0)
    @CqlName(value = "asset_type_name")
    private String assetTypeName;

    @ClusteringColumn(1)
    @CqlName(value = "asset_preset_id")
    private UUID assetPresetId;

    @ClusteringColumn(2)
    @CqlName(value = "point_location_name")
    private String pointLocationName;

    @CqlName(value = "asset_preset_name")
    private String assetPresetName;
    
    @CqlName(value = "device_preset_type")
    private String devicePresetType;
    
    @CqlName(value = "device_preset_id")
    private UUID devicePresetId;

    @CqlName(value = "sample_interval")
    private int sampleInterval;

    @CqlName(value = "ff_set_ids")
    private String ffSetIds;

    @CqlName(value = "roll_diameter")
    private float rollDiameter;

    @CqlName(value = "roll_diameter_units")
    private String rollDiameterUnits;

    @CqlName(value = "speed_ratio")
    private float speedRatio;

    @CqlName(value = "tach_id")
    private UUID tachId;

    @CqlName(value = "description")
    private String description;

    public GlobalAssetPreset() {
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public UUID getAssetPresetId() {
        return assetPresetId;
    }

    public void setAssetPresetId(UUID assetPresetId) {
        this.assetPresetId = assetPresetId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getAssetPresetName() {
        return assetPresetName;
    }

    public void setAssetPresetName(String assetPresetName) {
        this.assetPresetName = assetPresetName;
    }

    public String getDevicePresetType() {
        return devicePresetType;
    }

    public void setDevicePresetType(String devicePresetType) {
        this.devicePresetType = devicePresetType;
    }
    
    public UUID getDevicePresetId() {
        return devicePresetId;
    }

    public void setDevicePresetId(UUID devicePresetId) {
        this.devicePresetId = devicePresetId;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.customerAccount);
        hash = 17 * hash + Objects.hashCode(this.assetTypeName);
        hash = 17 * hash + Objects.hashCode(this.assetPresetId);
        hash = 17 * hash + Objects.hashCode(this.pointLocationName);
        hash = 17 * hash + Objects.hashCode(this.assetPresetName);
        hash = 17 * hash + Objects.hashCode(this.devicePresetId);
        hash = 17 * hash + this.sampleInterval;
        hash = 17 * hash + Objects.hashCode(this.ffSetIds);
        hash = 17 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 17 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 17 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 17 * hash + Objects.hashCode(this.tachId);
        hash = 17 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GlobalAssetPreset other = (GlobalAssetPreset) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeName, other.assetTypeName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetName, other.assetPresetName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetId, other.assetPresetId)) {
            return false;
        }
        if (!Objects.equals(this.devicePresetId, other.devicePresetId)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GlobalAssetPreset{" + "customerAccount=" + customerAccount + ", assetTypeName=" + assetTypeName + ", assetPresetId=" + assetPresetId + ", pointLocationName=" + pointLocationName + ", assetPresetName=" + assetPresetName + ", devicePresetType=" + devicePresetType + ", devicePresetId=" + devicePresetId + ", sampleInterval=" + sampleInterval + ", ffSetIds=" + ffSetIds + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", speedRatio=" + speedRatio + ", tachId=" + tachId + ", description=" + description + '}';
    }

}
