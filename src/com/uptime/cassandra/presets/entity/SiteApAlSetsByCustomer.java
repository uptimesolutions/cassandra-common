/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
@Entity
@CqlName(value = "site_ap_al_sets_by_customer")
public class SiteApAlSetsByCustomer {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "sensor_type")
    private String sensorType;

    @ClusteringColumn(1)
    @CqlName(value = "ap_set_id")
    private UUID apSetId;

    @ClusteringColumn(2)
    @CqlName(value = "al_set_id")
    private UUID alSetId;

    @CqlName(value = "ap_set_name")
    String apSetName;

    @CqlName(value = "al_set_name")
    String alSetName;

    public SiteApAlSetsByCustomer() {
    }

    public SiteApAlSetsByCustomer(SiteApAlSetsByCustomer entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        apSetId = entity.getApSetId();
        alSetId = entity.getAlSetId();
        apSetName = entity.getApSetName();
        alSetName = entity.getAlSetName();
        sensorType = entity.getSensorType();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.apSetId);
        hash = 97 * hash + Objects.hashCode(this.alSetId);
        hash = 97 * hash + Objects.hashCode(this.sensorType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteApAlSetsByCustomer other = (SiteApAlSetsByCustomer) obj;

        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteApAlSetsByCustomer{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", sensorType=" + sensorType + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", apSetName=" + apSetName + ", alSetName=" + alSetName + '}';
    }

}
