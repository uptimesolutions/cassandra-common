/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
@Entity
@CqlName(value = "site_asset_preset")
public class SiteAssetPreset {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;
    
    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "asset_type_name")
    private String assetTypeName;
    
    @ClusteringColumn(1)
    @CqlName(value = "asset_preset_id")
    private UUID assetPresetId;

    @ClusteringColumn(2)
    @CqlName(value = "point_location_name")
    private String pointLocationName;

    @CqlName(value = "asset_preset_name")
    private String assetPresetName;
    
    @CqlName(value = "device_preset_type")
    private String devicePresetType;
    
    @CqlName(value = "device_preset_id")
    private UUID devicePresetId;
    
    @CqlName(value = "sample_interval")
    private int sampleInterval;
    
    @CqlName(value = "ff_set_ids")
    private String ffSetIds;
    
    @CqlName(value = "roll_diameter")
    private float rollDiameter;
    
    @CqlName(value = "roll_diameter_units")
    private String rollDiameterUnits;
    
    @CqlName(value = "speed_ratio")
    private float speedRatio;
    
    @CqlName(value = "tach_id")
    private UUID tachId;

    @CqlName(value = "description")
    private String description;
    
    public SiteAssetPreset(){
    }
    
    public SiteAssetPreset(SiteAssetPreset entity){
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public UUID getAssetPresetId() {
        return assetPresetId;
    }

    public void setAssetPresetId(UUID assetPresetId) {
        this.assetPresetId = assetPresetId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getAssetPresetName() {
        return assetPresetName;
    }

    public void setAssetPresetName(String assetPresetName) {
        this.assetPresetName = assetPresetName;
    }

    public String getDevicePresetType() {
        return devicePresetType;
    }

    public void setDevicePresetType(String devicePresetType) {
        this.devicePresetType = devicePresetType;
    }

    public void setDevicePresetId(UUID devicePresetId) {
        this.devicePresetId = devicePresetId;
    }
    
    public UUID getDevicePresetId() {
        return devicePresetId;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.customerAccount);
        hash = 83 * hash + Objects.hashCode(this.siteId);
        hash = 83 * hash + Objects.hashCode(this.assetTypeName);
        hash = 83 * hash + Objects.hashCode(this.assetPresetId);
        hash = 83 * hash + Objects.hashCode(this.pointLocationName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteAssetPreset other = (SiteAssetPreset) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeName, other.assetTypeName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetId, other.assetPresetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteAssetPreset{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", assetTypeName=" + assetTypeName + ", assetPresetId=" + assetPresetId + ", pointLocationName=" + pointLocationName + ", assetPresetName=" + assetPresetName + ", devicePresetType=" + devicePresetType + ", devicePresetId=" + devicePresetId + ", sampleInterval=" + sampleInterval + ", ffSetIds=" + ffSetIds + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", speedRatio=" + speedRatio + ", tachId=" + tachId + ", description=" + description + '}';
    }

}
