/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "open_work_orders")
public class OpenWorkOrders {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "area_id")
    private UUID areaId;

    @ClusteringColumn(1)
    @CqlName(value = "asset_id")
    private UUID assetId;

    @ClusteringColumn(2)
    @CqlName(value = "priority")
    private byte priority;

    @ClusteringColumn(3)
    @CqlName(value = "created_date")
    private Instant createdDate ;

    @CqlName(value = "action")
    private String action;

    @CqlName(value = "area_name")
    private String areaName;
    
    @CqlName(value = "asset_component")
    private String assetComponent;
    
    @CqlName(value = "asset_name")
    private String assetName;

    @CqlName(value = "description")
    private String description;
    
    @CqlName(value = "issue")
    private String issue;
    
    @CqlName(value = "notes")
    private String notes;
    
    @CqlName(value = "user")
    private String user;

    public OpenWorkOrders() {
    }

    public OpenWorkOrders(OpenWorkOrders entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        priority = entity.getPriority();
        createdDate = entity.getCreatedDate();
        action = entity.getAction();
        areaName = entity.getAreaName();
        assetComponent = entity.getAssetComponent();
        assetName = entity.getAssetName();
        description = entity.getDescription();
        issue = entity.getIssue();
        notes = entity.getNotes();
        user = entity.getUser();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetComponent() {
        return assetComponent;
    }

    public void setAssetComponent(String assetComponent) {
        this.assetComponent = assetComponent;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.areaId);
        hash = 79 * hash + Objects.hashCode(this.assetId);
        hash = 79 * hash + this.priority;
        hash = 79 * hash + Objects.hashCode(this.createdDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OpenWorkOrders other = (OpenWorkOrders) obj;
        if (this.priority != other.priority) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetComponent, other.assetComponent)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.issue, other.issue)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OpenWorkOrders{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", priority=" + priority + ", createdDate=" + createdDate + ", action=" + action + ", areaName=" + areaName + ", assetComponent=" + assetComponent + ", assetName=" + assetName + ", description=" + description + ", issue=" + issue + ", notes=" + notes + ", user=" + user + '}';
    }
}
