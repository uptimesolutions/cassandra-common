/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * The ClosedWorkOrders entity represents a closed work order.
 * 
 * @author madhavi
 */
@Entity
@CqlName(value = "closed_work_orders")
public class ClosedWorkOrders {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;
    
    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;
    
    @PartitionKey(2)
    @CqlName(value = "opened_year")
    private short openedYear;
    
    @ClusteringColumn(0)
    @CqlName(value = "area_id")
    private UUID areaId;
        
    @ClusteringColumn(1)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @ClusteringColumn(2)
    @CqlName(value = "created_date")
    private Instant createdDate ;
    
    @CqlName(value = "work_order_details")
    private WorkOrderRecord workOrderDetails; 

    public ClosedWorkOrders() {
    }

    public ClosedWorkOrders(ClosedWorkOrders entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        openedYear = entity.getOpenedYear();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        createdDate = entity.getCreatedDate();
        workOrderDetails = (entity.getWorkOrderDetails() != null) ? new WorkOrderRecord(entity.getWorkOrderDetails()) : null;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public short getOpenedYear() {
        return openedYear;
    }

    public void setOpenedYear(short openedYear) {
        this.openedYear = openedYear;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public WorkOrderRecord getWorkOrderDetails() {
        return workOrderDetails;
    }

    public void setWorkOrderDetails(WorkOrderRecord workOrderDetails) {
        this.workOrderDetails = workOrderDetails;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.customerAccount);
        hash = 23 * hash + Objects.hashCode(this.siteId);
        hash = 23 * hash + this.openedYear;
        hash = 23 * hash + Objects.hashCode(this.areaId);
        hash = 23 * hash + Objects.hashCode(this.assetId);
        hash = 23 * hash + Objects.hashCode(this.createdDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClosedWorkOrders other = (ClosedWorkOrders) obj;
        if (this.openedYear != other.openedYear) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        if (!Objects.equals(this.workOrderDetails, other.workOrderDetails)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClosedWorkOrders{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", openedYear=" + openedYear + ", areaId=" + areaId + ", assetId=" + assetId + ", createdDate=" + createdDate + ", workOrderDetails=" + workOrderDetails + '}';
    }
}
