/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mohd Juned Alam
 */
@Entity
@CqlName(value = "work_order_counts")
public class WorkOrderCounts {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "area_id")
    private UUID areaId;

    @ClusteringColumn(1)
    @CqlName(value = "asset_id")
    private UUID assetId;
 
    @CqlName(value = "priority_1")
    private int priority1;
    
    @CqlName(value = "priority_2")
    private int priority2;
   
    @CqlName(value = "priority_3")
    private int priority3;
    
    @CqlName(value = "priority_4")
    private int priority4;

    @CqlName(value = "total_count")
    private int totalCount;

    public WorkOrderCounts() {
    }

    public WorkOrderCounts(WorkOrderCounts entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        priority1 = entity.getPriority1();
        priority2 = entity.getPriority2();
        priority3 = entity.getPriority3();
        priority4 = entity.getPriority4();
        totalCount = entity.getTotalCount();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public int getPriority1() {
        return priority1;
    }

    public void setPriority1(int priority1) {
        this.priority1 = priority1;
    }

    public int getPriority2() {
        return priority2;
    }

    public void setPriority2(int priority2) {
        this.priority2 = priority2;
    }

    public int getPriority3() {
        return priority3;
    }

    public void setPriority3(int priority3) {
        this.priority3 = priority3;
    }

    public int getPriority4() {
        return priority4;
    }

    public void setPriority4(int priority4) {
        this.priority4 = priority4;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.areaId);
        hash = 29 * hash + Objects.hashCode(this.assetId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrderCounts other = (WorkOrderCounts) obj;
        if (this.priority1 != other.priority1) {
            return false;
        }
        if (this.priority2 != other.priority2) {
            return false;
        }
        if (this.priority3 != other.priority3) {
            return false;
        }
        if (this.priority4 != other.priority4) {
            return false;
        }
        if (this.totalCount != other.totalCount) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WorkOrderCounts{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", priority1=" + priority1 + ", priority2=" + priority2 + ", priority3=" + priority3 + ", priority4=" + priority4 + ", totalCount=" + totalCount + '}';
    }
}
