/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.entity;

import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import java.time.Instant;
import java.util.Objects;

/**
 * The WorkOrderRecord entity represents the work_order_record User Defined Type whose primary purpose it to store the closed work order details.
 * 
 * @author mbathala
 */
@Entity
@CqlName(value = "work_order_record")
public class WorkOrderRecord {

    @CqlName(value = "area_name")
    private String areaName;
    
    @CqlName(value = "asset_component")
    private String assetComponent;
    
    @CqlName(value = "asset_name")
    private String assetName;
    
    @CqlName(value = "priority")
    private String priority;

    @CqlName(value = "close_date")
    private Instant closeDate ;

    @CqlName(value = "action")
    private String action;

    @CqlName(value = "description")
    private String description;
    
    @CqlName(value = "issue")
    private String issue;
    
    @CqlName(value = "notes")
    private String notes;
    
    @CqlName(value = "user")
    private String user;

    public WorkOrderRecord() {
    }

    public WorkOrderRecord(WorkOrderRecord entity) {
        areaName = entity.getAreaName();
        assetComponent = entity.getAssetComponent();
        assetName = entity.getAssetName();
        priority = entity.getPriority();
        closeDate = entity.getCloseDate();
        action = entity.getAction();
        description = entity.getDescription();
        issue = entity.getIssue();
        notes = entity.getNotes();
        user = entity.getUser();
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetComponent() {
        return assetComponent;
    }

    public void setAssetComponent(String assetComponent) {
        this.assetComponent = assetComponent;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Instant getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Instant closeDate) {
        this.closeDate = closeDate;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.areaName);
        hash = 43 * hash + Objects.hashCode(this.assetComponent);
        hash = 43 * hash + Objects.hashCode(this.assetName);
        hash = 43 * hash + Objects.hashCode(this.priority);
        hash = 43 * hash + Objects.hashCode(this.closeDate);
        hash = 43 * hash + Objects.hashCode(this.action);
        hash = 43 * hash + Objects.hashCode(this.description);
        hash = 43 * hash + Objects.hashCode(this.issue);
        hash = 43 * hash + Objects.hashCode(this.notes);
        hash = 43 * hash + Objects.hashCode(this.user);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrderRecord other = (WorkOrderRecord) obj;
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetComponent, other.assetComponent)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.priority, other.priority)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.issue, other.issue)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.closeDate, other.closeDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WorkOrderRecord{" + "areaName=" + areaName + ", assetComponent=" + assetComponent + ", assetName=" + assetName + ", priority=" + priority + ", closeDate=" + closeDate + ", action=" + action + ", description=" + description + ", issue=" + issue + ", notes=" + notes + ", user=" + user + '}';
    }
}
