/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "work_order_trends")
public class WorkOrderTrends {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "area_id")
    private UUID areaId;

    @PartitionKey(3)
    @CqlName(value = "asset_id")
    private UUID assetId;

    @PartitionKey(4)
    @CqlName(value = "priority")
    private byte priority;

    @PartitionKey(5)
    @CqlName(value = "created_date")
    private Instant createdDate ;

    @ClusteringColumn(0)
    @CqlName(value = "row_id")
    private UUID rowId;
    
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @CqlName(value = "ap_set_id")
    private UUID apSetId;
    
    @CqlName(value = "param_id")
    private String paramName;
    
    @CqlName(value = "channel_type")
    private String channelType; // AC/DC

    public WorkOrderTrends() {
    }

    public WorkOrderTrends(WorkOrderTrends entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        priority = entity.getPriority();
        createdDate = entity.getCreatedDate();
        rowId = entity.getRowId();
        pointLocationId = entity.getPointLocationId();
        pointId = entity.getPointId();
        apSetId = entity.getApSetId();
        paramName = entity.getParamName();
        channelType = entity.getChannelType();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public UUID getRowId() {
        return rowId;
    }

    public void setRowId(UUID rowId) {
        this.rowId = rowId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.customerAccount);
        hash = 61 * hash + Objects.hashCode(this.siteId);
        hash = 61 * hash + Objects.hashCode(this.areaId);
        hash = 61 * hash + Objects.hashCode(this.assetId);
        hash = 61 * hash + this.priority;
        hash = 61 * hash + Objects.hashCode(this.createdDate);
        hash = 61 * hash + Objects.hashCode(this.rowId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrderTrends other = (WorkOrderTrends) obj;
        if (this.priority != other.priority) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        if (!Objects.equals(this.rowId, other.rowId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WorkOrderTrends{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", priority=" + priority + ", createdDate=" + createdDate + ", rowId=" + rowId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", paramName=" + paramName + ", channelType=" + channelType + '}';
    }
}
