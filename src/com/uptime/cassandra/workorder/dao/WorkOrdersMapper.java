/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;

/**
 *
 * @author madhavi
 */
@Mapper
public interface WorkOrdersMapper {

    @DaoFactory
    OpenWorkOrdersDAO openWorkOrdersDAO();
    
    @DaoFactory
    WorkOrderTrendsDAO workOrderTrendsDAO();

    @DaoFactory
    WorkOrderCountsDAO workOrderCountsDAO();

    @DaoFactory
    ClosedWorkOrdersDAO closedWorkOrdersDAO();
}
