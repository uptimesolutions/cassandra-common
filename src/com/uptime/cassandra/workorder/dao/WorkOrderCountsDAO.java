/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Mohd Juned Alam
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface WorkOrderCountsDAO extends BaseDAO<WorkOrderCounts> {

   Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(WorkOrderCountsDAO.class.getName());

    /**
     * Inserts the given workOrderCounts into the work_order_counts table
     *
     * @param entity, WorkOrderCounts Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(WorkOrderCounts entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given workOrderCounts into the work_order_counts table
     *
     * @param entity, WorkOrderCounts Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(WorkOrderCounts entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given workOrderCounts into the work_order_counts table
     *
     * @param entity, WorkOrderCounts Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(WorkOrderCounts entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * Truncate the work_order_counts table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE work_order_counts")
    public void truncateAllWorkOrderCounts() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the work_order_counts table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllWorkOrderCounts();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new WorkOrderCounts(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new WorkOrderCounts(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns WorkOrderCounts Objects for the given customer account and site id
     * from work_order_counts table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<WorkOrderCounts> queryByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of WorkOrderCounts Objects for the given customer account
     * and site id from work_order_counts table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<WorkOrderCounts> findByCustomerSiteId(String customerAccount, UUID siteId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        return queryByCustomerSiteId(customerAccount, siteId).all();
    }

    /**
     * Returns WorkOrderCounts Objects for the given customer account, site id,
     * and area id from work_order_counts table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return PagingIterable of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<WorkOrderCounts> queryByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of WorkOrderCounts Objects for the given customer account,
     * site id, and area id from work_order_counts table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<WorkOrderCounts> findByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        return queryByCustomerSiteArea(customerAccount, siteId, areaId).all();
    }

    /**
     * Returns WorkOrderCounts Objects for the given customer account, site id,
     * and area id from work_order_counts table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return PagingIterable of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Select
    PagingIterable<WorkOrderCounts> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of WorkOrderCounts Objects for the given customer account,
     * site id, and area id from work_order_counts table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<WorkOrderCounts> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null or empty.");
        }

        return queryByPK(customerAccount, siteId, areaId, assetId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and
     * statementType
     *
     * @param entity, WorkOrderCounts Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(WorkOrderCounts entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null && entity.getAreaId() != null && entity.getAssetId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO work_order_counts(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",priority_1");
                    values.append(",").append(entity.getPriority1());
                    query.append(",priority_2");
                    values.append(",").append(entity.getPriority2());
                    query.append(",priority_3");
                    values.append(",").append(entity.getPriority3());
                    query.append(",priority_4");
                    values.append(",").append(entity.getPriority4());
                    query.append(",total_count");
                    values.append(",").append(entity.getTotalCount());

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM work_order_counts WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
