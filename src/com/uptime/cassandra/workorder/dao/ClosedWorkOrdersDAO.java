/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.workorder.entity.ClosedWorkOrders;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface ClosedWorkOrdersDAO extends BaseDAO<ClosedWorkOrders> {
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(ClosedWorkOrdersDAO.class.getName());

    /**
     * Inserts the given entity into the closed_work_orders table
     * @param entity, ClosedWorkOrders Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(ClosedWorkOrders entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the closed_work_orders table
     * @param entity, ClosedWorkOrders Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(ClosedWorkOrders entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the closed_work_orders table.
     * @param entity, ClosedWorkOrders Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(ClosedWorkOrders entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * Truncate the closed_work_orders table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE closed_work_orders")
    public void truncateAllClosedWorkOrders() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the closed_work_orders table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllClosedWorkOrders();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new ClosedWorkOrders(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new ClosedWorkOrders(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns ClosedWorkOrders Objects for the given customer account, site id and year from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @return PagingIterable of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ClosedWorkOrders> queryByCustomerSiteYear(String customerAccount, UUID siteId, short year) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id and year from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<ClosedWorkOrders> findByCustomerSiteYear(String customerAccount, UUID siteId, short year) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (year == 0) {
            throw new IllegalArgumentException("Argument year cannot be 0.");
        }
        return queryByCustomerSiteYear(customerAccount, siteId, year).all();
    }

    /**
     * Returns ClosedWorkOrders Objects for the given customer account, site id, year and area id from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @return PagingIterable of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ClosedWorkOrders> queryByCustomerSiteYearArea(String customerAccount, UUID siteId, short year, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year and area id from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<ClosedWorkOrders> findByCustomerSiteYearArea(String customerAccount, UUID siteId, short year, UUID areaId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (year == 0) {
            throw new IllegalArgumentException("Argument year cannot 0.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        return queryByCustomerSiteYearArea(customerAccount, siteId, year, areaId).all();
    }

    /**
     * Returns ClosedWorkOrders Objects for the given customer account, site id, and area id from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return PagingIterable of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ClosedWorkOrders> queryByCustomerSiteYearAreaAsset(String customerAccount, UUID siteId, short year, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year, area id and asset id from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<ClosedWorkOrders> findByCustomerSiteYearAreaAsset(String customerAccount, UUID siteId, short year, UUID areaId, UUID assetId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (year == 0) {
            throw new IllegalArgumentException("Argument year cannot be 0.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null or empty.");
        }
        return queryByCustomerSiteYearAreaAsset(customerAccount, siteId, year, areaId, assetId).all();
    }

    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year, area id, asset id and createdDate from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param createdDate, Instant Object
     * @return PagingIterable of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ClosedWorkOrders> queryByPK(String customerAccount, UUID siteId, short year, UUID areaId, UUID assetId, Instant createdDate) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year, area id, asset id and createdDate from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param createdDate, Instant Object
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<ClosedWorkOrders> findByPK(String customerAccount, UUID siteId, short year, UUID areaId, UUID assetId, Instant createdDate) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (year == 0) {
            throw new IllegalArgumentException("Argument year cannot be 0.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null or empty.");
        }
        if (createdDate == null) {
            throw new IllegalArgumentException("Argument createdDate cannot be null or empty.");
        }
        return queryByPK(customerAccount, siteId, year, areaId, assetId, createdDate).all();
    }
    
    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, ClosedWorkOrders Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(ClosedWorkOrders entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getSiteId() != null && entity.getAreaId() != null && 
                entity.getAssetId() != null && entity.getCreatedDate() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO closed_work_orders(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",opened_year");
                    values.append(",").append(entity.getOpenedYear());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",created_date");
                    values.append(",'").append(entity.getCreatedDate()).append("'");
                   
                    if (entity.getWorkOrderDetails() != null) {
                        query.append(",work_order_details");
                        values.append(",'").append(entity.getWorkOrderDetails()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM closed_work_orders WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND opened_year=").append(entity.getOpenedYear());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND created_date='").append(entity.getCreatedDate()).append("';");
                }
                break;
            }
        }
        
        return query.toString();
    }

}
