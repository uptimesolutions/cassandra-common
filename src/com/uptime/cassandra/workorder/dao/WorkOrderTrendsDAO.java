/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Delete;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface WorkOrderTrendsDAO extends BaseDAO<WorkOrderTrends> {
      Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(WorkOrderTrendsDAO.class.getName());

    /**
     * Inserts the given workOrderTrends into the work_order_trends table
     * @param entity, WorkOrderTrends Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(WorkOrderTrends entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given workOrderTrends into the work_order_trends table
     * @param entity, WorkOrderTrends Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(WorkOrderTrends entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Delete the given workOrderTrends from the work_order_trends table
     * @param entity, WorkOrderTrends Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(WorkOrderTrends entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given partition from the work_order_trends table.
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Delete(entityClass = WorkOrderTrends.class)
    void deleteByCustomerSiteAreaAssetPriorityDate(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate) throws UnavailableException, WriteTimeoutException;
       
    /**
     * Truncate the work_order_trends table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE work_order_trends")
    public void truncateAllWorkOrderTrends() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the work_order_trends table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllWorkOrderTrends();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new WorkOrderTrends(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new WorkOrderTrends(), "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * Returns WorkOrderTrends Objects for the given customer account, site id, and area id from work_order_trends table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @return PagingIterable of WorkOrderTrends Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<WorkOrderTrends> queryByCustomerSiteAreaAssetPriorityDate(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of WorkOrderTrends Objects for the given customer account, site id, and area id from work_order_trends table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @return List of WorkOrderTrends Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<WorkOrderTrends> findByCustomerSiteAreaAssetPriorityDate(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null or empty.");
        }
        if (priority <= 0) {
            throw new IllegalArgumentException("Argument priority cannot be zero or less than zero.");
        }
        if (createdDate == null) {
            throw new IllegalArgumentException("Argument createdDate cannot be null or empty.");
        }
        return queryByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate).all();
    }

    /**
     * Returns WorkOrderTrends Objects for the given customer account, site id, and area id from work_order_trends table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @param rowId, UUID Object
     * @return PagingIterable of WorkOrderTrends Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<WorkOrderTrends> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate, UUID rowId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of WorkOrderTrends Objects for the given customer account, site id, and area id from work_order_trends table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @param rowId, UUID Object
     * @return List of WorkOrderTrends Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default public List<WorkOrderTrends> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate, UUID rowId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null or empty.");
        }
        if (priority <= 0) {
            throw new IllegalArgumentException("Argument priority cannot be zero or less than zero.");
        }
        if (createdDate == null) {
            throw new IllegalArgumentException("Argument createdDate cannot be null or empty.");
        }
        if (rowId == null) {
            throw new IllegalArgumentException("Argument rowId cannot be null or empty.");
        }
        return queryByPK(customerAccount, siteId, areaId, assetId, priority, createdDate, rowId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, WorkOrderTrends Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(WorkOrderTrends entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getSiteId() != null && entity.getAreaId() != null && 
                entity.getAssetId() != null && entity.getRowId() != null  &&
                entity.getCreatedDate() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO work_order_trends(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",priority");
                    values.append(",").append(entity.getPriority());
                    query.append(",created_date");
                    values.append(",'").append(entity.getCreatedDate()).append("'");
                    query.append(",row_id");
                    values.append(",").append(entity.getRowId());
                    
                    if (entity.getPointLocationId() != null) {
                        query.append(",point_location_id");
                        values.append(",").append(entity.getPointLocationId());
                    }
                    if (entity.getPointId() != null) {
                        query.append(",point_id");
                        values.append(",").append(entity.getPointId());
                    }
                    if (entity.getApSetId() != null) {
                        query.append(",ap_set_id");
                        values.append(",").append(entity.getApSetId());
                    }
                    if (entity.getParamName() != null && !entity.getParamName().isEmpty()) {
                        query.append(",param_id");
                        values.append(",'").append(entity.getParamName()).append("'");
                    }
                    
                    if (entity.getChannelType() != null && !entity.getChannelType().isEmpty()) {
                        query.append(",channel_type");
                        values.append(",'").append(entity.getChannelType()).append("'");
                    }
                    
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM work_order_trends WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND priority=").append(entity.getPriority());
                    query.append(" AND created_date='").append(entity.getCreatedDate()).append("'");
                    query.append(" AND row_id=").append(entity.getRowId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
