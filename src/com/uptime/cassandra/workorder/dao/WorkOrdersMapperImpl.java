/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author aswani
 */
public class WorkOrdersMapperImpl {
     private static WorkOrdersMapper instance = null;

    public static WorkOrdersMapper getInstance() {
        if (instance == null) {
            new WorkOrdersMapperImpl();
        }
        return instance;
    }

    private WorkOrdersMapperImpl() {
        instance = new WorkOrdersMapperBuilder(CassandraData.getSession()).build();
    }
}
