/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.SiteContacts;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface SiteContactsDAO extends BaseDAO<SiteContacts> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteContactsDAO.class);

    /**
     * Inserts the given siteContacts into the site_contacts table
     * @param entity, SiteContacts Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void create(SiteContacts entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of siteContacts into the site_contacts table
     * @param entities, List Object of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<SiteContacts> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * Update the given siteContacts into the site_contacts table
     * @param entity, SiteContacts Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void update(SiteContacts entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of siteContacts into the site_contacts table
     * @param entities, List Object of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void update(List<SiteContacts> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * delete the given siteContacts from the site_contacts table
     * @param entity, SiteContacts Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void delete(SiteContacts entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given list of siteContacts from the site_contacts table
     * @param entities, List Object of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void delete(List<SiteContacts> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Gets the Site Contacts data by customer account from site_contacts table
     * @param customerAccount, String Object
     * @return PagingIterable of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteContacts> queryByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the Site Contacts data by customer account from site_contacts table
     * @param customerAccount, String Object
     * @return List of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<SiteContacts> findByCustomer(String customerAccount) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer accountis required.");
        }
        return queryByCustomer(customerAccount).all();
    }

    /**
     * Gets the Site Contacts data by customer account and site id from site_contacts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteContacts> queryByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the Site Contacts data by customer account and site id from site_contacts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<SiteContacts> findByCustomerSiteId(String customerAccount, UUID siteId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer accountis required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        return queryByCustomerSiteId(customerAccount, siteId).all();
    }

    /**
     * Gets the Site Contacts data by customer account, site id, and role from site_contacts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param role, String Object
     * @return PagingIterable of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteContacts> queryByPK(String customerAccount, UUID siteId, String role) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the Site Contacts data by customer account, site id and role from site_contacts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param role, String Object
     * @return List of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<SiteContacts> findByPK(String customerAccount, UUID siteId, String role) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer accountis required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (role == null || role.length() < 1) {
            throw new IllegalArgumentException("Argument role is required.");
        }
        return queryByPK(customerAccount, siteId, role).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, SiteContacts Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteContacts entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getRole() != null && !entity.getRole().isEmpty() &&
                entity.getSiteId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_contacts(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",role");
                    values.append(",'").append(entity.getRole()).append("'");
                    if (entity.getSiteName() != null) {
                        query.append(",site_name");
                        values.append(",'").append(entity.getSiteName()).append("'");
                    }
                    if (entity.getContactName() != null) {
                        query.append(",contact_name");
                        values.append(",'").append(entity.getContactName()).append("'");
                    }
                    if (entity.getContactPhone() != null) {
                        query.append(",contact_phone");
                        values.append(",'").append(entity.getContactPhone()).append("'");
                    }
                    if (entity.getContactTitle() != null) {
                        query.append(",contact_title");
                        values.append(",'").append(entity.getContactTitle()).append("'");
                    }
                    if (entity.getContactEmail() != null) {
                        query.append(",contact_email");
                        values.append(",'").append(entity.getContactEmail()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_contacts WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND role='").append(entity.getRole()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

}
