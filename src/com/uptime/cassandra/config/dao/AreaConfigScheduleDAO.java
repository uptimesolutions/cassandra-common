/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface AreaConfigScheduleDAO extends BaseDAO<AreaConfigSchedule> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(AreaConfigScheduleDAO.class);

    /**
     * Inserts the given entity into the area_config_schedule table
     * @param entity, AreaConfigSchedule Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(AreaConfigSchedule entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of areaConfigSchedulea into the area_config_schedule table
     * @param entities, List Object of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<AreaConfigSchedule> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * update the given entity in the area_config_schedule table
     * @param entity, AreaConfigSchedule Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(AreaConfigSchedule entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }
    
    /**
     * update the given entities in the area_config_schedule table
     * @param entities, List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(List<AreaConfigSchedule> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the area_config_schedule table.
     * @param entity, AreaConfigSchedule Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(AreaConfigSchedule entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entities from the area_config_schedule table.
     * @param entities, List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(List<AreaConfigSchedule> entities) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Returns AreaConfigSchedule Objects for the given customerAccount, siteId, and areaId from area_config_schedule table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return PagingIterable of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AreaConfigSchedule> queryByCustomerSiteIdAreaId(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AreaConfigSchedule Objects for the given customerAccount, siteId, and areaId from area_config_schedule table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AreaConfigSchedule> findByCustomerSiteIdAreaId(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        return queryByCustomerSiteIdAreaId(customerAccount, siteId, areaId).all();
    }

    /**
     * Returns AreaConfigSchedule Objects for the given customerAccount, siteId, areaId, and scheduleId from area_config_schedule table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param scheduleId, UUID Object
     * @return PagingIterable of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AreaConfigSchedule> queryByCustomerSiteIdAreaIdScheduleId(String customerAccount, UUID siteId, UUID areaId, UUID scheduleId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AreaConfigSchedule Objects for the given customerAccount, siteId, areaId, and scheduleId from area_config_schedule table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param scheduleId, UUID Object
     * @return List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AreaConfigSchedule> findByCustomerSiteIdAreaIdScheduleId(String customerAccount, UUID siteId, UUID areaId, UUID scheduleId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (scheduleId == null) {
            throw new IllegalArgumentException("Argument scheduleId cannot be null or empty.");
        }
        return queryByCustomerSiteIdAreaIdScheduleId(customerAccount, siteId, areaId, scheduleId).all();
    }

    /**
     * Returns AreaConfigSchedule Objects for the given customerAccount, siteId, areaId, scheduleId, and dayOfWeek from area_config_schedule table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param scheduleId, UUID Object
     * @param dayOfWeek, byte
     * @return PagingIterable of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AreaConfigSchedule> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID scheduleId, byte dayOfWeek) throws UnavailableException, ReadTimeoutException;
    
    /**
     * Returns a List of AreaConfigSchedule Objects for the given customerAccount, siteId, areaId, scheduleId, and dayOfWeek from area_config_schedule table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param scheduleId, UUID Object
     * @param dayOfWeek, byte
     * @return List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AreaConfigSchedule> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID scheduleId, byte dayOfWeek) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        if (scheduleId == null) {
            throw new IllegalArgumentException("Argument scheduleId cannot be null or empty.");
        }
        if (dayOfWeek > 8 || dayOfWeek < 0) {
            throw new IllegalArgumentException("Argument dayOfWeek cannot be greater than 6 or less than 0");
        }
        return queryByPK(customerAccount, siteId, areaId, scheduleId, dayOfWeek).all();
    }
    
     
    /**
     * Truncate the area_config_schedule table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE area_config_schedule")
    public void truncateAllAreaConfigSchedule() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the area_config_schedule table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllAreaConfigSchedule();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new AreaConfigSchedule(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new AreaConfigSchedule(), "DELETE"));
            }
            throw e;
        }
    }
    

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, AreaConfigSchedule Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(AreaConfigSchedule entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getSiteId() != null && entity.getAreaId() != null && entity.getScheduleId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO area_config_schedule(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",schedule_id");
                    values.append(",").append(entity.getScheduleId());
                    query.append(",day_of_week");
                    values.append(",").append(entity.getDayOfWeek());
                    query.append(",is_continuous");
                    values.append(",").append(entity.isContinuous());
                    if (entity.getAreaName() != null) {
                        query.append(",area_name");
                        values.append(",'").append(entity.getAreaName()).append("'");
                    }
                    if (entity.getScheduleName() != null) {
                        query.append(",schedule_name");
                        values.append(",'").append(entity.getScheduleName()).append("'");
                    }
                    if (entity.getDescription() != null) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    if (entity.getHoursOfDay() != null) {
                        query.append(",hours_of_day");
                        values.append(",'").append(entity.getHoursOfDay()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_customer WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND schedule_id=").append(entity.getScheduleId());
                    query.append(" AND day_of_week=").append(entity.getDayOfWeek()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }
}
