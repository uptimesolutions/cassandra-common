/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.config.entity.Points;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface PointsDAO extends BaseDAO<Points>{
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(PointsDAO.class);
    
    /**
     * Inserts the given points into the points table
     * @param entity, Points Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(Points entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of points into the points table
     * @param entities, Object List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<Points> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * Update the given points into the points table
     * @param entity, Points Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(Points entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of points into the points table
     * @param entities, Object List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(List<Points> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Update the alarm_enabled field into the points table
     * @param query, Query to be executed.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void updateAlarmEnabledField(String query) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        CassandraData.getSession().execute(query);
    }
    
    /**
     * delete the given points from the points table
     * @param entity, Points Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(Points entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given List of points from the points table
     * @param entities, Object List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<Points> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, and assetId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return PagingIterable of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Points> queryByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, and assetId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<Points> findByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        return queryByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId ).all();
    }

    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, assetId, and pointLocationId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return PagingIterable of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Points> queryByCustomerSiteAreaAssetLocation(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, assetId, and pointLocationId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<Points> findByCustomerSiteAreaAssetLocation(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        return queryByCustomerSiteAreaAssetLocation(customerAccount, siteId, areaId, assetId, pointLocationId).all();
    }

    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, and pointId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return PagingIterable of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Points> queryByCustomerSiteAreaAssetLocationPoint(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, and pointId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<Points> findByCustomerSiteAreaAssetLocationPoint(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        return queryByCustomerSiteAreaAssetLocationPoint(customerAccount, siteId, areaId, assetId, pointLocationId, pointId).all();
    }
    
    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId and apSetId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return PagingIterable of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Points> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) throws UnavailableException, ReadTimeoutException;
    
    /**
     * Returns Points Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId and apSetId from points table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<Points> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId is required.");
        }
        return queryByPK(customerAccount, siteId, areaId, assetId,  pointLocationId, pointId, apSetId).all();
    }
    
    /**
     * Truncate the points table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE points")
    public void truncateAllPoints() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the points table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllPoints();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new Points(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new Points(), "DELETE"));
            }
            throw e;
        }
    }
    
    
    default public String printCQL(Points entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getAreaId() != null
                && entity.getAssetId() != null
                && entity.getPointLocationId() != null
                && entity.getPointId() != null
                && entity.getApSetId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO points(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",point_location_id");
                    values.append(",").append(entity.getPointLocationId());
                    query.append(",point_id");
                    values.append(",").append(entity.getPointId());
                    query.append(",ap_set_id");
                    values.append(",").append(entity.getApSetId());
                    query.append(",auto_acknowledge");
                    values.append(",").append(entity.isAutoAcknowledge());
                    query.append(",is_disabled");
                    values.append(",").append(entity.isDisabled());
                    query.append(",sensor_channel_num ");
                    values.append(",").append(entity.getSensorChannelNum());
                    query.append(",sensor_offset");
                    values.append(",").append(entity.getSensorOffset());
                    query.append(",sensor_sensitivity");
                    values.append(",").append(entity.getSensorSensitivity());
                    query.append(",alarm_enabled");
                    values.append(",").append(entity.isAlarmEnabled());
                    if (entity.getAlSetId()!= null) {
                        query.append(",al_set_id");
                        values.append(",").append(entity.getAlSetId());
                    }
                    if (entity.getPointName() != null) {
                        query.append(",point_name");
                        values.append(",'").append(entity.getPointName()).append("'");
                    }
                    if (entity.getPointType() != null) {
                        query.append(",point_type");
                        values.append(",'").append(entity.getPointType()).append("'");
                    }
                    if (entity.getSensorType()!= null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }
                    if (entity.getSensorUnits()!= null) {
                        query.append(",sensor_units");
                        values.append(",'").append(entity.getSensorUnits()).append("'");
                    }
                    if (entity.getSensorSubType() != null) {
                        query.append(",sensor_sub_type");
                        values.append(",'").append(entity.getSensorSubType()).append("'");
                    }
                    if (entity.getSensorLocalOrientation() != null) {
                        query.append(",sensor_local_orientation");
                        values.append(",'").append(entity.getSensorLocalOrientation()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM points WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND point_location_id=").append(entity.getPointLocationId());
                    query.append(" AND point_id=").append(entity.getPointId());
                    query.append(" AND ap_set_id=").append(entity.getApSetId()).append(";");
                }
                break;
            }
        }
        return query.toString();
    }

}
