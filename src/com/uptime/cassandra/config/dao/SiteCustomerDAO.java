/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.SiteCustomer;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface SiteCustomerDAO extends BaseDAO<SiteCustomer> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteCustomerDAO.class);

    /**
     * Inserts the given entity into the site_customer table
     * @param entity, SiteCustomer Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(SiteCustomer entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the site_customer table
     * @param entity, SiteCustomer Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(SiteCustomer entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the site_customer table.
     * @param entity, SiteCustomer Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(SiteCustomer entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns SiteCustomer Objects for the given customer account from site_customer table
     * @param customerAccount, String Object
     * @return PagingIterable of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteCustomer> queryByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of SiteCustomer Objects for the given customer account from site_customer table
     * @param customerAccount, String Object
     * @return List of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteCustomer> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        return queryByCustomer(customerAccount).all();
    }

    /**
     * Returns SiteCustomer Objects for the given customer account and site Id from site_customer table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteCustomer> queryByPK(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of SiteCustomer Objects for the given customer account and site Id from site_customer table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteCustomer> findByPK(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        return queryByPK(customerAccount, siteId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, SiteCustomer Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteCustomer entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getSiteId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_customer(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    if (entity.getSiteName() != null) {
                        query.append(",site_name");
                        values.append(",'").append(entity.getSiteName()).append("'");
                    }
                    if (entity.getRegion() != null) {
                        query.append(",region");
                        values.append(",'").append(entity.getRegion()).append("'");
                    }
                    if (entity.getCountry() != null) {
                        query.append(",country");
                        values.append(",'").append(entity.getCountry()).append("'");
                    }
                    if (entity.getBillingAddress1() != null) {
                        query.append(",billing_address_1");
                        values.append(",'").append(entity.getBillingAddress1()).append("'");
                    }
                    if (entity.getBillingAddress2() != null) {
                        query.append(",billing_address_2");
                        values.append(",'").append(entity.getBillingAddress2()).append("'");
                    }
                    if (entity.getBillingCity() != null) {
                        query.append(",billing_city");
                        values.append(",'").append(entity.getBillingCity()).append("'");
                    }
                    if (entity.getBillingPostalCode() != null) {
                        query.append(",billing_postal_code");
                        values.append(",'").append(entity.getBillingPostalCode()).append("'");
                    }
                    if (entity.getBillingStateProvince() != null) {
                        query.append(",billing_state_province");
                        values.append(",'").append(entity.getBillingStateProvince()).append("'");
                    }
                    if (entity.getIndustry() != null) {
                        query.append(",industry");
                        values.append(",'").append(entity.getIndustry()).append("'");
                    }
                    if (entity.getLatitude() != null) {
                        query.append(",latitude");
                        values.append(",'").append(entity.getLatitude()).append("'");
                    }
                    if (entity.getLongitude() != null) {
                        query.append(",longitude");
                        values.append(",'").append(entity.getLongitude()).append("'");
                    }
                    if (entity.getPhysicalAddress1() != null) {
                        query.append(",physical_address_1");
                        values.append(",'").append(entity.getPhysicalAddress1()).append("'");
                    }
                    if (entity.getPhysicalAddress2() != null) {
                        query.append(",physical_address_2");
                        values.append(",'").append(entity.getPhysicalAddress2()).append("'");
                    }
                    if (entity.getPhysicalCity() != null) {
                        query.append(",physical_city");
                        values.append(",'").append(entity.getPhysicalCity()).append("'");
                    }
                    if (entity.getPhysicalPostalCode() != null) {
                        query.append(",physical_postal_code");
                        values.append(",'").append(entity.getPhysicalPostalCode()).append("'");
                    }
                    if (entity.getPhysicalStateProvince() != null) {
                        query.append(",physical_state_province");
                        values.append(",'").append(entity.getPhysicalStateProvince()).append("'");
                    }
                    if (entity.getShipAddress1() != null) {
                        query.append(",ship_address_1");
                        values.append(",'").append(entity.getShipAddress1()).append("'");
                    }
                    if (entity.getShipAddress2() != null) {
                        query.append(",ship_address_2");
                        values.append(",'").append(entity.getShipAddress2()).append("'");
                    }
                    if (entity.getShipCity() != null) {
                        query.append(",ship_city");
                        values.append(",'").append(entity.getShipCity()).append("'");
                    }
                    if (entity.getShipPostalCode() != null) {
                        query.append(",ship_postal_code");
                        values.append(",'").append(entity.getShipPostalCode()).append("'");
                    }
                    if (entity.getShipStateProvince() != null) {
                        query.append(",ship_state_province");
                        values.append(",'").append(entity.getShipStateProvince()).append("'");
                    }
                    if (entity.getTimezone() != null) {
                        query.append(",timezone");
                        values.append(",'").append(entity.getTimezone()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_customer WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }
}
