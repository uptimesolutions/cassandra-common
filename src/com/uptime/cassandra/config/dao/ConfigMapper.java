/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;

/**
 *
 * @author aswani
 */
@Mapper
public interface ConfigMapper {

    @DaoFactory
    AreaConfigScheduleDAO areaConfigScheduleDAO();

    @DaoFactory
    AreaSiteDAO areaSiteDAO();

    @DaoFactory
    SiteCustomerDAO siteCustomerDAO();

    @DaoFactory
    AssetSiteAreaDAO assetSiteAreaDAO();

    @DaoFactory
    SiteRegionCountryDAO siteRegionCountryDAO();
   
    @DaoFactory
    SiteContactsDAO siteContactsDAO();
    
    @DaoFactory
    PointLocationsDAO pointLocationsDAO();
    
    @DaoFactory
    PointsDAO pointsDAO();
    
    @DaoFactory
    PointToHwUnitDAO pointToHwUnitDAO();
    
    @DaoFactory
    HwUnitToPointDAO hwUnitToPointDAO();
    
    @DaoFactory
    ApAlByPointDAO apAlByPointDAO();
    
    @DaoFactory
    SubscriptionsDAO subscriptionsDAO();
    
    @DaoFactory
    PointLocationHwUnitHistoryDAO pointLocationHwUnitHistoryDAO();
}
