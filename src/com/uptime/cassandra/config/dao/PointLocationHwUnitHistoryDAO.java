/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.PointLocationHwUnitHistory;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface PointLocationHwUnitHistoryDAO extends BaseDAO<PointLocationHwUnitHistory>{
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(PointLocationHwUnitHistoryDAO.class);
    
    /**
     * Inserts the given entity into the point_location_hwunit_history table
     * @param entity, PointLocationHwUnitHistory Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(PointLocationHwUnitHistory entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given entity into the point_location_hwunit_history table
     * @param entity, PointLocationHwUnitHistory Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(PointLocationHwUnitHistory entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * delete the given entity from the point_location_hwunit_history table
     * @param entity, PointLocationHwUnitHistory Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(PointLocationHwUnitHistory entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns PointLocationHwUnitHistory Objects for the given deviceId
     * from point_location_hwunit_history table
     * @param pointLocationId, UUID Object
     * @return PagingIterable of PointLocationHwUnitHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PointLocationHwUnitHistory> queryByPointLocation(UUID pointLocationId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns PointLocationHwUnitHistory Objects for the given deviceId
     * from point_location_hwunit_history table
     * @param pointLocationId, UUID Object
     * @return List of PointLocationHwUnitHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<PointLocationHwUnitHistory> findByPointLocation(UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        
        return queryByPointLocation(pointLocationId).all();
    }

    /**
     * Returns PointLocationHwUnitHistory Objects for the given pointLocationId, deviceId and dateFrom
     * from point_location_hwunit_history table
     * @param pointLocationId, UUID Object
     * @param deviceId, String Object
     * @param dateFrom, Instant Object
     * @return PagingIterable of PointLocationHwUnitHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PointLocationHwUnitHistory> queryByPK(UUID pointLocationId, String deviceId, Instant dateFrom) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns PointLocationHwUnitHistory Objects for the given pointLocationId, deviceId and dateFrom
     * from point_location_hwunit_history table
     * @param pointLocationId, UUID Object
     * @param deviceId, String Object
     * @param dateFrom, Instant Object
     * @return List of PointLocationHwUnitHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<PointLocationHwUnitHistory> findByPK(UUID pointLocationId, String deviceId, Instant dateFrom) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (deviceId == null || deviceId.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }
        if (dateFrom == null) {
            throw new IllegalArgumentException("Argument dateFrom is required.");
        }
        
        return queryByPK(pointLocationId, deviceId.trim(), dateFrom).all();
    }

    /**
     * Truncate the point_location_hwunit_history table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE point_location_hwunit_history")
    public void truncateAllPLHwunitHistory() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the point_location_hwunit_history table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllPLHwunitHistory();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new PointLocationHwUnitHistory(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new PointLocationHwUnitHistory(), "DELETE"));
            }
            throw e;
        }
    }
    
    
    default public String printCQL(PointLocationHwUnitHistory entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getPointLocationId()!= null
                && entity.getDeviceId() != null && !entity.getDeviceId().isEmpty()
                && entity.getDateFrom()!= null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO point_location_hwunit_history(point_location_id");
                    values.append("'").append(entity.getPointLocationId()).append("'");
                    query.append(",device_id");
                    values.append(",'").append(entity.getDeviceId()).append("'");
                    query.append(",date_from");
                    values.append(",'").append(entity.getDateFrom()).append("'");
                    if (entity.getCustomerAccount() != null) {
                        query.append(",customer_acct");
                        values.append(",'").append(entity.getCustomerAccount()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM point_location_hwunit_history WHERE ");
                    query.append(" point_location_id=").append(entity.getPointLocationId());
                    query.append(" and device_id='").append(entity.getDeviceId()).append("'");
                    query.append(" date_from='").append(entity.getDateFrom()).append("';");
                }
                break;
            }
        }
        return query.toString();
    }

}
