/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface PointToHwUnitDAO extends BaseDAO<PointToHwUnit>{
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(PointToHwUnitDAO.class);
    
    /**
     * Inserts the given entity into the point_to_hwunit table
     * @param entity, PointToHwUnit Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(PointToHwUnit entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given entity into the point_to_hwunit table
     * @param entity, PointToHwUnit Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(PointToHwUnit entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * delete the given entity from the point_to_hwunit table
     * @param entity, PointToHwUnit Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(PointToHwUnit entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns PointToHwUnit Objects for the given pointId from point_to_hwunit table
     * @param pointId, UUID Object
     * @return PagingIterable of PointToHwUnit Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PointToHwUnit> queryByPK(UUID pointId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns PointToHwUnit Objects for the given pointId from point_to_hwunit table
     * @param pointId, UUID Object
     * @return List of PointToHwUnit Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<PointToHwUnit> findByPK(UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        
        return queryByPK(pointId).all();
    }
    
    /**
     * Inserts the given List of PointToHwUnit into the point_to_hwunit table
     * @param entities, List Object of PointToHwUnit Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<PointToHwUnit> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * Delete the given List of PointToHwUnit from the point_to_hwunit table
     * @param entities, List Object of PointToHwUnit Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<PointToHwUnit> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     * Truncate the point_to_hwunit table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE point_to_hwunit")
    public void truncateAllPointToHwUnit() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the point_to_hwunit table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllPointToHwUnit();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new PointToHwUnit(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new PointToHwUnit(), "DELETE"));
            }
            throw e;
        }
    }
    
    default public String printCQL(PointToHwUnit entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getDeviceId() != null && !entity.getDeviceId().isEmpty()
                && entity.getChannelType() != null && !entity.getChannelType().isEmpty()
                && entity.getChannelNum() != 0
                && entity.getPointId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO point_to_hwunit(device_id");
                    values.append("'").append(entity.getDeviceId()).append("'");
                    query.append(",channel_type");
                    values.append(",'").append(entity.getChannelType()).append("'");
                    query.append(",channel_num");
                    values.append(",").append(entity.getChannelNum());
                    query.append(",point_id");
                    values.append(",").append(entity.getPointId());
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM point_to_hwunit WHERE ");
                    query.append(" point_id=").append(entity.getPointId()).append(";");
                }
                break;
            }
        }
        return query.toString();
    }

}
