/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.config.entity.PointLocations;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface PointLocationsDAO extends BaseDAO<PointLocations>{
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(PointLocationsDAO.class);
    
    /**
     * Inserts the given pointLocations into the point_locations table
     * @param entity, PointLocations Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(PointLocations entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of pointLocations into the point_locations table
     * @param entities, Object List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<PointLocations> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * Update the given pointLocations into the point_locations table
     * @param entity, PointLocations Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(PointLocations entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of pointLocations into the point_locations table
     * @param entities, Object List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(List<PointLocations> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Update the custom fields into the point_locations table
     * @param query, Query to be executed.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void updateWithCustomQuery(String query) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        CassandraData.getSession().execute(query);
    }
    
    /**
     * Truncate the point_locations table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE point_locations")
    public void truncateAllPointLocations() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the point_locations table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllPointLocations();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new PointLocations(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new PointLocations(), "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * delete the given pointLocations from the point_locations table
     * @param entity, PointLocations Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(PointLocations entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given List of pointLocations from the point_locations table
     * @param entities, Object List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<PointLocations> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Gets the PointLocations data by customer account, siteId and areaId from point_locations table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return PagingIterable of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PointLocations> queryByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of PointLocations Objects for the given customer account, siteId and areaId from point_locations table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<PointLocations> findByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        return queryByCustomerSiteArea(customerAccount, siteId, areaId).all();
    }

    /**
     * Gets the PointLocations data by customer account, site name, area name, and asset name from point_locations table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return PagingIterable of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PointLocations> queryByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of PointLocations Objects for the given customer account, site name, area name, and asset name from point_locations table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<PointLocations> findByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        return queryByCustomerSiteAreaAsset(customerAccount, siteId, areaId,assetId ).all();
    }

    /**
     * Returns PointLocations Objects for the given customer account, site name, area name, point location name, and asset name from point_locations table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return PagingIterable of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PointLocations> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of PointLocations Objects for the given customer account, site name, area name, point location name, and asset name from point_locations table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<PointLocations> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        return queryByPK(customerAccount, siteId, areaId, assetId,  pointLocationId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, PointLocations Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(PointLocations entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getAreaId() != null
                && entity.getAssetId() != null
                && entity.getPointLocationId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO point_locations(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",point_location_id");
                    values.append(",").append(entity.getPointLocationId());
                    query.append(",speed_ratio");
                    values.append(",").append(entity.getSpeedRatio());
                    query.append(",sample_interval");
                    values.append(",").append(entity.getSampleInterval());
                    query.append(",basestation_port_num");
                    values.append(",").append(entity.getBasestationPortNum());
                    query.append(",alarm_enabled");
                    values.append(",").append(entity.isAlarmEnabled());
                    query.append(",roll_diameter");
                    values.append(",").append(entity.getRollDiameter());
                    if (entity.getPointLocationName() != null) {
                        query.append(",point_location_name");
                        values.append(",'").append(entity.getPointLocationName()).append("'");
                    }
                    if (entity.getDescription() != null) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    if (entity.getDeviceSerialNumber() != null) {
                        query.append(",device_serial_number");
                        values.append(",'").append(entity.getDeviceSerialNumber()).append("'");
                    }
                    if (entity.getFfSetIds() != null) {
                        query.append(",ff_set_ids");
                        values.append(",'").append(entity.getFfSetIds()).append("'");
                    }
                    if (entity.getTachId() != null) {
                        query.append(",tach_id");
                        values.append(",").append(entity.getTachId());
                    }
                    if (entity.getRollDiameterUnits() != null) {
                        query.append(",roll_diameter_units");
                        values.append(",'").append(entity.getRollDiameterUnits()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM point_locations WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND point_location_id=").append(entity.getPointLocationId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }
}
