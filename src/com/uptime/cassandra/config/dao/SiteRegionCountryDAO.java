/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface SiteRegionCountryDAO extends BaseDAO<SiteRegionCountry> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SiteRegionCountryDAO.class);

    /**
     * Inserts the given user into the site_region_country table
     * @param entity, SiteRegionCountry Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void create(SiteRegionCountry entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException | UnavailableException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given user in the site_region_country table.
     * @param entity, SiteRegionCountry Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(SiteRegionCountry entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given user from the site_region_country table.
     * @param entity, SiteRegionCountry Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(SiteRegionCountry entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }

    }

    /**
     * Returns a List of SiteRegionCountry Objects for the given customer from site_region_country table
     * @param customer, String Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteRegionCountry> findByCustomer(String customer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }

        List<SiteRegionCountry> siteRegionCountries = queryByCustomer(customer).all();
        return siteRegionCountries;
    }

    /**
     * Returns SiteRegionCountry Objects for the given customer from site_region_country table
     * @param customer, String Object
     * @return PagingIterable of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteRegionCountry> queryByCustomer(String customer) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of SiteRegionCountry Objects for the given customer and region from site_region_country table
     * @param customer, String Object
     * @param region, String Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteRegionCountry> findByCustomerRegion(String customer, String region) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (region == null || region.length() < 1) {
            throw new IllegalArgumentException("Argument region is required.");
        }

        List<SiteRegionCountry> siteRegionCountries = queryByCustomerRegion(customer, region).all();
        return siteRegionCountries;
    }

    /**
     * Returns SiteRegionCountry Objects for the given customer and region from site_region_country table
     * @param customer, String Object
     * @param region, String Object
     * @return PagingIterable of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteRegionCountry> queryByCustomerRegion(String customer, String region) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of SiteRegionCountry Objects for the given customer, region and country from site_region_country table
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteRegionCountry> findByCustomerRegionCountry(String customer, String region, String country) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (region == null || region.length() < 1) {
            throw new IllegalArgumentException("Argument region is required.");
        }
        if (country == null || country.length() < 1) {
            throw new IllegalArgumentException("Argument country is required.");
        }
        List<SiteRegionCountry> siteRegionCountrys = queryByCustomerRegionCountry(customer, region, country).all();
        return siteRegionCountrys;
    }

    /**
     * Returns SiteRegionCountry Objects for the given customer, region and country from site_region_country table
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @return PagingIterable of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteRegionCountry> queryByCustomerRegionCountry(String customer, String region, String country) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of SiteRegionCountry Objects for the given customer, region, country and siteId from site_region_country table
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @param siteId, UUID Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteRegionCountry> findByPK(String customer, String region, String country, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (region == null || region.length() < 1) {
            throw new IllegalArgumentException("Argument region is required.");
        }
        if (country == null || country.length() < 1) {
            throw new IllegalArgumentException("Argument country is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        List<SiteRegionCountry> siteRegionCountrys = queryByPK(customer, region, country, siteId).all();
        return siteRegionCountrys;
    }

    /**
     * Returns SiteRegionCountry Objects for the given customer, region, country and siteId from site_region_country table
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<SiteRegionCountry> queryByPK(String customer, String region, String country, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, SiteRegionCountry Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(SiteRegionCountry entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getRegion() != null && !entity.getRegion().isEmpty() &&
                entity.getCountry() != null && !entity.getCountry().isEmpty() &&
                entity.getSiteId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO site_region_country (customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",region");
                    values.append(",'").append(entity.getRegion()).append("'");
                    query.append(",country");
                    values.append(",'").append(entity.getCountry()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    if (entity.getSiteName() != null) {
                        query.append(",site_name");
                        values.append(",'").append(entity.getSiteName()).append("'");
                    }
                    if (entity.getBillingAddress1() != null) {
                        query.append(",billing_address_1");
                        values.append(",'").append(entity.getBillingAddress1()).append("'");
                    }
                    if (entity.getBillingAddress2() != null) {
                        query.append(",billing_address_2");
                        values.append(",'").append(entity.getBillingAddress2()).append("'");
                    }
                    if (entity.getBillingCity() != null) {
                        query.append(",billing_city");
                        values.append(",'").append(entity.getBillingCity()).append("'");
                    }
                    if (entity.getBillingPostalCode() != null) {
                        query.append(",billing_postal_code");
                        values.append(",'").append(entity.getBillingPostalCode()).append("'");
                    }
                    if (entity.getBillingStateProvince() != null) {
                        query.append(",billing_state_province");
                        values.append(",'").append(entity.getBillingStateProvince()).append("'");
                    }
                    if (entity.getIndustry() != null) {
                        query.append(",industry");
                        values.append(",'").append(entity.getIndustry()).append("'");
                    }
                    if (entity.getLatitude() != null) {
                        query.append(",latitude");
                        values.append(",'").append(entity.getLatitude()).append("'");
                    }
                    if (entity.getLongitude() != null) {
                        query.append(",longitude");
                        values.append(",'").append(entity.getLongitude()).append("'");
                    }
                    if (entity.getPhysicalAddress1() != null) {
                        query.append(",physical_address_1");
                        values.append(",'").append(entity.getPhysicalAddress1()).append("'");
                    }
                    if (entity.getPhysicalAddress2() != null) {
                        query.append(",physical_address_2");
                        values.append(",'").append(entity.getPhysicalAddress2()).append("'");
                    }
                    if (entity.getPhysicalCity() != null) {
                        query.append(",physical_city");
                        values.append(",'").append(entity.getPhysicalCity()).append("'");
                    }
                    if (entity.getPhysicalPostalCode() != null) {
                        query.append(",physical_postal_code");
                        values.append(",'").append(entity.getPhysicalPostalCode()).append("'");
                    }
                    if (entity.getPhysicalStateProvince() != null) {
                        query.append(",physical_state_province");
                        values.append(",'").append(entity.getPhysicalStateProvince()).append("'");
                    }
                    if (entity.getShipAddress1() != null) {
                        query.append(",ship_address_1");
                        values.append(",'").append(entity.getShipAddress1()).append("'");
                    }
                    if (entity.getShipAddress2() != null) {
                        query.append(",ship_address_2");
                        values.append(",'").append(entity.getShipAddress2()).append("'");
                    }
                    if (entity.getShipCity() != null) {
                        query.append(",ship_city");
                        values.append(",'").append(entity.getShipCity()).append("'");
                    }
                    if (entity.getShipPostalCode() != null) {
                        query.append(",ship_postal_code");
                        values.append(",'").append(entity.getShipPostalCode()).append("'");
                    }
                    if (entity.getShipStateProvince() != null) {
                        query.append(",ship_state_province");
                        values.append(",'").append(entity.getShipStateProvince()).append("'");
                    }
                    if (entity.getTimezone() != null) {
                        query.append(",timezone");
                        values.append(",'").append(entity.getTimezone()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM site_region_country WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND region='").append(entity.getRegion()).append("'");
                    query.append(" AND country='").append(entity.getCountry()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
