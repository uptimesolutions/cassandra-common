/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface HwUnitToPointDAO extends BaseDAO<HwUnitToPoint>{
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(HwUnitToPointDAO.class);
    
    /**
     * Inserts the given entity into the hwunit_to_point table
     * @param entity, HwUnitToPoint Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(HwUnitToPoint entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given entity into the hwunit_to_point table
     * @param entity, HwUnitToPoint Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(HwUnitToPoint entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * delete the given entity from the hwunit_to_point table
     * @param entity, HwUnitToPoint Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(HwUnitToPoint entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns HwUnitToPoint Objects for the given deviceId
     * from hwunit_to_point table
     * @param deviceId, String Object
     * @return PagingIterable of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<HwUnitToPoint> queryByDevice(String deviceId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns HwUnitToPoint Objects for the given deviceId
     * from hwunit_to_point table
     * @param deviceId, String Object
     * @return List of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<HwUnitToPoint> findByDevice(String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        
        if (deviceId == null || deviceId.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }
        
        return queryByDevice(deviceId.trim()).all();
    }

    /**
     * Returns HwUnitToPoint Objects for the given deviceId, channelType and channelNum
     * from hwunit_to_point table
     * @param deviceId, String Object
     * @param channelType, String Object
     * @param channelNum, int
     * @return PagingIterable of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<HwUnitToPoint> queryByPK(String deviceId, String channelType, byte channelNum) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns HwUnitToPoint Objects for the given deviceId, channelType and channelNum
     * from hwunit_to_point table
     * @param deviceId, String Object
     * @param channelType, String Object
     * @param channelNum, int
     * @return List of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<HwUnitToPoint> findByPK(String deviceId, String channelType, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        
        if (deviceId == null || deviceId.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }
        if (channelType == null || channelType.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument channelType is required.");
        }
        if (channelNum < 0) {
            throw new IllegalArgumentException("Argument channelNum is should be greater than or equal to 0.");
        }
        
        return queryByPK(deviceId.trim(), channelType.trim(), channelNum).all();
    }
    
    /**
     * Inserts the given List of HwUnitToPoint into the hwunit_to_point table
     * @param entities, List Object of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<HwUnitToPoint> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * Delete the given List of HwUnitToPoint from the hwunit_to_point table
     * @param entities, List Object of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<HwUnitToPoint> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     * Truncate the hwunit_to_point table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE hwunit_to_point")
    public void truncateAllHwUnitToPoint() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the hwunit_to_point table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllHwUnitToPoint();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new HwUnitToPoint(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new HwUnitToPoint(), "DELETE"));
            }
            throw e;
        }
    }
    
    
    default public String printCQL(HwUnitToPoint entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getDeviceId() != null && !entity.getDeviceId().isEmpty()
                && entity.getChannelType() != null && !entity.getChannelType().isEmpty()
                && entity.getChannelNum() != 0
                && entity.getPointId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO hwunit_to_point(device_id");
                    values.append("'").append(entity.getDeviceId()).append("'");
                    query.append(",channel_type");
                    values.append(",'").append(entity.getChannelType()).append("'");
                    query.append(",channel_num");
                    values.append(",").append(entity.getChannelNum());
                    if (entity.getCustomerAcct() != null && !entity.getCustomerAcct().isEmpty()) {
                        query.append(",customer_acct");
                        values.append(",'").append(entity.getPointId()).append("'");
                    }
                    if (entity.getSiteId() != null) {
                        query.append(",site_id");
                        values.append(",").append(entity.getSiteId());
                    }
                    if (entity.getAreaId() != null) {
                        query.append(",area_id");
                        values.append(",").append(entity.getAreaId());
                    }
                    if (entity.getAssetId() != null) {
                        query.append(",asset_id");
                        values.append(",").append(entity.getAssetId());
                    }
                    if (entity.getPointLocationId() != null) {
                        query.append(",point_location_id");
                        values.append(",").append(entity.getPointLocationId());
                    }
                    if (entity.getPointId() != null) {
                        query.append(",point_id");
                        values.append(",").append(entity.getPointId());
                    }
                    if (entity.getApSetId() != null) {
                        query.append(",ap_set_id");
                        values.append(",").append(entity.getApSetId());
                    }
                    
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM hwunit_to_point WHERE ");
                    query.append(" device_id='").append(entity.getDeviceId()).append("'");
                    query.append(" channel_type='").append(entity.getChannelType()).append("'");
                    query.append(" channel_num=").append(entity.getChannelNum()).append(";");
                }
                break;
            }
        }
        return query.toString();
    }

}
