/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface AssetSiteAreaDAO extends BaseDAO<AssetSiteArea> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(AssetSiteAreaDAO.class);

    /**
     * Inserts the given user into the asset_site_area table
     * @param entity, AssetSiteArea Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(AssetSiteArea entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException | UnavailableException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given user in the asset_site_area table.
     * @param entity, AssetSiteArea Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(AssetSiteArea entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given user from the asset_site_area table.
     * @param entity, AssetSiteArea Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(AssetSiteArea entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }

    }

    /**
     * Returns AssetSiteArea Objects for the given customerAccount and siteId from asset_site_area table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AssetSiteArea> queryByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AssetSiteArea Objects for the given customerAccount and siteId from asset_site_area table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AssetSiteArea> findByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        return queryByCustomerSite(customerAccount, siteId).all();
    }

    /**
     * Returns AssetSiteArea Objects for the given customerAccount, siteId and areaId from asset_site_area table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return PagingIterable of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AssetSiteArea> queryByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AssetSiteArea Objects for the given customerAccount, siteId and areaId from asset_site_area table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AssetSiteArea> findByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        return queryByCustomerSiteArea(customerAccount, siteId, areaId).all();
    }

    /**
     * Returns AssetSiteArea Objects for the given customerAccount, siteId, areaId and assetId from asset_site_area table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return PagingIterable of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AssetSiteArea> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AssetSiteArea Objects for the given customerAccount, siteId, areaId and assetId from asset_site_area table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AssetSiteArea> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        return queryByPK(customerAccount, siteId, areaId, assetId).all();
    }
    
    /**
     * Truncate the asset_site_area table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE asset_site_area")
    public void truncateAllAssetSiteArea() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the asset_site_area table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllAssetSiteArea();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new AssetSiteArea(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new AssetSiteArea(), "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, AssetSiteArea Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(AssetSiteArea entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getAreaId() != null
                && entity.getAssetId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO asset_site_area(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",sample_interval");
                    values.append(",").append(entity.getSampleInterval());
                    query.append(",alarm_enabled");
                    values.append(",").append(entity.isAlarmEnabled());
                    if (entity.getAssetName() != null) {
                        query.append(",asset_name");
                        values.append(",'").append(entity.getAssetName()).append("'");
                    }
                    if (entity.getAssetType() != null) {
                        query.append(",asset_type");
                        values.append(",'").append(entity.getAssetType()).append("'");
                    }
                    if (entity.getDescription() != null) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM asset_site_area WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }
}
