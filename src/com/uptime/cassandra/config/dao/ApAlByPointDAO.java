/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Mohd Juned Alam
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface ApAlByPointDAO extends BaseDAO<ApAlByPoint> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(ApAlByPointDAO.class);

    /**
     * Inserts the given entity into the ap_al_by_point table
     * @param entity, ApAlByPoint Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(ApAlByPoint entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of ApAlByPoint into the ap_al_by_point table
     * @param entities, List Object of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<ApAlByPoint> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * update the given entity in the ap_al_by_point table
     * @param entity, ApAlByPoint Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(ApAlByPoint entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of ApAlByPoint into the ap_al_by_point table
     * @param entities, List Object of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(List<ApAlByPoint> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Deletes the given entity from the ap_al_by_point table.
     * @param entity, ApAlByPoint Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(ApAlByPoint entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given List of ApAlByPoint from the ap_al_by_point table
     * @param entities, List Object of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<ApAlByPoint> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId, and apSetId from ap_al_by_point table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return PagingIterable of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ApAlByPoint> queryByCustomerSiteAreaAssetLocationPointApSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) throws UnavailableException, ReadTimeoutException;
    
    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId, and apSetId from ap_al_by_point table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ApAlByPoint> findByCustomerSiteAreaAssetLocationPointApSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId is required.");
        }
        return queryByCustomerSiteAreaAssetLocationPointApSet(customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId).all();
    }
    
    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId and list of apSetIds from ap_al_by_point table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetIdList, List of UUID Objects
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<ApAlByPoint> findByCustomerSiteAreaAssetLocationPointApSets(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, List<UUID> apSetIdList) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument Customer Account cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (apSetIdList == null || apSetIdList.isEmpty()) {
            throw new IllegalArgumentException("Argument apSetIdList cannot be null.");
        }
        List<String> apSetList = new ArrayList();
        apSetIdList.stream().forEach(apSetId -> apSetList.add(apSetId.toString()));
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM ap_al_by_point WHERE customer_acct = '");
        query.append(customerAccount).append("'");
        query.append(" and site_id= ");
        query.append(siteId);
        query.append(" and area_id= ");
        query.append(areaId);
        query.append(" and asset_id= ");
        query.append(assetId);
        query.append(" and point_location_id= ");
        query.append(pointLocationId);
        query.append(" and point_id= ");
        query.append(pointId);
        query.append(" and ap_set_id in (");
        for (int i = 0; i < apSetIdList.size(); i++) {
            query.append(apSetIdList.get(i)).append(i < apSetIdList.size() - 1 ? "," : "");
        }
        query.append(");");
        BoundStatement query_apSets = CassandraData.getSession().prepare(query.toString()).bind();
        ResultSet resultSet = CassandraData.getSession().execute(query_apSets);
        
        // Get the results of the query
        List<ApAlByPoint> apAlByPointList = new ArrayList<>();
        while (resultSet.iterator().hasNext()) {
            ApAlByPoint apAlByPoints = new ApAlByPoint();
            Row row = resultSet.iterator().next();
            apAlByPoints.setCustomerAccount(row.getString("customer_acct"));
            apAlByPoints.setSiteId(row.getUuid("site_id"));
            apAlByPoints.setAreaId(row.getUuid("area_id"));
            apAlByPoints.setAssetId(row.getUuid("asset_id"));
            apAlByPoints.setPointLocationId(row.getUuid("point_location_id"));
            apAlByPoints.setPointId(row.getUuid("point_id"));
            apAlByPoints.setApSetId(row.getUuid("ap_set_id"));
            apAlByPoints.setAlSetId(row.getUuid("al_set_id"));
            apAlByPoints.setParamName(row.getString("param_name"));
            apAlByPoints.setDemodEnabled(row.getBoolean("demod_enabled"));
            apAlByPoints.setDemodHighPass(row.getFloat("demod_high_pass"));
            apAlByPoints.setDemodLowPass(row.getFloat("demod_low_pass"));
            apAlByPoints.setDwell(row.getInt("dwell"));
            apAlByPoints.setfMax(row.getInt("fmax"));
            apAlByPoints.setFreqUnits(row.getString("freq_units"));
            apAlByPoints.setCustomized(row.getBoolean("is_customized"));
            apAlByPoints.setHighAlert(row.getFloat("high_alert"));
            apAlByPoints.setHighAlertActive(row.getBoolean("high_alert_active"));
            apAlByPoints.setHighFault(row.getFloat("high_fault"));
            apAlByPoints.setHighFaultActive(row.getBoolean("high_fault_active"));
            apAlByPoints.setLowAlert(row.getFloat("low_alert"));
            apAlByPoints.setLowAlertActive(row.getBoolean("low_alert_active"));
            apAlByPoints.setLowFault(row.getFloat("low_fault"));
            apAlByPoints.setLowFaultActive(row.getBoolean("low_fault_active"));
            apAlByPoints.setMaxFreq(row.getFloat("max_freq"));
            apAlByPoints.setMinFreq(row.getFloat("min_freq"));
            apAlByPoints.setParamAmpFactor(row.getString("param_amp_factor"));
            apAlByPoints.setParamType(row.getString("param_type"));
            apAlByPoints.setParamUnits(row.getString("param_units"));
            apAlByPoints.setPeriod(row.getFloat("period"));
            apAlByPoints.setResolution(row.getInt("resolution"));
            apAlByPoints.setSampleRate(row.getFloat("sample_rate"));
            apAlByPoints.setSensorType(row.getString("sensor_type"));
            apAlByPointList.add(apAlByPoints);
        }
        return apAlByPointList;
    }

    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, and alSetId from ap_al_by_point table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return PagingIterable of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ApAlByPoint> queryByCustomerSiteAreaAssetLocationPointApSetAlSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException;
    
    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, and alSetId from ap_al_by_point table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ApAlByPoint> findByCustomerSiteAreaAssetLocationPointApSetAlSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId is required.");
        }
        if (alSetId == null) {
            throw new IllegalArgumentException("Argument alSetId is required.");
        }
        return queryByCustomerSiteAreaAssetLocationPointApSetAlSet(customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId).all();
    }
    
    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, and paramName from ap_al_by_point table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param apSetId, UUID Object
     * @param pointId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return PagingIterable of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ApAlByPoint> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId, String paramName) throws UnavailableException, ReadTimeoutException;
    
    /**
     * Returns ApAlByPoint Objects for the given customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, and paramName from ap_al_by_point table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param apSetId, UUID Object
     * @param pointId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ApAlByPoint> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument PointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId is required.");
        }
        if (alSetId == null) {
            throw new IllegalArgumentException("Argument alSetId is required.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument paramName is required.");
        }
        return queryByPK(customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName).all();
    }
    
    /**
     * Returns List of ApAlByPoint Objects for the given query from ap_al_by_point table
     * @param query, String Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */    
    default public List<ApAlByPoint> query(String query) throws UnavailableException, ReadTimeoutException {
        List<ApAlByPoint> list = new ArrayList<>();
        ResultSet rs = CassandraData.getSession().execute(query);
        for (Row row : rs) {
            ApAlByPoint obj = new ApAlByPoint();
            obj.setHighAlertActive(row.getBoolean("high_alert_active"));
            obj.setLowAlertActive(row.getBoolean("low_alert_active"));
            obj.setLowFaultActive(row.getBoolean("low_fault_active"));
            obj.setHighFaultActive(row.getBoolean("high_fault_active"));
            obj.setPointId(row.getUuid("point_id"));
            obj.setParamName(row.getString("param_name"));
            list.add(obj);
        }
        return list;
    }
    
    /**
     * Truncate the ap_al_by_point table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE ap_al_by_point")
    public void truncateAllApAlByPoint() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the ap_al_by_point table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllApAlByPoint();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new ApAlByPoint(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new ApAlByPoint(), "DELETE"));
            }
            throw e;
        }
    }
        
    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, AreaConfigSchedule Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(ApAlByPoint entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null
                && entity.getAreaId() != null
                && entity.getAssetId() != null
                && entity.getPointLocationId() != null
                && entity.getPointId() != null
                && entity.getApSetId() != null
                && entity.getAlSetId() != null
                && entity.getParamName() != null && !entity.getParamName().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO ap_al_by_point(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",point_location_id");
                    values.append(",").append(entity.getPointLocationId());
                    query.append(",point_id");
                    values.append(",").append(entity.getPointId());
                    query.append(",ap_set_id");
                    values.append(",").append(entity.getApSetId());
                    query.append(",al_set_id");
                    values.append(",").append(entity.getAlSetId());
                    query.append(",param_name");
                    values.append(",'").append(entity.getParamName()).append("'");
                    query.append(",demod_enabled");
                    values.append(",").append(entity.isDemodEnabled());
                    query.append(",demod_high_pass");
                    values.append(",").append(entity.getDemodHighPass());
                    query.append(",demod_low_pass");
                    values.append(",").append(entity.getDemodLowPass());
                    query.append(",dwell");
                    values.append(",").append(entity.getDwell());
                    query.append(",fmax");
                    values.append(",").append(entity.getfMax());
                    query.append(",high_alert");
                    values.append(",").append(entity.getHighAlert());
                    query.append(",high_alert_active");
                    values.append(",").append(entity.isHighAlertActive());
                    query.append(",high_fault");
                    values.append(",").append(entity.getHighFault());
                    query.append(",high_fault_active");
                    values.append(",").append(entity.isHighFaultActive());
                    query.append(",is_customized");
                    values.append(",").append(entity.isCustomized());
                    query.append(",low_alert");
                    values.append(",").append(entity.getLowAlert());
                    query.append(",low_alert_active");
                    values.append(",").append(entity.isLowAlertActive());
                    query.append(",low_fault");
                    values.append(",").append(entity.getLowAlert());
                    query.append(",low_fault_active");
                    values.append(",").append(entity.isLowFaultActive());
                    query.append(",max_freq");
                    values.append(",").append(entity.getMaxFreq());
                    query.append(",min_freq");
                    values.append(",").append(entity.getMinFreq());
                    query.append(",period");
                    values.append(",").append(entity.getPeriod());
                    query.append(",resolution");
                    values.append(",").append(entity.getResolution());
                    query.append(",sample_rate");
                    values.append(",").append(entity.getSampleRate());
                    if (entity.getFreqUnits() != null) {
                        query.append(",freq_units");
                        values.append(",'").append(entity.getFreqUnits()).append("'");
                    }
                    if (entity.getParamAmpFactor() != null) {
                        query.append(",param_amp_factor");
                        values.append(",'").append(entity.getParamAmpFactor()).append("'");
                    }
                    if (entity.getParamType() != null) {
                        query.append(",param_type");
                        values.append(",'").append(entity.getParamType()).append("'");
                    }
                    if (entity.getParamUnits() != null) {
                        query.append(",param_units");
                        values.append(",'").append(entity.getParamUnits()).append("'");
                    }
                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM ap_al_by_point WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND point_id=").append(entity.getPointId());
                    query.append(" AND ap_set_id=").append(entity.getApSetId());
                    query.append(" AND al_set_id=").append(entity.getAlSetId());
                    query.append(" AND param_name='").append(entity.getParamName()).append("';");
                }
                break;
            }
        }
        return query.toString();
    }

}
