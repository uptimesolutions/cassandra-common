/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Delete;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.Subscriptions;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */

@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface SubscriptionsDAO extends BaseDAO<Subscriptions> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(SubscriptionsDAO.class);

    /**
     * Inserts the given subscriptions into the subscriptions table
     * @param entity, Subscriptions Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void create(Subscriptions entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of subscriptions into the subscriptions table
     * @param entities, List Object of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     *
     */
    default public void create(List<Subscriptions> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * Update the given subscriptions into the subscriptions table
     * @param entity, Subscriptions Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void update(Subscriptions entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of subscriptions into the subscriptions table
     * @param entities, List Object of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void update(List<Subscriptions> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * delete the given subscriptions from the subscriptions table
     * @param entity, Subscriptions Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void delete(Subscriptions entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given list of subscriptions from the subscriptions table
     * @param entities, List Object of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default public void delete(List<Subscriptions> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Deletes the given partition from the subscriptions table.
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     */
    @Delete(entityClass = Subscriptions.class)
    void deleteByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, WriteTimeoutException;
     
    /**
     * Gets the subscriptions data by customer account and site id from subscriptions table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Subscriptions> queryByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the subscriptions data by customer account and site id from subscriptions table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<Subscriptions> findByCustomerSiteId(String customerAccount, UUID siteId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer accountis required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        return queryByCustomerSiteId(customerAccount, siteId).all();
    }

    /**
     * Gets the subscriptions data by customer account, site id, and subscriptionType from subscriptions table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param subscriptionType, String Object
     * @return PagingIterable of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Subscriptions> queryByPK(String customerAccount, UUID siteId, String subscriptionType) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the subscriptions data by customer account, site id and subscriptionType from subscriptions table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param subscriptionType, String Object
     * @return List of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<Subscriptions> findByPK(String customerAccount, UUID siteId, String subscriptionType) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer accountis required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (subscriptionType == null || subscriptionType.length() < 1) {
            throw new IllegalArgumentException("Argument subscriptionType is required.");
        }
        return queryByPK(customerAccount, siteId, subscriptionType).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, Subscriptions Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(Subscriptions entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getSubscriptionType() != null && !entity.getSubscriptionType().isEmpty() &&
                entity.getSiteId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO subscriptions(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",subscription_type");
                    values.append(",'").append(entity.getSubscriptionType()).append("'");
                    if (entity.getMqProtocol() != null) {
                        query.append(",mq_protocol");
                        values.append(",'").append(entity.getMqProtocol()).append("'");
                    }
                    if (entity.getMqConnectString() != null) {
                        query.append(",mq_connect_string");
                        values.append(",'").append(entity.getMqConnectString()).append("'");
                    }
                    if (entity.getMqUser() != null) {
                        query.append(",mq_user");
                        values.append(",'").append(entity.getMqUser()).append("'");
                    }
                    if (entity.getMqPwd() != null) {
                        query.append(",mq_pwd");
                        values.append(",'").append(entity.getMqPwd()).append("'");
                    }
                    if (entity.getMqQueueName() != null) {
                        query.append(",mq_queue_name");
                        values.append(",'").append(entity.getMqQueueName()).append("'");
                    }
                    if (entity.getMqClientId() != null) {
                        query.append(",mq_client_id");
                        values.append(",'").append(entity.getMqClientId()).append("'");
                    }
                    query.append(",is_internal");
                    values.append(",").append(entity.isInternal());
                    if (entity.getWebhookUrl() != null) {
                        query.append(",webhook_url");
                        values.append(",'").append(entity.getWebhookUrl()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM subscriptions WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND subscription_type='").append(entity.getSubscriptionType()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

}
