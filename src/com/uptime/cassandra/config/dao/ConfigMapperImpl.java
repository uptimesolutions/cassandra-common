/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author aswani
 */
public class ConfigMapperImpl {

    private static ConfigMapper instance = null;

    public static ConfigMapper getInstance() {
        if (instance == null) {
            new ConfigMapperImpl();
        }
        return instance;
    }

    private ConfigMapperImpl() {
        instance = new ConfigMapperBuilder(CassandraData.getSession()).build();
    }
}
