/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.config.entity.AreaSite;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface AreaSiteDAO extends BaseDAO<AreaSite> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(AreaSiteDAO.class);

    /**
     * Inserts the given areaSite into the area_site table
     * @param entity, AreaSite Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(AreaSite entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given areaSite into the area_site table
     * @param entity, AreaSite Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(AreaSite entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given areaSite into the area_site table
     * @param entity, AreaSite Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(AreaSite entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns AreaSite Objects for the given customer account and site id from area_site table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of AreaSite Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AreaSite> queryByCustomerSiteId(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AreaSite Objects for the given customer account and site id from area_site table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of AreaSite Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AreaSite> findByCustomerSiteId(String customerAccount, UUID siteId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        return queryByCustomerSiteId(customerAccount, siteId).all();
    }
    
    /**
     * Returns AreaSite Objects for the given customer account, site id, and area id from area_site table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return PagingIterable of AreaSite Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AreaSite> queryByPK(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of AreaSite Objects for the given customer account, site id, and area id from area_site table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AreaSite Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AreaSite> findByPK(String customerAccount, UUID siteId, UUID areaId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null or empty.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null or empty.");
        }
        return queryByPK(customerAccount, siteId, areaId).all();
    }
    
    /**
     * Truncate the area_site table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE area_site")
    public void truncateAllAreaSite() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the area_site table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllAreaSite();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new AreaSite(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new AreaSite(), "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, AreaSite Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(AreaSite entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null && 
                entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty() &&
                entity.getSiteId() != null && entity.getAreaId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO area_site(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",climate_controlled");
                    values.append(",").append(entity.isClimateControlled());
                    if (entity.getAreaName() != null) {
                        query.append(",area_name");
                        values.append(",'").append(entity.getAreaName()).append("'");
                    }
                    if (entity.getDescription() != null) {
                        query.append(",description");
                        values.append(",'").append(entity.getDescription()).append("'");
                    }
                    if (entity.getEnvironment() != null) {
                        query.append(",environment");
                        values.append(",'").append(entity.getEnvironment()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM area_site WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

}
