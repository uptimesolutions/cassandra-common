/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
@Entity
@CqlName(value = "site_region_country")
public class SiteRegionCountry {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @ClusteringColumn(0)
    @CqlName(value = "region")
    private String region;

    @ClusteringColumn(1)
    @CqlName(value = "country")
    private String country;

    @ClusteringColumn(2)
    @CqlName(value = "site_id")
    UUID siteId;

    @CqlName(value = "site_name")
    private String siteName;

    @CqlName(value = "physical_address_1")
    private String physicalAddress1;

    @CqlName(value = "physical_address_2")
    private String physicalAddress2;

    @CqlName(value = "physical_city")
    private String physicalCity;

    @CqlName(value = "physical_state_province")
    private String physicalStateProvince;

    @CqlName(value = "physical_postal_code")
    private String physicalPostalCode;

    @CqlName(value = "latitude")
    private String latitude;

    @CqlName(value = "longitude")
    private String longitude;

    @CqlName(value = "timezone")
    private String timezone;

    @CqlName(value = "industry")
    private String industry;

    @CqlName(value = "ship_address_1")
    private String shipAddress1;

    @CqlName(value = "ship_address_2")
    private String shipAddress2;

    @CqlName(value = "ship_city")
    private String shipCity;

    @CqlName(value = "ship_state_province")
    private String shipStateProvince;

    @CqlName(value = "ship_postal_code")
    private String shipPostalCode;

    @CqlName(value = "billing_address_1")
    private String billingAddress1;

    @CqlName(value = "billing_address_2")
    private String billingAddress2;

    @CqlName(value = "billing_city")
    private String billingCity;

    @CqlName(value = "billing_state_province")
    private String billingStateProvince;

    @CqlName(value = "billing_postal_code")
    private String billingPostalCode;

    public SiteRegionCountry() {
    }

    public SiteRegionCountry(SiteRegionCountry entity) {
        customerAccount = entity.getCustomerAccount();
        region = entity.getRegion();
        country = entity.getCountry();
        siteId = entity.getSiteId();
        siteName = entity.getSiteName();
        physicalAddress1 = entity.getPhysicalAddress1();
        physicalAddress2 = entity.getPhysicalAddress2();
        physicalCity = entity.getPhysicalCity();
        physicalStateProvince = entity.getPhysicalStateProvince();
        physicalPostalCode = entity.getPhysicalPostalCode();
        latitude = entity.getLatitude();
        longitude = entity.getLongitude();
        timezone = entity.getTimezone();
        industry = entity.getIndustry();
        shipAddress1 = entity.getShipAddress1();
        shipAddress2 = entity.getShipAddress2();
        shipCity = entity.getShipCity();
        shipStateProvince = entity.getShipStateProvince();
        shipPostalCode = entity.getShipPostalCode();
        billingAddress1 = entity.getBillingAddress1();
        billingAddress2 = entity.getBillingAddress2();
        billingCity = entity.getBillingCity();
        billingStateProvince = entity.getBillingStateProvince();
        billingPostalCode = entity.getBillingPostalCode();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getPhysicalAddress1() {
        return physicalAddress1;
    }

    public void setPhysicalAddress1(String physicalAddress1) {
        this.physicalAddress1 = physicalAddress1;
    }

    public String getPhysicalAddress2() {
        return physicalAddress2;
    }

    public void setPhysicalAddress2(String physicalAddress2) {
        this.physicalAddress2 = physicalAddress2;
    }

    public String getPhysicalCity() {
        return physicalCity;
    }

    public void setPhysicalCity(String physicalCity) {
        this.physicalCity = physicalCity;
    }

    public String getPhysicalStateProvince() {
        return physicalStateProvince;
    }

    public void setPhysicalStateProvince(String physicalStateProvince) {
        this.physicalStateProvince = physicalStateProvince;
    }

    public String getPhysicalPostalCode() {
        return physicalPostalCode;
    }

    public void setPhysicalPostalCode(String physicalPostalCode) {
        this.physicalPostalCode = physicalPostalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getShipAddress1() {
        return shipAddress1;
    }

    public void setShipAddress1(String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    public String getShipAddress2() {
        return shipAddress2;
    }

    public void setShipAddress2(String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipStateProvince() {
        return shipStateProvince;
    }

    public void setShipStateProvince(String shipStateProvince) {
        this.shipStateProvince = shipStateProvince;
    }

    public String getShipPostalCode() {
        return shipPostalCode;
    }

    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingStateProvince() {
        return billingStateProvince;
    }

    public void setBillingStateProvince(String billingStateProvince) {
        this.billingStateProvince = billingStateProvince;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.region);
        hash = 79 * hash + Objects.hashCode(this.country);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteRegionCountry other = (SiteRegionCountry) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.region, other.region)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.physicalAddress1, other.physicalAddress1)) {
            return false;
        }
        if (!Objects.equals(this.physicalAddress2, other.physicalAddress2)) {
            return false;
        }
        if (!Objects.equals(this.physicalCity, other.physicalCity)) {
            return false;
        }
        if (!Objects.equals(this.physicalStateProvince, other.physicalStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.physicalPostalCode, other.physicalPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.timezone, other.timezone)) {
            return false;
        }
        if (!Objects.equals(this.industry, other.industry)) {
            return false;
        }
        if (!Objects.equals(this.shipAddress1, other.shipAddress1)) {
            return false;
        }
        if (!Objects.equals(this.shipAddress2, other.shipAddress2)) {
            return false;
        }
        if (!Objects.equals(this.shipCity, other.shipCity)) {
            return false;
        }
        if (!Objects.equals(this.shipStateProvince, other.shipStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.shipPostalCode, other.shipPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.billingAddress1, other.billingAddress1)) {
            return false;
        }
        if (!Objects.equals(this.billingAddress2, other.billingAddress2)) {
            return false;
        }
        if (!Objects.equals(this.billingCity, other.billingCity)) {
            return false;
        }
        if (!Objects.equals(this.billingStateProvince, other.billingStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.billingPostalCode, other.billingPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteRegionCountry{" + "customerAccount=" + customerAccount + ", region=" + region + ", country=" + country + ", siteId=" + siteId + ", siteName=" + siteName + ", physicalAddress1=" + physicalAddress1 + ", physicalAddress2=" + physicalAddress2 + ", physicalCity=" + physicalCity + ", physicalStateProvince=" + physicalStateProvince + ", physicalPostalCode=" + physicalPostalCode + ", latitude=" + latitude + ", longitude=" + longitude + ", timezone=" + timezone + ", industry=" + industry + ", shipAddress1=" + shipAddress1 + ", shipAddress2=" + shipAddress2 + ", shipCity=" + shipCity + ", shipStateProvince=" + shipStateProvince + ", shipPostalCode=" + shipPostalCode + ", billingAddress1=" + billingAddress1 + ", billingAddress2=" + billingAddress2 + ", billingCity=" + billingCity + ", billingStateProvince=" + billingStateProvince + ", billingPostalCode=" + billingPostalCode + '}';
    }
}
