/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */

@CqlName(value = "point_location_hwunit_history")
@Entity
public class PointLocationHwUnitHistory {
    
    @PartitionKey(0)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;

    @ClusteringColumn(0)
    @CqlName(value = "device_id")
    private String deviceId;

    @ClusteringColumn(1)
    @CqlName(value = "date_from")
    private Instant dateFrom;
    
    @CqlName(value = "customer_acct")
    private String customerAccount;

    public PointLocationHwUnitHistory() {
    }

    public PointLocationHwUnitHistory(PointLocationHwUnitHistory entity) {
        pointLocationId = entity.getPointLocationId();
        deviceId = entity.getDeviceId();
        dateFrom = entity.getDateFrom();
        customerAccount = entity.getCustomerAccount();
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Instant getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.pointLocationId);
        hash = 79 * hash + Objects.hashCode(this.deviceId);
        hash = 79 * hash + Objects.hashCode(this.dateFrom);
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointLocationHwUnitHistory other = (PointLocationHwUnitHistory) obj;
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        return Objects.equals(this.dateFrom, other.dateFrom);
    }

    @Override
    public String toString() {
        return "PointLocationHwUnitHistory{" + "pointLocationId=" + pointLocationId + ", deviceId=" + deviceId + ", dateFrom=" + dateFrom + ", customerAccount=" + customerAccount + '}';
    }

}
