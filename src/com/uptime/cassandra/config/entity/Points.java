/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */

@CqlName(value = "points")
@Entity
public class Points {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "area_id")
    UUID areaId;
        
    @PartitionKey(3)
    @CqlName(value = "asset_id")
    UUID assetId;
    
    @ClusteringColumn(0)
    @CqlName(value = "point_location_id")
    UUID pointLocationId;
    
    @ClusteringColumn(1)
    @CqlName(value = "point_id")
    UUID pointId;
    
    @ClusteringColumn(2)
    @CqlName(value = "ap_set_id")
    UUID apSetId;
    
    @CqlName(value = "al_set_id")
    UUID alSetId;
    
    @CqlName(value = "auto_acknowledge")
    boolean autoAcknowledge;
    
    @CqlName(value = "is_disabled")
    boolean disabled;
    
    @CqlName(value = "point_name")
    String pointName;
    
    @CqlName(value = "point_type")
    String pointType;

    @CqlName(value = "sensor_channel_num")
    int sensorChannelNum;
    
    @CqlName(value = "sensor_offset")
    float sensorOffset;

    @CqlName(value = "sensor_sensitivity")
    float sensorSensitivity;

    @CqlName(value = "sensor_type")
    String sensorType;
    
    @CqlName(value = "sensor_units")
    String sensorUnits;

    @CqlName(value = "alarm_enabled")
    private boolean alarmEnabled;
    
    @CqlName(value = "sensor_sub_type")
    String sensorSubType;
    
    @CqlName(value = "sensor_local_orientation")
    String sensorLocalOrientation;
    
    public Points() {
    }

    public Points(Points entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        pointLocationId = entity.getPointLocationId();
        pointId = entity.getPointId();
        apSetId = entity.getApSetId();
        alSetId = entity.getAlSetId();
        autoAcknowledge = entity.isAutoAcknowledge();
        disabled = entity.isDisabled();
        pointName = entity.getPointName();
        pointType = entity.getPointType();
        sensorChannelNum = entity.getSensorChannelNum();
        sensorOffset = entity.getSensorOffset();
        sensorSensitivity = entity.getSensorSensitivity();
        sensorType = entity.getSensorType();
        sensorUnits = entity.getSensorUnits();
        sensorSubType=entity.getSensorSubType();
        sensorLocalOrientation=entity.getSensorLocalOrientation();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public boolean isAutoAcknowledge() {
        return autoAcknowledge;
    }

    public void setAutoAcknowledge(boolean autoAcknowledge) {
        this.autoAcknowledge = autoAcknowledge;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.customerAccount);
        hash = 61 * hash + Objects.hashCode(this.siteId);
        hash = 61 * hash + Objects.hashCode(this.areaId);
        hash = 61 * hash + Objects.hashCode(this.assetId);
        hash = 61 * hash + Objects.hashCode(this.pointLocationId);
        hash = 61 * hash + Objects.hashCode(this.pointId);
        hash = 61 * hash + Objects.hashCode(this.apSetId);
        hash = 61 * hash + Objects.hashCode(this.alSetId);
        hash = 61 * hash + (this.autoAcknowledge ? 1 : 0);
        hash = 61 * hash + (this.disabled ? 1 : 0);
        hash = 61 * hash + Objects.hashCode(this.pointName);
        hash = 61 * hash + Objects.hashCode(this.pointType);
        hash = 61 * hash + this.sensorChannelNum;
        hash = 61 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 61 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 61 * hash + Objects.hashCode(this.sensorType);
        hash = 61 * hash + Objects.hashCode(this.sensorUnits);
        hash = 61 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 61 * hash + Objects.hashCode(this.sensorSubType);
        hash = 61 * hash + Objects.hashCode(this.sensorLocalOrientation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Points other = (Points) obj;
        if (this.autoAcknowledge != other.autoAcknowledge) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorSubType, other.sensorSubType)) {
            return false;
        }
        if (!Objects.equals(this.sensorLocalOrientation, other.sensorLocalOrientation)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return Objects.equals(this.alSetId, other.alSetId);
    }

    @Override
    public String toString() {
        return "Points{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", autoAcknowledge=" + autoAcknowledge + ", disabled=" + disabled + ", pointName=" + pointName + ", pointType=" + pointType + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", alarmEnabled=" + alarmEnabled + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + '}';
    }

    

}
