/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
@Entity
@CqlName(value = "area_config_schedule")
public class AreaConfigSchedule {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "area_id")
    UUID areaId;

    @ClusteringColumn(0)
    @CqlName(value = "schedule_id")
    UUID scheduleId;

    @ClusteringColumn(1)
    @CqlName(value = "day_of_week")
    byte dayOfWeek;

    @CqlName(value = "area_name")
    String areaName;
    
    @CqlName(value = "schedule_name")
    String scheduleName;

    @CqlName(value = "description")
    String description;

    @CqlName(value = "is_continuous")
    boolean continuous;
    
    @CqlName(value = "hours_of_day")
    String hoursOfDay;

    public AreaConfigSchedule() {
    }

    public AreaConfigSchedule(AreaConfigSchedule entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        scheduleId = entity.getScheduleId();
        areaName = entity.getAreaName();
        scheduleName = entity.getScheduleName();
        dayOfWeek = entity.getDayOfWeek();
        description = entity.getDescription();
        continuous = entity.isContinuous();
        hoursOfDay = entity.getHoursOfDay();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(UUID scheduleId) {
        this.scheduleId = scheduleId;
    }

    public byte getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(byte dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public String getHoursOfDay() {
        return hoursOfDay;
    }

    public void setHoursOfDay(String hoursOfDay) {
        this.hoursOfDay = hoursOfDay;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.areaId);
        hash = 97 * hash + Objects.hashCode(this.scheduleId);
        hash = 97 * hash + this.dayOfWeek;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AreaConfigSchedule other = (AreaConfigSchedule) obj;
        if (this.dayOfWeek != other.dayOfWeek) {
            return false;
        }
        if (this.continuous != other.continuous) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.scheduleName, other.scheduleName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.hoursOfDay, other.hoursOfDay)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.scheduleId, other.scheduleId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AreaConfigSchedule{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", scheduleId=" + scheduleId + ", dayOfWeek=" + dayOfWeek + ", areaName=" + areaName + ", scheduleName=" + scheduleName + ", description=" + description + ", continuous=" + continuous + ", hoursOfDay=" + hoursOfDay + '}';
    }
}
