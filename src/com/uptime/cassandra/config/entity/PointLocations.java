/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */

@CqlName(value = "point_locations")
@Entity
public class PointLocations {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "area_id")
    private UUID areaId;
        
    @ClusteringColumn(0)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @ClusteringColumn(1)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @CqlName(value = "description")
    private String description;

    @CqlName(value = "device_serial_number")
    private String deviceSerialNumber;

    @CqlName(value = "ff_set_ids")
    private String ffSetIds;
    
    @CqlName(value = "point_location_name")
    private String pointLocationName;
    
    @CqlName(value = "sample_interval")
    private short sampleInterval;
    
    @CqlName(value = "basestation_port_num")
    private short basestationPortNum;
    
    @CqlName(value = "speed_ratio")
    private float speedRatio;
     
    @CqlName(value = "tach_id")
    private UUID tachId;
    
    @CqlName(value = "alarm_enabled")
    private boolean alarmEnabled;
    
    @CqlName(value = "roll_diameter")
    private float rollDiameter;
    
    @CqlName(value = "roll_diameter_units")
    private String rollDiameterUnits;

    public PointLocations() {
    }

    public PointLocations(PointLocations entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        pointLocationId = entity.getPointLocationId();
        description = entity.getDescription();
        deviceSerialNumber = entity.getDeviceSerialNumber();
        ffSetIds = entity.getFfSetIds();
        pointLocationName = entity.getPointLocationName();
        sampleInterval = entity.getSampleInterval();
        basestationPortNum = entity.getBasestationPortNum();
        speedRatio = entity.getSpeedRatio();
        tachId = entity.getTachId();
        alarmEnabled = entity.isAlarmEnabled();
        rollDiameter = entity.getRollDiameter();
        rollDiameterUnits = entity.getRollDiameterUnits();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.areaId);
        hash = 29 * hash + Objects.hashCode(this.assetId);
        hash = 29 * hash + Objects.hashCode(this.pointLocationId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointLocations other = (PointLocations) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.deviceSerialNumber, other.deviceSerialNumber)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PointLocations{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", description=" + description + ", deviceSerialNumber=" + deviceSerialNumber + ", ffSetIds=" + ffSetIds + ", pointLocationName=" + pointLocationName + ", sampleInterval=" + sampleInterval + ", basestationPortNum=" + basestationPortNum + ", speedRatio=" + speedRatio + ", tachId=" + tachId + ", alarmEnabled=" + alarmEnabled + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + '}';
    }
}
