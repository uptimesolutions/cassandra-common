/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
@Entity
@CqlName(value = "site_contacts")
public class SiteContacts {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @ClusteringColumn(0)
    @CqlName(value = "site_id")
    UUID siteId;

    @ClusteringColumn(1)
    @CqlName(value = "role")
    private String role;
    
    @CqlName(value = "site_name")
    private String siteName;
    
    @CqlName(value = "contact_name")
    private String contactName;
    
    @CqlName(value = "contact_title")
    private String contactTitle;
    
    @CqlName(value = "contact_phone")
    private String contactPhone;
    
    @CqlName(value = "contact_email")
    private String contactEmail;

    public SiteContacts() {
    }

    public SiteContacts(SiteContacts entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        role = entity.getRole();
        siteName = entity.getSiteName();
        contactName = entity.getContactName();
        contactTitle = entity.getContactTitle();
        contactPhone = entity.getContactPhone();
        contactEmail = entity.getContactEmail();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.customerAccount);
        hash = 83 * hash + Objects.hashCode(this.siteId);
        hash = 83 * hash + Objects.hashCode(this.role);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteContacts other = (SiteContacts) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.role, other.role)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.contactName, other.contactName)) {
            return false;
        }
        if (!Objects.equals(this.contactTitle, other.contactTitle)) {
            return false;
        }
        if (!Objects.equals(this.contactPhone, other.contactPhone)) {
            return false;
        }
        if (!Objects.equals(this.contactEmail, other.contactEmail)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteContacts{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", role=" + role + ", siteName=" + siteName + ", contactName=" + contactName + ", contactTitle=" + contactTitle + ", contactPhone=" + contactPhone + ", contactEmail=" + contactEmail + '}';
    }
}
