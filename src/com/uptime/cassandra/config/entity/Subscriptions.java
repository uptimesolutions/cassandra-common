/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "subscriptions")
public class Subscriptions {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "subscription_type")
    private String subscriptionType;
    
    @CqlName(value = "mq_protocol")
    private String mqProtocol;
    
    @CqlName(value = "mq_connect_string")
    private String mqConnectString;
    
    @CqlName(value = "mq_user")
    private String mqUser;
    
    @CqlName(value = "mq_pwd")
    private String mqPwd;
    
    @CqlName(value = "mq_queue_name")
    private String mqQueueName;
    
    @CqlName(value = "mq_client_id")
    private String mqClientId;
    
    @CqlName(value = "is_internal")
    private boolean internal;
    
    @CqlName(value = "webhook_url")
    private String webhookUrl;

    public Subscriptions() {
    }

    public Subscriptions(Subscriptions entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        subscriptionType = entity.getSubscriptionType();
        mqProtocol = entity.getMqProtocol();
        mqConnectString = entity.getMqConnectString();
        mqUser = entity.getMqUser();
        mqPwd = entity.getMqPwd();
        mqQueueName = entity.getMqQueueName();
        mqClientId = entity.getMqClientId();
        internal = entity.isInternal();
        webhookUrl = entity.getWebhookUrl();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getMqProtocol() {
        return mqProtocol;
    }

    public void setMqProtocol(String mqProtocol) {
        this.mqProtocol = mqProtocol;
    }

    public String getMqConnectString() {
        return mqConnectString;
    }

    public void setMqConnectString(String mqConnectString) {
        this.mqConnectString = mqConnectString;
    }

    public String getMqUser() {
        return mqUser;
    }

    public void setMqUser(String mqUser) {
        this.mqUser = mqUser;
    }

    public String getMqPwd() {
        return mqPwd;
    }

    public void setMqPwd(String mqPwd) {
        this.mqPwd = mqPwd;
    }

    public String getMqQueueName() {
        return mqQueueName;
    }

    public void setMqQueueName(String mqQueueName) {
        this.mqQueueName = mqQueueName;
    }

    public String getMqClientId() {
        return mqClientId;
    }

    public void setMqClientId(String mqCientId) {
        this.mqClientId = mqCientId;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public String getWebhookUrl() {
        return webhookUrl;
    }

    public void setWebhookUrl(String webhookUrl) {
        this.webhookUrl = webhookUrl;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.customerAccount);
        hash = 67 * hash + Objects.hashCode(this.siteId);
        hash = 67 * hash + Objects.hashCode(this.subscriptionType);
        hash = 67 * hash + Objects.hashCode(this.mqProtocol);
        hash = 67 * hash + Objects.hashCode(this.mqConnectString);
        hash = 67 * hash + Objects.hashCode(this.mqUser);
        hash = 67 * hash + Objects.hashCode(this.mqPwd);
        hash = 67 * hash + Objects.hashCode(this.mqQueueName);
        hash = 67 * hash + Objects.hashCode(this.mqClientId);
        hash = 67 * hash + (this.internal ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.webhookUrl);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Subscriptions other = (Subscriptions) obj;
        if (this.internal != other.internal) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.subscriptionType, other.subscriptionType)) {
            return false;
        }
        if (!Objects.equals(this.mqProtocol, other.mqProtocol)) {
            return false;
        }
        if (!Objects.equals(this.mqConnectString, other.mqConnectString)) {
            return false;
        }
        if (!Objects.equals(this.mqUser, other.mqUser)) {
            return false;
        }
        if (!Objects.equals(this.mqPwd, other.mqPwd)) {
            return false;
        }
        if (!Objects.equals(this.mqQueueName, other.mqQueueName)) {
            return false;
        }
        if (!Objects.equals(this.mqClientId, other.mqClientId)) {
            return false;
        }
        if (!Objects.equals(this.webhookUrl, other.webhookUrl)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

    @Override
    public String toString() {
        return "Subscriptions{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", subscriptionType=" + subscriptionType + ", mqProtocol=" + mqProtocol + ", mqConnectString=" + mqConnectString + ", mqUser=" + mqUser + ", mqPwd=" + mqPwd + ", mqQueueName=" + mqQueueName + ", mqClientId=" + mqClientId + ", internal=" + internal + ", webhookUrl=" + webhookUrl + '}';
    }

}
