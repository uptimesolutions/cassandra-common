/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mohd Juned Alam
 */
@Entity
@CqlName(value = "ap_al_by_point")
public class ApAlByPoint {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "area_id")
    private UUID areaId;
        
    @PartitionKey(3)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @PartitionKey(4)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @PartitionKey(5)
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @PartitionKey(6)
    @CqlName(value = "ap_set_id")
    private UUID apSetId;
    
    @ClusteringColumn(0)
    @CqlName(value = "al_set_id")
    private UUID alSetId;
    
    @ClusteringColumn(1)
    @CqlName(value = "param_name")
    private String paramName;
    
    @CqlName(value = "demod_enabled")
    private boolean demodEnabled;

    @CqlName(value = "demod_high_pass")
    private float demodHighPass;

    @CqlName(value = "demod_low_pass")
    private float demodLowPass;
    
    @CqlName(value = "dwell")
    private int dwell;
     
    @CqlName(value = "fmax")
    private int fMax;

    @CqlName(value = "freq_units")
    private String freqUnits;
    
    @CqlName(value = "high_alert")
    private float highAlert;
    
    @CqlName(value = "high_alert_active")
    private boolean highAlertActive;
    
    @CqlName(value = "high_fault")
    private float highFault;

    @CqlName(value = "high_fault_active")
    private boolean highFaultActive;
    
    @CqlName(value = "is_customized")
    private boolean customized;
    
    @CqlName(value = "low_alert")
    private float lowAlert;
    
    @CqlName(value = "low_alert_active")
    private boolean lowAlertActive;
    
    @CqlName(value = "low_fault")
    private float lowFault;
    
    @CqlName(value = "low_fault_active")
    private boolean lowFaultActive;
    
    @CqlName(value = "max_freq")
    private float maxFreq;
    
    @CqlName(value = "min_freq")
    private float minFreq;
    
    @CqlName(value = "param_amp_factor")
    private String paramAmpFactor;
    
    @CqlName(value = "param_type")
    private String paramType;

    @CqlName(value = "param_units")
    private String paramUnits;

    @CqlName(value = "period")
    private float period;

    @CqlName(value = "resolution")
    private int resolution;
    
    @CqlName(value = "sample_rate")
    private float sampleRate;
    
    @CqlName(value = "sensor_type")
    private String sensorType;

    public ApAlByPoint() {

    }

    public ApAlByPoint(ApAlByPoint entity) {
        alSetId = entity.getAlSetId();
        apSetId = entity.getApSetId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        customerAccount = entity.getCustomerAccount();
        demodHighPass = entity.getDemodHighPass();
        demodLowPass = entity.getDemodLowPass();
        freqUnits = entity.getFreqUnits();
        highAlert = entity.getHighAlert();
        highFault = entity.getHighFault();
        lowAlert = entity.getLowAlert();
        lowFault = entity.getLowFault();
        maxFreq = entity.getMaxFreq();
        minFreq = entity.getMinFreq();
        paramAmpFactor = entity.getParamAmpFactor();
        paramName = entity.getParamName();
        paramType = entity.getParamType();
        paramUnits = entity.getParamUnits();
        period = entity.getPeriod();
        pointLocationId = entity.getPointLocationId();
        pointId = entity.getPointId();
        resolution = entity.getResolution();
        sampleRate = entity.getSampleRate();
        sensorType = entity.getSensorType();
        siteId = entity.getSiteId();
        dwell = entity.getDwell();
        fMax = entity.getfMax();
        lowFaultActive = entity.isLowFaultActive();
        highFaultActive = entity.isHighFaultActive();
        highAlertActive = entity.isHighAlertActive();
        lowAlertActive = entity.isLowAlertActive();
        demodEnabled = entity.isDemodEnabled();
        customized = entity.isCustomized();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public boolean isDemodEnabled() {
        return demodEnabled;
    }

    public void setDemodEnabled(boolean demodEnabled) {
        this.demodEnabled = demodEnabled;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getfMax() {
        return fMax;
    }

    public void setfMax(int fMax) {
        this.fMax = fMax;
    }

    public String getFreqUnits() {
        return freqUnits;
    }

    public void setFreqUnits(String freqUnits) {
        this.freqUnits = freqUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFreq() {
        return maxFreq;
    }

    public void setMaxFreq(float maxFreq) {
        this.maxFreq = maxFreq;
    }

    public float getMinFreq() {
        return minFreq;
    }

    public void setMinFreq(float minFreq) {
        this.minFreq = minFreq;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.areaId);
        hash = 29 * hash + Objects.hashCode(this.assetId);
        hash = 29 * hash + Objects.hashCode(this.pointLocationId);
        hash = 29 * hash + Objects.hashCode(this.pointId);
        hash = 29 * hash + Objects.hashCode(this.apSetId);
        hash = 29 * hash + Objects.hashCode(this.alSetId);
        hash = 29 * hash + Objects.hashCode(this.paramName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApAlByPoint other = (ApAlByPoint) obj;
        if (this.demodEnabled != other.demodEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fMax != other.fMax) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (this.customized != other.customized) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFreq) != Float.floatToIntBits(other.maxFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFreq) != Float.floatToIntBits(other.minFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.freqUnits, other.freqUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApAlByPoint{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", paramName=" + paramName + ", demodEnabled=" + demodEnabled + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fMax=" + fMax + ", freqUnits=" + freqUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", customized=" + customized + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFreq=" + maxFreq + ", minFreq=" + minFreq + ", paramAmpFactor=" + paramAmpFactor + ", paramType=" + paramType + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", sensorType=" + sensorType + '}';
    }
}
