/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */

@CqlName(value = "point_to_hwunit")
@Entity
public class PointToHwUnit {
    
    @PartitionKey
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @CqlName(value = "channel_type")
    private String channelType;

    @CqlName(value = "channel_num")
    private byte channelNum;
        
    @CqlName(value = "device_id")
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.pointId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointToHwUnit other = (PointToHwUnit) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HwUnitToPoint{" + "deviceId=" + deviceId + ", channelType=" + channelType + ", channelNum=" + channelNum + ", pointId=" + pointId + '}';
    }
    
}
