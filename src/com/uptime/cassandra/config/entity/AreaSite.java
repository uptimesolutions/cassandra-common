/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
@CqlName(value = "area_site")
@Entity
public class AreaSite {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    UUID siteId;

    @ClusteringColumn
    @CqlName(value = "area_id")
    UUID areaId;

    @CqlName(value = "area_name")
    String areaName;

    @CqlName(value = "description")
    String description;

    @CqlName(value = "environment")
    String environment;

    @CqlName(value = "climate_controlled")
    boolean climateControlled;

    public AreaSite() {
    }

    public AreaSite(AreaSite entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        areaName = entity.getAreaName();
        description = entity.getDescription();
        environment = entity.getEnvironment();
        climateControlled = entity.isClimateControlled();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public boolean isClimateControlled() {
        return climateControlled;
    }

    public void setClimateControlled(boolean climateControlled) {
        this.climateControlled = climateControlled;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.customerAccount);
        hash = 11 * hash + Objects.hashCode(this.siteId);
        hash = 11 * hash + Objects.hashCode(this.areaId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AreaSite other = (AreaSite) obj;
        if (this.climateControlled != other.climateControlled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.environment, other.environment)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AreaSite{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", areaName=" + areaName + ", description=" + description + ", environment=" + environment + ", climateControlled=" + climateControlled + '}';
    }
}
