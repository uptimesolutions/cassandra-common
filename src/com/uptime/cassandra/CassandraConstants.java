/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra;

import java.io.IOException;

/**
 *
 * @author ksimmons
 */
public class CassandraConstants {

    // public static Logger CQL_LOG = null;
    public static String KEYSPACE = PropertiesSingletonBean.getInstance().getProperty("cassandra.KEYSPACE");
    public static String CLUSTER_IPS[] = PropertiesSingletonBean.getInstance().getProperty("cassandra.CLUSTER_IPS").split(",");
    public static String PORT= PropertiesSingletonBean.getInstance().getProperty("cassandra.PORT").trim();
    public static String LOAD_BALANCING_POLICY_CLASS = PropertiesSingletonBean.getInstance().getProperty("cassandra.LOAD_BALANCING_POLICY_CLASS");
    public static String LOAD_BALANCING_LOCAL_DATACENTER = PropertiesSingletonBean.getInstance().getProperty("cassandra.LOAD_BALANCING_LOCAL_DATACENTER");

    static {
        if ("_LOCAL".trim().equalsIgnoreCase(PropertiesSingletonBean.getInstance().getEnv())) {
            try {
               // CQL_LOG = Logger.getLogger("CQL Logger");
                //CQL_LOG.setUseParentHandlers(false);
                //FileHandler cqlFile = new FileHandler("./cqllog.%u.txt");
                //cqlFile.setFormatter(new SimpleFormatter());
                //CQL_LOG.addHandler(cqlFile);
            } catch (SecurityException securityException) {
                
            }
        }
    }
}
