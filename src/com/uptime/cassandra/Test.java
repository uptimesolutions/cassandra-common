/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.uuid.Uuids;
import java.net.InetSocketAddress;
import java.time.Instant;

/**
 *
 * @author ksimmons
 */
public class Test {
    public static void main(String[] args){
       
//        MaintenanceArea ma = new MaintenanceArea();
//        ma.setCustomerAccount("CUST1");
//        ma.setSiteName("Site 1");
//        ma.setAreaName("Area 1");
//        ma.setCreatedYear((byte)2021);
//        ma.setMaintenanceId(Uuids.random());
//        ma.setTrendId(Uuids.random());
//        ma.setAssetName("My Aseet 1");
//        ma.setUserName("Aswani Kumar");
//        ma.setPriority((byte)1);
//        ma.setIssue("General issue");
//        ma.setAction("Replace part");
//        ma.setDescription("Repaire in the module");
//        ma.setProgressNotes("Under progress");
//        ma.setCreatedDate(Instant.now());
//        ma.setHardwareSerialNumber("hwSerialNumber 1");
//        ma.setResolved(false);
//        System.out.println("Stm " + printCQL(ma, "INSERT"));
//        System.out.println("Stm " + printCQL(ma, "DELETE"));
//        
//        CqlSession session = null;
//        try{
//            session = CqlSession.builder()
//                .addContactPoint(new InetSocketAddress("18.118.141.224", 9042))
//                .withLocalDatacenter("datacenter1")
//                .withKeyspace("worldview_dev1")
//                .build();
//            
//            ResultSet rs = session.execute("SELECT application,toTimestamp(created_date) as created_date,ip_address,port,data FROM worldview_dev1.events");
//            for (Row row : rs) {
//                System.out.println(row.getString("application"));
//                System.out.println(row.getInstant("created_date"));
//                System.out.println(row.getString("ip_address"));
//                System.out.println(row.getString("port"));
//                System.out.println(row.getString("data"));
//            }
//        }
//        catch(Exception e){
//            e.printStackTrace();
//        }
//        finally{
//            try{
//                if(session != null)
//                    session.close();
//            }
//            catch(Exception e){
//                e.printStackTrace();
//            }
//        }
    }
   /*
    static String printCQL(MaintenanceArea area, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO maintenance_area(customer_acct,site_name,area_name,created_year");
                values.append(" ('").append(area.getCustomerAccount()).append("',");
                values.append("'").append(area.getSiteName()).append("',");
                values.append("'").append(area.getAreaName()).append("',");
                values.append("").append(area.getCreatedYear()).append("");
                if (area.getMaintenanceId() != null) {
                    query.append(", maintenance_id");
                    values.append(",").append(area.getMaintenanceId()).append("");
                }
                if (area.getTrendId()!= null) {
                    query.append(", trend_id");
                    values.append(", ").append(area.getMaintenanceId()).append("");
                }
                if (area.getAssetName()!= null) {
                    query.append(", asset_name");
                    values.append(", '").append(area.getAssetName()).append("'");
                }
                if (area.getUserName()!= null) {
                    query.append(", username");
                    values.append(", '").append(area.getUserName()).append("'");
                }
                
                query.append(", priority");
                values.append(", ").append(area.getPriority()).append("");

                if (area.getIssue()!= null) {
                    query.append(", issue");
                    values.append(", '").append(area.getIssue()).append("'");
                }
                if (area.getAction()!= null) {
                    query.append(", action");
                    values.append(", '").append(area.getAction()).append("'");
                }
                if (area.getDescription()!= null) {
                    query.append(", description");
                    values.append(", '").append(area.getDescription()).append("'");
                }
                if (area.getProgressNotes()!= null) {
                    query.append(", progress_notes");
                    values.append(", '").append(area.getProgressNotes()).append("'");
                }
                if (area.getCreatedDate()!= null) {
                    query.append(", created_date");
                    values.append(", '").append(area.getCreatedDate()).append("'");
                }
                if (area.getHardwareSerialNumber()!= null) {
                    query.append(", hw_serial_num");
                    values.append(", '").append(area.getHardwareSerialNumber()).append("'");
                }
                query.append(", is_resolved");
                values.append(", ").append(area.isResolved()).append("");

                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM maintenance_area WHERE customer_acct='").append(area.getCustomerAccount()).append("'");
                query.append(" AND site_name='").append(area.getSiteName()).append("'");
                query.append(" AND area_name='").append(area.getAreaName()).append("'");
                query.append(" AND created_year=").append(area.getAreaName()).append(";");
            }
            break;
        }

        return query.toString();
    }*/
}
