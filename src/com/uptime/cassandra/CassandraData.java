/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra;

import com.datastax.oss.driver.api.core.ConsistencyLevel;
import com.datastax.oss.driver.api.core.CqlIdentifier;
import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.CqlSessionBuilder;
import com.datastax.oss.driver.api.core.config.DefaultDriverOption;
import com.datastax.oss.driver.api.core.config.DriverConfigLoader;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author ksimmons
 */
public class CassandraData {

    // A static variable that holds the session.  Only one of these will exist for the whole application
    private static CqlSession cassandraSession = null;
    public static final String DEFAULT_ROOT_PATH = "datastax-java-driver";

    private static String userName;
    private static String password;
    
    public CassandraData() {
    }

    /**
     * Return the Cassandra session. When the application starts up, the session
     * is set to null. When this function is called, it checks to see if the
     * session is null. If so, it creates a new session, and sets the static
     * session.
     *
     * All of the DAO classes are subclasses of this
     *
     * @return a valid cassandra session
     */
    public static CqlSession getSession() {

        if (cassandraSession == null) {
            cassandraSession = createSession();
        }
        return cassandraSession;

    }

    /**
     * Create a new cassandra Cluster() and Session(). Returns the Session.
     *
     * @return
     */
    protected static CqlSession createSession() {
        CqlSessionBuilder cqlSessionBuilder;
        DriverConfigLoader driverConfigLoader;

        if (cassandraSession == null) {
            driverConfigLoader = DriverConfigLoader.programmaticBuilder()
                    .withDuration(DefaultDriverOption.HEARTBEAT_INTERVAL, Duration.ofSeconds(60))
                    .withString(DefaultDriverOption.REQUEST_CONSISTENCY, ConsistencyLevel.LOCAL_QUORUM.name())
                    .withString(DefaultDriverOption.RETRY_POLICY_CLASS, "DefaultRetryPolicy")
                    .withString(DefaultDriverOption.RECONNECTION_POLICY_CLASS, "ConstantReconnectionPolicy")
                    .withDuration(DefaultDriverOption.RECONNECTION_BASE_DELAY, Duration.ofSeconds(5))
                    .withDuration(DefaultDriverOption.CONNECTION_INIT_QUERY_TIMEOUT, Duration.ofSeconds(30))
                    .withBoolean(DefaultDriverOption.RECONNECT_ON_INIT, true)
                    .withDuration(DefaultDriverOption.REQUEST_TIMEOUT, Duration.ofSeconds(30))
                    .withDuration(DefaultDriverOption.CONTROL_CONNECTION_TIMEOUT, Duration.ofSeconds(20))
                    .withString(DefaultDriverOption.LOAD_BALANCING_POLICY_CLASS, CassandraConstants.LOAD_BALANCING_POLICY_CLASS)
                    .withString(DefaultDriverOption.LOAD_BALANCING_LOCAL_DATACENTER, CassandraConstants.LOAD_BALANCING_LOCAL_DATACENTER)
                    .withString(DefaultDriverOption.SESSION_KEYSPACE, CassandraConstants.KEYSPACE)
                    .build();

            cqlSessionBuilder = CqlSession.builder();
            cqlSessionBuilder.withConfigLoader(driverConfigLoader);
            int port = Integer.parseInt(CassandraConstants.PORT.trim());
            System.out.println("Port - " + port);

            for (String ip : CassandraConstants.CLUSTER_IPS) {
                cqlSessionBuilder.addContactPoint(new InetSocketAddress(ip.trim(), port));
                System.out.println(ip);
            }

            loadCredentials();
            cqlSessionBuilder.withAuthCredentials(userName, password);
            cassandraSession = cqlSessionBuilder.build();
        }
        return cassandraSession;
    }

    private static void loadCredentials() {
        String filePath = System.getenv("PROP_FILE_LOCATION");
        filePath = filePath.replaceFirst("application.properties", "credential.properties");
        System.out.println("Cred File path -" + filePath);
        Properties properties = new Properties();
        try (InputStream is = new FileInputStream(new File(filePath))) {
            properties.load(is);
            userName = properties.getProperty("userName");
            password = properties.getProperty("password");
        } catch(Exception ex) {
            System.out.println("Fail to load credentials.properties file" + ex.toString());
            System.exit(0);
        } 
    }
    /**
     * Closes the cassandra session and cluster.
     */
    public static void close() {
        if (cassandraSession != null) {
            cassandraSession.close();
        }
    }
    
    public static void main(String[] args){
        CassandraData.createSession();
        UserPreferencesDAO udao = UserPreferencesMapperImpl.getInstance().userPreferencesDAO();
        List<UserPreferences> userList = udao.findByPK("test");
        System.out.println("userList -" + userList);
        System.exit(0);
    }
}
