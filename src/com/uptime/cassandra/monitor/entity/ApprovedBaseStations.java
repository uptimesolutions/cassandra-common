/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "approved_base_stations")
public class ApprovedBaseStations {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "mac_address")
    private String macAddress;

    @CqlName(value = "issued_token")
    private UUID issuedToken;
    
    @CqlName(value = "last_passcode")
    private Instant lastPasscode;
    
    @CqlName(value = "passcode")
    private UUID passcode;

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public UUID getIssuedToken() {
        return issuedToken;
    }

    public void setIssuedToken(UUID issuedToken) {
        this.issuedToken = issuedToken;
    }

    public Instant getLastPasscode() {
        return lastPasscode;
    }

    public void setLastPasscode(Instant lastPasscode) {
        this.lastPasscode = lastPasscode;
    }

    public UUID getPasscode() {
        return passcode;
    }

    public void setPasscode(UUID passcode) {
        this.passcode = passcode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.macAddress);
        hash = 89 * hash + Objects.hashCode(this.issuedToken);
        hash = 89 * hash + Objects.hashCode(this.lastPasscode);
        hash = 89 * hash + Objects.hashCode(this.passcode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApprovedBaseStations other = (ApprovedBaseStations) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.macAddress, other.macAddress)) {
            return false;
        }
        if (!Objects.equals(this.issuedToken, other.issuedToken)) {
            return false;
        }
        if (!Objects.equals(this.lastPasscode, other.lastPasscode)) {
            return false;
        }
        return Objects.equals(this.passcode, other.passcode);
    }

    @Override
    public String toString() {
        return "ApprovedBaseStations{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", macAddress=" + macAddress + ", issuedToken=" + issuedToken + ", lastPasscode=" + lastPasscode + ", passcode=" + passcode + '}';
    }

}
