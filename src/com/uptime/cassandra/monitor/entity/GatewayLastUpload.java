/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "gateway_last_upload")
public class GatewayLastUpload {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "device_id")
    private String deviceId;

    @CqlName(value = "site_name")
    private String siteName;
    
    @CqlName(value = "last_upload")
    private Instant lastUpload;


    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Instant getLastUpload() {
        return lastUpload;
    }

    public void setLastUpload(Instant lastUpload) {
        this.lastUpload = lastUpload;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.deviceId);
        hash = 89 * hash + Objects.hashCode(this.siteName);
        hash = 89 * hash + Objects.hashCode(this.lastUpload);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GatewayLastUpload other = (GatewayLastUpload) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }

        return Objects.equals(this.lastUpload, other.lastUpload);
    }

    @Override
    public String toString() {
        return "GatewayLastUpload{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceId=" + deviceId + ", siteName=" + siteName + ", lastUpload=" + lastUpload + '}';
    }
}
