/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_device_basestation")
public class SiteDeviceBasestation {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "device_id")
    private String deviceId;

    @CqlName(value = "base_station_port")
    private short baseStationPort;
    
    @CqlName(value = "is_disabled")
    boolean disabled;

    public SiteDeviceBasestation() {
    }

    public SiteDeviceBasestation(SiteDeviceBasestation siteDeviceBasestation) {
        this.customerAccount = siteDeviceBasestation.getCustomerAccount();
        this.siteId = siteDeviceBasestation.getSiteId();
        this.deviceId = siteDeviceBasestation.getDeviceId();
        this.baseStationPort = siteDeviceBasestation.getBaseStationPort();
        this.disabled = siteDeviceBasestation.isDisabled();
    }
    
    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.customerAccount);
        hash = 67 * hash + Objects.hashCode(this.siteId);
        hash = 67 * hash + this.baseStationPort;
        hash = 67 * hash + Objects.hashCode(this.deviceId);
        hash = 67 * hash + Objects.hashCode(this.disabled);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteDeviceBasestation other = (SiteDeviceBasestation) obj;
        if (this.baseStationPort != other.baseStationPort) {
            return false;
        }
        
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.disabled, other.disabled)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteDeviceBasestation{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceId=" + deviceId + ", baseStationPort=" + baseStationPort + ", disabled=" + disabled + '}';
    }

}
