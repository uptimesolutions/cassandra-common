/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "mist_diagnostics")
public class MistDiagnostics {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "echo_device_id")
    private String echoDeviceId;
    
    @ClusteringColumn(1)
    @CqlName(value = "device_id")
    private String deviceId;

    @ClusteringColumn(2)
    @CqlName(value = "mistlx_chan")
    byte mistlxChan; //hex convert decimal
    
    @CqlName(value = "last_sample")
    Instant lastSample; //use sample_time_adj from json
            
    @CqlName(value = "mistlx_rssi")
    byte mistlxRssi;
    
    @CqlName(value = "mistlx_fsamp")
    byte mistlxFsamp; //hex convert decimal
                    
    @CqlName(value = "mistlx_length")
    short mistlxLength;
    
    @CqlName(value = "mistlx_battery")
    float mistlxBattery; //divide by 51.2
    
    @CqlName(value = "mistlx_temp")
    float mistlxTemp; // (((value * .4185) * 9) / 5) +32
    
    @CqlName(value = "mistlx_delta")
    short mistlxDelta;
    
    @CqlName(value = "mistlx_pcorr")
    short mistlxPcorr;
    
    @CqlName(value = "mistlx_dbg")
    byte mistlxDbg; //hex convert decimal
    
    @CqlName(value = "mistlx_bleon")
    byte mistlxBleon; //hex convert decimal
    
    @CqlName(value = "mistlx_fpgaon")
    byte mistlxFpgaon; //hex convert decimal
    
    @CqlName(value = "mistlx_total_min")
    short mistlxTotalMin; //hex convert decimal
    
    @CqlName(value = "mistlx_mincnt")
    short mistlxMincnt;
    
    @CqlName(value = "mistlx_failcnt")
    short mistlxFailcnt; //hex convert decimal
    
    @CqlName(value = "mistlx_ctime")
    short mistlxCtime; //hex convert decimal
    
    @CqlName(value = "mistlx_ctime_mod")
    short mistlxCtimeMod;
    
    @CqlName(value = "site_name")
    private String siteName;
    
    @CqlName(value = "area_name")
    private String areaName;
    
    @CqlName(value = "asset_name")
    private String assetName;
    
    @CqlName(value = "point_location_name")
    private String pointLocationName;
    
    @CqlName(value = "point_name")
    private String pointName;
    
    @CqlName(value = "base_station_port")
    private short baseStationPort;
    
    @CqlName(value = "base_station_mac")
    private String baseStationMac;
    
    @CqlName(value = "base_station_hostname")
    private String baseStationHostName;

    public MistDiagnostics() {
    }

    public MistDiagnostics(MistDiagnostics entity) {
        this.customerAccount = entity.getCustomerAccount();
        this.siteId = entity.getSiteId();
        this.deviceId = entity.getDeviceId();
        this.echoDeviceId = entity.getEchoDeviceId();
        this.mistlxChan = entity.getMistlxChan();
        this.lastSample = entity.getLastSample();
        this.mistlxRssi = entity.getMistlxRssi();
        this.mistlxFsamp = entity.getMistlxFsamp();
        this.mistlxLength = entity.getMistlxLength();
        this.mistlxBattery = entity.getMistlxBattery();
        this.mistlxTemp = entity.getMistlxTemp();
        this.mistlxDelta = entity.getMistlxDelta();
        this.mistlxPcorr = entity.getMistlxPcorr();
        this.mistlxDbg = entity.getMistlxDbg();
        this.mistlxBleon = entity.getMistlxBleon();
        this.mistlxFpgaon = entity.getMistlxFpgaon();
        this.mistlxTotalMin = entity.getMistlxTotalMin();
        this.mistlxMincnt = entity.getMistlxMincnt();
        this.mistlxFailcnt = entity.getMistlxFailcnt();
        this.mistlxCtime = entity.getMistlxCtime();
        this.mistlxCtimeMod = entity.getMistlxCtimeMod();
        this.siteName = entity.getSiteName();
        this.areaName = entity.getAreaName();
        this.assetName = entity.getAssetName();
        this.pointLocationName = entity.getPointLocationName();
        this.pointName = entity.getPointName();
        this.baseStationPort = entity.getBaseStationPort();
        this.baseStationMac = entity.getBaseStationMac();
        this.baseStationHostName = entity.getBaseStationHostName();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getEchoDeviceId() {
        return echoDeviceId;
    }

    public void setEchoDeviceId(String echoDeviceId) {
        this.echoDeviceId = echoDeviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public byte getMistlxChan() {
        return mistlxChan;
    }

    public void setMistlxChan(byte mistlxChan) {
        this.mistlxChan = mistlxChan;
    }

    public Instant getLastSample() {
        return lastSample;
    }

    public void setLastSample(Instant lastSample) {
        this.lastSample = lastSample;
    }

    public byte getMistlxRssi() {
        return mistlxRssi;
    }

    public void setMistlxRssi(byte mistlxRssi) {
        this.mistlxRssi = mistlxRssi;
    }

    public byte getMistlxFsamp() {
        return mistlxFsamp;
    }

    public void setMistlxFsamp(byte mistlxFsamp) {
        this.mistlxFsamp = mistlxFsamp;
    }

    public short getMistlxLength() {
        return mistlxLength;
    }

    public void setMistlxLength(short mistlxLength) {
        this.mistlxLength = mistlxLength;
    }

    public float getMistlxBattery() {
        return mistlxBattery;
    }

    public void setMistlxBattery(float mistlxBattery) {
        this.mistlxBattery = mistlxBattery;
    }

    public float getMistlxTemp() {
        return mistlxTemp;
    }

    public void setMistlxTemp(float mistlxTemp) {
        this.mistlxTemp = mistlxTemp;
    }

    public short getMistlxDelta() {
        return mistlxDelta;
    }

    public void setMistlxDelta(short mistlxDelta) {
        this.mistlxDelta = mistlxDelta;
    }

    public short getMistlxPcorr() {
        return mistlxPcorr;
    }

    public void setMistlxPcorr(short mistlxPcorr) {
        this.mistlxPcorr = mistlxPcorr;
    }

    public byte getMistlxDbg() {
        return mistlxDbg;
    }

    public void setMistlxDbg(byte mistlxDbg) {
        this.mistlxDbg = mistlxDbg;
    }

    public byte getMistlxBleon() {
        return mistlxBleon;
    }

    public void setMistlxBleon(byte mistlxBleon) {
        this.mistlxBleon = mistlxBleon;
    }

    public byte getMistlxFpgaon() {
        return mistlxFpgaon;
    }

    public void setMistlxFpgaon(byte mistlxFpgaon) {
        this.mistlxFpgaon = mistlxFpgaon;
    }

    public short getMistlxTotalMin() {
        return mistlxTotalMin;
    }

    public void setMistlxTotalMin(short mistlxTotalMin) {
        this.mistlxTotalMin = mistlxTotalMin;
    }

    public short getMistlxMincnt() {
        return mistlxMincnt;
    }

    public void setMistlxMincnt(short mistlxMincnt) {
        this.mistlxMincnt = mistlxMincnt;
    }

    public short getMistlxFailcnt() {
        return mistlxFailcnt;
    }

    public void setMistlxFailcnt(short mistlxFailcnt) {
        this.mistlxFailcnt = mistlxFailcnt;
    }

    public short getMistlxCtime() {
        return mistlxCtime;
    }

    public void setMistlxCtime(short mistlxCtime) {
        this.mistlxCtime = mistlxCtime;
    }

    public short getMistlxCtimeMod() {
        return mistlxCtimeMod;
    }

    public void setMistlxCtimeMod(short mistlxCtimeMod) {
        this.mistlxCtimeMod = mistlxCtimeMod;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getBaseStationMac() {
        return baseStationMac;
    }

    public void setBaseStationMac(String baseStationMac) {
        this.baseStationMac = baseStationMac;
    }

    public String getBaseStationHostName() {
        return baseStationHostName;
    }

    public void setBaseStationHostName(String baseStationHostName) {
        this.baseStationHostName = baseStationHostName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.customerAccount);
        hash = 23 * hash + Objects.hashCode(this.siteId);
        hash = 23 * hash + Objects.hashCode(this.echoDeviceId);
        hash = 23 * hash + Objects.hashCode(this.deviceId);
        hash = 23 * hash + this.mistlxChan;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MistDiagnostics other = (MistDiagnostics) obj;
        if (this.mistlxChan != other.mistlxChan) {
            return false;
        }
        if (this.mistlxRssi != other.mistlxRssi) {
            return false;
        }
        if (this.mistlxFsamp != other.mistlxFsamp) {
            return false;
        }
        if (this.mistlxLength != other.mistlxLength) {
            return false;
        }
        if (Float.floatToIntBits(this.mistlxBattery) != Float.floatToIntBits(other.mistlxBattery)) {
            return false;
        }
        if (Float.floatToIntBits(this.mistlxTemp) != Float.floatToIntBits(other.mistlxTemp)) {
            return false;
        }
        if (this.mistlxDbg != other.mistlxDbg) {
            return false;
        }
        if (this.mistlxBleon != other.mistlxBleon) {
            return false;
        }
        if (this.mistlxFpgaon != other.mistlxFpgaon) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.echoDeviceId, other.echoDeviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.lastSample, other.lastSample)) {
            return false;
        }
        if (!Objects.equals(this.mistlxDelta, other.mistlxDelta)) {
            return false;
        }
        if (!Objects.equals(this.mistlxPcorr, other.mistlxPcorr)) {
            return false;
        }
        if (!Objects.equals(this.mistlxTotalMin, other.mistlxTotalMin)) {
            return false;
        }
        if (!Objects.equals(this.mistlxMincnt, other.mistlxMincnt)) {
            return false;
        }
        if (!Objects.equals(this.mistlxFailcnt, other.mistlxFailcnt)) {
            return false;
        }
        if (!Objects.equals(this.mistlxCtime, other.mistlxCtime)) {
            return false;
        }
        if (!Objects.equals(this.mistlxCtimeMod, other.mistlxCtimeMod)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.baseStationPort, other.baseStationPort)) {
            return false;
        }
        if (!Objects.equals(this.baseStationMac, other.baseStationMac)) {
            return false;
        }
        if (!Objects.equals(this.baseStationHostName, other.baseStationHostName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MistDiagnostics{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", echoDeviceId=" + echoDeviceId + ", deviceId=" + deviceId + ", mistlxChan=" + mistlxChan + ", lastSample=" + lastSample + ", mistlxRssi=" + mistlxRssi + ", mistlxFsamp=" + mistlxFsamp + ", mistlxLength=" + mistlxLength + ", mistlxBattery=" + mistlxBattery + ", mistlxTemp=" + mistlxTemp + ", mistlxDelta=" + mistlxDelta + ", mistlxPcorr=" + mistlxPcorr + ", mistlxDbg=" + mistlxDbg + ", mistlxBleon=" + mistlxBleon + ", mistlxFpgaon=" + mistlxFpgaon + ", mistlxTotalMin=" + mistlxTotalMin + ", mistlxMincnt=" + mistlxMincnt + ", mistlxFailcnt=" + mistlxFailcnt + ", mistlxCtime=" + mistlxCtime + ", mistlxCtimeMod=" + mistlxCtimeMod + ", siteName=" + siteName + ", areaName=" + areaName +", assetName=" + assetName +", pointLocationName=" + pointLocationName +", pointName=" + pointName +", baseStationPort=" + baseStationPort +", baseStationMac="+baseStationMac+", baseStationHostName="+baseStationHostName+"}";
    }

}
