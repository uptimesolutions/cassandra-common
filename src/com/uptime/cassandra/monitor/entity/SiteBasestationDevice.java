/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_basestation_device")
public class SiteBasestationDevice {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "base_station_port")
    private short baseStationPort;

    @ClusteringColumn(0)
    @CqlName(value = "device_id")
    private String deviceId;
    
    @ClusteringColumn(1)
    @CqlName(value = "point_id")
    private UUID pointId;

    @CqlName(value = "channel_type")
    private String channelType;
    
    @CqlName(value = "fmax")
    private int fmax;
	
    @CqlName(value = "is_disabled")
    boolean disabled;
    
    @CqlName(value = "last_sampled")
    private Instant lastSampled;
	
    @CqlName(value = "resolution")
    private int resolution;
	
    @CqlName(value = "sample_interval")
    private short sampleInterval ;
	
    @CqlName(value = "sensor_channel_num")
    private byte sensorChannelNum;

    public SiteBasestationDevice() {
    }

    public SiteBasestationDevice(SiteBasestationDevice siteBasestationDevice) {
        this.customerAccount = siteBasestationDevice.getCustomerAccount();
        this.siteId = siteBasestationDevice.getSiteId();
        this.baseStationPort = siteBasestationDevice.getBaseStationPort();
        this.deviceId = siteBasestationDevice.getDeviceId();
        this.pointId = siteBasestationDevice.getPointId();
        this.channelType = siteBasestationDevice.getChannelType();
        this.fmax = siteBasestationDevice.getFmax();
        this.disabled = siteBasestationDevice.isDisabled();
        this.lastSampled = siteBasestationDevice.getLastSampled();
        this.resolution = siteBasestationDevice.getResolution();
        this.sampleInterval = siteBasestationDevice.getSampleInterval();
        this.sensorChannelNum = siteBasestationDevice.getSensorChannelNum();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Instant getLastSampled() {
        return lastSampled;
    }

    public void setLastSampled(Instant lastSampled) {
        this.lastSampled = lastSampled;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public byte getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(byte sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.customerAccount);
        hash = 47 * hash + Objects.hashCode(this.siteId);
        hash = 47 * hash + this.baseStationPort;
        hash = 47 * hash + Objects.hashCode(this.deviceId);
        hash = 47 * hash + Objects.hashCode(this.pointId);
        hash = 47 * hash + Objects.hashCode(this.channelType);
        hash = 47 * hash + this.fmax;
        hash = 47 * hash + (this.disabled ? 1 : 0);
        hash = 47 * hash + Objects.hashCode(this.lastSampled);
        hash = 47 * hash + this.resolution;
        hash = 47 * hash + this.sampleInterval;
        hash = 47 * hash + this.sensorChannelNum;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteBasestationDevice other = (SiteBasestationDevice) obj;
        if (this.baseStationPort != other.baseStationPort) {
            return false;
        }
        if (this.fmax != other.fmax) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        return Objects.equals(this.lastSampled, other.lastSampled);
    }

    @Override
    public String toString() {
        return "SiteBasestationDevice{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", baseStationPort=" + baseStationPort + ", deviceId=" + deviceId + ", pointId=" + pointId + ", channelType=" + channelType + ", fmax=" + fmax + ", disabled=" + disabled + ", lastSampled=" + lastSampled + ", resolution=" + resolution + ", sampleInterval=" + sampleInterval + ", sensorChannelNum=" + sensorChannelNum + "}";
    }

}
