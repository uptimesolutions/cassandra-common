/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;

/**
 *
 * @author madhavi
 */
@Mapper
public interface BaseStationMapper {

    @DaoFactory
    ApprovedBaseStationsDAO approvedBaseStationsDAO();
    
    @DaoFactory
    GatewayLastUploadDAO gatewayLastUploadDAO();
    
    @DaoFactory
    MistDiagnosticsDAO mistDiagnosticsDAO();
    
    @DaoFactory
    EchoDiagnosticsDAO echoDiagnosticsDAO();
    
    @DaoFactory
    SiteDeviceBasestationDAO siteDeviceBasestationDAO();
    
    @DaoFactory
    SiteBasestationDeviceDAO siteBasestationDeviceDAO();
}
