/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.monitor.entity.GatewayLastUpload;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface GatewayLastUploadDAO extends BaseDAO<GatewayLastUpload> {

    Logger CQL_LOG = LoggerFactory.getLogger(GatewayLastUploadDAO.class);

    /**
     * Inserts the given gatewayLastUpload into the gateway_last_upload table
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(GatewayLastUpload entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given gatewayLastUpload in the gateway_last_upload table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(GatewayLastUpload entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given gatewayLastUpload from the gateway_last_upload table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(GatewayLastUpload entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     *
     * @param gatewayLastUpload
     * @param statementType
     * @return
     */
    default String printCQL(GatewayLastUpload gatewayLastUpload, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO gateway_last_upload(customer_acct,site_id,device_id");
                values.append("'").append(gatewayLastUpload.getCustomerAccount()).append("'");
                values.append(",").append(gatewayLastUpload.getSiteId());
                values.append(",'").append(gatewayLastUpload.getDeviceId()).append("'");

                if (gatewayLastUpload.getSiteName() != null) {
                    query.append(",site_name ");
                    values.append(",").append(gatewayLastUpload.getSiteName());
                }
  
                if (gatewayLastUpload.getLastUpload()!= null) {
                    query.append(",last_upload ");
                    values.append(",").append(gatewayLastUpload.getLastUpload());
                }

                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM gateway_last_upload WHERE customer_acct='").append(gatewayLastUpload.getCustomerAccount()).append("'");
                query.append(" AND site_id=").append(gatewayLastUpload.getSiteId());
                query.append(" AND device_id='").append(gatewayLastUpload.getDeviceId()).append("';");
            }
            break;
        }

        return query.toString();
    }

    /**
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of GatewayLastUpload objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    //@Query("SELECT * FROM gateway_last_upload WHERE customer_acct = :customer AND site_id = :site")
    @Select
    PagingIterable<GatewayLastUpload> queryByCustomerSiteId(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of GatewayLastUpload objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    default List<GatewayLastUpload> findByCustomerSiteId(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }

        List<GatewayLastUpload> users = queryByCustomerSiteId(customer, siteId).all();
        return users;
    }

    /**
     * Returns GatewayLastUpload Objects for the given customer, siteName and macAddress from gateway_last_upload table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     * @return PagingIterable List of Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<GatewayLastUpload> queryByPK(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of GatewayLastUpload Objects for the given customer, siteId and macAddress from gateway_last_upload table
     *
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     * @return List of GatewayLastUpload Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<GatewayLastUpload> findByPK(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }

        return  queryByPK(customer, siteId, deviceId).all();
    }

}
