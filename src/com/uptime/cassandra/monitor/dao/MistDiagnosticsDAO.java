/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.monitor.entity.MistDiagnostics;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface MistDiagnosticsDAO extends BaseDAO<MistDiagnostics> {

    Logger CQL_LOG = LoggerFactory.getLogger(MistDiagnosticsDAO.class);

    /**
     * Inserts the given mistDiagnostics into the mist_diagnostics table
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(MistDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given mistDiagnostics in the mist_diagnostics table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(MistDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given mistDiagnostics from the mist_diagnostics table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(MistDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns MistDiagnostics Objects for the given customer and siteId from mist_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @return PagingIterable of MistDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    @Select
    PagingIterable<MistDiagnostics> queryByCustomerSite(String customer, UUID siteÌd) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of MistDiagnostics Objects for the given customer and siteÌd from mist_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @return List of MistDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<MistDiagnostics> findByCustomerSite(String customer, UUID siteÌd) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }

        return queryByCustomerSite(customer, siteÌd).all();
    }

    /**
     * Returns MistDiagnostics Objects for the given customer, siteId and deviceId from mist_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @param echoDeviceId the serial # of the echobase associated to this Mist
     * @param deviceId, the serial # of the Mist
     * @return PagingIterable List of MistDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<MistDiagnostics> queryByCustomerSiteDevice(String customer, UUID siteÌd, String echoDeviceId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of MistDiagnostics Objects for the given customer, siteÌd and deviceId from mist_diagnostics table
     *
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @param echoDeviceId the serial # of the echobase associated to this Mist
     * @param deviceId, the serial # of the Mist
     * @return List of MistDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<MistDiagnostics> findByCustomerSiteDevice(String customer, UUID siteÌd, String echoDeviceId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }

        return queryByCustomerSiteDevice(customer, siteÌd, echoDeviceId, deviceId).all();
    }

    /**
     * Returns MistDiagnostics Objects for the given customer, siteId, deviceId and channelNum from mist_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @param echoDeviceId the serial # of the echobase associated to this Mist
     * @param deviceId, the serial # of the Mist
     * @param channelNum, byte
     * @return PagingIterable List of MistDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<MistDiagnostics> queryByPK(String customer, UUID siteÌd, String echoDeviceId, String deviceId, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of MistDiagnostics Objects for the given customer, siteÌd and deviceId from mist_diagnostics table
     *
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @param echoDeviceId the serial # of the echobase associated to this Mist
     * @param deviceId, the serial # of the Mist
     * @param channelNum, byte
     * @return List of MistDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<MistDiagnostics> findByPK(String customer, UUID siteÌd, String echoDeviceId, String deviceId, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }

        return queryByPK(customer, siteÌd, echoDeviceId, deviceId, channelNum).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param mistDiagnostics, MistDiagnostics Object
     * @param statementType, String Object
     * @return String
     */
    default String printCQL(MistDiagnostics mistDiagnostics, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();

        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO mist_diagnostics(customer_acct");
                values.append("'").append(mistDiagnostics.getCustomerAccount()).append("'");
                query.append(",site_id");
                values.append(",").append(mistDiagnostics.getSiteId());
                query.append(",echo_device_id");
                values.append(",'").append(mistDiagnostics.getEchoDeviceId()).append("'");
                query.append(",device_id");
                values.append(",'").append(mistDiagnostics.getDeviceId()).append("'");
                query.append(",mistlx_chan");
                values.append(",").append(mistDiagnostics.getMistlxChan());

                if (mistDiagnostics.getLastSample() != null) {
                    query.append(",last_sample");
                    values.append(",'").append(mistDiagnostics.getLastSample()).append("'");
                }

                query.append(",mistlx_rssi");
                values.append(",").append(mistDiagnostics.getMistlxRssi());
                query.append(",mistlx_fsamp");
                values.append(",").append(mistDiagnostics.getMistlxFsamp());
                query.append(",mistlx_length");
                values.append(",").append(mistDiagnostics.getMistlxLength());
                query.append(",mistlx_battery");
                values.append(",").append(mistDiagnostics.getMistlxBattery());
                query.append(",mistlx_temp");
                values.append(",").append(mistDiagnostics.getMistlxTemp());
                query.append(",mistlx_delta");
                values.append(",").append(mistDiagnostics.getMistlxDelta());
                query.append(",mistlx_pcorr");
                values.append(",").append(mistDiagnostics.getMistlxPcorr());
                query.append(",mistlx_dbg");
                values.append(",").append(mistDiagnostics.getMistlxDbg());
                query.append(",mistlx_bleon");
                values.append(",").append(mistDiagnostics.getMistlxBleon());
                query.append(",mistlx_fpgaon");
                values.append(",").append(mistDiagnostics.getMistlxFpgaon());
                query.append(",mistlx_total_min");
                values.append(",").append(mistDiagnostics.getMistlxTotalMin());
                query.append(",mistlx_mincnt");
                values.append(",").append(mistDiagnostics.getMistlxMincnt());
                query.append(",mistlx_failcnt");
                values.append(",").append(mistDiagnostics.getMistlxFailcnt());
                query.append(",mistlx_ctime");
                values.append(",").append(mistDiagnostics.getMistlxCtime());
                query.append(",mistlx_ctime_mod");
                values.append(",").append(mistDiagnostics.getMistlxCtimeMod());
                query.append(",site_name");
                values.append(",'").append(mistDiagnostics.getSiteName()).append("'");
                query.append(",area_name");
                values.append(",'").append(mistDiagnostics.getAreaName()).append("'");
                query.append(",asset_name");
                values.append(",'").append(mistDiagnostics.getAssetName()).append("'");
                query.append(",point_location_name");
                values.append(",'").append(mistDiagnostics.getPointLocationName()).append("'");
                query.append(",point_name");
                values.append(",'").append(mistDiagnostics.getPointName()).append("'");
                query.append(",base_station_port");
                values.append(",").append(mistDiagnostics.getBaseStationPort());
                query.append(",base_station_mac");
                values.append(",'").append(mistDiagnostics.getBaseStationMac()).append("'");
                query.append(",base_station_hostname");
                values.append(",'").append(mistDiagnostics.getBaseStationHostName()).append("'");

                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM mist_diagnostics WHERE customer_acct='").append(mistDiagnostics.getCustomerAccount()).append("'");
                query.append(" AND site_id=").append(mistDiagnostics.getSiteId());
                query.append(" AND echo_device_id='").append(mistDiagnostics.getEchoDeviceId()).append("'");
                query.append(" AND device_id='").append(mistDiagnostics.getDeviceId()).append("'");
                query.append(" AND mistlx_chan=").append(mistDiagnostics.getMistlxChan()).append(";");
            }
            break;
        }

        return query.toString();
    }

}
