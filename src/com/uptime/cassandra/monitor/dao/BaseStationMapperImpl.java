/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author madhavi
 */
public class BaseStationMapperImpl {

    private static BaseStationMapper instance = null;

    public static BaseStationMapper getInstance() {
        if (instance == null) {
            new BaseStationMapperImpl();
        }
        return instance;
    }

    private BaseStationMapperImpl() {
        instance = new BaseStationMapperBuilder(CassandraData.getSession()).build();
    }
}
