/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteBasestationDeviceDAO extends BaseDAO<SiteBasestationDevice> {
Logger CQL_LOG = LoggerFactory.getLogger(SiteBasestationDeviceDAO.class);
    /**
     * Inserts the given SiteBasestationDevice into the site_basestation_device
     * table
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(SiteBasestationDevice entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of site_basestation_device into the site_basestation_device table
     * @param entities, Object List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<SiteBasestationDevice> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }
    
    /**
     * Updates the given SiteBasestationDevice in the site_basestation_device
     * table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(SiteBasestationDevice entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of site_basestation_device into the site_basestation_device table
     * @param entities, Object List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(List<SiteBasestationDevice> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }
    
    /**
     * Update the sample_interval field into the site_basestation_device table
     * @param query, Query to be executed.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void updateSampleIntervalField(String query) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        CassandraData.getSession().execute(query);
    }
    
    /**
     * Deletes the given SiteBasestationDevice from the site_basestation_device
     * table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(SiteBasestationDevice entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given List of SiteBasestationDevice from the site_basestation_device table
     * @param entities, Object List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<SiteBasestationDevice> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }
    
    /**
     *
     * @param siteBasestationDevice
     * @param statementType
     * @return
     */
    default String printCQL(SiteBasestationDevice siteBasestationDevice, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO site_basestation_device(customer_acct,site_id,base_station_port,device_id,point_id");
                values.append("'").append(siteBasestationDevice.getCustomerAccount()).append("'");
                values.append(",").append(siteBasestationDevice.getSiteId());
                values.append(",").append(siteBasestationDevice.getBaseStationPort());
                values.append(",'").append(siteBasestationDevice.getDeviceId()).append("'");
                values.append(",").append(siteBasestationDevice.getPointId());

                if (siteBasestationDevice.getChannelType() != null) {
                    query.append(",channel_type");
                    values.append(",'").append(siteBasestationDevice.getChannelType()).append("'");
                }
                query.append(",fmax");
                values.append(",").append(siteBasestationDevice.getFmax());
                query.append(",is_disabled");
                values.append(",").append(siteBasestationDevice.isDisabled());
                if (siteBasestationDevice.getLastSampled() != null) {
                    query.append(",last_sampled");
                    values.append(",'").append(siteBasestationDevice.getLastSampled()).append("'");
                }
                query.append(",resolution");
                values.append(",").append(siteBasestationDevice.getResolution());
                query.append(",sample_interval");
                values.append(",").append(siteBasestationDevice.getSampleInterval());
                query.append(",sensor_channel_num");
                values.append(",").append(siteBasestationDevice.getSensorChannelNum());
                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM site_basestation_device WHERE customer_acct='").append(siteBasestationDevice.getCustomerAccount()).append("'");
                query.append(" AND site_id=").append(siteBasestationDevice.getSiteId());
                query.append(" AND base_station_port=").append(siteBasestationDevice.getBaseStationPort());
                query.append(" AND device_id='").append(siteBasestationDevice.getDeviceId()).append("'");
                query.append(" AND point_id=").append(siteBasestationDevice.getPointId()).append(";");
            }
            break;
        }

        return query.toString();
    }

    /**
     * Returns SiteBasestationDevice Objects for the given customer, siteName and baseStationPort   
     * from site_basestation_device table
     * 
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * 
     * @return PagingIterable List of SiteBasestationDevice Objects
     * 
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    @Select
    PagingIterable<SiteBasestationDevice> queryByCustomerSiteBasestation(String customer, UUID siteId, short baseStationPort) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns SiteBasestationDevice Objects for the given customer, siteName and baseStationPort
     * from site_basestation_device table
     * 
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * 
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    default List<SiteBasestationDevice> findByCustomerSiteBasestation(String customer, UUID siteId, short baseStationPort) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (baseStationPort == 0) {
            throw new IllegalArgumentException("Argument baseStationPort is required.");
        }
        
        List<SiteBasestationDevice> users = queryByCustomerSiteBasestation(customer, siteId, baseStationPort).all();
        return users;
    }

    /**
     * Returns SiteBasestationDevice Objects for the given customer, siteName, baseStationPort and deviceId 
     * from site_basestation_device table
     * 
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId, String Object
     * 
     * @return PagingIterable List of SiteBasestationDevice Objects
     * 
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    @Select
    PagingIterable<SiteBasestationDevice> queryByCustomerSiteBasestationDevice(String customer, UUID siteId, short baseStationPort, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns SiteBasestationDevice Objects for the given customer, siteName, baseStationPort and deviceId 
     * from site_basestation_device table
     * 
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId, String Object
     * 
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    default List<SiteBasestationDevice> findByCustomerSiteBasestationDevice(String customer, UUID siteId, short baseStationPort, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (baseStationPort == 0) {
            throw new IllegalArgumentException("Argument baseStationPort is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }
        
        List<SiteBasestationDevice> users = queryByCustomerSiteBasestationDevice(customer, siteId, baseStationPort, deviceId).all();
        return users;
    }

    /**
     * Returns SiteBasestationDevice Objects for the given customer, siteName, baseStationPort, deviceId and
     * pointId from site_basestation_device table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId, String Object
     * @param pointId, UUID Object
     * 
     * @return PagingIterable List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<SiteBasestationDevice> queryByPK(String customer, UUID siteId, short baseStationPort, String deviceId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;
    
    /**
     * Truncate the site_basestation_device table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE site_basestation_device")
    public void truncateAllSiteBasestationDevice() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the site_basestation_device table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllSiteBasestationDevice();
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(new SiteBasestationDevice(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(new SiteBasestationDevice(), "DELETE"));
            }
            throw e;
        }
    }
    
    
    /**
     * Returns a List of SiteBasestationDevice Objects for the given customer, siteName, baseStationPort, deviceId and
     * pointId from site_basestation_device table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId, String Object
     * @param pointId, UUID Object
     * 
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteBasestationDevice> findByPK(String customer, UUID siteId, short baseStationPort, String deviceId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (baseStationPort == 0) {
            throw new IllegalArgumentException("Argument baseStationPort is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        
        List<SiteBasestationDevice> approvedBaseStationsList = queryByPK(customer, siteId, baseStationPort, deviceId, pointId).all();
        return approvedBaseStationsList;
    }

}
