/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface EchoDiagnosticsDAO extends BaseDAO<EchoDiagnostics> {

    Logger CQL_LOG = LoggerFactory.getLogger(EchoDiagnosticsDAO.class);

    /**
     * Inserts the given echoDiagnostics into the echo_diagnostics table
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(EchoDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given echoDiagnostics in the echo_diagnostics table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(EchoDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given echoDiagnostics from the echo_diagnostics table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(EchoDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns EchoDiagnostics Objects for the given customer and siteId from echo_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @return PagingIterable of EchoDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    @Select
    PagingIterable<EchoDiagnostics> queryByCustomerSite(String customer, UUID siteÌd) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of EchoDiagnostics Objects for the given customer and siteÌd from echo_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @return List of EchoDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    default List<EchoDiagnostics> findByCustomerSite(String customer, UUID siteÌd) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }

        return queryByCustomerSite(customer, siteÌd).all();
    }

    /**
     * Returns EchoDiagnostics Objects for the given customer, siteId and deviceId from echo_diagnostics table
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @param baseStationHostname, String Object
     * @param deviceId, String Object
     * @return PagingIterable List of EchoDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<EchoDiagnostics> queryByPK(String customer, UUID siteÌd, String baseStationHostname, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of EchoDiagnostics Objects for the given customer, siteÌd and deviceId from echo_diagnostics table
     *
     *
     * @param customer, String Object
     * @param siteÌd, UUID Object
     * @param baseStationHostname, String Object
     * @param deviceId, String Object
     * @return List of EchoDiagnostics Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<EchoDiagnostics> findByPK(String customer, UUID siteÌd, String baseStationHostname, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }
        if (baseStationHostname == null || baseStationHostname.length() < 1) {
            throw new IllegalArgumentException("Argument baseStationHostname is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }

        return queryByPK(customer, siteÌd, baseStationHostname, deviceId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param echoDiagnostics, EchoDiagnostics Object
     * @param statementType, String Object
     * @return String
     */
    default String printCQL(EchoDiagnostics echoDiagnostics, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO echo_diagnostics(customer_acct");
                values.append("'").append(echoDiagnostics.getCustomerAccount()).append("'");
                query.append(",site_id");
                values.append(",").append(echoDiagnostics.getSiteId());
                query.append(",base_station_hostname");
                values.append(",'").append(echoDiagnostics.getBaseStationHostname()).append("'");
                query.append(",device_id");
                values.append(",'").append(echoDiagnostics.getDeviceId()).append("'");
                query.append(",base_station_mac");
                values.append(",'").append(echoDiagnostics.getBaseStationMac()).append("'");
                query.append(",site_name");
                values.append(",'").append(echoDiagnostics.getSiteName()).append("'");

                if (echoDiagnostics.getEchoFw() != null) {
                    query.append(",echo_fw");
                    values.append(",'").append(echoDiagnostics.getEchoFw()).append("'");
                }

                query.append(",echo_datasets");
                values.append(",").append(echoDiagnostics.getEchoDatasets());
                query.append(",echo_backlog");
                values.append(",").append(echoDiagnostics.getEchoBacklog());
                query.append(",echo_radio_fail");
                values.append(",").append(echoDiagnostics.getEchoRadioFail());
                query.append(",echo_terr");
                values.append(",").append(echoDiagnostics.getEchoTerr());
                query.append(",echo_restart");
                values.append(",").append(echoDiagnostics.getEchoRestart());
                query.append(",echo_pow_level");
                values.append(",").append(echoDiagnostics.getEchoPowLevel());
                query.append(",echo_reboot");
                values.append(",").append(echoDiagnostics.getEchoReboot());
                query.append(",echo_powerfault");
                values.append(",").append(echoDiagnostics.getEchoPowerfault());

                if (echoDiagnostics.getLastUpdate() != null) {
                    query.append(",last_update");
                    values.append(",'").append(echoDiagnostics.getLastUpdate()).append("'");
                }

                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM echo_diagnostics WHERE customer_acct='").append(echoDiagnostics.getCustomerAccount()).append("'");
                query.append(" AND site_id=").append(echoDiagnostics.getSiteId());
                query.append(" AND base_station_hostname='").append(echoDiagnostics.getBaseStationHostname()).append("'");
                query.append(" AND device_id='").append(echoDiagnostics.getDeviceId()).append("';");
            }
            break;
        }

        return query.toString();
    }

}
