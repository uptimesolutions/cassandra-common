/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface ApprovedBaseStationsDAO extends BaseDAO<ApprovedBaseStations> {

    Logger CQL_LOG = LoggerFactory.getLogger(ApprovedBaseStationsDAO.class);

    /**
     * Inserts the given approvedBaseStations into the approved_base_stations table
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(ApprovedBaseStations entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given approvedBaseStations in the approved_base_stations table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(ApprovedBaseStations entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given approvedBaseStations from the approved_base_stations table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(ApprovedBaseStations entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     *
     * @param approvedBaseStations
     * @param statementType
     * @return
     */
    default String printCQL(ApprovedBaseStations approvedBaseStations, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO approved_base_stations(customer_acct,site_id,mac_address");
                values.append("'").append(approvedBaseStations.getCustomerAccount()).append("'");
                values.append(",").append(approvedBaseStations.getSiteId());
                values.append(",'").append(approvedBaseStations.getMacAddress()).append("'");

                if (approvedBaseStations.getIssuedToken() != null) {
                    query.append(",issued_token");
                    values.append(",").append(approvedBaseStations.getIssuedToken());
                }
                if (approvedBaseStations.getLastPasscode() != null) {
                    query.append(",last_passcode");
                    values.append(",'").append(approvedBaseStations.getLastPasscode()).append("'");
                }
                if (approvedBaseStations.getPasscode() != null) {
                    query.append(",passcode");
                    values.append(",").append(approvedBaseStations.getPasscode());
                }

                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM approved_base_stations WHERE customer_acct='").append(approvedBaseStations.getCustomerAccount()).append("'");
                query.append(" AND site_id=").append(approvedBaseStations.getSiteId());
                query.append(" AND mac_address='").append(approvedBaseStations.getMacAddress()).append("';");
            }
            break;
        }

        return query.toString();
    }

    /**
     *
     * @param customer
     * @param siteÌd
     * @return
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    //@Query("SELECT * FROM approved_base_stations WHERE customer_acct = :customer AND site_id = :site")
    @Select
    PagingIterable<ApprovedBaseStations> queryByCustomerSiteId(String customer, UUID siteÌd) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     *
     * @param customer
     * @param siteÌd
     * @return
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    default List<ApprovedBaseStations> findByCustomerSiteId(String customer, UUID siteÌd) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }

        List<ApprovedBaseStations> users = queryByCustomerSiteId(customer, siteÌd).all();
        return users;
    }

    /**
     * Returns ApprovedBaseStations Objects for the given customer, siteName and macAddress from approved_base_stations table
     *
     * @param customer
     * @param siteÌd
     * @param macAddress
     * @return PagingIterable List of Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<ApprovedBaseStations> queryByPK(String customer, UUID siteÌd, String macAddress) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of ApprovedBaseStations Objects for the given customer, siteÌd and macAddress from approved_base_stations table
     *
     *
     * @param customer
     * @param siteÌd
     * @param macAddress
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<ApprovedBaseStations> findByPK(String customer, UUID siteÌd, String macAddress) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteÌd == null) {
            throw new IllegalArgumentException("Argument siteÌd is required.");
        }
        if (macAddress == null || macAddress.length() < 1) {
            throw new IllegalArgumentException("Argument macAddress is required.");
        }

        List<ApprovedBaseStations> approvedBaseStationsList = queryByPK(customer, siteÌd, macAddress).all();
        return approvedBaseStationsList;
    }

}
