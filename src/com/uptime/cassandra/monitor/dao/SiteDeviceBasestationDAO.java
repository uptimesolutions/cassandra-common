/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface SiteDeviceBasestationDAO extends BaseDAO<SiteDeviceBasestation> {

    Logger CQL_LOG = LoggerFactory.getLogger(SiteDeviceBasestationDAO.class);

    /**
     * Inserts the given siteDeviceBasestation into the site_device_basestation table
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(SiteDeviceBasestation entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given siteDeviceBasestation in the site_device_basestation table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(SiteDeviceBasestation entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given siteDeviceBasestation from the site_device_basestation table.
     *
     * @param entity
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(SiteDeviceBasestation entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     *
     * @param siteDeviceBasestation
     * @param statementType
     * @return
     */
    default String printCQL(SiteDeviceBasestation siteDeviceBasestation, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO site_device_basestation(customer_acct,site_id,device_id");
                values.append("'").append(siteDeviceBasestation.getCustomerAccount()).append("'");
                values.append(",").append(siteDeviceBasestation.getSiteId());
                values.append(",'").append(siteDeviceBasestation.getDeviceId()).append("'");

                if (siteDeviceBasestation.getBaseStationPort() != 0) {
                    query.append(",base_station_port");
                    values.append(",").append(siteDeviceBasestation.getBaseStationPort());
                }
                query.append(",is_disabled");
                values.append(",").append(siteDeviceBasestation.isDisabled());
                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM site_device_basestation WHERE customer_acct='").append(siteDeviceBasestation.getCustomerAccount()).append("'");
                query.append(" AND site_id=").append(siteDeviceBasestation.getSiteId());
                query.append(" AND device_id='").append(siteDeviceBasestation.getDeviceId()).append("';");
            }
            break;
        }

        return query.toString();
    }

    /**
     * Returns SiteDeviceBasestation Objects for the given customer and siteName from site_device_basestation table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     *
     * @return PagingIterable List of SiteDeviceBasestation Objects
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    @Select
    PagingIterable<SiteDeviceBasestation> queryByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns SiteDeviceBasestation Objects for the given customer and siteName from site_device_basestation table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     *
     * @return List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null
     */
    default List<SiteDeviceBasestation> findByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }

        List<SiteDeviceBasestation> users = queryByCustomerSite(customer, siteId).all();
        return users;
    }

    /**
     * Returns a List of SiteDeviceBasestation Objects for the given customer, siteName and deviceId from site_device_basestation table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     *
     * @return PagingIterable List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<SiteDeviceBasestation> queryByPK(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    /**
     * Returns a List of SiteDeviceBasestation Objects for the given customer, siteName and deviceId from site_device_basestation table
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     *
     * @return List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<SiteDeviceBasestation> findByPK(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customer == null || customer.length() < 1) {
            throw new IllegalArgumentException("Argument customer is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (deviceId == null || deviceId.length() < 1) {
            throw new IllegalArgumentException("Argument deviceId is required.");
        }

        List<SiteDeviceBasestation> approvedBaseStationsList = queryByPK(customer, siteId, deviceId).all();
        return approvedBaseStationsList;
    }

    /**
     * Inserts the given List of SiteDeviceBasestation into the site_device_basestation table
     *
     * @param entities, Object List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<SiteDeviceBasestation> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * Truncate the site_device_basestation table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE site_device_basestation")
    public void truncateAllSiteDeviceBasestation() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the site_device_basestation table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllSiteDeviceBasestation();
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(new SiteDeviceBasestation(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(new SiteDeviceBasestation(), "DELETE"));
            }
            throw e;
        }
    }

}
