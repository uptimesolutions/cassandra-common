/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.trend.entity.WaveData;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface WaveDataDAO extends BaseDAO<WaveData> {

      Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(WaveDataDAO.class.getName());
    /**
     * Inserts the given entity into the wave_data table
     * @param entity, WaveData Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(WaveData entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the wave_data table
     * @param entity, WaveData Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(WaveData entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the wave_data table.
     * @param entity, WaveData Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(WaveData entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns a List of WaveData Objects for the given customerAcct, siteId, areaId,
     * assetId, pointLocationId, pointId, sampleYear, sampleMonth and sampleTime from wave_data table
     * 
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param sampleYear, int
     * @param sampleMonth, int
     * @param sampleTime, Instant Object
     * @return PagingIterable of WaveData Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<WaveData> queryByPK(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, short sampleYear, byte sampleMonth, Instant sampleTime) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of WaveData Objects for the given customerAcct, siteId, areaId,
     * assetId, pointLocationId, pointId, sampleYear, sampleMonth and sampleTime from wave_data table
     * 
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param sampleYear, int
     * @param sampleMonth, int
     * @param sampleTime, Instant Object
     * @return List of WaveData Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<WaveData> findByPK(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, short sampleYear, byte sampleMonth, Instant sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAcct == null) {
            throw new IllegalArgumentException("Argument customerAcct cannot be null.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId cannot be null.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId cannot be null.");
        }
        if (sampleYear <= 0) {
            throw new IllegalArgumentException("Argument sampleYear cannot be less than or equal to 0.");
        }
        if (sampleMonth <= 0) {
            throw new IllegalArgumentException("Argument sampleMonth cannot be less than or equal to 0.");
        }
        if (sampleTime == null) {
            throw new IllegalArgumentException("Argument sampleTime cannot be null.");
        }
        return queryByPK(customerAcct, siteId, areaId, assetId, pointLocationId, pointId, sampleYear, sampleMonth, sampleTime).all();
    }
    
    /**
     * Truncate the wave_data table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE wave_data")
    public void truncateAllWaveData() throws UnavailableException, WriteTimeoutException;
    
    /**
     * Deletes the all the entities from the wave_data table.
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllWaveData();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new WaveData(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new WaveData(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, WaveData Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(WaveData entity, String statementType) {
        StringBuilder query, values;
        
        query = new StringBuilder();
        if (entity != null && statementType != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO wave_data(customer_acct, site_id, area_id, asset_id, point_location_id, point_id, sample_year, sample_month, sample_time ");
                    values.append("'").append(entity.getCustomerAccount()).append("',").append(entity.getSiteId()).append(",").append(entity.getAreaId()).append(",").append(entity.getAssetId()).append(",").append(entity.getPointLocationId()).append(",").append(entity.getPointId()).append(",").append(entity.getSampleYear()).append(",").append(entity.getSampleMonth()).append(",'").append(entity.getSampleTime()).append("'");
                    query.append(",sensitivity_value");
                    values.append(",").append(entity.getSensitivityValue());
                    
                    if (entity.getWave() != null) {
                        query.append(",wave");
                        values.append(",'").append(entity.getWave().toJsonString()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM wave_data WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND point_location_id=").append(entity.getPointLocationId());
                    query.append(" AND point_id=").append(entity.getPointId());
                    query.append(" AND sample_year=").append(entity.getSampleYear());
                    query.append(" AND sample_month=").append(entity.getSampleMonth());
                    query.append(" AND sample_time ='").append(entity.getSampleTime()).append("';");
                }
                break;
            }
        }
        
        return query.toString();
    }

}
