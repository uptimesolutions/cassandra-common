/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author twilcox
 */
public class TrendMapperImpl {
     private static TrendMapper instance = null;

    public static TrendMapper getInstance() {
        if (instance == null) {
            new TrendMapperImpl();
        }
        return instance;
    }

    private TrendMapperImpl() {
        instance = new TrendMapperBuilder(CassandraData.getSession()).build();
    }
}
