/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.trend.entity.TrendAcValues;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface TrendAcValuesDAO extends BaseDAO<TrendAcValues> {
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(TrendAcValuesDAO.class.getName());

    /**
     * Inserts the given entity into the trend_ac_values table
     *
     * @param entity, TrendAcValues Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(TrendAcValues entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the trend_ac_values table
     *
     * @param entity, TrendAcValues Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(TrendAcValues entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the trend_ac_values table.
     *
     * @param entity, TrendAcValues Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(TrendAcValues entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param paramName, String Object
     * @param sampleTime, Instant Object
     * @return PagingIterable of TrendAcValues Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<TrendAcValues> queryByPK(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramName, Instant sampleTime) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param paramName, String Object
     * @param sampleTime, Instant Object
     * @return List of TrendAcValues Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<TrendAcValues> findByPK(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramName, Instant sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAcct == null) {
            throw new IllegalArgumentException("Argument customerAcct cannot be null.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId cannot be null.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId cannot be null.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        if (paramName == null || paramName.length() == 0) {
            throw new IllegalArgumentException("Argument paramName cannot be null.");
        }
        if (sampleTime == null) {
            throw new IllegalArgumentException("Argument sampleTime cannot be null.");
        }
        return queryByPK(customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName, sampleTime).all();
    }

    /**
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param paramName, String Object
     * @param startTime, Instant Object
     * @param endTime, Instant Object
     * @return PagingIterable of TrendAcValues Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("select * from trend_ac_values where \n"
            + "customer_acct=:customerAcct\n"
            + "and site_id=:siteId \n"
            + "and area_id=:areaId \n"
            + "and asset_id=:assetId \n"
            + "and point_location_id=:pointLocationId \n"
            + "and point_id=:pointId  \n"
            + "and ap_set_id=:apSetId \n"
            + "and param_name=:paramName \n"
            + "and sample_time >= :startTime AND  sample_time <= :endTime")
    PagingIterable<TrendAcValues> queryByDateRange(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramName, Instant startTime, Instant endTime) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param paramName, String Object
     * @param startTime, Instant Object
     * @param endTime, Instant Object
     * @return List of TrendAcValues Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<TrendAcValues> findByDateRange(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramName, Instant startTime, Instant endTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAcct == null) {
            throw new IllegalArgumentException("Argument customerAcct cannot be null.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId cannot be null.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId cannot be null.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        if (paramName == null || paramName.length() == 0) {
            throw new IllegalArgumentException("Argument paramName cannot be null.");
        }
        if (startTime == null) {
            throw new IllegalArgumentException("Argument startTime cannot be null.");
        }
        if (endTime == null) {
            throw new IllegalArgumentException("Argument endTime cannot be null.");
        }
        return queryByDateRange(customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName, startTime, endTime).all();
    }

    /**
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId and paramName from trend_ac_values table
     *
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param paramName, String Object
     * @param limit, int
     * @return PagingIterable of TrendAcValues Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select(limit = ":l", orderBy = {"sample_time DESC"})
    PagingIterable<TrendAcValues> queryByTrend(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramName, @CqlName("l") int limit) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId and paramName from trend_ac_values table
     *
     * @param customerAcct, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param paramName, String Object
     * @param limit, int
     * @return List of TrendAcValues Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<TrendAcValues> findTrend(String customerAcct, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramName, int limit) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAcct == null) {
            throw new IllegalArgumentException("Argument customerAcct cannot be null.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId cannot be null.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId cannot be null.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId cannot be null.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId cannot be null.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId cannot be null.");
        }
        if (apSetId == null) {
            throw new IllegalArgumentException("Argument apSetId cannot be null.");
        }
        if (paramName == null || paramName.length() == 0) {
            throw new IllegalArgumentException("Argument paramName cannot be null.");
        }
        if (limit == 0) {
            throw new IllegalArgumentException("Argument limit cannot be 0.");
        }
        return queryByTrend(customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName, limit).all();
    }

    /**
     * Truncate the trend_ac_values table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE trend_ac_values")
    public void truncateAllTrendAcValues() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the trend_ac_values table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllTrendAcValues();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new TrendAcValues(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new TrendAcValues(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, TrendAcValues Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(TrendAcValues entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO trend_ac_values(customer_acct, site_id, area_id, asset_id, point_location_id, point_id, ap_set_id, param_name, sample_time ");
                    values.append("'").append(entity.getCustomerAccount()).append("',").append(entity.getSiteId()).append(",").append(entity.getAreaId()).append(",")
                            .append(entity.getAssetId()).append(",").append(entity.getPointLocationId()).append(",'").append(entity.getPointId()).append("',").
                            append(entity.getApSetId()).append(",'").append(entity.getParamName()).append("',").append(entity.getSampleTime());
                    if (entity.getAlSetId() != null) {
                        query.append(",al_set_id");
                        values.append(",").append(entity.getApSetId());
                    }
                    if (entity.getLabel() != null && !entity.getLabel().isEmpty()) {
                        query.append(",label");
                        values.append(",'").append(entity.getLabel()).append("'");
                    }
                    if (entity.getUnits() != null && !entity.getUnits().isEmpty()) {
                        query.append(",units");
                        values.append(",'").append(entity.getUnits()).append("'");
                    }
                    if (entity.getNotes() != null && !entity.getNotes().isEmpty()) {
                        query.append(",notes");
                        values.append(",'").append(entity.getNotes()).append("'");
                    }
                    query.append(",value");
                    values.append(",").append(entity.getValue());
                    query.append(",high_alert");
                    values.append(",").append(entity.getHighAlert());
                    query.append(",high_fault");
                    values.append(",").append(entity.getHighFault());
                    query.append(",low_alert");
                    values.append(",").append(entity.getLowAlert());
                    query.append(",low_fault");
                    values.append(",").append(entity.getLowFault());

                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM trend_ac_values WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(", site_id=").append(entity.getSiteId());
                    query.append(", area_id=").append(entity.getAreaId());
                    query.append(", asset_id=").append(entity.getAssetId());
                    query.append(", point_location_id=").append(entity.getAreaId());
                    query.append(", point_id=").append(entity.getAreaId());
                    query.append(", ap_set_id=").append(entity.getApSetId());
                    query.append(", param_name='").append(entity.getParamName());
                    query.append("', sample_time =").append(entity.getSampleTime()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
