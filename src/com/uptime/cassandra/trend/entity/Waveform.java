/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.entity;

import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
 * The Waveform entity represents the waveform User Defined Type whose primary purpose it to contain an array of sample values.
 * 
 * @author ksimmons
 */
@Entity
@CqlName(value = "waveform")
public class Waveform {
    @CqlName(value = "sample_time")
    private Instant sampleTime;
    
    @CqlName(value = "device_id")
    private String deviceId;
    
    @CqlName(value = "channel_num")
    private byte channelNum;
    
    @CqlName(value = "sample_rate_hz")
    private float sampleRateHz;
     
    @CqlName(value = "sensitivity_units")
    private String sensitivityUnits;
    
    @CqlName(value = "tach_speed")
    private float tachSpeed;
    
    @CqlName(value = "samples")
    private List<Double> samples;

    public Waveform() {
    }

    public Waveform(Waveform entity) {
        this.sampleTime = entity.getSampleTime();
        this.deviceId = entity.getDeviceId();
        this.channelNum = entity.getChannelNum();
        this.sampleRateHz = entity.getSampleRateHz();
        this.sensitivityUnits = entity.getSensitivityUnits();
        this.tachSpeed = entity.getTachSpeed();
        this.samples = entity.getSamples();
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public float getSampleRateHz() {
        return sampleRateHz;
    }

    public void setSampleRateHz(float sampleRateHz) {
        this.sampleRateHz = sampleRateHz;
    }

    public String getSensitivityUnits() {
        return sensitivityUnits;
    }

    public void setSensitivityUnits(String sensitivityUnits) {
        this.sensitivityUnits = sensitivityUnits;
    }

    public float getTachSpeed() {
        return tachSpeed;
    }

    public void setTachSpeed(float tachSpeed) {
        this.tachSpeed = tachSpeed;
    }

    public List<Double> getSamples() {
        return samples;
    }

    public void setSamples(List<Double> samples) {
        this.samples = samples;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.sampleTime);
        hash = 67 * hash + Objects.hashCode(this.deviceId);
        hash = 67 * hash + this.channelNum;
        hash = 67 * hash + Float.floatToIntBits(this.sampleRateHz);
        hash = 67 * hash + Objects.hashCode(this.sensitivityUnits);
        hash = 67 * hash + Float.floatToIntBits(this.tachSpeed);
        hash = 67 * hash + Objects.hashCode(this.samples);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Waveform other = (Waveform) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRateHz) != Float.floatToIntBits(other.sampleRateHz)) {
            return false;
        }
        if (Float.floatToIntBits(this.tachSpeed) != Float.floatToIntBits(other.tachSpeed)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.sensitivityUnits, other.sensitivityUnits)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.samples, other.samples)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Waveform{" + "sampleTime=" + sampleTime + ", deviceId=" + deviceId + ", channelNum=" + channelNum + ", sampleRateHz=" + sampleRateHz + ", sensitivityUnits=" + sensitivityUnits + ", tachSpeed=" + tachSpeed + ", samples=" + samples + '}';
    }

    public String toJsonString() {
        return "Waveform{" + "sampleTime : " + sampleTime + ", deviceId : " + deviceId + ", channelNum : " + channelNum + ", sampleRateHz : " + sampleRateHz + ", sensitivityUnits : " + sensitivityUnits + ", tachSpeed : " + tachSpeed + ", samples : " + samples + '}';
    }
    
}
