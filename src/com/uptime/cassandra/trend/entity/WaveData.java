/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * The WaveData entity represents a single waveform produced by a specific sensor.
 * 
 * @author ksimmons
 */
@Entity
@CqlName(value = "wave_data")
public class WaveData {
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;
    
    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;
    
    @PartitionKey(2)
    @CqlName(value = "area_id")
    private UUID areaId;
        
    @PartitionKey(3)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @PartitionKey(4)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @PartitionKey(5)
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @PartitionKey(6)
    @CqlName(value = "sample_year")
    private short sampleYear;
    
    @PartitionKey(7)
    @CqlName(value = "sample_month")
    private byte sampleMonth;
    
    @ClusteringColumn
    @CqlName(value = "sample_time")
    private Instant sampleTime;
    
    @CqlName(value = "sensitivity_value")
    private float sensitivityValue;
    
    @CqlName(value = "wave")
    private Waveform wave; 

    public WaveData() {
    }

    public WaveData(WaveData entity) {
        this.customerAccount = entity.getCustomerAccount();
        this.siteId = entity.getSiteId();
        this.areaId = entity.getAreaId();
        this.assetId = entity.getAssetId();
        this.pointLocationId = entity.getPointLocationId();
        this.pointId = entity.getPointId();
        this.sampleYear = entity.getSampleYear();
        this.sampleMonth = entity.getSampleMonth();
        this.sampleTime = entity.getSampleTime();
        this.sensitivityValue = entity.getSensitivityValue();
        if (entity.getWave() != null) {
            this.wave = new Waveform(entity.getWave());
        }
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public short getSampleYear() {
        return sampleYear;
    }

    public void setSampleYear(short sampleYear) {
        this.sampleYear = sampleYear;
    }

    public byte getSampleMonth() {
        return sampleMonth;
    }

    public void setSampleMonth(byte sampleMonth) {
        this.sampleMonth = sampleMonth;
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public float getSensitivityValue() {
        return sensitivityValue;
    }

    public void setSensitivityValue(float sensitivityValue) {
        this.sensitivityValue = sensitivityValue;
    }

    public Waveform getWave() {
        return wave;
    }

    public void setWave(Waveform wave) {
        this.wave = wave;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.customerAccount);
        hash = 71 * hash + Objects.hashCode(this.siteId);
        hash = 71 * hash + Objects.hashCode(this.areaId);
        hash = 71 * hash + Objects.hashCode(this.assetId);
        hash = 71 * hash + Objects.hashCode(this.pointLocationId);
        hash = 71 * hash + Objects.hashCode(this.pointId);
        hash = 71 * hash + this.sampleYear;
        hash = 71 * hash + this.sampleMonth;
        hash = 71 * hash + Objects.hashCode(this.sampleTime);
        hash = 71 * hash + Float.floatToIntBits(this.sensitivityValue);
        hash = 71 * hash + Objects.hashCode(this.wave);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WaveData other = (WaveData) obj;
        if (this.sampleYear != other.sampleYear) {
            return false;
        }
        if (this.sampleMonth != other.sampleMonth) {
            return false;
        }
        if (Float.floatToIntBits(this.sensitivityValue) != Float.floatToIntBits(other.sensitivityValue)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.wave, other.wave)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WaveData{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", sampleYear=" + sampleYear + ", sampleMonth=" + sampleMonth + ", sampleTime=" + sampleTime + ", sensitivityValue=" + sensitivityValue + ", wave=" + wave + '}';
    }
    
}
