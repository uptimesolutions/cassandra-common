/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "trend_dc_values")
public class TrendDcValues {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @PartitionKey(2)
    @CqlName(value = "area_id")
    private UUID areaId;
        
    @PartitionKey(3)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @PartitionKey(4)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @PartitionKey(5)
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @PartitionKey(6)
    @CqlName(value = "ap_set_id")
    private UUID apSetId;
    
    @ClusteringColumn
    @CqlName(value = "sample_time")
    private Instant sampleTime;
    
    @CqlName(value = "al_set_id")
    private UUID alSetId;
    
    @CqlName(value = "label")
    private String label;

    @CqlName(value = "units")
    private String units;
   
    @CqlName(value = "notes")
    private String notes;
    
    @CqlName(value = "value")
    private float value;
    
    @CqlName(value = "high_alert")
    private float highAlert;
    
    @CqlName(value = "high_fault")
    private float highFault;
    
    @CqlName(value = "low_alert")
    private float lowAlert;
    
    @CqlName(value = "low_fault")
    private float lowFault;

    @CqlName(value = "sensor_type")
    private String sensorType;

    public TrendDcValues() {
    }
    
    public TrendDcValues(TrendDcValues entity) {
        this.alSetId = entity.getAlSetId();
        this.apSetId = entity.getApSetId();
        this.areaId = entity.getAreaId();
        this.assetId = entity.getAssetId();
        this.customerAccount = entity.getCustomerAccount();
        this.highAlert = entity.getHighAlert();
        this.highFault = entity.getLowFault();
        this.lowAlert = entity.getLowAlert();
        this.lowFault = entity.getLowFault();
        this.notes = entity.getNotes();
        this.label = entity.getLabel();
        this.pointId = entity.getPointId();
        this.pointLocationId = entity.getPointLocationId();
        this.sampleTime = entity.getSampleTime();
        this.sensorType = entity.getSensorType();
        this.siteId = entity.getSiteId();
        this.value = entity.getValue();
        this.units = entity.getUnits();

    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        hash = 37 * hash + Objects.hashCode(this.areaId);
        hash = 37 * hash + Objects.hashCode(this.assetId);
        hash = 37 * hash + Objects.hashCode(this.pointLocationId);
        hash = 37 * hash + Objects.hashCode(this.pointId);
        hash = 37 * hash + Objects.hashCode(this.apSetId);
        hash = 37 * hash + Objects.hashCode(this.sampleTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrendDcValues other = (TrendDcValues) obj;
        if (Float.floatToIntBits(this.value) != Float.floatToIntBits(other.value)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        if (!Objects.equals(this.units, other.units)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TrendDcValues{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", sampleTime=" + sampleTime + ", alSetId=" + alSetId + ", label=" + label + ", units=" + units + ", notes=" + notes + ", value=" + value + ", highAlert=" + highAlert + ", highFault=" + highFault + ", lowAlert=" + lowAlert + ", lowFault=" + lowFault + ", sensorType=" + sensorType + '}';
    }

}
