/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author aswani
 */
public class UserPreferencesMapperImpl {
    private static UserPreferencesMapper instance = null;

    public static UserPreferencesMapper getInstance() {
        if (instance == null) {
            new UserPreferencesMapperImpl();
        }
        return instance;
    }

    private UserPreferencesMapperImpl() {
        instance = new UserPreferencesMapperBuilder(CassandraData.getSession()).build();
    }
}
