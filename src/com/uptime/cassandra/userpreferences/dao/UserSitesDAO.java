/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface UserSitesDAO extends BaseDAO<UserSites> {

     Logger CQL_LOG = LoggerFactory.getLogger(UserSitesDAO.class.getName());

    /**
     * Inserts the given userSites into the user_sites table
     *
     * @param entity, UserSites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(UserSites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given userSites into the user_sites table
     *
     * @param entity, UserSites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(UserSites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Delete the given userSites into the user_sites table
     *
     * @param entity, UserSites Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(UserSites entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns UserSites Objects for the given userId from user_sites table
     *
     * @param userId, String Object
     * @return PagingIterable of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<UserSites> queryByUserId(String userId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns UserSites Objects for the given userId from user_sites table
     *
     * @param userId, String Object
     * @return List of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<UserSites> findByUserId(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (userId == null || userId.length() < 1) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        return queryByUserId(userId).all();
    }

    /**
     * Returns UserSites Objects for the given userId and customerAccount from user_sites table
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @return PagingIterable of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<UserSites> queryByUserIdCustomerAccount(String userId, String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns UserSites Objects for the given userId and customerAccount from user_sites table
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @return List of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<UserSites> findByUserIdCustomerAccount(String userId, String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (userId == null || userId.length() < 1) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customerAccount is required.");
        }
        return queryByUserIdCustomerAccount(userId, customerAccount).all();
    }

    /**
     * Returns UserSites Objects for the given userId, customerAccount, and siteId from user_sites table
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return PagingIterable of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<UserSites> queryByPK(String userId, String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns UserSites Objects for the given userId, customerAccount, and siteId from user_sites table
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<UserSites> findByPK(String userId, String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (userId == null || userId.length() < 1) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customerAccount is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        return queryByPK(userId, customerAccount, siteId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, UserSites Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(UserSites entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getUserId() != null && !entity.getUserId().isEmpty()
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO user_sites(userid");
                    values.append("'").append(entity.getUserId()).append("'");
                    query.append(",customer_acct");
                    values.append(",'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    if (entity.getSiteRole() != null) {
                        query.append(",site_role");
                        values.append(",'").append(entity.getSiteRole()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM user_sites WHERE userid='").append(entity.getUserId()).append("'");
                    query.append(" AND customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
