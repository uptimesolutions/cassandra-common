/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface AlarmUserSiteNotificationDAO extends BaseDAO<AlarmUserSiteNotification> {

     Logger CQL_LOG = LoggerFactory.getLogger(AlarmUserSiteNotificationDAO.class.getName());

    /**
     * Inserts the given alarmSiteNotification into the alarm_user_site_notification table
     *
     * @param entity, AlarmUserSiteNotification Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(AlarmUserSiteNotification entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        if (entity.getSiteId() == null) {
            throw new IllegalArgumentException("Argument Site id cannot be null or empty.");
        }
        if (entity.getUserId() == null) {
            throw new IllegalArgumentException("Argument User id cannot be null or empty.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of AlarmUserSiteNotification into the alarm_user_site_notification table
     *
     * @param entities, List Object of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<AlarmUserSiteNotification> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * Updates the given alarmSiteNotification into the alarm_user_site_notification table
     *
     * @param entity, AlarmUserSiteNotification Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(AlarmUserSiteNotification entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        if (entity.getSiteId() == null) {
            throw new IllegalArgumentException("Argument Site id cannot be null or empty.");
        }
        if (entity.getUserId() == null) {
            throw new IllegalArgumentException("Argument User id cannot be null or empty.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Delete the given alarmSiteNotification from the alarm_user_site_notification table
     *
     * @param entity, AlarmUserSiteNotification Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(AlarmUserSiteNotification entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        if (entity.getSiteId() == null) {
            throw new IllegalArgumentException("Argument Site id cannot be null or empty.");
        }
        if (entity.getUserId() == null) {
            throw new IllegalArgumentException("Argument User id cannot be null or empty.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Delete the given List of AlarmUserSiteNotification from the alarm_user_site_notification table
     *
     * @param entities, List Object of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<AlarmUserSiteNotification> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Returns AlarmUserSiteNotification Objects for the given siteId and userId from alarm_user_site_notification table
     *
     * @param userId, String Object
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<AlarmUserSiteNotification> queryByUserId(String userId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns AlarmUserSiteNotification Objects for the given siteId and userId from alarm_user_site_notification table
     *
     * @param userId, String Object
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AlarmUserSiteNotification> findByUserId(String userId) throws UnavailableException, ReadTimeoutException {
        if (userId == null) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        return queryByUserId(userId).all();
    }

    /**
     * Returns AlarmUserSiteNotification Objects for the given siteId and userId from alarm_user_site_notification table
     *
     * @param userId, String Object
     * @param siteId, UUID Object
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    @Select
    PagingIterable<AlarmUserSiteNotification> queryByPK(String userId, UUID siteId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns AlarmUserSiteNotification Objects for the given siteId and userId from alarm_user_site_notification table
     *
     * @param userId, String Object
     * @param siteId, UUID Object
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AlarmUserSiteNotification> findByPK(String userId, UUID siteId) throws UnavailableException, ReadTimeoutException {
        if (userId == null) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        return queryByPK(userId, siteId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, AlarmUserSiteNotification Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(AlarmUserSiteNotification entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getUserId() != null && !entity.getUserId().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO alarm_user_site_notification(userid, site_id");
                    values.append(entity.getUserId());
                    values.append(",'").append(entity.getSiteId()).append("'");

                    if (entity.getEmailAddress() != null) {
                        query.append(",email_address");
                        values.append(",'").append(entity.getEmailAddress()).append("'");
                    }
                    query.append(",fault");
                    values.append(",").append(entity.isFault());
                    query.append(",alert");
                    values.append(",").append(entity.isAlert());
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM alarm_user_site_notification WHERE site_id=")
                            .append(entity.getSiteId()).append(" and userid='")
                            .append(entity.getUserId()).append("';");
                }
                break;
            }
        }
        return query.toString();
    }

}
