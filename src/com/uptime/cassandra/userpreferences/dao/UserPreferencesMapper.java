/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;

/**
 *
 * @author aswani
 */
@Mapper
public interface UserPreferencesMapper {

    @DaoFactory
    CustomersDAO customersDAO();
    
    @DaoFactory
    UserSitesDAO userSitesDAO();
    
    @DaoFactory
    SiteUsersDAO siteUsersDAO();
    
    @DaoFactory
    UserPreferencesDAO userPreferencesDAO();
    
    @DaoFactory
    AlarmUserAssetNotificationDAO alarmUserAssetNotificationDAO();
   
    @DaoFactory
    AlarmUserSiteNotificationDAO alarmUserSiteNotificationDAO();
    
    @DaoFactory
    AlarmAssetNotificationDAO alarmAssetNotificationDAO();
    
    @DaoFactory
    AlarmSiteNotificationDAO alarmSiteNotificationDAO();
    
    @DaoFactory
    PasswordTokenDAO passwordTokenDAO();
}
