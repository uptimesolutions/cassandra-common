/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface UserPreferencesDAO extends BaseDAO<UserPreferences> {

    Logger CQL_LOG = LoggerFactory.getLogger(UserPreferencesDAO.class.getName());

    /**
     * Inserts the given userPreferences into the user_preferences table
     *
     * @param entity, UserPreferences Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(UserPreferences entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given userPreferences into the user_preferences table
     *
     * @param entity, UserPreferences Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(UserPreferences entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Delete the given userPreferences from the user_preferences table
     *
     * @param entity, UserPreferences Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(UserPreferences entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns UserPreferences Objects for the given userId from user_preferences table
     *
     * @param userId, String Object
     * @return PagingIterable of serPreferences Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<UserPreferences> queryByPK(String userId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns UserPreferences Objects for the given userId from user_preferences table
     *
     * @param userId, String Object
     * @return List of UserPreferences Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<UserPreferences> findByPK(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (userId == null || userId.length() < 1) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        return queryByPK(userId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, UserPreferences Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(UserPreferences entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getUserId() != null && !entity.getUserId().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO user_preferences(userid");
                    values.append("'").append(entity.getUserId()).append("'");
                    if (entity.getCustomerAccount() != null) {
                        query.append(",customer_acct");
                        values.append(",'").append(entity.getCustomerAccount()).append("'");
                    }
                    if (entity.getLanguage() != null) {
                        query.append(",language");
                        values.append(",'").append(entity.getLanguage()).append("'");
                    }
                    if (entity.getTimezone() != null) {
                        query.append(",timezone");
                        values.append(",'").append(entity.getTimezone()).append("'");
                    }
                    if (entity.getDefaultSite() != null) {
                        query.append(",default_site");
                        values.append(",").append(entity.getDefaultSite());
                    }
                    if (entity.getDefaultUnits() != null && !entity.getDefaultUnits().isEmpty()) {
                        query.append(",default_units");
                        values.append(",'").append(entity.getDefaultUnits().trim()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM user_preferences WHERE userid='").append(entity.getUserId()).append("';");
                }
                break;
            }
        }
        return query.toString();
    }

}
