/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.userpreferences.entity.Customers;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface CustomersDAO extends BaseDAO<Customers> {

     Logger CQL_LOG = LoggerFactory.getLogger(CustomersDAO.class.getName());

    /**
     * Inserts the given entity into the customers table
     *
     * @param entity, Customers Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(Customers entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * update the given entity in the customers table
     *
     * @param entity, Customers Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(Customers entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Deletes the given entity from the customers table.
     *
     * @param entity, Customers Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(Customers entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Returns Customers Objects for the given customerAccount from customers table
     *
     * @param customerAccount, String Object
     * @return PagingIterable of Customers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<Customers> queryByPK(String customerAccount) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns a List of Customers Objects for the given customerAccount from customers table
     *
     * @param customerAccount, String Object
     * @return List of Customers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<Customers> findByPK(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.isEmpty()) {
            throw new IllegalArgumentException("Argument customerAccount cannot be null or empty.");
        }
        return queryByPK(customerAccount).all();
    }

    /**
     * Fetch all the records from the customers table
     *
     * @return PagingIterable of Customers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("select * from customers")
    public PagingIterable<Customers> queryAllCustomers() throws UnavailableException, WriteTimeoutException;

    /**
     * Fetches the all the entities from the customers table.
     *
     * @return List of Customers Objects
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public List<Customers> findAllCustomers() throws UnavailableException, WriteTimeoutException {
        return queryAllCustomers().all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, Customers Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(Customers entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO customers(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    if (entity.getCustomerName() != null && !entity.getCustomerName().isEmpty()) {
                        query.append(",customer_name");
                        values.append(",'").append(entity.getCustomerName()).append("'");
                    }
                    if (entity.getFqdn() != null && !entity.getFqdn().isEmpty()) {
                        query.append(",fqdn");
                        values.append(",'").append(entity.getFqdn()).append("'");
                    }
                    if (entity.getLogo() != null && !entity.getLogo().isEmpty()) {
                        query.append(",logo");
                        values.append(",'").append(entity.getLogo()).append("'");
                    }
                    if (entity.getSkin() != null && !entity.getSkin().isEmpty()) {
                        query.append(",skin");
                        values.append(",'").append(entity.getSkin()).append("'");
                    }
                    if (entity.getSloUrl() != null && !entity.getSloUrl().isEmpty()) {
                        query.append(",slo_url");
                        values.append(",'").append(entity.getSloUrl()).append("'");
                    }
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM customers WHERE customer_acct='").append(entity.getCustomerAccount()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }
}
