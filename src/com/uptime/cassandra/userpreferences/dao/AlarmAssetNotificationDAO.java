/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface AlarmAssetNotificationDAO extends BaseDAO<AlarmAssetNotification>{
     Logger CQL_LOG = LoggerFactory.getLogger(AlarmAssetNotificationDAO.class.getName());
    /**
     * Inserts the given alarmAssetNotification into the alarm_asset_notification table
     * @param entity, AlarmAssetNotification Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(AlarmAssetNotification entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        if (entity.getUserId() == null) {
            throw new IllegalArgumentException("Argument User id cannot be null or empty.");
        }
        if (entity.getAssetId()== null) {
            throw new IllegalArgumentException("Argument Asset id cannot be null or empty.");
        }
        try {
            createLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }
    
    /**
     * Inserts the given List of AlarmAssetNotification into the alarm_asset_notification table
     * @param entities, List Object of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<AlarmAssetNotification> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }


    /**
     * Updates the given alarmAssetNotification into the alarm_asset_notification table
     * @param entity, AlarmAssetNotification Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void update(AlarmAssetNotification entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        if (entity.getUserId() == null) {
            throw new IllegalArgumentException("Argument User id cannot be null or empty.");
        }
        if (entity.getAssetId()== null) {
            throw new IllegalArgumentException("Argument Asset id cannot be null or empty.");
        }
        try {
            updateLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Delete the given alarmAssetNotification from the alarm_asset_notification table
     * @param entity, AlarmAssetNotification Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void delete(AlarmAssetNotification entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        if (entity.getAssetId() == null) {
            throw new IllegalArgumentException("Argument Asset id cannot be null or empty.");
        }
        if (entity.getUserId() == null) {
            throw new IllegalArgumentException("Argument User id cannot be null or empty.");
        }
        try {
            deleteLocal(entity);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }
    
    /**
     * Delete the given List of AlarmAssetNotification from the alarm_asset_notification table
     * @param entities, List Object of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<AlarmAssetNotification> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }


    /**
     * Returns AlarmAssetNotification Objects for the given assetId from alarm_asset_notification table
     * @param assetId, UUID Object
     * @return PagingIterable of serPreferences Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmAssetNotification> queryByAsset(UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns AlarmAssetNotification Objects for the given assetId from alarm_asset_notification table
     * @param assetId, UUID Object
     * @return List of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AlarmAssetNotification> findByAsset(UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        return queryByAsset(assetId).all();
    }
        
    /**
     * Returns AlarmAssetNotification Objects for the given assetId and userId from alarm_asset_notification table
     * @param assetId, UUID Object
     * @param userId, String Object
     * @return PagingIterable of serPreferences Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmAssetNotification> queryByPK(UUID assetId, String userId) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns AlarmAssetNotification Objects for the given assetId and userId from alarm_asset_notification table
     * @param assetId, UUID Object
     * @param userId, String Object
     * @return List of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<AlarmAssetNotification> findByPK(UUID assetId, String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (userId == null || userId.length() < 1) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        return queryByPK(assetId, userId).all();
    }
        
    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     * @param entity, AlarmAssetNotification Object
     * @param statementType, String Object
     * @return String Object
     */
    default String printCQL(AlarmAssetNotification entity, String statementType) {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getUserId() != null && !entity.getUserId().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO alarm_asset_notification(asset_id, userid");
                    values.append(entity.getAssetId());
                    values.append(",'").append(entity.getUserId()).append("'");
                    if (entity.getEmailAddress()!= null) {
                        query.append(",email_address");
                        values.append(",'").append(entity.getEmailAddress()).append("'");
                    }
                    query.append(",fault");
                    values.append(",").append(entity.isFault());
                    query.append(",alert");
                    values.append(",").append(entity.isAlert());
                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM alarm_asset_notification WHERE asset_id=")
                            .append(entity.getAssetId()).append(" and userid='")
                            .append(entity.getUserId()).append("';");
                }
                break;
            }
        }
        return query.toString();
    }
    
}
