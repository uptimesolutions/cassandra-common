/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Insert;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.userpreferences.entity.PasswordToken;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface PasswordTokenDAO {

     Logger CQL_LOG = LoggerFactory.getLogger(PasswordTokenDAO.class.getName());

    /**
     * Inserts the given PasswordToken into the password_token table with a ttl (Time To Live) value
     *
     * @param passwordToken, PasswordToken Object
     * @param ttl, int
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Insert(ttl = ":ttl")
    void createLocalWithTtl(PasswordToken passwordToken, int ttl) throws UnavailableException, WriteTimeoutException;

    /**
     * Inserts the given PasswordToken into the password_token table with a ttl (Time To Live) value
     *
     * @param entity, PasswordToken Object
     * @param ttl, int
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default void create(PasswordToken entity, int ttl) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity can't be null.");
        }
        try {
            createLocalWithTtl(entity, ttl);
            if (CQL_LOG != null) {
                CQL_LOG.info(printCQL(entity, ttl));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (CQL_LOG != null) {
                CQL_LOG.error(printCQL(entity, ttl));
            }
            throw e;
        }
    }

    /**
     * Returns PasswordToken Objects for the given userId and passwordToken from password_token table
     *
     * @param userId, String Object
     * @param passwordToken, UUID Object
     * @return PagingIterable of PasswordToken Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<PasswordToken> queryByPK(String userId, UUID passwordToken) throws UnavailableException, ReadTimeoutException;

    /**
     * Returns PasswordToken Objects for the given userId and passwordToken from password_token table
     *
     * @param userId, String Object
     * @param passwordToken, UUID Object
     * @return List of PasswordToken Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default List<PasswordToken> findByPK(String userId, UUID passwordToken) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (userId == null || userId.length() < 1) {
            throw new IllegalArgumentException("Argument userId is required.");
        }
        if (passwordToken == null) {
            throw new IllegalArgumentException("Argument passwordToken is required.");
        }
        return queryByPK(userId, passwordToken).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and ttl
     *
     * @param entity, PasswordToken Object
     * @param ttl, int
     * @return String Object
     */
    default String printCQL(PasswordToken entity, int ttl) {
        StringBuilder query;

        query = new StringBuilder();
        if (entity != null && entity.getPasswordToken() != null
                && entity.getUserId() != null && !entity.getUserId().isEmpty()) {
            query
                    .append("INSERT INTO password_token(userid,pwd_token) values(")
                    .append("'").append(entity.getUserId()).append("'")
                    .append(",").append(entity.getPasswordToken())
                    .append(") USING TTL ").append(ttl).append(";");
        }

        return query.toString();
    }
}
