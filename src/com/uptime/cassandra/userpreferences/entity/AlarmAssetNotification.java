/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "alarm_asset_notification")
public class AlarmAssetNotification {
    
    @PartitionKey(0)
    @CqlName(value = "asset_id")
    UUID assetId;
    
    @ClusteringColumn(0)
    @CqlName(value = "userid")
    String userId;

    @CqlName(value = "email_address")
    String emailAddress ;
    
    @CqlName(value = "fault")
    private boolean fault;

    @CqlName(value = "alert")
    private boolean alert;

    public AlarmAssetNotification() {
    }

    public AlarmAssetNotification(AlarmAssetNotification entity) {
        userId = entity.getUserId();
        assetId = entity.getAssetId();
        emailAddress = entity.getEmailAddress();
        fault = entity.isFault();
        alert = entity.isAlert();
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isFault() {
        return fault;
    }

    public void setFault(boolean fault) {
        this.fault = fault;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.assetId);
        hash = 47 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmAssetNotification other = (AlarmAssetNotification) obj;
        if (this.fault != other.fault) {
            return false;
        }
        if (this.alert != other.alert) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.emailAddress, other.emailAddress)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlarmSiteNotification{" + "assetId=" + assetId + ", userId=" + userId + ", emailAddress=" + emailAddress + ", fault=" + fault + ", alert=" + alert + '}';
    }

}
