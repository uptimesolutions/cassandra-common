/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "user_preferences")
public class UserPreferences {
    
    @PartitionKey(0)
    @CqlName(value = "userid")
    String userId;

    @CqlName(value = "customer_acct")
    String customerAccount;
    
    @CqlName(value = "default_site")
    UUID defaultSite;
    
    @CqlName(value = "default_units")
    String defaultUnits;

    @CqlName(value = "language")
    String language;

    @CqlName(value = "timezone")
    String timezone;

    public UserPreferences() {
    }

    public UserPreferences(UserPreferences entity) {
        userId = entity.getUserId();
        customerAccount = entity.getCustomerAccount();
        defaultSite = entity.getDefaultSite();
        defaultUnits = entity.getDefaultUnits();
        language = entity.getLanguage();
        timezone = entity.getTimezone();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getDefaultSite() {
        return defaultSite;
    }

    public void setDefaultSite(UUID defaultSite) {
        this.defaultSite = defaultSite;
    }

    public String getDefaultUnits() {
        return defaultUnits;
    }

    public void setDefaultUnits(String defaultUnits) {
        this.defaultUnits = defaultUnits;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserPreferences other = (UserPreferences) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        if (!Objects.equals(this.timezone, other.timezone)) {
            return false;
        }
        if (!Objects.equals(this.defaultSite, other.defaultSite)) {
            return false;
        }
        if (!Objects.equals(this.defaultUnits, other.defaultUnits)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserPreferences{" + "userId=" + userId + ", customerAccount=" + customerAccount + ", defaultSite=" + defaultSite + ", defaultUnits=" + defaultUnits + ", language=" + language + ", timezone=" + timezone + "}";
    }
}
