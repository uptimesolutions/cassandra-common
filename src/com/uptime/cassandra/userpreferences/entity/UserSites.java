/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
@Entity
@CqlName(value = "user_sites")
public class UserSites {

    @PartitionKey(0)
    @CqlName(value = "userid")
    String userId;

    @CqlName(value = "customer_acct")
    @ClusteringColumn(0)
    String customerAccount;

    @CqlName(value = "site_id")
    @ClusteringColumn(1)
    UUID siteId;

    @CqlName(value = "site_role")
    String siteRole;

    public UserSites() {
    }

    public UserSites(UserSites entity) {
        userId = entity.getUserId();
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        siteRole = entity.getSiteRole();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteRole() {
        return siteRole;
    }

    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.userId);
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserSites other = (UserSites) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteRole, other.siteRole)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserSites{" + "userId=" + userId + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteRole=" + siteRole + '}';
    }

}
