/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;


/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "customers")
public class Customers {
    @PartitionKey
    @CqlName(value = "customer_acct")
    String customerAccount;
    
    @CqlName(value = "customer_name")
    String customerName;

    @CqlName(value = "fqdn")
    String fqdn;

    @CqlName(value = "logo")
    String logo;

    @CqlName(value = "skin")
    String skin;

    @CqlName(value = "slo_url")
    String sloUrl;
       
    public Customers() {
    }

    public Customers(Customers entity) {
        customerAccount = entity.getCustomerAccount();
        customerName = entity.getCustomerName();
        fqdn = entity.getFqdn();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getSloUrl() {
        return sloUrl;
    }

    public void setSloUrl(String sloUrl) {
        this.sloUrl = sloUrl;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.customerName);
        hash = 97 * hash + Objects.hashCode(this.fqdn);
        hash = 97 * hash + Objects.hashCode(this.logo);
        hash = 97 * hash + Objects.hashCode(this.skin);
        hash = 97 * hash + Objects.hashCode(this.sloUrl);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customers other = (Customers) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.customerName, other.customerName)) {
            return false;
        }
        if (!Objects.equals(this.fqdn, other.fqdn)) {
            return false;
        }
        if (!Objects.equals(this.logo, other.logo)) {
            return false;
        }
        if (!Objects.equals(this.skin, other.skin)) {
            return false;
        }
        return Objects.equals(this.sloUrl, other.sloUrl);
    }

    @Override
    public String toString() {
        return "Customers{" + "customerAccount=" + customerAccount + ", customerName=" + customerName + ", fqdn=" + fqdn + ", logo=" + logo + ", skin=" + skin + ", sloUrl=" + sloUrl + "}";
    }
}
