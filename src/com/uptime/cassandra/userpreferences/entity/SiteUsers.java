/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "site_users")
public class SiteUsers {

    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    String customerAccount;
    
    @ClusteringColumn(0)
    @CqlName(value = "site_id")
    UUID siteId;
    
    @ClusteringColumn(1)
    @CqlName(value = "userid")
    String userId;

    @CqlName(value = "first_name")
    String firstName;
    
    @CqlName(value = "last_name")
    String lastName;
    
    @CqlName(value = "site_role")
    String siteRole;

    public SiteUsers() {
    }

    public SiteUsers(SiteUsers entity) {
        userId = entity.getUserId();
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        siteRole = entity.getSiteRole();
        firstName = entity.getFirstName();
        lastName = entity.getLastName();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteRole() {
        return siteRole;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.userId);
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteUsers other = (SiteUsers) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.siteRole, other.siteRole)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteUsers{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", siteRole=" + siteRole + '}';
    }

}
