/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
@Entity
@CqlName(value = "alarm_user_asset_notification")
public class AlarmUserAssetNotification {

    @PartitionKey(0)
    @CqlName(value = "userid")
    String userId;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "asset_id")
    UUID assetId;

    @CqlName(value = "email_address")
    String emailAddress;

    @CqlName(value = "alert")
    private boolean alert;

    @CqlName(value = "fault")
    private boolean fault;

    public AlarmUserAssetNotification() {
    }

    public AlarmUserAssetNotification(AlarmUserAssetNotification entity) {
        this.userId = entity.getUserId();
        this.siteId = entity.getSiteId();
        this.assetId = entity.getAssetId();
        this.emailAddress = entity.getEmailAddress();
        this.alert = entity.isAlert();
        this.fault = entity.isFault();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public boolean isFault() {
        return fault;
    }

    public void setFault(boolean fault) {
        this.fault = fault;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.userId);
        hash = 43 * hash + Objects.hashCode(this.siteId);
        hash = 43 * hash + Objects.hashCode(this.assetId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmUserAssetNotification other = (AlarmUserAssetNotification) obj;
        if (this.alert != other.alert) {
            return false;
        }
        if (this.fault != other.fault) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.emailAddress, other.emailAddress)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlarmUserAssetNotification{" + "userId=" + userId + ", siteId=" + siteId + ", assetId=" + assetId + ", emailAddress=" + emailAddress + ", alert=" + alert + ", fault=" + fault + '}';
    }

}
