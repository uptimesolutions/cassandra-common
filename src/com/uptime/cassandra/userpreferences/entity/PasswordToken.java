/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
@Entity
@CqlName(value = "password_token")
public class PasswordToken {
    @PartitionKey
    @CqlName(value = "userid")
    String userId;

    @ClusteringColumn
    @CqlName(value = "pwd_token")
    UUID passwordToken;

    public PasswordToken() {
    }

    public PasswordToken(PasswordToken entity) {
        userId = entity.getUserId();
        passwordToken = entity.getPasswordToken();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UUID getPasswordToken() {
        return passwordToken;
    }

    public void setPasswordToken(UUID passwordToken) {
        this.passwordToken = passwordToken;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.userId);
        hash = 89 * hash + Objects.hashCode(this.passwordToken);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PasswordToken other = (PasswordToken) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.passwordToken, other.passwordToken)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PasswordToken{" + "userId=" + userId + ", passwordToken=" + passwordToken + '}';
    }
}
