/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
@Entity
@CqlName(value = "alarm_user_site_notification")
public class AlarmUserSiteNotification {

    @PartitionKey(0)
    @CqlName(value = "userid")
    String userId;

    @ClusteringColumn(0)
    @CqlName(value = "site_id")
    UUID siteId;

    @CqlName(value = "email_address")
    String emailAddress;

    @CqlName(value = "alert")
    private boolean alert;

    @CqlName(value = "fault")
    private boolean fault;

    public AlarmUserSiteNotification() {
    }

    public AlarmUserSiteNotification(AlarmUserSiteNotification entity) {
        this.userId = entity.getUserId();
        this.siteId = entity.getSiteId();
        this.emailAddress = entity.getEmailAddress();
        this.alert = entity.isAlert();
        this.fault = entity.isFault();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public boolean isFault() {
        return fault;
    }

    public void setFault(boolean fault) {
        this.fault = fault;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.userId);
        hash = 59 * hash + Objects.hashCode(this.siteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmUserSiteNotification other = (AlarmUserSiteNotification) obj;
        if (this.alert != other.alert) {
            return false;
        }
        if (this.fault != other.fault) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.emailAddress, other.emailAddress)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlarmUserSiteNotification{" + "userId=" + userId + ", siteId=" + siteId + ", emailAddress=" + emailAddress + ", alert=" + alert + ", fault=" + fault + '}';
    }

}
