/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.entity;

import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import java.util.UUID;



/**
 *
 * @author mbathala
 */
@Entity
@CqlName( value = "events")
public class Events {

    @CqlName(value = "application")
    @PartitionKey
    private String application;
    
    @ClusteringColumn
    @CqlName(value = "created_date")
    private UUID createdDate;
    
    @CqlName(value = "data")
    private String data;
    @CqlName(value = "ip_address")
    private String ipAddress;
    @CqlName(value = "port")
    private String port;

    public Events() {
    }

    public Events(Row row) {
        this.application = row.getString("application");
        this.createdDate = row.getUuid("created_date");
        this.data = row.getString("data");
        this.ipAddress = row.getString("ip_address");
        this.port = row.getString("port");
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public UUID getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(UUID createdDate) {
        this.createdDate = createdDate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

}
