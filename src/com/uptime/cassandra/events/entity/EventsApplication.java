/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.entity;

import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;

/**
 *
 * @author ksimmons
 */
@Entity
@CqlName(value = "events_application") 
public class EventsApplication {
    @PartitionKey
    @CqlName(value = "application")
    private String application;
    @CqlName(value = "email")
    private String email;
    @CqlName(value = "notification")
    private Boolean notification;
    
    public EventsApplication() {
    }

    public EventsApplication(Row row) {
        this.application = row.getString("application");
        this.email = row.getString("email");
        this.notification = row.getBoolean("notification");
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }
}
