/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.events.entity.EventsApplication;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface  EventsApplicationDAO extends BaseDAO<EventsApplication> {
    
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(EventsApplicationDAO.class.getName());
    /**
     * Inserts the given eventApplication into the event_application table
     *
     * @param entity the event_application to insert
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(EventsApplication entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        if (entity.getApplication() == null || entity.getApplication().length() == 0) {
            throw new IllegalArgumentException("Argument application cannot be null or empty.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }
    
    /**
     * Updates the given EventApplication in the event_application table.
     *
     * @param entity the EventApplication to update
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(EventsApplication entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }
    
    /**
     * Deletes the given EventApplication from the event_application table.
     *
     * @param entity the EventApplication to delete
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(EventsApplication entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }
    
    @Select
    PagingIterable<EventsApplication> queryByApplication(String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    default List<EventsApplication> findByApplication(String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (application == null || application.length() < 1) {
            throw new IllegalArgumentException("Argument application is required.");
        }
        List<EventsApplication> eventApp = queryByApplication(application).all();
        return eventApp;
    }
    
    /**
     * Prints the CQL statement to the log file
     *
     * @param application
     * @param statementType can be "INSERT" or "DELETE"
     * @return
     */
    default String printCQL(EventsApplication application, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO event_application(application");
                values.append("'").append(application.getApplication()).append("'");

                if (application.getEmail() != null) {
                    query.append(",email");
                    values.append(",'").append(application.getEmail()).append("'");
                }
                if (application.getNotification() != null) {
                    query.append(",notification");
                    values.append(",'").append(application.getNotification()).append("'");
                }
                
                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM event_application WHERE application='").append(application.getApplication()).append("'");
                query.append(" AND email='").append(application.getEmail()).append("'");
                query.append(" AND notification='").append(application.getNotification()).append("';");
            }
            break;
        }

        //System.out.println(query.toString());
        return query.toString();
    }
}
