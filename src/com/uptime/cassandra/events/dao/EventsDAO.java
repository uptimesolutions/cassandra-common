/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.events.entity.Events;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author ksimmons
 */
@DefaultNullSavingStrategy(DO_NOT_SET)
@Dao
public interface EventsDAO extends BaseDAO<Events>{
     Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(EventsDAO.class.getName());

    /**
     * Inserts the given event into the events table
     *
     * @param entity the events to insert
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(Events entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        if (entity.getCreatedDate() == null) {
            throw new IllegalArgumentException("Argument CreatedDate cannot be null or empty.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }
    
     /**
     * Inserts the given event into the events table
     *
     * @param entity the events to insert
     * @param ttl the time-to-live in seconds
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void create(Events entity, int ttl) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        create(entity);
    }
    
    /**
     * Updates the given event in the events table.
     *
     * @param entity the events to update
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(Events entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Updates the given event in the events table.
     *
     * @param entity the events to update
     * @param ttl the time-to-live in seconds
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void update(Events entity, int ttl) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        update(entity);
    }

    /**
     * Deletes the given event from the events table.
     *
     * @param entity the events to delete
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the entity is null
     */
    default void delete(Events entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Gets the list of events for the given application
     *
     * @param application
     * @return the list of events
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the application is null or empty string
     */
    @Select
    PagingIterable<Events> queryByApplication(String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException;

    default List<Events> findByApplication(String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (application == null || application.length() < 1) {
            throw new IllegalArgumentException("Argument application is required.");
        }
        List<Events> events = queryByApplication(application).all();
        return events;
    }
        
    /**
     * Inserts the given events into the events table
     *
     * @param applicationList the events to insert
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the applicationList is null or empty
     */
    default void create(List<Events> applicationList) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (applicationList == null || applicationList.isEmpty()) {
            throw new IllegalArgumentException("Argument applicationList cannot be null or empty.");
        }
        try {
            for (Events app : applicationList) {
                create(app);
            }
        } catch (WriteTimeoutException e) {
            throw e;
        }
    }

    /**
     * Inserts the given events into the events table
     *
     * @param applicationList the events to insert
     * @param ttl the time-to-live in seconds
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the applicationList is null or empty
     */
    default void create(List<Events> applicationList, int ttl) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        create(applicationList);
    }

    /**
     * Updates the given events in the events table.
     *
     * @param applicationList the events to update
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the applicationList is null or empty
     */
    default void update(List<Events> applicationList) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (applicationList == null || applicationList.isEmpty()) {
            throw new IllegalArgumentException("Argument applicationList cannot be null or empty.");
        }
        try {
           for (Events app : applicationList) {
                update(app);
            }
        } catch (WriteTimeoutException e) {          
            throw e;
        }
    }

    /**
     * Updates the given events in the events table.
     *
     * @param applicationList the events to update
     * @param ttl the time-to-live in seconds
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the acctList is applicationList or empty
     */
    default void update(List<Events> applicationList, int ttl) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        update(applicationList);
    }

    /**
     * Deletes the given events from the events table.
     *
     * @param applicationList the events to delete
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the applicationList is null or empty
     */
    default void delete(List<Events> applicationList) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (applicationList == null || applicationList.isEmpty()) {
            throw new IllegalArgumentException("Argument applicationList cannot be null or empty.");
        }
         try {
             for (Events app : applicationList) {
                delete(app);
            }
        } catch (WriteTimeoutException e) {
            throw e;
        }
    }
    
    /**
     * Prints the CQL statement to the log file
     *
     * @param event
     * @param statementType can be "SELECT" or "DELETE"
     * @return
     */
    default String printCQL(Events event, String statementType) {
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        switch (statementType) {
            case ("INSERT"): {
                query.append("INSERT INTO events(application,created_date");
                values.append("'").append(event.getApplication()).append("'");
                values.append(",").append(event.getCreatedDate());
                if (event.getData() != null) {
                    query.append(",data");
                    values.append(",'").append(event.getData()).append("'");
                }
                if (event.getIpAddress() != null) {
                    query.append(",ip_address");
                    values.append(",'").append(event.getIpAddress()).append("'");
                }
                if (event.getPort() != null) {
                    query.append(",port");
                    values.append(",'").append(event.getPort()).append("'");
                }

                query.append(") values(").append(values.toString()).append(");");
            }
            break;
            case ("DELETE"): {
                query.append("DELETE FROM events WHERE application='").append(event.getApplication()).append("'");
                query.append(" AND created_date=").append(event.getCreatedDate()).append(";");
            }
            break;
        }

        return query.toString();
    }
}
