/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author ksimmons
 */
public class EventsMapperImpl {
    private static EventsMapper instance = null;

    public static EventsMapper getInstance() {
        if (instance == null) {
            new EventsMapperImpl();
        }
        return instance;
    }

    private EventsMapperImpl() {
        instance = new EventsMapperBuilder(CassandraData.getSession()).build();
    }
}
