/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;

/**
 *
 * @author ksimmons
 */
@Mapper
public interface EventsMapper {
    @DaoFactory
    EventsDAO eventsDao();
    
    @DaoFactory()
    EventsApplicationDAO eventsApplicationDAO();
}
