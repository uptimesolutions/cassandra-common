/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.alarms.entity.AlarmHistory;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface AlarmHistoryDAO extends BaseDAO<AlarmHistory> {

    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(AlarmHistoryDAO.class);

    /**
     * Inserts the given entity into the alarm_history table
     *
     * @param entity, AlarmHistory Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(AlarmHistory entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) { // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of AlarmHistory into the alarm_history table
     *
     * @param entities, List Object of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<AlarmHistory> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * Update the given entity into the alarm_history table
     *
     * @param entity, AlarmHistory Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(AlarmHistory entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "UPDATE"));
            }
        } catch (WriteTimeoutException e) { // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "UPDATE"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of AlarmHistory into the alarm_history table
     *
     * @param entities, List Object of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(List<AlarmHistory> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Delete the given entity from alarm_history table
     *
     * @param entity, AlarmHistory Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(AlarmHistory entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) { // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given List of AlarmHistory from the alarm_history table
     *
     * @param entities, List Object of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<AlarmHistory> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Truncate the alarm_history table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE alarm_history")
    public void truncateAllAlarmHistory() throws UnavailableException, WriteTimeoutException;

    /**
     * Deletes the all the entities from the alarm_history table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllAlarmHistory();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new AlarmHistory(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new AlarmHistory(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId and pointLocationId from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return PagingIterable of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmHistory> queryByCustomerSiteYearAreaAssetPtLoc(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId and pointLocationId from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AlarmHistory> findByCustomerSiteYearAreaAssetPtLoc(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (createdYear == 0) {
            throw new IllegalArgumentException("Argument createdYear is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        return queryByCustomerSiteYearAreaAssetPtLoc(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId).all();
    }

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId and pointId from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return PagingIterable of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmHistory> queryByCustomerSiteYearAreaAssetPtLocPoint(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId and pointId from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AlarmHistory> findByCustomerSiteYearAreaAssetPtLocPoint(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (createdYear == 0) {
            throw new IllegalArgumentException("Argument createdYear is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        return queryByCustomerSiteYearAreaAssetPtLocPoint(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId).all();
    }

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId, pointId and paramName from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @return PagingIterable of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmHistory> queryByCustomerSiteYearAreaAssetPtLocPointParam(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId, pointId and paramName from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AlarmHistory> findByCustomerSiteYearAreaAssetPtLocPointParam(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (createdYear == 0) {
            throw new IllegalArgumentException("Argument createdYear is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument paramName is required.");
        }

        return queryByCustomerSiteYearAreaAssetPtLocPointParam(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName).all();
    }

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName and alarmType from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return PagingIterable of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmHistory> queryByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName and alarmType from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AlarmHistory> findByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (createdYear == 0) {
            throw new IllegalArgumentException("Argument createdYear is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument paramName is required.");
        }
        if (alarmType == null || alarmType.isEmpty()) {
            throw new IllegalArgumentException("Argument alarmType is required.");
        }

        return queryByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType).all();
    }

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType and rowId from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @param rowId, UUID Object
     * @return PagingIterable of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<AlarmHistory> queryByPK(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType, UUID rowId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the AlarmHistory data by customer account, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType and rowId from the alarm_history table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param createdYear, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @param rowId, UUID Object
     * @return List of AlarmHistory Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<AlarmHistory> findByPK(String customerAccount, UUID siteId, short createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType, UUID rowId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (createdYear == 0) {
            throw new IllegalArgumentException("Argument createdYear is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument paramName is required.");
        }
        if (alarmType == null || alarmType.isEmpty()) {
            throw new IllegalArgumentException("Argument alarmType is required.");
        }
        if (rowId == null) {
            throw new IllegalArgumentException("Argument rowId is required.");
        }
        return queryByPK(customerAccount, siteId, createdYear, areaId, assetId, pointLocationId, pointId, paramName, alarmType, rowId).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, AlarmHistory Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(AlarmHistory entity, String statementType) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null && entity.getAreaId() != null
                && entity.getAssetId() != null && entity.getPointLocationId() != null
                && entity.getPointId() != null
                && entity.getParamName() != null && !entity.getParamName().isEmpty()
                && entity.getAlarmType() != null && !entity.getAlarmType().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO alarm_history(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",created_year");
                    values.append(",").append(entity.getCreatedYear());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",point_location_id");
                    values.append(",").append(entity.getPointLocationId());
                    query.append(",point_id");
                    values.append(",").append(entity.getPointId());
                    query.append(",param_name");
                    values.append(",'").append(entity.getParamName()).append("'");
                    query.append(",row_id");
                    values.append(",").append(entity.getRowId());
                    query.append(",alarm_type");
                    values.append(",'").append(entity.getAlarmType()).append("'");
                    query.append(",exceed_percent");
                    values.append(",").append(entity.getExceedPercent());
                    query.append(",high_alert");
                    values.append(",").append(entity.getHighAlert());
                    query.append(",high_fault");
                    values.append(",").append(entity.getHighFault());
                    query.append(",is_acknowledged");
                    values.append(",").append(entity.isAcknowledged());
                    query.append(",low_alert");
                    values.append(",").append(entity.getLowAlert());
                    query.append(",low_fault");
                    values.append(",").append(entity.getLowFault());

                    if (entity.getParamType() != null) {
                        query.append(",param_type");
                        values.append(",'").append(entity.getParamType()).append("'");
                    }

                    if (entity.getSampleTimestamp() != null) {
                        query.append(",sample_timestamp");
                        values.append(",'").append(entity.getSampleTimestamp()).append("'");
                    }

                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }

                    query.append(",trend_value");
                    values.append(",").append(entity.getTrendValue());

                    if (entity.getTriggerTime() != null) {
                        query.append(",sample_timestamp");
                        values.append(",'").append(entity.getSampleTimestamp()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM alarm_history WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND created_year=").append(entity.getCreatedYear());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND point_location_id=").append(entity.getPointLocationId());
                    query.append(" AND point_id=").append(entity.getPointId());
                    query.append(" AND param_name='").append(entity.getParamName()).append("'");
                    query.append(" AND alarm_type='").append(entity.getAlarmType()).append("'");
                    query.append(" AND row_id=").append(entity.getRowId()).append(";");
                }
                break;
            }
        }

        return query.toString();
    }

}
