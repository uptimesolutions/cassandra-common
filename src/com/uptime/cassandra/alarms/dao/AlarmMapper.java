/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;

/**
 *
 * @author madhavi
 */
@Mapper
public interface AlarmMapper {

    @DaoFactory
    ActiveAlarmsDAO activeAlarmsDAO();
    
    @DaoFactory
    AlarmCountDAO alarmCountDAO();
    
    @DaoFactory
    AlarmHistoryDAO alarmHistoryDAO();
}
