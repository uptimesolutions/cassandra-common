/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.uptime.cassandra.CassandraData;

/**
 *
 * @author madhavi
 */
public class AlarmMapperImpl {

    private static AlarmMapper instance = null;

    public static AlarmMapper getInstance() {
        if (instance == null) {
            new AlarmMapperImpl();
        }
        return instance;
    }

    private AlarmMapperImpl() {
        instance = new AlarmMapperBuilder(CassandraData.getSession()).build();
    }
}
