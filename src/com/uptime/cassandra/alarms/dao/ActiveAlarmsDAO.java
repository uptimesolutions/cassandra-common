/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.DefaultNullSavingStrategy;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.annotations.Select;
import static com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy.DO_NOT_SET;
import com.uptime.cassandra.BaseDAO;
import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
@Dao
@DefaultNullSavingStrategy(DO_NOT_SET)
public interface ActiveAlarmsDAO extends BaseDAO<ActiveAlarms> {
    Logger SLF4J_CQL_LOG = LoggerFactory.getLogger(ActiveAlarmsDAO.class);

    /**
     * Inserts the given entity into the active_alarms table
     *
     * @param entity, ActiveAlarms Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(ActiveAlarms entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {

        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            createLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "INSERT"));
            }
        } catch (WriteTimeoutException e) { // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "INSERT"));
            }
            throw e;
        }
    }

    /**
     * Inserts the given List of ActiveAlarms into the active_alarms table
     *
     * @param entities, List Object of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void create(List<ActiveAlarms> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> create(entity));
    }

    /**
     * Update the given entity into the active_alarms table
     *
     * @param entity, ActiveAlarms Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(ActiveAlarms entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            updateLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "UPDATE"));
            }
        } catch (WriteTimeoutException e) { // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "UPDATE"));
            }
            throw e;
        }
    }

    /**
     * Update the given list of ActiveAlarms into the active_alarms table
     *
     * @param entities, List Object of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void update(List<ActiveAlarms> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> update(entity));
    }

    /**
     * Delete the given entity from active_alarms table
     *
     * @param entity, ActiveAlarms Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(ActiveAlarms entity) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {

        if (entity == null) {
            throw new IllegalArgumentException("Argument entity cannot be null.");
        }
        try {
            deleteLocal(entity);
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(entity, "DELETE"));
            }
        } catch (WriteTimeoutException e) { // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(entity, "DELETE"));
            }
            throw e;
        }
    }

    /**
     * delete the given List of ActiveAlarms from the active_alarms table
     *
     * @param entities, List Object of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public void delete(List<ActiveAlarms> entities) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        if (entities == null) {
            throw new IllegalArgumentException("Argument entities cannot be null.");
        }
        entities.forEach(entity -> delete(entity));
    }

    /**
     * Truncate the active_alarms table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Query("TRUNCATE active_alarms")
    public void truncateAllActiveAlarms() throws UnavailableException, WriteTimeoutException;

    /**
     * Delete the all the entities from the active_alarms table.
     *
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws WriteTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    default public void deleteAll() throws UnavailableException, WriteTimeoutException {
        try {
            truncateAllActiveAlarms();
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.info(printCQL(new ActiveAlarms(), "DELETE"));
            }
        } catch (WriteTimeoutException e) {
            // log statement to file
            if (SLF4J_CQL_LOG != null) {
                SLF4J_CQL_LOG.error(printCQL(new ActiveAlarms(), "DELETE"));
            }
            throw e;
        }
    }

    /**
     * Gets the ActiveAlarms data by customer account, siteId and areaId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return PagingIterable of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ActiveAlarms> queryByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the ActiveAlarms data by customer account, siteId and areaId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ActiveAlarms> findByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        return queryByCustomerSiteArea(customerAccount, siteId, areaId).all();
    }

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId and assetId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return PagingIterable of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ActiveAlarms> queryByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId and assetId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ActiveAlarms> findByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        return queryByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId).all();
    }

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId and pointLocationId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return PagingIterable of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ActiveAlarms> queryByCustomerSiteAreaAssetPtLoc(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId and pointLocationId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ActiveAlarms> findByCustomerSiteAreaAssetPtLoc(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        return queryByCustomerSiteAreaAssetPtLoc(customerAccount, siteId, areaId, assetId, pointLocationId).all();
    }

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId, pointLocationId and pointId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return PagingIterable of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ActiveAlarms> queryByCustomerSiteAreaAssetPtLocPoint(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId, pointLocationId and pointId from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ActiveAlarms> findByCustomerSiteAreaAssetPtLocPoint(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        return queryByCustomerSiteAreaAssetPtLocPoint(customerAccount, siteId, areaId, assetId, pointLocationId, pointId).all();
    }

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId, pointLocationId, pointId and paramName from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @return PagingIterable of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ActiveAlarms> queryByCustomerSiteAreaAssetPtLocPointParam(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId, pointLocationId, pointId and paramName from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID
     * @param paramName, String Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ActiveAlarms> findByCustomerSiteAreaAssetPtLocPointParam(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument paramName is required.");
        }
        return queryByCustomerSiteAreaAssetPtLocPointParam(customerAccount, siteId, areaId, assetId, pointLocationId, pointId, paramName).all();
    }

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId, pointLocationId, pointId, paramName and alarmType from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return PagingIterable of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    @Select
    PagingIterable<ActiveAlarms> queryByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException;

    /**
     * Gets the ActiveAlarms data by customer account, siteId, areaId, assetId, pointLocationId, pointId, paramName and alarmType from the active_alarms table
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @param pointId, UUID Object
     * @param paramName, String Object
     * @param alarmType, String Object
     * @return List of ActiveAlarms Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the a given value is null
     */
    default public List<ActiveAlarms> findByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName, String alarmType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        if (customerAccount == null || customerAccount.length() < 1) {
            throw new IllegalArgumentException("Argument customer account is required.");
        }
        if (siteId == null) {
            throw new IllegalArgumentException("Argument siteId is required.");
        }
        if (areaId == null) {
            throw new IllegalArgumentException("Argument areaId is required.");
        }
        if (assetId == null) {
            throw new IllegalArgumentException("Argument assetId is required.");
        }
        if (pointLocationId == null) {
            throw new IllegalArgumentException("Argument pointLocationId is required.");
        }
        if (pointId == null) {
            throw new IllegalArgumentException("Argument pointId is required.");
        }
        if (paramName == null || paramName.isEmpty()) {
            throw new IllegalArgumentException("Argument paramName is required.");
        }
        if (alarmType == null || alarmType.isEmpty()) {
            throw new IllegalArgumentException("Argument alarmType is required.");
        }
        return queryByPK(customerAccount, siteId, areaId, assetId, pointLocationId, pointId, paramName, alarmType).all();
    }

    /**
     * Return a String Object of the CQL Statement based on the given entity and statementType
     *
     * @param entity, ActiveAlarms Object
     * @param statementType, String Object
     * @return String Object
     */
    default public String printCQL(ActiveAlarms entity, String statementType) throws IllegalArgumentException, UnavailableException, WriteTimeoutException {
        StringBuilder query, values;

        query = new StringBuilder();
        if (entity != null && statementType != null
                && entity.getCustomerAccount() != null && !entity.getCustomerAccount().isEmpty()
                && entity.getSiteId() != null && entity.getAreaId() != null
                && entity.getAssetId() != null && entity.getPointLocationId() != null
                && entity.getPointId() != null
                && entity.getParamName() != null && !entity.getParamName().isEmpty()
                && entity.getAlarmType() != null && !entity.getAlarmType().isEmpty()) {
            switch (statementType) {
                case ("INSERT"): {
                    values = new StringBuilder();
                    query.append("INSERT INTO active_alarms(customer_acct");
                    values.append("'").append(entity.getCustomerAccount()).append("'");
                    query.append(",site_id");
                    values.append(",").append(entity.getSiteId());
                    query.append(",area_id");
                    values.append(",").append(entity.getAreaId());
                    query.append(",asset_id");
                    values.append(",").append(entity.getAssetId());
                    query.append(",point_location_id");
                    values.append(",").append(entity.getPointLocationId());
                    query.append(",point_id");
                    values.append(",").append(entity.getPointId());
                    query.append(",param_name");
                    values.append(",'").append(entity.getParamName()).append("'");
                    query.append(",alarm_type");
                    values.append(",'").append(entity.getAlarmType()).append("'");

                    if (entity.getApSetId() != null) {
                        query.append(",ap_set_id");
                        values.append(",").append(entity.getApSetId());
                    }

                    query.append(",exceed_percent");
                    values.append(",").append(entity.getExceedPercent());
                    query.append(",high_alert");
                    values.append(",").append(entity.getHighAlert());
                    query.append(",high_fault");
                    values.append(",").append(entity.getHighFault());

                    query.append(",is_acknowledged");
                    values.append(",").append(entity.isAcknowledged());
                    query.append(",is_deleted");
                    values.append(",").append(entity.isDeleted());

                    query.append(",low_alert");
                    values.append(",").append(entity.getLowAlert());
                    query.append(",low_fault");
                    values.append(",").append(entity.getLowFault());

                    if (entity.getParamType() != null) {
                        query.append(",param_type");
                        values.append(",'").append(entity.getParamType()).append("'");
                    }

                    if (entity.getSampleTimestamp() != null) {
                        query.append(",sample_timestamp");
                        values.append(",'").append(entity.getSampleTimestamp()).append("'");
                    }

                    if (entity.getSensorType() != null) {
                        query.append(",sensor_type");
                        values.append(",'").append(entity.getSensorType()).append("'");
                    }

                    query.append(",trend_value");
                    values.append(",").append(entity.getTrendValue());

                    if (entity.getTriggerTime() != null) {
                        query.append(",sample_timestamp");
                        values.append(",'").append(entity.getSampleTimestamp()).append("'");
                    }

                    query.append(") values(").append(values.toString()).append(");");
                }
                break;
                case ("DELETE"): {
                    query.append("DELETE FROM active_alarms WHERE customer_acct='").append(entity.getCustomerAccount()).append("'");
                    query.append(" AND site_id=").append(entity.getSiteId());
                    query.append(" AND area_id=").append(entity.getAreaId());
                    query.append(" AND asset_id=").append(entity.getAssetId());
                    query.append(" AND point_location_id=").append(entity.getPointLocationId());
                    query.append(" AND point_id=").append(entity.getPointId());
                    query.append(" AND param_name='").append(entity.getParamName()).append("'");
                    query.append(" AND alarm_type='").append(entity.getAlarmType()).append("';");
                }
                break;
            }
        }

        return query.toString();
    }

}
