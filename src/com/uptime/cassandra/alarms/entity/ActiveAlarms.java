/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "active_alarms")
public class ActiveAlarms {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;
    
    @PartitionKey(2)
    @CqlName(value = "area_id")
    private UUID areaId;
    
    @ClusteringColumn(0)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @ClusteringColumn(1)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @ClusteringColumn(2)
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @ClusteringColumn(3)
    @CqlName(value = "param_name")
    private String paramName;

    @ClusteringColumn(4)
    @CqlName(value = "alarm_type")
    private String alarmType;

    @CqlName(value = "ap_set_id")
    private UUID apSetId;
    
    @CqlName(value = "exceed_percent")
    private float exceedPercent;
    
    @CqlName(value = "high_alert")
    private float highAlert;
    
    @CqlName(value = "high_fault")
    private float highFault;

    @CqlName(value = "is_acknowledged")
    private boolean acknowledged;

    @CqlName(value = "is_deleted")
    private boolean deleted;

    @CqlName(value = "low_alert")
    private float lowAlert;
    
    @CqlName(value = "low_fault")
    private float lowFault;
    
    @CqlName(value = "param_type")
    private String paramType;
    
    @CqlName(value = "sample_timestamp")
    private Instant sampleTimestamp;

    @CqlName(value = "sensor_type")
    private String sensorType;
    
    @CqlName(value = "trend_value")
    private float trendValue;
    
    @CqlName(value = "trigger_time")
    private Instant triggerTime;

    public ActiveAlarms() {
    }

    public ActiveAlarms(ActiveAlarms entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        pointLocationId = entity.getPointLocationId();
        pointId = entity.getPointId();
        paramName = entity.getParamName();
        alarmType = entity.getAlarmType();
        apSetId = entity.getApSetId();
        exceedPercent = entity.getExceedPercent();
        highAlert = entity.getHighAlert();
        highFault = entity.getHighFault();
        acknowledged = entity.isAcknowledged();
        deleted = entity.isDeleted();
        lowAlert = entity.getLowAlert();
        lowFault = entity.getLowFault();
        paramType = entity.getParamType();
        sampleTimestamp = entity.getSampleTimestamp();
        sensorType = entity.getSensorType();
        trendValue = entity.getTrendValue();
        triggerTime = entity.getTriggerTime();
    }
    
    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public float getExceedPercent() {
        return exceedPercent;
    }

    public void setExceedPercent(float exceedPercent) {
        this.exceedPercent = exceedPercent;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public Instant getSampleTimestamp() {
        return sampleTimestamp;
    }

    public void setSampleTimestamp(Instant sampleTimestamp) {
        this.sampleTimestamp = sampleTimestamp;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public float getTrendValue() {
        return trendValue;
    }

    public void setTrendValue(float trendValue) {
        this.trendValue = trendValue;
    }

    public Instant getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(Instant triggerTime) {
        this.triggerTime = triggerTime;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.assetId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationId);
        hash = 89 * hash + Objects.hashCode(this.pointId);
        hash = 89 * hash + Objects.hashCode(this.paramName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActiveAlarms other = (ActiveAlarms) obj;
        if (Float.floatToIntBits(this.exceedPercent) != Float.floatToIntBits(other.exceedPercent)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.acknowledged != other.acknowledged) {
            return false;
        }
        if (this.deleted != other.deleted) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.trendValue) != Float.floatToIntBits(other.trendValue)) {
            return false;
        }
        if (!Objects.equals(this.triggerTime, other.triggerTime)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.alarmType, other.alarmType)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.sampleTimestamp, other.sampleTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ActiveAlarms{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", paramName=" + paramName + ", alarmType=" + alarmType + ", apSetId=" + apSetId + ", exceedPercent=" + exceedPercent + ", highAlert=" + highAlert + ", highFault=" + highFault + ", acknowledged=" + acknowledged + ", deleted=" + deleted + ", lowAlert=" + lowAlert + ", lowFault=" + lowFault + ", paramType=" + paramType + ", sampleTimestamp=" + sampleTimestamp + ", sensorType=" + sensorType + ", trendValue=" + trendValue + ", triggerTime=" + triggerTime + "}";
    }
}
