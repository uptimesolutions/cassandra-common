/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.entity;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
@Entity
@CqlName(value = "alarm_count")
public class AlarmCount {

    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;
    
    @ClusteringColumn(0)
    @CqlName(value = "area_id")
    private UUID areaId;
    
    @ClusteringColumn(1)
    @CqlName(value = "asset_id")
    private UUID assetId;
    
    @ClusteringColumn(2)
    @CqlName(value = "point_location_id")
    private UUID pointLocationId;
    
    @ClusteringColumn(3)
    @CqlName(value = "point_id")
    private UUID pointId;
    
    @CqlName(value = "alarm_count")
    private int alarmCount;
    
    @CqlName(value = "alert_count")
    private int alertCount;
    
    @CqlName(value = "fault_count")
    private int faultCount;
    
    

    public AlarmCount() {
    }

    public AlarmCount(AlarmCount entity) {
        customerAccount = entity.getCustomerAccount();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        pointLocationId = entity.getPointLocationId();
        pointId = entity.getPointId();
        alarmCount = entity.getAlarmCount();
        alertCount=entity.getAlertCount();
        faultCount=entity.getFaultCount();
    }
    
    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public int getAlarmCount() {
        return alarmCount;
    }

    public void setAlarmCount(int alarmCount) {
        this.alarmCount = alarmCount;
    }

    public int getAlertCount() {
        return alertCount;
    }

    public void setAlertCount(int alertCount) {
        this.alertCount = alertCount;
    }

    public int getFaultCount() {
        return faultCount;
    }

    public void setFaultCount(int faultCount) {
        this.faultCount = faultCount;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.areaId);
        hash = 29 * hash + Objects.hashCode(this.assetId);
        hash = 29 * hash + Objects.hashCode(this.pointLocationId);
        hash = 29 * hash + Objects.hashCode(this.pointId);
        hash = 29 * hash + this.alarmCount;
        hash = 29 * hash + this.alertCount;
        hash = 29 * hash + this.faultCount;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmCount other = (AlarmCount) obj;
        if (this.alarmCount != other.alarmCount) {
            return false;
        }
        if (this.alertCount != other.alertCount) {
            return false;
        }
        if (this.faultCount != other.faultCount) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        return Objects.equals(this.pointId, other.pointId);
    }

    @Override
    public String toString() {
        return "AlarmCount{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", alarmCount=" + alarmCount + ", alertCount=" + alertCount + ", faultCount=" + faultCount + '}';
    }
    
    
}
