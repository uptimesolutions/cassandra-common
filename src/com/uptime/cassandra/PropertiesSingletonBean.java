/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author akumar
 */
public class PropertiesSingletonBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesSingletonBean.class.getName());

    private static final PropertiesSingletonBean INSTANCE = new PropertiesSingletonBean();
    private final Properties properties;
    private String env = null;

    public String getEnv() {
        return this.env;
    }
    
    /**
     * Private Constructor
     */
    private PropertiesSingletonBean() {
        properties = new Properties();
        env = null;
        
        try (InputStream is = new FileInputStream(new File(System.getenv("PROP_FILE_LOCATION")))) {
            properties.load(is);
            this.env = properties.getProperty("env");
            if(env == null || env.isEmpty())
                throw new Exception("Env variable missing");
        } catch(Exception ex) {
            LOGGER.error("Fail to load properties file", ex);
            System.exit(0);
        } 
    }

    /**
     * return a instance of the class 
     * @return PropertiesSingletonBean Object
     */
    public static PropertiesSingletonBean getInstance() {
        return INSTANCE;
    }
    
    /**
     * return the property based on the given key
     * @param key, String object
     * @return String Object
     */
    public String getProperty(String key) {
        return properties.getProperty(key + env);
    }
}
