/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.dao;

import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.PreparedStatement;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
public class UpdateStatementsDAO {
    private static PreparedStatement UPDATE_SITE_BASESTATION_DEVICE_LAST_SAMPLED = null;
    private static PreparedStatement UPDATE_ECHO_DIAGNOSTICS_LAST_UPDATE = null;
    Logger CQL_LOG = LoggerFactory.getLogger(UpdateStatementsDAO.class);
    
    /**
     * Updates the site_basestation_device.last_sampled for the given entity. All PK columns are required in the entity.
     * @param entity
     * @throws UnavailableException
     * @throws WriteTimeoutException
     * @throws IllegalArgumentException 
     */
    public void updateSiteBasestationDeviceLastSampled(SiteBasestationDevice entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null)
            throw new IllegalArgumentException("Argument entity cannot be null.");
        
        // check that all PK fields and last_sampled are not null
        if(entity.getCustomerAccount()!=null && entity.getSiteId()!=null && entity.getBaseStationPort()!= 0 && entity.getDeviceId()!=null 
                && entity.getPointId()!=null && entity.getLastSampled()!= null){
            try{
                // the prepared statement will only be prepared once
                if(UPDATE_SITE_BASESTATION_DEVICE_LAST_SAMPLED == null)
                    UPDATE_SITE_BASESTATION_DEVICE_LAST_SAMPLED = CassandraData.getSession().prepare("INSERT INTO site_basestation_device(customer_acct,site_id,base_station_port,device_id,point_id,last_sampled) VALUES(?,?,?,?,?,?)");
                // set the bind variables
                BoundStatement bound = UPDATE_SITE_BASESTATION_DEVICE_LAST_SAMPLED.bind(entity.getCustomerAccount(),entity.getSiteId(),entity.getBaseStationPort(),
                        entity.getDeviceId(),entity.getPointId(),entity.getLastSampled());
                CassandraData.getSession().execute(bound);
                printCQL(entity);
            }
            catch(UnavailableException |  WriteTimeoutException e){
                printCQL(entity);
                throw e;
            }
        }
        else
            throw new IllegalArgumentException("Missing required entity field.");
    }
    
    /**
     * Updates the echo_diagnostics.last_update for the given entity. All PK columns are required in the entity.
     * @param entity the row to update
     * @throws UnavailableException
     * @throws WriteTimeoutException
     * @throws IllegalArgumentException 
     */
    public void updateEchoDiagnosticsLastUpdate(EchoDiagnostics entity) throws UnavailableException, WriteTimeoutException, IllegalArgumentException {
        if (entity == null)
            throw new IllegalArgumentException("Argument entity cannot be null.");

        // check that all PK fields and last_update are not null
        if(entity.getCustomerAccount() != null && entity.getSiteId() != null && entity.getDeviceId() != null && entity.getLastUpdate()!= null){
            try{
                if(UPDATE_ECHO_DIAGNOSTICS_LAST_UPDATE == null)
                        UPDATE_ECHO_DIAGNOSTICS_LAST_UPDATE = CassandraData.getSession().prepare("INSERT INTO echo_diagnostics(customer_acct,site_id,device_id,last_update) VALUES(?,?,?,?)");
                BoundStatement bound = UPDATE_ECHO_DIAGNOSTICS_LAST_UPDATE.bind(entity.getCustomerAccount(),entity.getSiteId(),entity.getDeviceId(),
                        entity.getLastUpdate());
                CassandraData.getSession().execute(bound);
                printCQL(entity);
            }
            catch(UnavailableException |  WriteTimeoutException e){
                printCQL(entity);
                throw e;
            }
        }
        else
            throw new IllegalArgumentException("Missing required entity field.");
    }
    
    private void printCQL(SiteBasestationDevice entity) {
        if (CQL_LOG != null) {
            StringBuilder query = new StringBuilder();
            query.append("INSERT INTO site_basestation_device(customer_acct,site_id,base_station_port,device_id,point_id,last_sampled) VALUES('")
                    .append(entity.getCustomerAccount()).append("',")
                    .append(entity.getSiteId()).append(",")
                    .append(entity.getBaseStationPort()).append(",'")
                    .append(entity.getDeviceId()).append("',")
                    .append(entity.getPointId()).append(",'")
                    .append(entity.getLastSampled()).append("')");
            CQL_LOG.info(query.toString());
        }
    }
    
    private void printCQL(EchoDiagnostics entity) {
        if (CQL_LOG != null) {
            StringBuilder query = new StringBuilder();
            query.append("INSERT INTO echo_diagnostics(customer_acct,site_id,device_id,last_update) VALUES('")
                    .append(entity.getCustomerAccount()).append("',")
                    .append(entity.getSiteId()).append(",'")
                    .append(entity.getDeviceId()).append("','")
                    .append(entity.getLastUpdate()).append("')");
            CQL_LOG.info(query.toString());
        }
    }
}
