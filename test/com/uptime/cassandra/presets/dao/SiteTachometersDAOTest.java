/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteTachometers;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteTachometersDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SiteTachometersDAO instance;
    static SiteTachometers entity;
    
    public SiteTachometersDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().siteTachometersDAO();
        
        entity = new SiteTachometers();
        entity.setCustomerAccount("Test 77777");
        entity.setSiteId(UUID.randomUUID());
        entity.setTachId(UUID.randomUUID());
        entity.setTachName("Test Tac Name");
        entity.setTachType("Tac Type");
        entity.setTachReferenceSpeed(10.00f);
        entity.setTachReferenceUnits("Tac Units");
    }


    /**
     * Test of create method, of class SiteTachometersDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getTachId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class SiteTachometersDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        entity.setTachReferenceSpeed(new Float(15.00));
        instance.update(entity);
        List<SiteTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getTachId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSite method, of class SiteTachometersDAO.
     */
    @Test
    public void test3_FindByCustomerSite() {
        System.out.println("findByCustomer");
        List<SiteTachometers> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class SiteTachometersDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<SiteTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getTachId());
        assertEquals(entity.toString(), result.get(0).toString());
    }
    
    /**
     * Test of delete method, of class SiteTachometersDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getTachId());
        assertEquals(0, result.size());
    } 
}
