/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalTachometers;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalTachometersDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static GlobalTachometersDAO instance;
    static GlobalTachometers entity;
    
    public GlobalTachometersDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().globalTachometersDAO();
        
        entity = new GlobalTachometers();
        entity.setCustomerAccount("Test 77777");
        entity.setTachId(UUID.randomUUID());
        entity.setTachName("Test Tac Name");
        entity.setTachType("Tac Type");
        entity.setTachReferenceSpeed(2.2f);
        entity.setTachReferenceUnits("Tac Units");
    }


    /**
     * Test of create method, of class GlobalTachometersDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<GlobalTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getTachId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class GlobalTachometersDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        entity.setTachReferenceSpeed(4);
        instance.update(entity);
        List<GlobalTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getTachId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    

    /**
     * Test of findByCustomer method, of class GlobalTachometersDAO.
     */
    @Test
    public void test3_FindByCustomer() {
        System.out.println("findByCustomer");
        String customerAccount = entity.getCustomerAccount();
        List<GlobalTachometers> result = instance.findByCustomer(customerAccount);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class GlobalTachometersDAO.
     */
    @Test
    public void test2_FindByPK() {
        System.out.println("findByPK");
        List<GlobalTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getTachId());
        assertEquals(entity.toString(), result.get(0).toString());
    }
    
    /**
     * Test of delete method, of class GlobalTachometersDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<GlobalTachometers> result = instance.findByPK(entity.getCustomerAccount(), entity.getTachId());
        assertEquals(0, result.size());
    }
}
