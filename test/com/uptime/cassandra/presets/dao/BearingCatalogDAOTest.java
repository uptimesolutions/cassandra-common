/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.BearingCatalog;
import java.util.List;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BearingCatalogDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static BearingCatalogDAO instance;
    static BearingCatalog entity;

    public BearingCatalogDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().bearingCatalogDAO();
        entity = new BearingCatalog();
        entity.setVendorId("VendorId 1");
        entity.setBearingId("BearingId 1");
        entity.setBc((short)1);
        entity.setBpfi(1.0f);
        entity.setBpfo(2.0f);
        entity.setBsf(3.0f);
        entity.setFtf(4.0f);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class BearingCatalogDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<BearingCatalog> result = instance.findByPK(entity.getVendorId(), entity.getBearingId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class BearingCatalogDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        entity.setFtf(6.0f);
        instance.update(entity);
        List<BearingCatalog> result = instance.findByPK(entity.getVendorId(), entity.getBearingId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class BearingCatalogDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<BearingCatalog> result = instance.findByPK(entity.getVendorId(), entity.getBearingId());
        System.out.println("testFindByPK result - " + result.get(0).toString());
        System.out.println("testFindByPK entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByVendorId method, of class BearingCatalogDAO.
     */
    @Test
    public void test4_FindByVendorId() {
        System.out.println("findByVendorId");
        List<BearingCatalog> result = instance.findByVendorId(entity.getVendorId());
        System.out.println("testQueryByVendorId result - " + result.get(0).toString());
        System.out.println(" testQueryByVendorId entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());

    }

    /**
     * Test of findByVendorId method, of class BearingCatalogDAO.
     */
    @Test
    public void test5_FindDistinctVendors() {
        System.out.println("findDistinctVendors");
        List<String> result = instance.findDistinctVendors();
        System.out.println("test5_FindDistinctVendors result - " + result.get(0));
        System.out.println(" test5_FindDistinctVendors entity - " + entity.toString());
        assertEquals(entity.getVendorId(), result.get(0));
    }

    /**
     * Test of delete method, of class BearingCatalogDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<BearingCatalog> result = instance.findByPK(entity.getVendorId(), entity.getBearingId());
        assertEquals(0, result.size());
    }

}
