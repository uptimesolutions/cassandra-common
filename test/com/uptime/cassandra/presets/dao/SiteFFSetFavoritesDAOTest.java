/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteFFSetFavoritesDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SiteFFSetFavoritesDAO instance;
    static SiteFFSetFavorites entity;

    public SiteFFSetFavoritesDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().siteFFSetFavoritesDAO();
        entity = new SiteFFSetFavorites();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.randomUUID());
        entity.setFfSetId(UUID.randomUUID());
        entity.setFfId(UUID.randomUUID());
        entity.setFfName("Test Name");
        entity.setFfSetDesc("Test Desc");
        entity.setFfSetName("Test Set Name");
        entity.setFfUnit("Test Unit");
        entity.setFfValue((float) 2);
        entity.setGlobal(true);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteFFSetFavorites> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        entity.setFfSetDesc("Test Updated Desc");
        instance.update(entity);
        List<SiteFFSetFavorites> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        UUID ffSetId = entity.getFfSetId();
        UUID ffId = entity.getFfId();
        List<SiteFFSetFavorites> result = instance.findByPK(customerAccount, siteId, ffSetId, ffId);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSite method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void test4_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();

        List<SiteFFSetFavorites> result = instance.findByCustomerSite(customerAccount, siteId);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void testDelete_SiteFFSetFavorites() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteFFSetFavorites> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(0, result.size());
    }

    /**
     * Test of delete method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void testDelete_3args() {
        System.out.println("delete");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        UUID ffSetId = entity.getFfSetId();
        instance.delete(customerAccount, siteId, ffSetId);
        List<SiteFFSetFavorites> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(0, result.size());
    }

    /**
     * Test of delete method, of class SiteFFSetFavoritesDAO.
     */
    @Test
    public void testDelete_String_UUID() {
        System.out.println("delete");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        instance.delete(customerAccount, siteId);
        List<SiteFFSetFavorites> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(0, result.size());
    }

}
