/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteBearingFavoritesDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SiteBearingFavoritesDAO instance;
    static SiteBearingFavorites entity;
    
    public SiteBearingFavoritesDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().siteBearingFavoritesDAO();
        entity = new SiteBearingFavorites();
        entity.setCustomerAcct("77777");
        entity.setSiteId(UUID.fromString("a37bdbdb-14fe-47b8-a749-55def0680bef"));
        entity.setVendorId("Test Vendor");
        entity.setBearingId("Test Bearing");
        entity.setFtf(2);
        entity.setBsf(2);
        entity.setBpfo(2);
        entity.setBpfi(2);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteBearingFavorites> result = instance.findByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getVendorId(), entity.getBearingId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSite method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<SiteBearingFavorites> result = instance.findByCustomerSite(entity.getCustomerAcct(), entity.getSiteId());
        System.out.println("findByCustomerSite result - " + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSite method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test21_FindByCustomerSiteVendor() {
        System.out.println("findByCustomerSiteVendor");
        List<SiteBearingFavorites> result = instance.findByCustomerSiteVendor(entity.getCustomerAcct(), entity.getSiteId(), entity.getVendorId());
        System.out.println("findByCustomerSite result - " + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<SiteBearingFavorites> result = instance.findByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getVendorId(), entity.getBearingId());
        System.out.println("findByPK result - " + result);
        assertEquals(entity, result.get(0));
    }
    
    /**
     * Test of update method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        entity.setFtf(10);
        instance.update(entity);
        List<SiteBearingFavorites> result = instance.findByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getVendorId(), entity.getBearingId());
        assertEquals(entity.getFtf(), result.get(0).getFtf(), 0);
    }

    /**
     * Test of delete method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteBearingFavorites> result = instance.findByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getVendorId(), entity.getBearingId());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "INSERT INTO site_bearing_favorites(customer_acct,site_id,vendor_id,bearing_id,ftf,bsf,bpfo,bpfi ) values('77777',a37bdbdb-14fe-47b8-a749-55def0680bef,'Test Vendor','Test Bearing',10.0,2.0,2.0,2.0);";
        String result = instance.printCQL(entity, "INSERT");
        System.out.println("INSERT printCQL result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of printCQL method, of class SiteBearingFavoritesDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "DELETE FROM site_bearing_favorites WHERE customer_acct='77777' AND site_id=a37bdbdb-14fe-47b8-a749-55def0680bef AND vendor_id='Test Vendor' AND bearing_id='Test Bearing';";
        String result = instance.printCQL(entity, "DELETE");
        System.out.println("DELETE printCQL result - " + result);
        assertEquals(expResult, result);
    }

}
