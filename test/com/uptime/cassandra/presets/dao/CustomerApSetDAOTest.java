/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.CustomerApSet;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerApSetDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static CustomerApSetDAO instance;
    static CustomerApSet entity;

    public CustomerApSetDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {

        instance = PresetsMapperImpl.getInstance().customerApSetDAO();
        entity = new CustomerApSet();
        entity.setCustomerAccount("77777");
        entity.setApSetId(UUID.randomUUID());
        entity.setApSetName("Test AP Set 1");
        entity.setFmax(1);
        entity.setResolution(2);
        entity.setSensorType("Acceleration");
        entity.setPeriod(2.5F);
        entity.setSampleRate(3F);
        entity.setSiteId(UUID.randomUUID());
    }

    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of create method, of class CustomerApSetDAO.
     */
    @Test
    public void test1Create() {
        instance.create(entity);
        List<CustomerApSet> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class CustomerApSetDAO.
     */
    @Test
    public void test4Update() {
        entity.setSampleRate(55F);
        instance.update(entity);
        List<CustomerApSet> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class CustomerApSetDAO.
     */
    @Test
    public void test5Delete() {
        instance.delete(entity);
        List<CustomerApSet> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId());
        Assert.assertThat(result.size(), is(0));
    }

    /**
     * Test of findByCustomerApSet method, of class CustomerApSetDAO.
     */
    @Test
    public void test2FindByCustomer() {
        List<CustomerApSet> result = instance.findByCustomer(entity.getCustomerAccount());
        System.out.println("test2FindByCustomer result - " + result.get(0).toString());
        System.out.println(" test2FindByCustomer entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class CustomerApSetDAO.
     */
    @Test
    public void test3FindByPK() {
        List<CustomerApSet> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId());
        System.out.println("test3FindByPK result - " + result.get(0).toString());
        System.out.println("test3FindByPK entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }

}
