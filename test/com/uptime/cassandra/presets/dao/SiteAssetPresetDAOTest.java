/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteAssetPresetDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteAssetPresetDAO instance;
    private static SiteAssetPreset entity = null;
    
    public SiteAssetPresetDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("****************************8setUpClass");
        instance = PresetsMapperImpl.getInstance().siteAssetPresetDAO();
        entity = new SiteAssetPreset();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("3384f9d0-d1ff-4690-bcc9-57cba146f992"));
        entity.setAssetPresetId(UUID.fromString("3384f9d0-d1ff-4690-bcc9-57cba146f992"));
        entity.setAssetPresetName("Test Asset Preset Name");
        entity.setAssetTypeName("Test Asset preset type name");
        entity.setDescription("Test Desc");
        entity.setDevicePresetId(UUID.fromString("3384f9d0-d1ff-4690-bcc9-57cba146f992"));
        entity.setFfSetIds("Test ffsetId");
        entity.setPointLocationName("Test Pl Name");
        entity.setRollDiameter(0);
        entity.setRollDiameterUnits("Test Roll Diam");
        entity.setSampleInterval(0);
        entity.setSampleInterval(10);
        entity.setSpeedRatio(10);
        entity.setDevicePresetType("MistLX");
        entity.setSpeedRatio(0);
        entity.setTachId(UUID.fromString("3384f9d0-d1ff-4690-bcc9-57cba146f992"));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test0_Create_SiteAssetPreset() {
        System.out.println("create");
        instance.create(entity);
        
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAssetTypeName(), entity.getAssetPresetId(), entity.getPointLocationName());
        assertEquals(expResult, result);
    }

    

    /**
     * Test of update method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test1_Update_SiteAssetPreset() {
        System.out.println("update");
        instance.update(entity);
        
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAssetTypeName(), entity.getAssetPresetId(), entity.getPointLocationName());
        assertEquals(expResult, result);
    }


    /**
     * Test of findByCustomerSiteAssetType method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByCustomerSite(customerAccount, siteId);
        assertEquals(expResult, result);
        System.out.println("findByCustomerSite Completed");
     
    } 
    /**
     * Test of findByCustomerSiteAssetType method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test3_FindByCustomerSiteAssetType() {
        System.out.println("findByCustomerSiteAssetType");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        String assetTypeName = entity.getAssetTypeName();
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByCustomerSiteAssetType(customerAccount, siteId, assetTypeName);
        assertEquals(expResult, result);
        System.out.println("findByCustomerSiteAssetType Completed");
     
    }
    
    /**
     * Test of findByCustomerSiteAssetType method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test4_FindByCustomerSiteAssetType() {
        System.out.println("findByCustomerSiteAssetType");
        String customerAccount = "Test cust Acc";
        UUID siteId = entity.getSiteId();
        String assetTypeName = entity.getAssetTypeName();
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByCustomerSiteAssetType(customerAccount, siteId, assetTypeName);
        assertNotEquals(expResult, result);
        System.out.println("findByCustomerSiteAssetType Completed");
     
    }


    /**
     * Test of findByCustomerSiteAssetTypePresetId method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test5_FindByCustomerSiteAssetTypePresetId() {
        System.out.println("findByCustomerSiteAssetTypePresetId");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        String assetTypeName = entity.getAssetTypeName();
        UUID assetPresetId = entity.getAssetPresetId();
        
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByCustomerSiteAssetTypePresetId(customerAccount, siteId, assetTypeName, assetPresetId);
        assertEquals(expResult, result);
        System.out.println("findByCustomerSiteAssetTypePresetId Completed");
    }
    
    /**
     * Test of findByCustomerSiteAssetTypePresetId method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test6_FindByCustomerSiteAssetTypePresetId() {
        System.out.println("findByCustomerSiteAssetTypePresetId");
        String customerAccount = "Test cust Acc";
        UUID siteId = entity.getSiteId();
        String assetTypeName = entity.getAssetTypeName();
        UUID assetPresetId = entity.getAssetPresetId();
        
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByCustomerSiteAssetTypePresetId(customerAccount, siteId, assetTypeName, assetPresetId);
        assertNotEquals(expResult, result);
        System.out.println("findByCustomerSiteAssetTypePresetId Completed");
    }


    /**
     * Test of findByPK method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test7_FindByPK() {
        System.out.println("findByPK");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        String assetTypeName = entity.getAssetTypeName();
        UUID assetPresetId = entity.getAssetPresetId();
        String pointLocationName = entity.getPointLocationName();
        
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByPK(customerAccount, siteId, assetTypeName, assetPresetId, pointLocationName);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of findByPK method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test8_FindByPK() {
        System.out.println("findByPK");
        String customerAccount = "Test cust Acc";
        UUID siteId = entity.getSiteId();
        String assetTypeName = entity.getAssetTypeName();
        UUID assetPresetId = entity.getAssetPresetId();
        String pointLocationName = entity.getPointLocationName();
        
        List<SiteAssetPreset> expResult = new ArrayList();
        expResult.add(entity);
        List<SiteAssetPreset> result = instance.findByPK(customerAccount, siteId, assetTypeName, assetPresetId, pointLocationName);
        assertNotEquals(expResult, result);
    }
    
    /**
     * Test of delete method, of class SiteAssetPresetDAO.
     */
    @Test
    public void test9_Delete_SiteAssetPreset() {
        System.out.println("delete");
        instance.delete(entity);
        System.out.println("Deleted Entity");
    }

    
}
