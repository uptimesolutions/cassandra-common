/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalAssetTypes;
import java.util.List;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalAssetTypesDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalAssetTypesDAO instance;
    private static GlobalAssetTypes entity = null;
    
    public GlobalAssetTypesDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().globalAssetTypesDAO();
        entity = new GlobalAssetTypes();
        entity.setCustomerAccount("77777");
        entity.setAssetTypeName("Test Asset Type 1");
        entity.setDescription("Test Asset Type Desc");
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GlobalAssetTypesDAO.
     */
    @Test
    public void test1_Create_GlobalAssetTypes() {
        System.out.println("create");
        instance.create(entity);
    }

    /**
     * Test of findByCustomer method, of class GlobalAssetTypesDAO.
     */
    @Test
    public void test2_FindByCustomer() {
        System.out.println("findByCustomer");
        List<GlobalAssetTypes> result = instance.findByCustomer(entity.getCustomerAccount());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerDeviceType method, of class GlobalAssetTypesDAO.
     */
    @Test
    public void test3_FindByCustomerDeviceType() {
        System.out.println("findByCustomerDeviceType");
        List<GlobalAssetTypes> result = instance.findByPK(entity.getCustomerAccount(), entity.getAssetTypeName());
        assertEquals(entity, result.get(0));
    }

    
    /**
     * Test of update method, of class GlobalAssetTypesDAO.
     */
    @Test
    public void test4_Update_GlobalAssetTypes() {
        System.out.println("update");
        entity.setDescription("Updated Desc");
        instance.update(entity);
        List<GlobalAssetTypes> result = instance.findByPK(entity.getCustomerAccount(), entity.getAssetTypeName());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class GlobalAssetTypesDAO.
     */
    @Test
    public void test5_Delete_GlobalAssetTypes() {
        System.out.println("delete");
        instance.delete(entity);
        List<GlobalAssetTypes> result = instance.findByPK(entity.getCustomerAccount(), entity.getAssetTypeName());
        assertEquals(0, result.size());
    }
    
    /**
     * Test of printCQL method, of class GlobalAssetTypesDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL method testing.");
        String result;
        
        result  = instance.printCQL(entity, "INSERT");
        System.out.println("result INSERT - " + result);
        assertEquals("INSERT INTO global_asset_types(customer_acct,asset_type_name,description) values('77777','Test Asset Type 1','Updated Desc');", result);
        result  = instance.printCQL(entity, "DELETE");
        
        System.out.println("result DElete - " + result);
        assertEquals("DELETE FROM global_asset_types WHERE customer_acct='77777' AND asset_type_name='Test Asset Type 1';", result);
        result  = instance.printCQL(null, null);
        assertEquals("", result);
        System.out.println("printCQL method complete.");
    }

}
