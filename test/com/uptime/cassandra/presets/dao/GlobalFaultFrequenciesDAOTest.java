/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalFaultFrequenciesDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static GlobalFaultFrequenciesDAO instance;
    static GlobalFaultFrequencies entity;

    public GlobalFaultFrequenciesDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().globalFaultFrequenciesDAO();
        entity = new GlobalFaultFrequencies();
        entity.setCustomerAccount("77777");
        entity.setFfSetId(UUID.randomUUID());
        entity.setFfId(UUID.randomUUID());
        entity.setFfName("Test Name");
        entity.setFfSetDesc("Test Desc");
        entity.setFfSetName("Test Set Name");
        entity.setFfUnit("Test Unit");
        entity.setFfValue((float) 2);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GlobalFaultFrequenciesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<GlobalFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class GlobalFaultFrequenciesDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        instance.update(entity);
        List<GlobalFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class GlobalFaultFrequenciesDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<GlobalFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getFfSetId(), entity.getFfId());
        assertEquals(0, result.size());
    }

    /**
     * Test of findByCustomer method, of class GlobalFaultFrequenciesDAO.
     */
    @Test
    public void test3_FindByCustomer() {
        System.out.println("findByCustomer");       
        List<GlobalFaultFrequencies> result = instance.findByCustomer(entity.getCustomerAccount());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class GlobalFaultFrequenciesDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<GlobalFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSetName method, of class GlobalFaultFrequenciesDAO.
     */
    @Test
    public void test5_FindByCustomerSetId() {
        System.out.println("findByCustomerSetId");
        List<GlobalFaultFrequencies> result = instance.findByCustomerSetId(entity.getCustomerAccount(), entity.getFfSetId());
         assertEquals(entity, result.get(0));
    }

}
