/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteApAlSets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteApAlSetsDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SiteApAlSetsDAO instance;
    static SiteApAlSets entity, siteApAlSets;

    public SiteApAlSetsDAOTest() {
    }

    @BeforeClass
    public static void setUp() {

        instance = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        entity = new SiteApAlSets();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        entity.setApSetId(UUID.fromString("dc10fad1-bb37-4ee8-af3f-4c9b7f1f075"));
        entity.setFmax(1);
        entity.setResolution(2);
        entity.setSensorType("Acceleration");
        entity.setAlSetId(UUID.randomUUID());
        entity.setParamName("paramName 1");
        entity.setPeriod(2.5F);
        entity.setSampleRate(3F);
        entity.setDemod(true);
        entity.setDemodHighPass(200F);
        entity.setDemodLowPass(100F);
        entity.setParamType("paramType 1");
        entity.setFrequencyUnits("HZ");
        entity.setParamUnits("HK");
        entity.setParamAmpFactor("paramAmpFactor 1");
        entity.setMinFrequency(1F);
        entity.setMaxFrequency(100F);
        entity.setDwell(10);
        entity.setLowFaultActive(true);
        entity.setLowAlertActive(true);
        entity.setHighAlertActive(true);
        entity.setHighFaultActive(true);
        entity.setLowFault(20);
        entity.setLowAlert(200);
        entity.setHighAlert(2000);
        entity.setHighFault(300);

        siteApAlSets = new SiteApAlSets();
        siteApAlSets.setCustomerAccount("77777");
        siteApAlSets.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        siteApAlSets.setApSetId(UUID.fromString("ab10c195-d6e1-40b3-8ebc-9a46dfbbf192"));
        siteApAlSets.setFmax(1);
        siteApAlSets.setResolution(2);
        siteApAlSets.setSensorType("Ultrasonic");
        siteApAlSets.setAlSetId(UUID.randomUUID());
        siteApAlSets.setParamName("paramName 2");
        siteApAlSets.setPeriod(2.5F);
        siteApAlSets.setSampleRate(3F);
        siteApAlSets.setDemod(true);
        siteApAlSets.setDemodHighPass(200F);
        siteApAlSets.setDemodLowPass(100F);
        siteApAlSets.setParamType("paramType 1");
        siteApAlSets.setFrequencyUnits("HZ");
        siteApAlSets.setParamUnits("HK");
        siteApAlSets.setParamAmpFactor("paramAmpFactor 1");
        siteApAlSets.setMinFrequency(1F);
        siteApAlSets.setMaxFrequency(100F);
        siteApAlSets.setDwell(10);
        siteApAlSets.setLowFaultActive(true);
        siteApAlSets.setLowAlertActive(true);
        siteApAlSets.setHighAlertActive(true);
        siteApAlSets.setHighFaultActive(true);
        siteApAlSets.setLowFault(20);
        siteApAlSets.setLowAlert(200);
        siteApAlSets.setHighAlert(2000);
        siteApAlSets.setHighFault(300);
    }

    /**
     * Test of create method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test1Create() {
        instance.create(entity);
        instance.create(siteApAlSets);
        List<SiteApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test2Update() {
        entity.setPeriod(4.5F);
        instance.update(entity);
        List<SiteApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test7Delete() {
        instance.delete(entity);
        List<SiteApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        Assert.assertThat(result.size(), is(0));
    }

    /**
     * Test of findByCustomerApSet method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test3FindByCustomerApSet() {
        List<SiteApAlSets> result = instance.findByCustomerSiteApSet(entity.getCustomerAccount(), entity.getSiteId(), entity.getApSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerApSet method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test31FindByCustomerApSets() {
        List<UUID> uuidList = new ArrayList<>();
        uuidList.add(entity.getApSetId());
        uuidList.add(siteApAlSets.getApSetId());
        System.out.println("uuidList - " + uuidList.toString());
        List<SiteApAlSets> result = instance.findByCustomerSiteApSets(entity.getCustomerAccount(), entity.getSiteId(), uuidList);
        System.out.println("result size- " + result.size());
        assertEquals(siteApAlSets.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerApSetAlSet method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test4FindByCustomerApSetAlSet() {
        List<SiteApAlSets> result = instance.findByCustomerSiteApSetAlSet(entity.getCustomerAccount(), entity.getSiteId(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class SiteApAlSetsDAO.
     */
    @Test
    public void test5FindByPK() {
        List<SiteApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

}
