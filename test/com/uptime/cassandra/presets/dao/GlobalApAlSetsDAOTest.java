/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalApAlSetsDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static GlobalApAlSetsDAO instance;
    static GlobalApAlSets entity, globalApAlSets;

    public GlobalApAlSetsDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        entity = new GlobalApAlSets();
        entity.setCustomerAccount("77777");
        entity.setApSetId(UUID.randomUUID());
        entity.setAlSetId(UUID.randomUUID());
        entity.setApSetName("Test AP Set 1");
        entity.setFmax(1);
        entity.setResolution(2);
        entity.setSensorType("Acceleration");
        entity.setAlSetName("alSetName 1");
        entity.setParamName("paramName 1");
        entity.setPeriod(2.5F);
        entity.setSampleRate(3F);
        entity.setDemod(true);
        entity.setDemodHighPass(200F);
        entity.setDemodLowPass(100F);
        entity.setParamType("paramType 1");
        entity.setFrequencyUnits("HZ");
        entity.setParamUnits("HK");
        entity.setParamAmpFactor("paramAmpFactor 1");
        entity.setMinFrequency(1F);
        entity.setMaxFrequency(100F);
        entity.setDwell(10);
        entity.setLowFaultActive(true);
        entity.setLowAlertActive(true);
        entity.setHighAlertActive(true);
        entity.setHighFaultActive(true);
        entity.setLowFault(20);
        entity.setLowAlert(200);
        entity.setHighAlert(2000);
        entity.setHighFault(300);

        globalApAlSets = new GlobalApAlSets();
        globalApAlSets.setCustomerAccount("77777");
        globalApAlSets.setApSetId(UUID.randomUUID());
        globalApAlSets.setAlSetId(UUID.randomUUID());
        globalApAlSets.setApSetName("Test AP Set 1");
        globalApAlSets.setFmax(1);
        globalApAlSets.setResolution(2);
        globalApAlSets.setSensorType("Acceleration");
        globalApAlSets.setAlSetName("alSetName 1");
        globalApAlSets.setParamName("paramName 1");
        globalApAlSets.setPeriod(2.5F);
        globalApAlSets.setSampleRate(3F);
        globalApAlSets.setDemod(true);
        globalApAlSets.setDemodHighPass(200F);
        globalApAlSets.setDemodLowPass(100F);
        globalApAlSets.setParamType("paramType 1");
        globalApAlSets.setFrequencyUnits("HZ");
        globalApAlSets.setParamUnits("HK");
        globalApAlSets.setParamAmpFactor("paramAmpFactor 1");
        globalApAlSets.setMinFrequency(1F);
        globalApAlSets.setMaxFrequency(100F);
        globalApAlSets.setDwell(10);
        globalApAlSets.setLowFaultActive(true);
        globalApAlSets.setLowAlertActive(true);
        globalApAlSets.setHighAlertActive(true);
        globalApAlSets.setHighFaultActive(true);
        globalApAlSets.setLowFault(20);
        globalApAlSets.setLowAlert(200);
        globalApAlSets.setHighAlert(2000);
        globalApAlSets.setHighFault(300);

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test1Create() {
        instance.create(entity);
        List<GlobalApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test6Update() {
        entity.setDwell(20);
        instance.update(entity);
        List<GlobalApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test7Delete() {
        instance.delete(entity);
        List<GlobalApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        Assert.assertThat(result.size(), is(0));
    }

    /**
     * Test of findByCustomerApSet method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test3FindByCustomerApSet() {
        List<GlobalApAlSets> result = instance.findByCustomerApSet(entity.getCustomerAccount(), entity.getApSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerApSet method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test31FindByCustomerApSets() {
        instance.create(globalApAlSets);
        List<UUID> uuidList = new ArrayList<>();
        uuidList.add(entity.getApSetId());
//        uuidList.add(globalApAlSets.getApSetId());
        List<GlobalApAlSets> result = instance.findByCustomerApSets(entity.getCustomerAccount(), uuidList);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerApSetAlSet method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test4FindByCustomerApSetAlSet() {
        List<GlobalApAlSets> result = instance.findByCustomerApSetAlSet(entity.getCustomerAccount(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class GlobalApAlSetsDAO.
     */
    @Test
    public void test5FindByPK() {
        List<GlobalApAlSets> result = instance.findByPK(entity.getCustomerAccount(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

}
