/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import java.util.List;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalPtLocationNamesDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static GlobalPtLocationNamesDAO instance;
    static GlobalPtLocationNames entity;

    public GlobalPtLocationNamesDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {

        instance = PresetsMapperImpl.getInstance().globalPtLocationNamesDAO();
        entity = new GlobalPtLocationNames();
        entity.setCustomerAccount("77777");
        entity.setPointLocationName("Test Point Location Name 1");
        entity.setDescription("Test Point Location Name Description 1");
    }

    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of create method, of class GlobalPtLocationNamesDAO.
     */
    @Test
    public void test1Create() {
        instance.create(entity);
        List<GlobalPtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getPointLocationName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerApSet method, of class GlobalPtLocationNamesDAO.
     */
    @Test
    public void test2FindByCustomer() {
        List<GlobalPtLocationNames> result = instance.findByCustomer(entity.getCustomerAccount());
        System.out.println("test2FindByCustomer result - " + result.get(0).toString());
        System.out.println(" test2FindByCustomer entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class GlobalPtLocationNamesDAO.
     */
    @Test
    public void test3FindByPK() {
        List<GlobalPtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getPointLocationName());
        System.out.println("test3FindByPK result - " + result.get(0).toString());
        System.out.println("test3FindByPK entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }
    
    /**
     * Test of update method, of class GlobalPtLocationNamesDAO.
     */
    @Test
    public void test4Update() {
        entity.setDescription("Updated Desc");
        instance.update(entity);
        List<GlobalPtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getPointLocationName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class GlobalPtLocationNamesDAO.
     */
    @Test
    public void test5Delete() {
        instance.delete(entity);
        List<GlobalPtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getPointLocationName());
        Assert.assertThat(result.size(), is(0));
    }


}
