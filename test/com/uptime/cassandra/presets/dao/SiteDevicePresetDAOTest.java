/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteDevicePresetDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteDevicePresetDAO instance;
    private static SiteDevicePreset entity = null;
    
    public SiteDevicePresetDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().siteDevicePresetDAO();
        entity = new SiteDevicePreset();
        entity.setAlSetId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        entity.setApSetId(UUID.fromString("668e4093-b882-4c6a-b9f5-a4af8b8fa47f"));
        entity.setAlarmEnabled(true);
        entity.setAutoAcknowledge(false);
        entity.setChannelNumber((byte) 1);
        entity.setChannelType("DC");
        entity.setCustomerAccount("77777");
        entity.setDeviceType("Mist");
        entity.setDisabled(false);
        entity.setName("Preset Site A");
        entity.setPresetId(UUID.fromString("3384f9d0-d1ff-4690-bcc9-57cba146f992"));
        entity.setSiteId(UUID.fromString("584a02ef-ce87-434c-91d3-ae45830ea19e"));
        entity.setSampleInterval(25);
        entity.setSensorOffset(0.8f);
        entity.setSensorSensitivity(0.95f);
        entity.setSensorType("TEMP");
        entity.setSensorUnits("Celcius");
        entity.setSensorSubType("Test sensor subtype");
        entity.setSensorLocalOrientation("H");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
    }

    /**
     * Test of findByCustomer method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<SiteDevicePreset> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerDeviceType method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test3_FindByCustomerSiteDeviceType() {
        System.out.println("findByCustomerSiteDeviceType");
        List<SiteDevicePreset> result = instance.findByCustomerSiteDeviceType(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceType());
        assertEquals(entity, result.get(0));
    }
    
    /**
     * Test of findByCustomerDeviceType method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test31_FindByCustomerSiteDeviceTypePreset() {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        List<SiteDevicePreset> result = instance.findByCustomerSiteDeviceTypePreset(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceType(), entity.getPresetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<SiteDevicePreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceType(), entity.getPresetId(), entity.getChannelType(), entity.getChannelNumber());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test5_Update() {
        System.out.println("update");
        entity.setSampleInterval(200);
        instance.update(entity);
        List<SiteDevicePreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceType(), entity.getPresetId(), entity.getChannelType(), entity.getChannelNumber());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class GlobalDevicePresetDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteDevicePreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceType(), entity.getPresetId(), entity.getChannelType(), entity.getChannelNumber());
        assertEquals(0, result.size());
    }
    
}
