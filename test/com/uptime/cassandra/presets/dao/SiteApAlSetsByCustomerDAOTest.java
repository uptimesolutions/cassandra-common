/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteApAlSetsByCustomerDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SiteApAlSetsByCustomerDAO instance;
    static SiteApAlSetsByCustomer entity;

    public SiteApAlSetsByCustomerDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().siteApAlSetsByCustomerDAO();
        entity = new SiteApAlSetsByCustomer();
        entity.setCustomerAccount("77777");
        entity.setSensorType("Accelaration");
        entity.setSiteId(UUID.fromString("efc6093c-3042-4918-a9e6-70b757e801fe"));
        entity.setApSetId(UUID.randomUUID());
        entity.setAlSetId(UUID.randomUUID());
        entity.setApSetName("Site Test Ap Set Name");
        entity.setAlSetName("Site Test Al Set Name");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteApAlSetsByCustomer> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSensorType(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test6_Update() {
        System.out.println("update");
        entity.setAlSetName("Updated Test Site Al Set Name");
        instance.update(entity);
        List<SiteApAlSetsByCustomer> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSensorType(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test7_Delete_SiteApAlSetsByCustomer() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteApAlSetsByCustomer> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSensorType(), entity.getApSetId(), entity.getAlSetId());
        Assert.assertThat(result.size(), is(0));
    }

    /**
     * Test of findByCustomerSiteId method, of class SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test2_FindByCustomerSiteId() {
        System.out.println("findByCustomerSiteId");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        List<SiteApAlSetsByCustomer> result = instance.findByCustomerSiteId(customerAccount, siteId);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSensorType method, of class
     * SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test3_FindByCustomerSensorType() {
        System.out.println("findByCustomerSensorType");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        String sensorType = entity.getSensorType();

        List<SiteApAlSetsByCustomer> result = instance.findByCustomerSensorType(customerAccount, siteId, sensorType);
        assertEquals(entity.toString(), result.get(0).toString());

    }

    /**
     * Test of findByCustomerSiteIdSensorTypeApSet method, of class
     * SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test4_FindByCustomerSiteIdSensorTypeApSet() {
        System.out.println("findByCustomerSiteIdSensorTypeApSet");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        String sensorType = entity.getSensorType();
        UUID apSetId = entity.getApSetId();
        List<SiteApAlSetsByCustomer> result = instance.findByCustomerSiteIdSensorTypeApSet(customerAccount, siteId, sensorType, apSetId);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class SiteApAlSetsByCustomerDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        String customerAccount = entity.getCustomerAccount();
        UUID siteId = entity.getSiteId();
        String sensorType = entity.getSensorType();
        UUID apSetId = entity.getApSetId();
        UUID alSetId = entity.getAlSetId();
        List<SiteApAlSetsByCustomer> result = instance.findByPK(customerAccount, siteId, sensorType, apSetId, alSetId);
        assertEquals(entity.toString(), result.get(0).toString());
    }

}
