/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalApAlSetsByCustomerDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static GlobalApAlSetsByCustomerDAO instance;
    static GlobalApAlSetsByCustomer entity;

    public GlobalApAlSetsByCustomerDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().globalApAlSetsByCustomerDAO();
        entity = new GlobalApAlSetsByCustomer();
        entity.setCustomerAccount("77777");
        entity.setSensorType("Accelaration");
        entity.setApSetId(UUID.randomUUID());
        entity.setAlSetId(UUID.randomUUID());
        entity.setApSetName("Global Test Ap Set Name");
        entity.setAlSetName("Global Test Al Set Name");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<GlobalApAlSetsByCustomer> result = instance.findByPK(entity.getCustomerAccount(), entity.getSensorType(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test6_Update() {
        System.out.println("update");
        entity.setAlSetName("Updated Test Global Al Set Name");
        instance.update(entity);
        List<GlobalApAlSetsByCustomer> result = instance.findByPK(entity.getCustomerAccount(), entity.getSensorType(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test7_Delete_GlobalApAlSetsByCustomer() {
        System.out.println("delete");
        instance.delete(entity);
        List<GlobalApAlSetsByCustomer> result = instance.findByPK(entity.getCustomerAccount(), entity.getSensorType(), entity.getApSetId(), entity.getAlSetId());
        Assert.assertThat(result.size(), is(0));
    }

    /**
     * Test of findByCustomer method, of class GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test2_FindByCustomer() {
        System.out.println("findByCustomer");
        String customerAccount = entity.getCustomerAccount();
        List<GlobalApAlSetsByCustomer> result = instance.findByCustomer(customerAccount);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSensorType method, of class
     * GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test3_FindByCustomerSensorType() {
        System.out.println("findByCustomerSensorType");
        String customerAccount = entity.getCustomerAccount();
        String sensorType = entity.getSensorType();
        List<GlobalApAlSetsByCustomer> result = instance.findByCustomerSensorType(customerAccount, sensorType);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSensorTypeApSet method, of class
     * GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test4_FindByCustomerSensorTypeApSet() {
        System.out.println("findByCustomerSensorTypeApSet");
        String customerAccount = entity.getCustomerAccount();
        String sensorType = entity.getSensorType();
        UUID apSetId = entity.getApSetId();
        List<GlobalApAlSetsByCustomer> result = instance.findByCustomerSensorTypeApSet(customerAccount, sensorType, apSetId);
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class GlobalApAlSetsByCustomerDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        String customerAccount = entity.getCustomerAccount();
        String sensorType = entity.getSensorType();
        UUID apSetId = entity.getApSetId();
        UUID alSetId = entity.getAlSetId();
        List<GlobalApAlSetsByCustomer> result = instance.findByPK(customerAccount, sensorType, apSetId, alSetId);
        assertEquals(entity.toString(), result.get(0).toString());
    }

}
