/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SitePtLocationNamesDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SitePtLocationNamesDAO instance;
    static SitePtLocationNames entity;

    public SitePtLocationNamesDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {

        instance = PresetsMapperImpl.getInstance().sitePtLocationNamesDAO();
        entity = new SitePtLocationNames();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("efc6093c-3042-4918-a9e6-70b757e801fe"));
        entity.setPointLocationName("Test Point Location Name 1");
        entity.setDescription("Test Point Location Name Description 1");
    }

    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of create method, of class SitePtLocationNamesDAO.
     */
    @Test
    public void test1Create() {
        instance.create(entity);
        List<SitePtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId() ,entity.getPointLocationName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerApSet method, of class SitePtLocationNamesDAO.
     */
    @Test
    public void test2FindByCustomer() {
        List<SitePtLocationNames> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        System.out.println("test2FindByCustomer result - " + result.get(0).toString());
        System.out.println(" test2FindByCustomer entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class SitePtLocationNamesDAO.
     */
    @Test
    public void test3FindByPK() {
        List<SitePtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getPointLocationName());
        System.out.println("test3FindByPK result - " + result.get(0).toString());
        System.out.println("test3FindByPK entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }
    
    /**
     * Test of update method, of class SitePtLocationNamesDAO.
     */
    @Test
    public void test4Update() {
        entity.setDescription("Updated Desc");
        instance.update(entity);
        List<SitePtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getPointLocationName());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class SitePtLocationNamesDAO.
     */
    @Test
    public void test5Delete() {
        instance.delete(entity);
        List<SitePtLocationNames> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getPointLocationName());
        Assert.assertThat(result.size(), is(0));
    }

}
