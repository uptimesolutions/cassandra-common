/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalAssetPresetDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalAssetPresetDAO instance;
    private static GlobalAssetPreset entity = null;

    public GlobalAssetPresetDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().globalAssetPresetDAO();
        entity = new GlobalAssetPreset();
        entity.setCustomerAccount("77777");
        entity.setDevicePresetType("MistLX");
        entity.setAssetPresetId(UUID.randomUUID());
        entity.setAssetPresetName("Test Asset Preset Name 1");
        entity.setAssetTypeName("Test Asset Type 1");
        entity.setDevicePresetId(UUID.randomUUID());
        entity.setDescription("Test Asset Type Desc");
        entity.setFfSetIds("Test FFsetIds");
        entity.setPointLocationName("Test Point Location 1");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create");
        instance.create(entity);

        List<GlobalAssetPreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getAssetTypeName(), entity.getAssetPresetId(), entity.getPointLocationName());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test1_Update() {
        System.out.println("update");
        entity.setDescription("Description Updated");
        instance.update(entity);
          List<GlobalAssetPreset> result = instance.findByPK(entity.getCustomerAccount(), entity.getAssetTypeName(), entity.getAssetPresetId(), entity.getPointLocationName());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
    }

    /**
     * Test of findByCustomerAssetType method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test2_FindByCustomer() {
        System.out.println("findByCustomer");
        List<GlobalAssetPreset> result = instance.findByCustomer(entity.getCustomerAccount());
        assertEquals(entity, result.get(0));
    }
    
    /**
     * Test of findByCustomerAssetType method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test3_FindByCustomerAssetType() {
        System.out.println("findByCustomerAssetType");
        List<GlobalAssetPreset> result = instance.findByCustomerAssetType(entity.getCustomerAccount(), entity.getAssetTypeName());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerAssetTypePresetId method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test4_FindByCustomerAssetTypePresetId() {
        System.out.println("findByCustomerAssetTypePresetId");
        String customer = entity.getCustomerAccount();
        String assetTypeName = entity.getAssetTypeName();
        UUID assetPresetId = entity.getAssetPresetId();

        List<GlobalAssetPreset> result = instance.findByCustomerAssetTypePresetId(customer, assetTypeName, assetPresetId);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class GlobalAssetPresetDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        String customer = entity.getCustomerAccount();
        String assetTypeName = entity.getAssetTypeName();
        UUID assetPresetId = entity.getAssetPresetId();
        String pointLocationName = entity.getPointLocationName();
        List<GlobalAssetPreset> result = instance.findByPK(customer, assetTypeName, assetPresetId, pointLocationName);
        assertEquals(entity, result.get(0));
    }

}
