/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.presets.dao;

import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteFaultFrequenciesDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    static SiteFaultFrequenciesDAO instance;
    static SiteFaultFrequencies entity;

    public SiteFaultFrequenciesDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = PresetsMapperImpl.getInstance().siteFaultFrequenciesDAO();
        entity = new SiteFaultFrequencies();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.randomUUID());
        entity.setFfSetId(UUID.randomUUID());
        entity.setFfId(UUID.randomUUID());
        entity.setFfName("Test Name");
        entity.setFfSetDesc("Test Desc");
        entity.setFfSetName("Test Set Name");
        entity.setFfUnit("Test Unit");
        entity.setFfValue((float) 2);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteFaultFrequenciesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class SiteFaultFrequenciesDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        entity.setFfSetDesc("Test Updated Desc");
        instance.update(entity);
        List<SiteFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findBySetName method, of class SiteFaultFrequenciesDAO.
     */
    @Test
    public void test3_FindByCustomerSite() {
        System.out.println("findBySetName");
        List<SiteFaultFrequencies> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class SiteFaultFrequenciesDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<SiteFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteSetName method, of class
     * SiteFaultFrequenciesDAO.
     */
    @Test
    public void test5_FindByCustomerSiteSetName() {
        System.out.println("findByCustomerSiteSetName");
        List<SiteFaultFrequencies> result = instance.findByCustomerSiteSet(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class SiteFaultFrequenciesDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteFaultFrequencies> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getFfSetId(), entity.getFfId());
        assertEquals(0, result.size());
    }

}
