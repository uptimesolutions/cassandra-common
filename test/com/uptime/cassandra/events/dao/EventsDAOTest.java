/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.dao;

import com.uptime.cassandra.events.entity.Events;
import java.util.ArrayList;
import java.util.List;
import org.apache.cassandra.utils.UUIDGen;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EventsDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//events.cql", true, true, "worldview_dev1"));

    static EventsDAO instance;
    static Events events;
    static List<Events> eventsList;

    public EventsDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = EventsMapperImpl.getInstance().eventsDao();

        events = new Events();
        events.setApplication("Area");
        events.setCreatedDate(UUIDGen.getTimeUUID());
        events.setData("Test Exception Message 4444");
        events.setIpAddress("10.1.1.2");
        events.setPort("8090");

        eventsList = new ArrayList<>();
        eventsList.add(events);

    }

    /**
     * Test of create method, of class EventsApplicationDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create method testing.");
        instance.create(eventsList.get(0));
        System.out.println("create method complete.");
    }

    /**
     * Test of findByCustomerSiteArea method, of class EventsApplicationDAO.
     */
    @Test
    public void test1_FindByApplication() {
        System.out.println("findByApplication method testing.");
        List<Events> result = instance.findByApplication(eventsList.get(0).getApplication());

        assertEquals(eventsList.size(), result.size());
        assertEquals(eventsList.get(0).getApplication(), result.get(0).getApplication());
        assertEquals(eventsList.get(0).getCreatedDate(), result.get(0).getCreatedDate());
        assertEquals(eventsList.get(0).getData(), result.get(0).getData());
        assertEquals(eventsList.get(0).getIpAddress(), result.get(0).getIpAddress());
        assertEquals(eventsList.get(0).getPort(), result.get(0).getPort());
        System.out.println("findByCustomerSiteArea method complete.");
    }

    /**
     * Test of update method, of class EventsApplicationDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update method testing.");
        instance.update(eventsList.get(0));
        System.out.println("update method complete.");
    }

    /**
     * Test of delete method, of class EventsApplicationDAO.
     */
    @Test
    public void test3_Delete() {
        System.out.println("delete method testing.");
        instance.delete(eventsList.get(0));
        System.out.println("delete method complete.");
    }

}
