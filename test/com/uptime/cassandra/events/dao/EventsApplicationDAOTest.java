/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.events.dao;

import com.uptime.cassandra.events.entity.EventsApplication;
import java.util.ArrayList;
import java.util.List;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Mohd Juned Alam
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EventsApplicationDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//events.cql", true, true, "worldview_dev1"));
    private static EventsApplicationDAO instance;
    static List<EventsApplication> eaList1 = new ArrayList<>();

    @BeforeClass
    public static void setUpData() {
        instance = EventsMapperImpl.getInstance().eventsApplicationDAO();
        EventsApplication entitity = new EventsApplication();

        entitity.setApplication("1");
        entitity.setEmail("app@gmail.com");
        entitity.setNotification(Boolean.FALSE);
        eaList1.add(entitity);
    }

    /**
     * Test of create method, of class EventsApplicationDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create method testing.");
        instance.create(eaList1.get(0));
        System.out.println("create method complete.");
    }

    /**
     * Test of findByCustomerSiteArea method, of class EventsApplicationDAO.
     */
    @Test
    public void test1_FindByApplication() {
        System.out.println("findByApplication method testing.");
        List<EventsApplication> result = instance.findByApplication(eaList1.get(0).getApplication());

        assertEquals(eaList1.size(), result.size());
        assertEquals(eaList1.get(0).getApplication(), result.get(0).getApplication());
        assertEquals(eaList1.get(0).getEmail(), result.get(0).getEmail());
        assertEquals(eaList1.get(0).getNotification(), result.get(0).getNotification());
        System.out.println("findByCustomerSiteArea method complete.");
    }

    /**
     * Test of update method, of class EventsApplicationDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update method testing.");
        instance.update(eaList1.get(0));
        System.out.println("update method complete.");
    }

    /**
     * Test of delete method, of class EventsApplicationDAO.
     */
    @Test
    public void test3_Delete() {
        System.out.println("delete method testing.");
        instance.delete(eaList1.get(0));
        System.out.println("delete method complete.");
    }

}
