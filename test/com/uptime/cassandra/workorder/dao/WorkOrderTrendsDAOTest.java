/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WorkOrderTrendsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//workorders.cql", true, true, "worldview_dev1"));
    
    static WorkOrderTrendsDAO instance;
    static WorkOrderTrends entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    @BeforeClass
    public static void setUpClass() {
        instance = WorkOrdersMapperImpl.getInstance().workOrderTrendsDAO();
        
        entity = new WorkOrderTrends();
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setApSetId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setChannelType("AC");
        entity.setCreatedDate(LocalDateTime.parse("2022-10-19 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setCustomerAccount("77777");
        entity.setParamName("Peak2Peak");
        entity.setPriority((byte)2);
        entity.setPointLocationId(UUID.fromString("665611d8-382f-4c95-b5af-a23432e59dac"));
        entity.setPointId(UUID.fromString("60685b3e-758a-4295-a243-3d600d8d75b5"));
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.setRowId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
    }

    /**
     * Test of create method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<WorkOrderTrends> result = instance.findByCustomerSiteAreaAssetPriorityDate(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test2_FindByCustomerSiteAreaAssetPriorityDate() {
        System.out.println("findByCustomerSiteAreaAssetPriorityDate");
        List<WorkOrderTrends> result = instance.findByCustomerSiteAreaAssetPriorityDate(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<WorkOrderTrends> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate(), entity.getRowId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        instance.update(entity);
        List<WorkOrderTrends> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate(), entity.getRowId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<WorkOrderTrends> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate(), entity.getRowId());
        assertEquals(0, result.size());
    }
    
    /**
     * Test of printCQL method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO work_order_trends(customer_acct,site_id,area_id,asset_id,priority,created_date,row_id,point_location_id,point_id,ap_set_id,param_id,channel_type) values('77777',15676cd8-61f6-42af-ae85-f3cfd1442913,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,2,'2022-10-19T01:00:00Z',f33fcf5a-5f93-4504-bc07-74a61a460bf0,665611d8-382f-4c95-b5af-a23432e59dac,60685b3e-758a-4295-a243-3d600d8d75b5,2d2a3fc1-f82a-4710-ab83-8dfc935f9726,'Peak2Peak','AC');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class WorkOrderTrendsDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM work_order_trends WHERE customer_acct='77777' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac AND priority=2 AND created_date='2022-10-19T01:00:00Z' AND row_id=f33fcf5a-5f93-4504-bc07-74a61a460bf0;";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result DELETE - " + result);
        assertEquals(expResult, result);
    }

}
