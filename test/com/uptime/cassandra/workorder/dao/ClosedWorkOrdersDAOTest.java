/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.uptime.cassandra.workorder.entity.ClosedWorkOrders;
import com.uptime.cassandra.workorder.entity.WorkOrderRecord;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClosedWorkOrdersDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//workorders.cql", true, true, "worldview_dev1"));
    
    static ClosedWorkOrdersDAO instance;
    static ClosedWorkOrders entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    @BeforeClass
    public static void setUpClass() {
        instance = WorkOrdersMapperImpl.getInstance().closedWorkOrdersDAO();
        
        entity = new ClosedWorkOrders();
        entity.setWorkOrderDetails(new WorkOrderRecord());
        entity.setOpenedYear((short)2022);
        entity.getWorkOrderDetails().setAction("Test Action 1");
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.getWorkOrderDetails().setAreaName("Test Area 1");
        entity.getWorkOrderDetails().setAssetComponent("XXX Pump");
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.getWorkOrderDetails().setAssetName("Test Asset 1");
        entity.setCreatedDate(LocalDateTime.parse("2022-10-19 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.getWorkOrderDetails().setCloseDate(LocalDateTime.parse("2022-10-28 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setCustomerAccount("77777");
        entity.getWorkOrderDetails().setDescription("Client Asset Desc 1");
        entity.getWorkOrderDetails().setIssue("Test issue");
        entity.getWorkOrderDetails().setNotes("Test Note");
        entity.getWorkOrderDetails().setPriority("2");
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.getWorkOrderDetails().setUser("Uptime");
    }

    /**
     * Test of create method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<ClosedWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear(), entity.getAreaId(), entity.getAssetId(), entity.getCreatedDate());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSiteYear method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test2_FindByCustomerSiteYear() {
        System.out.println("findByCustomerSiteYear");
        List<ClosedWorkOrders> result = instance.findByCustomerSiteYear(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear());
        System.out.println("result - " + result.get(0).toString());
        System.out.println("entity - " + entity.toString());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSiteYearArea method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test3_FindByCustomerSiteYearArea() {
        System.out.println("findByCustomerSiteYearArea");
        List<ClosedWorkOrders> result = instance.findByCustomerSiteYearArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear(), entity.getAreaId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByCustomerSiteYearAreaAsset method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test4_FindByCustomerSiteYearAreaAsset() {
        System.out.println("findByCustomerSiteYearAreaAsset");
        List<ClosedWorkOrders> result = instance.findByCustomerSiteYearAreaAsset(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of findByPK method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        List<ClosedWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear(), entity.getAreaId(), entity.getAssetId(), entity.getCreatedDate());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of update method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test6_Update() {
        System.out.println("update");
        entity.getWorkOrderDetails().setAction("Test Action 2");
        instance.update(entity);
        List<ClosedWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear(), entity.getAreaId(), entity.getAssetId(), entity.getCreatedDate());
        assertEquals(entity.toString(), result.get(0).toString());
    }

    /**
     * Test of delete method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test7_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<ClosedWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getOpenedYear(), entity.getAreaId(), entity.getAssetId(), entity.getCreatedDate());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test8_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO closed_work_orders(customer_acct,site_id,opened_year,area_id,asset_id,created_date,work_order_details) values('77777',15676cd8-61f6-42af-ae85-f3cfd1442913,2022,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,'2022-10-19T01:00:00Z','WorkOrderRecord{areaName=Test Area 1, assetComponent=XXX Pump, assetName=Test Asset 1, priority=2, closeDate=2022-10-28T01:00:00Z, action=Test Action 2, description=Client Asset Desc 1, issue=Test issue, notes=Test Note, user=Uptime}');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of printCQL method, of class ClosedWorkOrdersDAO.
     */
    @Test
    public void test9_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM closed_work_orders WHERE customer_acct='77777' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND opened_year=2022 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac AND created_date='2022-10-19T01:00:00Z';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }

}
