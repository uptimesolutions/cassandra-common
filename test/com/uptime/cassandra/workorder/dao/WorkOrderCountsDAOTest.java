/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Mohd Juned Alam
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WorkOrderCountsDAOTest {
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//workorders.cql", true, true, "worldview_dev1"));
    
    static WorkOrderCountsDAO instance;
    static WorkOrderCounts entity;

    @BeforeClass
    public static void setUpClass() {
        instance = WorkOrdersMapperImpl.getInstance().workOrderCountsDAO();
        
        entity = new WorkOrderCounts();
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.setCustomerAccount("Uptime");
        entity.setPriority1(1);
        entity.setPriority2(2);
        entity.setPriority3(3);
        entity.setPriority4(4);
        entity.setTotalCount(11);
    }

    /**
     * Test of create method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<WorkOrderCounts> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteId method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test2_FindByCustomerSiteId() {
        System.out.println("findByCustomerSiteId");
        List<WorkOrderCounts> result = instance.findByCustomerSiteId(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteArea method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test3_FindByCustomerSiteArea() {
        System.out.println("findByCustomerSiteArea");
        List<WorkOrderCounts> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test6_FindByPK() {
        System.out.println("findByPK");
        List<WorkOrderCounts> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
    }
    
    /**
     * Test of update method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test7_Update() {
        System.out.println("update");
        entity.setPriority1(111);
        instance.update(entity);
        List<WorkOrderCounts> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
        assertEquals(entity.getPriority1(), result.get(0).getPriority1());
    }

    /**
     * Test of delete method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test8_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<WorkOrderCounts> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(0, result.size());
    }
    
    /**
     * Test of printCQL method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test90_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO work_order_counts(customer_acct,site_id,area_id,asset_id,priority_1,priority_2,priority_3,priority_4,total_count) values('Uptime',15676cd8-61f6-42af-ae85-f3cfd1442913,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,111,2,3,4,11);";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class WorkOrderCountsDAO.
     */
    @Test
    public void test91_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM work_order_counts WHERE customer_acct='Uptime' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac;";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result DELETE - " + result);
        assertEquals(expResult, result);

    }
}
