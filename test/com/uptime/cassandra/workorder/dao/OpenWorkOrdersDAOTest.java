/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.workorder.dao;

import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OpenWorkOrdersDAOTest {
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//workorders.cql", true, true, "worldview_dev1"));
    
    static OpenWorkOrdersDAO instance;
    static OpenWorkOrders entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    @BeforeClass
    public static void setUpClass() {
        instance = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
        
        entity = new OpenWorkOrders();
        entity.setAction("");
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setAreaName("Test Area 1");
        entity.setAssetComponent("XXX Pump");
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setAssetName("Test Asset 1");
        entity.setCreatedDate(LocalDateTime.parse("2022-10-19 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setCustomerAccount("77777");
        entity.setDescription("Client Asset Desc 1");
        entity.setIssue("Test issue");
        entity.setNotes("Test Note");
        entity.setPriority((byte)2);
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.setUser("Uptime");
    }

    /**
     * Test of create method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<OpenWorkOrders> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteId method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test2_FindByCustomerSiteId() {
        System.out.println("findByCustomerSiteId");
        List<OpenWorkOrders> result = instance.findByCustomerSiteId(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteArea method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test3_FindByCustomerSiteArea() {
        System.out.println("findByCustomerSiteArea");
        List<OpenWorkOrders> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test4_FindByCustomerSiteAreaAsset() {
        System.out.println("findByCustomerSiteAreaAsset");
        List<OpenWorkOrders> result = instance.findByCustomerSiteAreaAsset(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test5_FindByCustomerSiteAreaAssetPriority() {
        System.out.println("findByCustomerSiteAreaAssetPriority");
        List<OpenWorkOrders> result = instance.findByCustomerSiteAreaAssetPriority(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test6_FindByPK() {
        System.out.println("findByPK");
        List<OpenWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test7_Update() {
        System.out.println("update");
        entity.setNotes(entity.getNotes() + " AAA");
        instance.update(entity);
        List<OpenWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test8_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<OpenWorkOrders> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPriority(), entity.getCreatedDate());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test90_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO open_work_orders(customer_acct,site_id,area_id,asset_id,priority,created_date,area_name,asset_component,asset_name,description,issue,notes,user) values('77777',15676cd8-61f6-42af-ae85-f3cfd1442913,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,2,'2022-10-19T01:00:00Z','Test Area 1','XXX Pump','Test Asset 1','Client Asset Desc 1','Test issue','Test Note AAA','Uptime');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class OpenWorkOrdersDAO.
     */
    @Test
    public void test91_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM open_work_orders WHERE customer_acct='77777' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac AND priority=2 AND created_date='2022-10-19T01:00:00Z';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result DELETE - " + result);
        assertEquals(expResult, result);
    }

}
