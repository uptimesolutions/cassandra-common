/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.uptime.cassandra.alarms.entity.AlarmHistory;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmHistoryDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//alarms.cql", true, true, "worldview_dev1"));
    static AlarmHistoryDAO instance;
    static AlarmHistory entity;
    
    @BeforeClass
    public static void setUpClass() {
        instance = AlarmMapperImpl.getInstance().alarmHistoryDAO();
        entity = new AlarmHistory();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.setRowId(UUID.fromString("dec7c64f-4a02-4c26-8876-4064174f1a6c"));
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setPointLocationId(UUID.fromString("3a7ef876-8844-419f-a26a-182b292376eb"));
        entity.setPointId(UUID.fromString("587cf1f2-1363-4250-aa60-27c8cc20c2a4"));
        entity.setParamName("Test Param Name 1");
        entity.setAlarmType("Test Alarm Type 1");
        entity.setAcknowledged(true);
        entity.setCreatedYear((short)2022);
        entity.setExceedPercent((float) 0.145);
        entity.setHighAlert((float) 0.145);
        entity.setHighFault((float) 0.145);
        entity.setLowAlert((float) 0.145);
        entity.setLowFault((float) 0.145);
        entity.setParamType("Test param Type 1");
        entity.setSampleTimestamp(Instant.parse("2021-01-01T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        entity.setTrendValue((float) 0.145);
        entity.setSensorType("Test Sensor Type 1");
        entity.setTriggerTime(Instant.parse("2021-01-02T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class AlarmHistoryDAO.
     */
    @Test
    public void test1_Create_AlarmHistory() {
        System.out.println("create");
        instance.create(entity);
        List<AlarmHistory> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType(), entity.getRowId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteYearAreaAssetPtLoc method, of class AlarmHistoryDAO.
     */
    @Test
    public void test2_FindByCustomerSiteYearAreaAssetPtLoc() {
        System.out.println("findByCustomerSiteYearAreaAssetPtLoc");
        List<AlarmHistory> result = instance.findByCustomerSiteYearAreaAssetPtLoc(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteYearAreaAssetPtLocPoint method, of class AlarmHistoryDAO.
     */
    @Test
    public void test3_FindByCustomerSiteYearAreaAssetPtLocPoint() {
        System.out.println("findByCustomerSiteYearAreaAssetPtLocPoint");
        List<AlarmHistory> result = instance.findByCustomerSiteYearAreaAssetPtLocPoint(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId());
        assertEquals(entity, result.get(0));

    }

    /**
     * Test of findByCustomerSiteYearAreaAssetPtLocPointParam method, of class AlarmHistoryDAO.
     */
    @Test
    public void test4_FindByCustomerSiteYearAreaAssetPtLocPointParam() {
        System.out.println("findByCustomerSiteYearAreaAssetPtLocPointParam");
        List<AlarmHistory> result = instance.findByCustomerSiteYearAreaAssetPtLocPointParam(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteYearAreaAssetPtLocPointParamAlarmType method, of class AlarmHistoryDAO.
     */
    @Test
    public void test41_FindByCustomerSiteYearAreaAssetPtLocPointParamAlarmType() {
        System.out.println("findByCustomerSiteYearAreaAssetPtLocPointParamAlarmType");
        List<AlarmHistory> result = instance.findByCustomerSiteYearAreaAssetPtLocPointParamAlarmType(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class AlarmHistoryDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        List<AlarmHistory> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType(), entity.getRowId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class AlarmHistoryDAO.
     */
    @Test
    public void test6_Update_AlarmHistory() {
        System.out.println("update");
        entity.setExceedPercent((float) 0.888);
        instance.update(entity);
        List<AlarmHistory> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType(), entity.getRowId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class AlarmHistoryDAO.
     */
    @Test
    public void test7_Delete_AlarmHistory() {
        System.out.println("delete");
        instance.delete(entity);
        List<AlarmHistory> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getCreatedYear(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType(), entity.getRowId());
        assertEquals(0, result.size());
    }

        
    /**
     * Test of printCQL method, of class AlarmHistoryDAO.
     */
    @Test
    public void test70_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO alarm_history(customer_acct,site_id,created_year,area_id,asset_id,point_location_id,point_id,param_name,row_id,alarm_type,exceed_percent,high_alert,high_fault,is_acknowledged,low_alert,low_fault,param_type,sample_timestamp,sensor_type,trend_value,sample_timestamp) values('77777',15676cd8-61f6-42af-ae85-f3cfd1442913,2022,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,3a7ef876-8844-419f-a26a-182b292376eb,587cf1f2-1363-4250-aa60-27c8cc20c2a4,'Test Param Name 1',dec7c64f-4a02-4c26-8876-4064174f1a6c,'Test Alarm Type 1',0.888,0.145,0.145,true,0.145,0.145,'Test param Type 1','2021-01-01T01:00:00Z','Test Sensor Type 1',0.145,'2021-01-01T01:00:00Z');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class AlarmHistoryDAO.
     */
    @Test
    public void test71_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM alarm_history WHERE customer_acct='77777' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND created_year=2022 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac AND point_location_id=3a7ef876-8844-419f-a26a-182b292376eb AND point_id=587cf1f2-1363-4250-aa60-27c8cc20c2a4 AND param_name='Test Param Name 1' AND alarm_type='Test Alarm Type 1' AND row_id=dec7c64f-4a02-4c26-8876-4064174f1a6c;";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result DELETE - " + result);
        assertEquals(expResult, result);
    }
}
