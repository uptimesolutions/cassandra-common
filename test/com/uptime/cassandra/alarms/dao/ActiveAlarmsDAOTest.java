/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.uptime.cassandra.alarms.entity.ActiveAlarms;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ActiveAlarmsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//alarms.cql", true, true, "worldview_dev1"));
    static ActiveAlarmsDAO instance;
    static ActiveAlarms entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    @BeforeClass
    public static void setUpClass() {
        instance = AlarmMapperImpl.getInstance().activeAlarmsDAO();
        entity = new ActiveAlarms();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.setApSetId(UUID.fromString("dec7c64f-4a02-4c26-8876-4064174f1a6c"));
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setPointLocationId(UUID.fromString("3a7ef876-8844-419f-a26a-182b292376eb"));
        entity.setPointId(UUID.fromString("587cf1f2-1363-4250-aa60-27c8cc20c2a4"));
        entity.setParamName("Test Param Name 1");
        entity.setAlarmType("Test Alarm Type 1");
        entity.setAcknowledged(true);
        entity.setDeleted(false);
        entity.setExceedPercent((float) 0.145);
        entity.setHighAlert((float) 0.145);
        entity.setHighFault((float) 0.145);
        entity.setLowAlert((float) 0.145);
        entity.setLowFault((float) 0.145);
        entity.setParamType("Test param Type 1");
        entity.setSampleTimestamp(LocalDateTime.parse("2021-01-01 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setTrendValue((float) 0.145);
        entity.setSensorType("Test Sensor Type 1");
        entity.setTriggerTime(LocalDateTime.parse("2021-01-02 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        System.out.println("Saple Timestamp - " + entity.getSampleTimestamp());
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test1_Create_ActiveAlarms() {
        System.out.println("create");
        instance.create(entity);
        
    }

    /**
     * Test of findByCustomerSiteArea method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test3_FindByCustomerSiteArea() {
        System.out.println("findByCustomerSiteArea");
        List<ActiveAlarms> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test4_FindByCustomerSiteAreaAsset() {
        System.out.println("findByCustomerSiteAreaAsset");
        List<ActiveAlarms> result = instance.findByCustomerSiteAreaAsset(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAssetPtLoc method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test5_FindByCustomerSiteAreaAssetPtLoc() {
        System.out.println("findByCustomerSiteAreaAssetPtLoc");
        List<ActiveAlarms> result = instance.findByCustomerSiteAreaAssetPtLoc(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAssetPtLocPoint method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test6_FindByCustomerSiteAreaAssetPtLocPoint() {
        System.out.println("findByCustomerSiteAreaAssetPtLocPoint");
        List<ActiveAlarms> result = instance.findByCustomerSiteAreaAssetPtLocPoint(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAssetPtLocPointParam method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test7_FindByCustomerSiteAreaAssetPtLocPointParam() {
        System.out.println("findByCustomerSiteAreaAssetPtLocPointParam");
        List<ActiveAlarms> result = instance.findByCustomerSiteAreaAssetPtLocPointParam(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test8_FindByPK() {
        System.out.println("findByPK");
        List<ActiveAlarms> expResult = null;
        List<ActiveAlarms> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType());
        assertEquals(entity, result.get(0));
    }

    
    /**
     * Test of update method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test8_Update_ActiveAlarms() {
        System.out.println("update");
        entity.setAlarmType("Test Alarm Type 2");
        instance.update(entity);
        List<ActiveAlarms> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType());
        assertEquals(entity, result.get(0));
        
    }

    /**
     * Test of delete method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test9_Delete_ActiveAlarms() {
        System.out.println("delete");
        instance.delete(entity);
        List<ActiveAlarms> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getParamName(), entity.getAlarmType());
        assertEquals(0, result.size());
    }
    
    /**
     * Test of printCQL method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test90_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO active_alarms(customer_acct,site_id,area_id,asset_id,point_location_id,point_id,param_name,alarm_type,ap_set_id,exceed_percent,high_alert,high_fault,is_acknowledged,is_deleted,low_alert,low_fault,param_type,sample_timestamp,sensor_type,trend_value,sample_timestamp) values('77777',15676cd8-61f6-42af-ae85-f3cfd1442913,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,3a7ef876-8844-419f-a26a-182b292376eb,587cf1f2-1363-4250-aa60-27c8cc20c2a4,'Test Param Name 1','Test Alarm Type 2',dec7c64f-4a02-4c26-8876-4064174f1a6c,0.145,0.145,0.145,true,false,0.145,0.145,'Test param Type 1','2021-01-01T01:00:00Z','Test Sensor Type 1',0.145,'2021-01-01T01:00:00Z');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class ActiveAlarmsDAO.
     */
    @Test
    public void test91_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM active_alarms WHERE customer_acct='77777' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac AND point_location_id=3a7ef876-8844-419f-a26a-182b292376eb AND point_id=587cf1f2-1363-4250-aa60-27c8cc20c2a4 AND param_name='Test Param Name 1' AND alarm_type='Test Alarm Type 2';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result DELETE - " + result);
        assertEquals(expResult, result);

    }
    
}
