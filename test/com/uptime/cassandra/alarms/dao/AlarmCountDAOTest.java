/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.alarms.dao;

import com.uptime.cassandra.alarms.entity.AlarmCount;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmCountDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//alarms.cql", true, true, "worldview_dev1"));
    static AlarmCountDAO instance;
    static AlarmCount entity;
    
    @BeforeClass
    public static void setUpClass() {
        instance = AlarmMapperImpl.getInstance().alarmCountDAO();
        entity = new AlarmCount();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setPointLocationId(UUID.fromString("3a7ef876-8844-419f-a26a-182b292376eb"));
        entity.setPointId(UUID.fromString("587cf1f2-1363-4250-aa60-27c8cc20c2a4"));
        entity.setAlarmCount(1);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class AlarmCountDAO.
     */
    @Test
    public void test1_Create_AlarmCount() {
        System.out.println("create");
        instance.create(entity);
    }
    
    /**
     * Test of findByCustomerSite method, of class AlarmCountDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<AlarmCount> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteArea method, of class AlarmCountDAO.
     */
    @Test
    public void test2_FindByCustomerSiteArea() {
        System.out.println("findByCustomerSiteArea");
        List<AlarmCount> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class AlarmCountDAO.
     */
    @Test
    public void test3_FindByCustomerSiteAreaAsset() {
        System.out.println("findByCustomerSiteAreaAsset");
        List<AlarmCount> result = instance.findByCustomerSiteAreaAsset(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));

    }

    /**
     * Test of findByCustomerSiteAreaAssetPtLoc method, of class AlarmCountDAO.
     */
    @Test
    public void test4_FindByCustomerSiteAreaAssetPtLoc() {
        System.out.println("findByCustomerSiteAreaAssetPtLoc");
        List<AlarmCount> result = instance.findByCustomerSiteAreaAssetPtLoc(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class AlarmCountDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        List<AlarmCount> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId());
        assertEquals(entity, result.get(0));
    }
    
    /**
     * Test of update method, of class AlarmCountDAO.
     */
    @Test
    public void test6_Update_AlarmCount() {
        System.out.println("update");
        entity.setAlarmCount(10);
        instance.update(entity);
        List<AlarmCount> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class AlarmCountDAO.
     */
    @Test
    public void test7_Delete_AlarmCount() {
        System.out.println("delete");
        instance.delete(entity);
        List<AlarmCount> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId());
        assertEquals(0, result.size());
    }


}
