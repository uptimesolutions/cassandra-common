/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.Points;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PointsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//point_locations.cql", true, true, "worldview_dev1"));

    static PointsDAO instance;
    static Points points;
    
    public PointsDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().pointsDAO();
        points = new Points();
        points.setCustomerAccount("77777");
        points.setSiteId(UUID.randomUUID());
        points.setAreaId(UUID.randomUUID());
        points.setAssetId(UUID.randomUUID());
        points.setPointLocationId(UUID.randomUUID());
        points.setPointId(UUID.randomUUID());
        points.setPointName("Test Point Id 1");
        points.setApSetId(UUID.randomUUID());
        points.setAlSetId(UUID.randomUUID());
        points.setAutoAcknowledge(true);
        points.setDisabled(true);
        points.setPointType("Test Point Type 1");
        //points.setSampleInterval(2);
        points.setSensorChannelNum(0);
        points.setSensorOffset(0);
        points.setSensorSensitivity(0);
        points.setSensorType("Test Sensor Type 1");
        points.setSensorUnits("Test Sensor Unit 1");
        points.setAlarmEnabled(true);
//        points.setUseCustomLimits(true);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class PointToHwUnitDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(points);
        System.out.println("points.getCustomerAccount() - " + points.getCustomerAccount());
        List<Points> result = instance.findByPK(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId());
        System.out.println("result " + result);
        assertEquals(1, result.size());
        assertEquals(points, result.get(0));
    }

    /**
     * Test of update method, of class PointToHwUnitDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        points.setPointType("Test Point Type 111");
        instance.update(points);
        List<Points> result = instance.findByPK(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId());
        assertEquals(points, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class PointToHwUnitDAO.
     */
    @Test
    public void test3_FindByCustomerSiteAreaAsset() {
        System.out.println("findByCustomerSiteAreaAsset");
        String customerAccount = points.getCustomerAccount();
        UUID siteId = points.getSiteId();
        UUID areaId = points.getAreaId();
        UUID assetId = points.getAssetId();
        List<Points> expResult = new ArrayList<>();
        expResult.add(points);
        List<Points> result = instance.findByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteAreaAssetLocation method, of class PointToHwUnitDAO.
     */
    @Test
    public void test4_FindByCustomerSiteAreaAssetLocation() {
        System.out.println("findByCustomerSiteAreaAssetLocation");
        List<Points> expResult = new ArrayList<>();
        expResult.add(points);
        List<Points> result = instance.findByCustomerSiteAreaAssetLocation(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId());
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }
    
    /**
     * Test of findByCustomerSiteAreaAssetLocation method, of class PointToHwUnitDAO.
     */
    @Test
    public void test5_FindByCustomerSiteAreaAssetLocationPoint() {
        System.out.println("findByCustomerSiteAreaAssetLocationPoint");
        List<Points> expResult = new ArrayList<>();
        expResult.add(points);
        List<Points> result = instance.findByCustomerSiteAreaAssetLocationPoint(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId());
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }
    
    /**
     * Test of findByPK method, of class PointToHwUnitDAO.
     */
    @Test
    public void test6_FindByPK() {
        System.out.println("findByPK");
        List<Points> expResult = new ArrayList<>();
        expResult.add(points);
        List<Points> result = instance.findByPK(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId());
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }

    /**
     * Test of delete method, of class PointToHwUnitDAO.
     */
    @Test
    public void test7_Delete() {
        System.out.println("delete");
        instance.delete(points);
        List<Points> result = instance.findByPK(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId());
        List<Points> dbAssetSiteList = new ArrayList<>();
        assertEquals(0, result.size());
        assertEquals(dbAssetSiteList, result);
    }

}
