/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.AssetSiteArea;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssetSiteAreaDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));

    static AssetSiteAreaDAO instance;
    static AssetSiteArea entity;

    public AssetSiteAreaDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        entity = new AssetSiteArea();
        
        entity.setAreaId(UUID.randomUUID());
        entity.setAssetId(UUID.randomUUID());
        entity.setSiteId(UUID.randomUUID());
        entity.setAssetName("AssetName1");
        entity.setAssetType("assetType");
        entity.setCustomerAccount("Customer1");
        entity.setDescription("Test Description");
        entity.setAlarmEnabled(true);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class AssetSiteAreaDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<AssetSiteArea> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class AssetSiteAreaDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        entity.setDescription("Updated Description");
        instance.update(entity);
        List<AssetSiteArea> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class AssetSiteAreaDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<AssetSiteArea> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(0, result.size());
    }

    /**
     * Test of findByCustomerSite method, of class AssetSiteAreaDAO.
     */
    @Test
    public void test3_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<AssetSiteArea> result = instance.findByCustomerSiteId(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteArea method, of class AssetSiteAreaDAO.
     */
    @Test
    public void test4_FindByCustomerSiteArea() {
        System.out.println("findByCustomerSiteArea");
        List<AssetSiteArea> result = instance.findByCustomerSiteArea(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId());
        assertEquals(entity, result.get(0));

    }

    /**
     * Test of findByPK method, of class AssetSiteAreaDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        List<AssetSiteArea> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId());
        assertEquals(entity, result.get(0));
    }

}
