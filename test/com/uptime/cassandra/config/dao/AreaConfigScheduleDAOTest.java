/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AreaConfigScheduleDAOTest {
    private static final List<AreaConfigSchedule> areaConfigScheduleList = new ArrayList();
    private static AreaConfigScheduleDAO instance;
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    
    @BeforeClass
    public static void setUpClass() {
        AreaConfigSchedule entity;
        
        instance = ConfigMapperImpl.getInstance().areaConfigScheduleDAO();
        
        entity = new AreaConfigSchedule();
        entity.setCustomerAccount("customerAccount");
        entity.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        entity.setAreaId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
        entity.setScheduleId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa838"));
        entity.setAreaName("areaName");
        entity.setScheduleName("scheduleName");
        entity.setDayOfWeek((byte)0);
        entity.setDescription("description");
        entity.setContinuous(true);
        entity.setHoursOfDay("hoursOfDay");
        
        areaConfigScheduleList.add(entity);
    }

    /**
     * Test of create method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create method testing.");
        instance.create(areaConfigScheduleList.get(0));
        System.out.println("create method complete.");
    }

    /**
     * Test of findByCustomerSiteIdAreaId method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test1_FindByCustomerSiteIdAreaId() {
        System.out.println("findByCustomerSiteIdAreaId method testing.");
        List<AreaConfigSchedule> result = instance.findByCustomerSiteIdAreaId(areaConfigScheduleList.get(0).getCustomerAccount(), areaConfigScheduleList.get(0).getSiteId(), areaConfigScheduleList.get(0).getAreaId());
        
        assertEquals(areaConfigScheduleList.size(), result.size());
        assertEquals(areaConfigScheduleList.get(0).getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(areaConfigScheduleList.get(0).getSiteId(), result.get(0).getSiteId());
        assertEquals(areaConfigScheduleList.get(0).getAreaId(), result.get(0).getAreaId());
        assertEquals(areaConfigScheduleList.get(0).getScheduleId(), result.get(0).getScheduleId());
        assertEquals(areaConfigScheduleList.get(0).getAreaName(), result.get(0).getAreaName());
        assertEquals(areaConfigScheduleList.get(0).getScheduleName(), result.get(0).getScheduleName());
        assertEquals(areaConfigScheduleList.get(0).getDayOfWeek(), result.get(0).getDayOfWeek());
        assertEquals(areaConfigScheduleList.get(0).getDescription(), result.get(0).getDescription());
        assertEquals(areaConfigScheduleList.get(0).isContinuous(), result.get(0).isContinuous());
        assertEquals(areaConfigScheduleList.get(0).getHoursOfDay(), result.get(0).getHoursOfDay());
        System.out.println("findByCustomerSiteIdAreaId method complete.");
    }

    /**
     * Test of findByCustomerSiteIdAreaIdScheduleId method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test2_FindByCustomerSiteIdAreaIdScheduleIde() {
        System.out.println("findByCustomerSiteIdAreaIdScheduleId method testing.");
        List<AreaConfigSchedule> result = instance.findByCustomerSiteIdAreaIdScheduleId(areaConfigScheduleList.get(0).getCustomerAccount(), areaConfigScheduleList.get(0).getSiteId(), areaConfigScheduleList.get(0).getAreaId(), areaConfigScheduleList.get(0).getScheduleId());
        
        assertEquals(areaConfigScheduleList.size(), result.size());
        assertEquals(areaConfigScheduleList.get(0).getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(areaConfigScheduleList.get(0).getSiteId(), result.get(0).getSiteId());
        assertEquals(areaConfigScheduleList.get(0).getAreaId(), result.get(0).getAreaId());
        assertEquals(areaConfigScheduleList.get(0).getScheduleId(), result.get(0).getScheduleId());
        assertEquals(areaConfigScheduleList.get(0).getAreaName(), result.get(0).getAreaName());
        assertEquals(areaConfigScheduleList.get(0).getScheduleName(), result.get(0).getScheduleName());
        assertEquals(areaConfigScheduleList.get(0).getDayOfWeek(), result.get(0).getDayOfWeek());
        assertEquals(areaConfigScheduleList.get(0).getDescription(), result.get(0).getDescription());
        assertEquals(areaConfigScheduleList.get(0).isContinuous(), result.get(0).isContinuous());
        assertEquals(areaConfigScheduleList.get(0).getHoursOfDay(), result.get(0).getHoursOfDay());
        System.out.println("findByCustomerSiteIdAreaIdScheduleId method complete.");
    }

    /**
     * Test of findByPK method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK method testing.");
        List<AreaConfigSchedule> result = instance.findByPK(areaConfigScheduleList.get(0).getCustomerAccount(), areaConfigScheduleList.get(0).getSiteId(), areaConfigScheduleList.get(0).getAreaId(), areaConfigScheduleList.get(0).getScheduleId(), areaConfigScheduleList.get(0).getDayOfWeek());
        
        assertEquals(areaConfigScheduleList.size(), result.size());
        assertEquals(areaConfigScheduleList.get(0).getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(areaConfigScheduleList.get(0).getSiteId(), result.get(0).getSiteId());
        assertEquals(areaConfigScheduleList.get(0).getAreaId(), result.get(0).getAreaId());
        assertEquals(areaConfigScheduleList.get(0).getScheduleId(), result.get(0).getScheduleId());
        assertEquals(areaConfigScheduleList.get(0).getAreaName(), result.get(0).getAreaName());
        assertEquals(areaConfigScheduleList.get(0).getScheduleName(), result.get(0).getScheduleName());
        assertEquals(areaConfigScheduleList.get(0).getDayOfWeek(), result.get(0).getDayOfWeek());
        assertEquals(areaConfigScheduleList.get(0).getDescription(), result.get(0).getDescription());
        assertEquals(areaConfigScheduleList.get(0).isContinuous(), result.get(0).isContinuous());
        assertEquals(areaConfigScheduleList.get(0).getHoursOfDay(), result.get(0).getHoursOfDay());
        System.out.println("findByPK method complete.");
    }

    /**
     * Test of update method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update method testing.");
        instance.update(areaConfigScheduleList.get(0));
        System.out.println("update method complete.");
    }

    /**
     * Test of delete method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete method testing.");
        instance.delete(areaConfigScheduleList.get(0));
        System.out.println("delete method complete.");
    }

    /**
     * Test of printCQL method, of class AreaConfigScheduleDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL method testing.");
        String result;
        
        result  = instance.printCQL(areaConfigScheduleList.get(0), "INSERT");
        assertEquals("INSERT INTO area_config_schedule(customer_acct,site_id,area_id,schedule_id,day_of_week,is_continuous,area_name,schedule_name,description,hours_of_day) values('customerAccount',f35e2a60-67e1-11ec-a5da-8fb4174fa836,f35e2a60-67e1-11ec-a5da-8fb4174fa837,f35e2a60-67e1-11ec-a5da-8fb4174fa838,0,true,'areaName','scheduleName','description','hoursOfDay');", result);
        result  = instance.printCQL(areaConfigScheduleList.get(0), "DELETE");
        assertEquals("DELETE FROM site_customer WHERE customer_acct='customerAccount' AND site_id=f35e2a60-67e1-11ec-a5da-8fb4174fa836 AND area_id=f35e2a60-67e1-11ec-a5da-8fb4174fa837 AND schedule_id=f35e2a60-67e1-11ec-a5da-8fb4174fa838 AND day_of_week=0;", result);
        result  = instance.printCQL(null, null);
        assertEquals("", result);
        System.out.println("printCQL method complete.");
    }
}
