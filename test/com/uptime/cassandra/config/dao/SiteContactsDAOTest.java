/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.SiteContacts;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteContactsDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//site_contacts.cql", true, true, "worldview_dev1"));
    static SiteContactsDAO instance;
    static SiteContacts siteContacts;

    public SiteContactsDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().siteContactsDAO();
        siteContacts = new SiteContacts();
        siteContacts.setCustomerAccount("12");
        siteContacts.setContactName("Aswani");
        siteContacts.setContactPhone("7022998436");
        siteContacts.setContactTitle("Mr");
        siteContacts.setContactEmail("ramisetti.k@gmail.com");
        siteContacts.setRole("Site Access");
        siteContacts.setSiteName("Site 1");
        siteContacts.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));

    }

    /**
     * Test of create method, of class SiteContactsDAO.
     */
    @Test
    public void test1Create() {
        instance.create(siteContacts);
        assertEquals("Site Contacts are not same ", siteContacts, instance.findByPK(siteContacts.getCustomerAccount(), siteContacts.getSiteId(), siteContacts.getRole()).get(0));
    }

    /**
     * Test of update method, of class SiteContactsDAO.
     */
    @Test
    public void test2Update() {
        siteContacts.setContactName("Aswani Kumar");
        siteContacts.setContactTitle("Dr");
        instance.update(siteContacts);
        assertEquals("Site Contacts are not same ", siteContacts, instance.findByPK(siteContacts.getCustomerAccount(), siteContacts.getSiteId(), siteContacts.getRole()).get(0));
    }

    /**
     * Test of delete method, of class SiteContactsDAO.
     */
    @Test
    public void test6Delete() {
        instance.delete(siteContacts);
        assertEquals("Site Contacts is not deleted ", 0, instance.findByPK(siteContacts.getCustomerAccount(), siteContacts.getSiteId(), siteContacts.getRole()).size());
    }

    /**
     * Test of findByPK method, of class SiteContactsDAO.
     */
    @Test
    public void test3findByPK() {
        assertEquals(1, instance.findByPK(siteContacts.getCustomerAccount(), siteContacts.getSiteId(), siteContacts.getRole()).size());
    }

    /**
     * Test of findByCustomerSite method, of class SiteContactsDAO.
     */
    @Test
    public void test4findByCustomerSiteId() {
        assertEquals(1, instance.findByCustomerSiteId(siteContacts.getCustomerAccount(), siteContacts.getSiteId()).size());
    }

    /**
     * Test of findByCustomer method, of class SiteContactsDAO.
     */
    @Test
    public void test5findByCustomer() {
        assertEquals(1, instance.findByCustomer(siteContacts.getCustomerAccount()).size());
    }

}
