/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.SiteCustomer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteCustomerDAOTest {
    private static final List<SiteCustomer> siteCustomerList = new ArrayList();
    private static SiteCustomerDAO instance;
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    
    @BeforeClass
    public static void setUpClass() {
        SiteCustomer entity;
        
        instance = ConfigMapperImpl.getInstance().siteCustomerDAO();
        
        entity = new SiteCustomer();
        entity.setBillingAddress1("billingAddress1");
        entity.setBillingAddress2("billingAddress2");
        entity.setBillingCity("billingCity");
        entity.setBillingPostalCode("billingPostalCode");
        entity.setBillingStateProvince("billingStateProvince");
        entity.setCountry("country");
        entity.setCustomerAccount("customerAccount");
        entity.setIndustry("industry");
        entity.setLatitude("latitude");
        entity.setLongitude("longitude");
        entity.setPhysicalAddress1("physicalAddress1");
        entity.setPhysicalAddress2("physicalAddress2");
        entity.setPhysicalCity("physicalCity");
        entity.setPhysicalPostalCode("physicalPostalCode");
        entity.setPhysicalStateProvince("physicalStateProvince");
        entity.setRegion("region");
        entity.setShipAddress1("shipAddress1");
        entity.setShipAddress2("shipAddress2");
        entity.setShipCity("shipCity");
        entity.setShipPostalCode("shipPostalCode");
        entity.setShipStateProvince("shipStateProvince");
        entity.setSiteName("siteName");
        entity.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        entity.setTimezone("timezone");
        
        siteCustomerList.add(entity);
    }

    /**
     * Test of create method, of class SiteCustomerDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create method testing.");
        instance.create(siteCustomerList.get(0));
        System.out.println("create method complete.");
    }

    /**
     * Test of findByPK method, of class SiteCustomerDAO.
     */
    @Test
    public void test1_FindByPK() {
        System.out.println("findByPK method testing.");
        List<SiteCustomer> result = instance.findByPK(siteCustomerList.get(0).getCustomerAccount(),siteCustomerList.get(0).getSiteId());
        
        assertEquals(siteCustomerList.size(), result.size());
        assertEquals(siteCustomerList.get(0).getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(siteCustomerList.get(0).getSiteId(), result.get(0).getSiteId());
        assertEquals(siteCustomerList.get(0).getSiteName(), result.get(0).getSiteName());
        assertEquals(siteCustomerList.get(0).getRegion(), result.get(0).getRegion());
        assertEquals(siteCustomerList.get(0).getCountry(), result.get(0).getCountry());
        assertEquals(siteCustomerList.get(0).getBillingAddress1(), result.get(0).getBillingAddress1());
        assertEquals(siteCustomerList.get(0).getBillingAddress2(), result.get(0).getBillingAddress2());
        assertEquals(siteCustomerList.get(0).getBillingCity(), result.get(0).getBillingCity());
        assertEquals(siteCustomerList.get(0).getBillingPostalCode(), result.get(0).getBillingPostalCode());
        assertEquals(siteCustomerList.get(0).getBillingStateProvince(), result.get(0).getBillingStateProvince());
        assertEquals(siteCustomerList.get(0).getIndustry(), result.get(0).getIndustry());
        assertEquals(siteCustomerList.get(0).getLatitude(), result.get(0).getLatitude());
        assertEquals(siteCustomerList.get(0).getLongitude(), result.get(0).getLongitude());
        assertEquals(siteCustomerList.get(0).getPhysicalAddress1(), result.get(0).getPhysicalAddress1());
        assertEquals(siteCustomerList.get(0).getPhysicalAddress2(), result.get(0).getPhysicalAddress2());
        assertEquals(siteCustomerList.get(0).getPhysicalCity(), result.get(0).getPhysicalCity());
        assertEquals(siteCustomerList.get(0).getPhysicalPostalCode(), result.get(0).getPhysicalPostalCode());
        assertEquals(siteCustomerList.get(0).getPhysicalStateProvince(), result.get(0).getPhysicalStateProvince());
        assertEquals(siteCustomerList.get(0).getShipAddress1(), result.get(0).getShipAddress1());
        assertEquals(siteCustomerList.get(0).getShipAddress2(), result.get(0).getShipAddress2());
        assertEquals(siteCustomerList.get(0).getShipCity(), result.get(0).getShipCity());
        assertEquals(siteCustomerList.get(0).getShipPostalCode(), result.get(0).getShipPostalCode());
        assertEquals(siteCustomerList.get(0).getShipStateProvince(), result.get(0).getShipStateProvince());
        assertEquals(siteCustomerList.get(0).getTimezone(), result.get(0).getTimezone());
        System.out.println("findByPK method complete.");
    }

    /**
     * Test of findByCustomer method, of class SiteCustomerDAO.
     */
    @Test
    public void test2_FindByCustomer() {
        System.out.println("findByCustomer method testing.");
        List<SiteCustomer> result = instance.findByCustomer(siteCustomerList.get(0).getCustomerAccount());
        
        assertEquals(siteCustomerList.size(), result.size());
        assertEquals(siteCustomerList.get(0).getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(siteCustomerList.get(0).getSiteId(), result.get(0).getSiteId());
        assertEquals(siteCustomerList.get(0).getSiteName(), result.get(0).getSiteName());
        assertEquals(siteCustomerList.get(0).getRegion(), result.get(0).getRegion());
        assertEquals(siteCustomerList.get(0).getCountry(), result.get(0).getCountry());
        assertEquals(siteCustomerList.get(0).getBillingAddress1(), result.get(0).getBillingAddress1());
        assertEquals(siteCustomerList.get(0).getBillingAddress2(), result.get(0).getBillingAddress2());
        assertEquals(siteCustomerList.get(0).getBillingCity(), result.get(0).getBillingCity());
        assertEquals(siteCustomerList.get(0).getBillingPostalCode(), result.get(0).getBillingPostalCode());
        assertEquals(siteCustomerList.get(0).getBillingStateProvince(), result.get(0).getBillingStateProvince());
        assertEquals(siteCustomerList.get(0).getIndustry(), result.get(0).getIndustry());
        assertEquals(siteCustomerList.get(0).getLatitude(), result.get(0).getLatitude());
        assertEquals(siteCustomerList.get(0).getLongitude(), result.get(0).getLongitude());
        assertEquals(siteCustomerList.get(0).getPhysicalAddress1(), result.get(0).getPhysicalAddress1());
        assertEquals(siteCustomerList.get(0).getPhysicalAddress2(), result.get(0).getPhysicalAddress2());
        assertEquals(siteCustomerList.get(0).getPhysicalCity(), result.get(0).getPhysicalCity());
        assertEquals(siteCustomerList.get(0).getPhysicalPostalCode(), result.get(0).getPhysicalPostalCode());
        assertEquals(siteCustomerList.get(0).getPhysicalStateProvince(), result.get(0).getPhysicalStateProvince());
        assertEquals(siteCustomerList.get(0).getShipAddress1(), result.get(0).getShipAddress1());
        assertEquals(siteCustomerList.get(0).getShipAddress2(), result.get(0).getShipAddress2());
        assertEquals(siteCustomerList.get(0).getShipCity(), result.get(0).getShipCity());
        assertEquals(siteCustomerList.get(0).getShipPostalCode(), result.get(0).getShipPostalCode());
        assertEquals(siteCustomerList.get(0).getShipStateProvince(), result.get(0).getShipStateProvince());
        assertEquals(siteCustomerList.get(0).getTimezone(), result.get(0).getTimezone());
        System.out.println("findByCustomer method complete.");
    }

    /**
     * Test of update method, of class SiteCustomerDAO.
     */
    @Test
    public void test3_Update() {
        System.out.println("update method testing.");
        instance.update(siteCustomerList.get(0));
        System.out.println("update method complete.");
    }

    /**
     * Test of delete method, of class SiteCustomerDAO.
     */
    @Test
    public void test4_Delete() {
        System.out.println("delete method testing.");
        instance.delete(siteCustomerList.get(0));
        System.out.println("delete method complete.");
    }

    /**
     * Test of printCQL method, of class SiteCustomerDAO.
     */
    @Test
    public void test5_PrintCQL() {
        System.out.println("printCQL method testing.");
        String result;
        
        result  = instance.printCQL(siteCustomerList.get(0), "INSERT");
        assertEquals("INSERT INTO site_customer(customer_acct,site_id,site_name,region,country,billing_address_1,billing_address_2,billing_city,billing_postal_code,billing_state_province,industry,latitude,longitude,physical_address_1,physical_address_2,physical_city,physical_postal_code,physical_state_province,ship_address_1,ship_address_2,ship_city,ship_postal_code,ship_state_province,timezone) values('customerAccount',f35e2a60-67e1-11ec-a5da-8fb4174fa836,'siteName','region','country','billingAddress1','billingAddress2','billingCity','billingPostalCode','billingStateProvince','industry','latitude','longitude','physicalAddress1','physicalAddress2','physicalCity','physicalPostalCode','physicalStateProvince','shipAddress1','shipAddress2','shipCity','shipPostalCode','shipStateProvince','timezone');", result);
        result  = instance.printCQL(siteCustomerList.get(0), "DELETE");
        assertEquals("DELETE FROM site_customer WHERE customer_acct='customerAccount' AND site_id=f35e2a60-67e1-11ec-a5da-8fb4174fa836;", result);
        result  = instance.printCQL(null, null);
        assertEquals("", result);
        System.out.println("printCQL method complete.");
    }
}
