/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.HwUnitToPoint;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HwUnitToPointDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    static HwUnitToPointDAO instance;
    static HwUnitToPoint hwUnitToPoint;
    
    public HwUnitToPointDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        hwUnitToPoint = new HwUnitToPoint();
        hwUnitToPoint.setApSetId(UUID.fromString("6c7b6b03-3fd0-45ca-8e65-6af3aaea62e7"));
        hwUnitToPoint.setAreaId(UUID.fromString("1441a3e6-ce47-4aee-b992-06e55668606f"));
        hwUnitToPoint.setAssetId(UUID.fromString("5551affa-0b84-4f21-ae0e-9fcf6d0df88d"));
        hwUnitToPoint.setChannelNum((byte)1);
        hwUnitToPoint.setChannelType("AC");
        hwUnitToPoint.setCustomerAcct("77777");
        hwUnitToPoint.setDeviceId("TEST_DEVICE_ID_1");
        hwUnitToPoint.setPointId(UUID.fromString("08e0faff-c12f-4115-9991-aae9fa01a061"));
        hwUnitToPoint.setPointLocationId(UUID.fromString("b195a1c9-2d5d-4693-b5cc-e7eba09be365"));
        hwUnitToPoint.setSiteId(UUID.fromString("f02bca05-e3cf-4040-83d4-d2d847fb4c54"));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class HwUnitToPointDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(hwUnitToPoint);
        List<HwUnitToPoint> expResult = new ArrayList<>();
        expResult.add(hwUnitToPoint);
        List<HwUnitToPoint> result = instance.findByPK(hwUnitToPoint.getDeviceId(), hwUnitToPoint.getChannelType(), hwUnitToPoint.getChannelNum());
        assertEquals(expResult, result);

    }

    /**
     * Test of findByPK method, of class HwUnitToPointDAO.
     */
    @Test
    public void test2_FindByDevice() {
        System.out.println("findByDevice");
        List<HwUnitToPoint> expResult = new ArrayList<>();
        expResult.add(hwUnitToPoint);
        List<HwUnitToPoint> result = instance.findByDevice(hwUnitToPoint.getDeviceId());
        assertEquals(expResult, result);
    }
    

    /**
     * Test of findByPK method, of class HwUnitToPointDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<HwUnitToPoint> expResult = new ArrayList<>();
        expResult.add(hwUnitToPoint);
        List<HwUnitToPoint> result = instance.findByPK(hwUnitToPoint.getDeviceId(), hwUnitToPoint.getChannelType(), hwUnitToPoint.getChannelNum());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of update method, of class HwUnitToPointDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        hwUnitToPoint.setCustomerAcct("66666");
        instance.update(hwUnitToPoint);
        List<HwUnitToPoint> expResult = new ArrayList<>();
        expResult.add(hwUnitToPoint);
        List<HwUnitToPoint> result = instance.findByPK(hwUnitToPoint.getDeviceId(), hwUnitToPoint.getChannelType(), hwUnitToPoint.getChannelNum());
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class HwUnitToPointDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(hwUnitToPoint);
        List<HwUnitToPoint> result = instance.findByPK(hwUnitToPoint.getDeviceId(), hwUnitToPoint.getChannelType(), hwUnitToPoint.getChannelNum());
        assertEquals(0, result.size());
    }    
}
