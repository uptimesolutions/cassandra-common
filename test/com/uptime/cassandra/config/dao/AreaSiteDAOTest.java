/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.AreaSite;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AreaSiteDAOTest {
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    static AreaSiteDAO instance;
    static AreaSite areaSite;

    public AreaSiteDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().areaSiteDAO();
        areaSite = new AreaSite();
        areaSite.setCustomerAccount("10");
        areaSite.setAreaName("Area 1");
        areaSite.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        areaSite.setAreaId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
        areaSite.setDescription("Test Desc");
        areaSite.setEnvironment("Test");
        areaSite.setClimateControlled(true);
    }

    /**
     * Test of create method, of class AreaSiteDAO.
     */
    @Test
    public void test1Create() {
        instance.create(areaSite);
        AreaSite areaSiteResult = instance.findByPK(areaSite.getCustomerAccount(), areaSite.getSiteId(), areaSite.getAreaId()).get(0);
        assertEquals(areaSite, areaSiteResult);

    }

    /**
     * Test of update method, of class AreaSiteDAO.
     */
    @Test
    public void test2Update() {
        System.out.println("testUpdate");
        areaSite.setDescription("Description updated");
        areaSite.setClimateControlled(false);
        instance.update(areaSite);
        AreaSite areaSiteResult = instance.findByPK(areaSite.getCustomerAccount(), areaSite.getSiteId(), areaSite.getAreaId()).get(0);
        assertEquals(areaSite, areaSiteResult);
    }

    /**
     * Test of findByCustomerSiteId method, of class AreaSiteDAO.
     */
    @Test
    public void test3findByCustomerSiteId() {
        List result = instance.findByCustomerSiteId(areaSite.getCustomerAccount(), areaSite.getSiteId());
        assertEquals(1, result.size());
    }

    /**
     * Test of findByPK method, of class AreaSiteDAO.
     */
    @Test
    public void test4findByPK() {
        List result = instance.findByPK(areaSite.getCustomerAccount(), areaSite.getSiteId(), areaSite.getAreaId());
        assertEquals(1, result.size());
    }

    /**
     * Test of delete method, of class AreaSiteDAO.
     */
    @Test
    public void test5Delete() {
        instance.delete(areaSite);
        List result = instance.findByPK(areaSite.getCustomerAccount(), areaSite.getSiteId(), areaSite.getAreaId());
        assertEquals(0, result.size());
    }
}
