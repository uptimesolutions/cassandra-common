/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.Subscriptions;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SubscriptionsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    static SubscriptionsDAO instance;
    static Subscriptions entity;
    
    public SubscriptionsDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().subscriptionsDAO();
        entity = new Subscriptions();
        entity.setCustomerAccount("FEDEX EXPRESS");
        entity.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        entity.setSubscriptionType("ALARMS");
        entity.setMqProtocol("ARTEMIS");
        entity.setMqConnectString("tcp://mqdev.corp.uptime-solutions.us:61616");
        entity.setMqUser("Test User");
        entity.setMqPwd("Test pwd");
        entity.setMqQueueName("Test Queue");
        entity.setMqClientId("Test Mq Client");
        entity.setInternal(true);
        entity.setWebhookUrl("Test Web hook");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SubscriptionsDAO.
     */
    @Test
    public void test01_CreateSubscriptions() {
        System.out.println("create");
        instance.create(entity);
        List<Subscriptions> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSubscriptionType());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteId method, of class SubscriptionsDAO.
     */
    @Test
    public void test02_FindByCustomerSiteId() {
        System.out.println("findByCustomerSiteId");
        List<Subscriptions> result = instance.findByCustomerSiteId(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }


    /**
     * Test of findByPK method, of class SubscriptionsDAO.
     */
    @Test
    public void test03_FindByPK() {
        System.out.println("findByPK");
        List<Subscriptions> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSubscriptionType());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class SubscriptionsDAO.
     */
    @Test
    public void test04_Update_Subscriptions() {
        System.out.println("update");
        entity.setWebhookUrl("Updated");
        instance.update(entity);
        List<Subscriptions> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSubscriptionType());
        assertEquals(entity.getWebhookUrl(), result.get(0).getWebhookUrl());
    }

    /**
     * Test of delete method, of class SubscriptionsDAO.
     */
    @Test
    public void test05_Delete_Subscriptions() {
        System.out.println("delete");
        instance.delete(entity);
        List<Subscriptions> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getSubscriptionType());
        assertEquals(0, result.size());
    }  
    
    /**
     * Test of printCQL method, of class SubscriptionsDAO.
     */
    @Test
    public void test05_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO subscriptions(customer_acct,site_id,subscription_type,mq_protocol,mq_connect_string,mq_user,mq_pwd,mq_queue_name,mq_client_id,is_internal,webhook_url) values('FEDEX EXPRESS',f35e2a60-67e1-11ec-a5da-8fb4174fa836,'ALARMS','ARTEMIS','tcp://mqdev.corp.uptime-solutions.us:61616','Test User','Test pwd','Test Queue','Test Mq Client',true,'Updated');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("test05_PrintCQL result - " + result);
        assertEquals(expResult, result);

    }
    /**
     * Test of printCQL method, of class SubscriptionsDAO.
     */
    @Test
    public void test06_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM subscriptions WHERE customer_acct='FEDEX EXPRESS' AND site_id=f35e2a60-67e1-11ec-a5da-8fb4174fa836 AND subscription_type='ALARMS';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("test06_PrintCQL result - " + result);
        assertEquals(expResult, result);
    }

    
}
