/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.SiteRegionCountry;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteRegionCountryDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));

    static SiteRegionCountryDAO instance;
    static SiteRegionCountry entity;

    public SiteRegionCountryDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().siteRegionCountryDAO();
        entity = new SiteRegionCountry();
        entity.setBillingAddress1("billingAddress1");
        entity.setBillingAddress2("billingAddress2");
        entity.setBillingCity("billingCity");
        entity.setBillingPostalCode("billingPostalCode");
        entity.setBillingStateProvince("billingStateProvince");
        entity.setCountry("country");
        entity.setCustomerAccount("customerAccount");
        entity.setIndustry("industry");
        entity.setLatitude("latitude");
        entity.setLongitude("longitude");
        entity.setPhysicalAddress1("physicalAddress1");
        entity.setPhysicalAddress2("physicalAddress2");
        entity.setPhysicalCity("physicalCity");
        entity.setPhysicalPostalCode("physicalPostalCode");
        entity.setPhysicalStateProvince("physicalStateProvince");
        entity.setRegion("region");
        entity.setShipAddress1("shipAddress1");
        entity.setShipAddress2("shipAddress2");
        entity.setShipCity("shipCity");
        entity.setShipPostalCode("shipPostalCode");
        entity.setShipStateProvince("shipStateProvince");
        entity.setSiteName("siteName");
        entity.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        entity.setTimezone("timezone");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteRegionCountryDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteRegionCountry> result = instance.findByPK(entity.getCustomerAccount(), entity.getRegion(), entity.getCountry(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class SiteRegionCountryDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        instance.update(entity);
        List<SiteRegionCountry> result = instance.findByPK(entity.getCustomerAccount(), entity.getRegion(), entity.getCountry(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomer method, of class SiteRegionCountryDAO.
     */
    @Test
    public void test3_FindByCustomer() {
        System.out.println("findByCustomer");
        List<SiteRegionCountry> result = instance.findByCustomer(entity.getCustomerAccount());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerRegion method, of class SiteRegionCountryDAO.
     */
    @Test
    public void test4_FindByCustomerRegion() {
        System.out.println("findByCustomerRegion");
        List<SiteRegionCountry> result = instance.findByCustomerRegion(entity.getCustomerAccount(), entity.getRegion());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerRegionCountry method, of class
     * SiteRegionCountryDAO.
     */
    @Test 
    public void test5_FindByCustomerRegionCountry() {
        System.out.println("findByCustomerRegionCountry");
        List<SiteRegionCountry> result = instance.findByCustomerRegionCountry(entity.getCustomerAccount(), entity.getRegion(), entity.getCountry());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class
     * SiteRegionCountryDAO.
     */
    @Test
    public void test6_FindByPK() {
        System.out.println("findByPK");
        List<SiteRegionCountry> result = instance.findByPK(entity.getCustomerAccount(), entity.getRegion(), entity.getCountry(), entity.getSiteId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class SiteRegionCountryDAO.
     */
    @Test
    public void test7_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteRegionCountry> result = instance.findByPK(entity.getCustomerAccount(), entity.getRegion(), entity.getCountry(), entity.getSiteId());
        assertEquals(0, result.size());
    }
}
