/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.PointLocationHwUnitHistory;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PointLocationHwUnitHistoryDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    private static PointLocationHwUnitHistoryDAO instance;
    private static PointLocationHwUnitHistory entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    public PointLocationHwUnitHistoryDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().pointLocationHwUnitHistoryDAO();
        entity = new PointLocationHwUnitHistory();
        entity.setPointLocationId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        entity.setDeviceId("BB004567");
        entity.setDateFrom(LocalDateTime.parse("2024-01-10 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setCustomerAccount("FEDEX EXPRESS");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test01_Create() {
        System.out.println("create");
        instance.create(entity);
    }


    /**
     * Test of findByPointLocation method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test02_FindByPointLocation() {
        System.out.println("findByPointLocation");
        List<PointLocationHwUnitHistory> result = instance.findByPointLocation(entity.getPointLocationId());
        assertEquals(entity.getPointLocationId(), result.get(0).getPointLocationId());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());
        assertEquals(entity.getDateFrom(), result.get(0).getDateFrom());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());
    }
    
    /**
     * Test of findByPK method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test03_FindByPK() {
        System.out.println("findByPK");
        List<PointLocationHwUnitHistory> result = instance.findByPK(entity.getPointLocationId(), entity.getDeviceId(), entity.getDateFrom());
        assertEquals(entity.getPointLocationId(), result.get(0).getPointLocationId());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());
        assertEquals(entity.getDateFrom(), result.get(0).getDateFrom());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());
    }

    /**
     * Test of update method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test04_Update() {
        System.out.println("update");
        entity.setCustomerAccount("Updated");
        instance.update(entity);
        List<PointLocationHwUnitHistory> result = instance.findByPK(entity.getPointLocationId(), entity.getDeviceId(), entity.getDateFrom());
        assertEquals(entity.getPointLocationId(), result.get(0).getPointLocationId());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());
        assertEquals(entity.getDateFrom(), result.get(0).getDateFrom());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());       
    }

    /**
     * Test of delete method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test04_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<PointLocationHwUnitHistory> result = instance.findByPK(entity.getPointLocationId(), entity.getDeviceId(), entity.getDateFrom());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test06_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO point_location_hwunit_history(point_location_id,device_id,date_from,customer_acct) values('f35e2a60-67e1-11ec-a5da-8fb4174fa836','BB004567','2024-01-10T01:00:00Z','Updated');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("test06_PrintCQL result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class PointLocationHwUnitHistoryDAO.
     */
    @Test
    public void test07_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM point_location_hwunit_history WHERE  point_location_id=f35e2a60-67e1-11ec-a5da-8fb4174fa836 and device_id='BB004567' date_from='2024-01-10T01:00:00Z';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("test07_PrintCQL result - " + result);
        assertEquals(expResult, result);
    }

}
