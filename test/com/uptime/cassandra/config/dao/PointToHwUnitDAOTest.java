/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.PointToHwUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PointToHwUnitDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    static PointToHwUnitDAO instance;
    static PointToHwUnit entity;
    
    public PointToHwUnitDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
        entity = new PointToHwUnit();
        entity.setChannelNum((byte) 1);
        entity.setChannelType("AC");
        entity.setDeviceId("TEST_DEVICE_ID_1");
        entity.setPointId(UUID.fromString("08e0faff-c12f-4115-9991-aae9fa01a061"));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class PointToHwUnitDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<PointToHwUnit> expResult = new ArrayList<>();
        expResult.add(entity);
        List<PointToHwUnit> result = instance.findByPK(entity.getPointId());
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class PointToHwUnitDAO.
     */
    @Test
    public void test2_FindByPK() {
        System.out.println("findByPK");
        List<PointToHwUnit> expResult = new ArrayList<>();
        expResult.add(entity);
        List<PointToHwUnit> result = instance.findByPK(entity.getPointId());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of update method, of class PointToHwUnitDAO.
     */
    @Test
    public void test3_Update() {
        System.out.println("update");
        entity.setDeviceId("Test Dvice Id 2");
        instance.update(entity);
        List<PointToHwUnit> expResult = new ArrayList<>();
        expResult.add(entity);
        List<PointToHwUnit> result = instance.findByPK(entity.getPointId());
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class PointToHwUnitDAO.
     */
    @Test
    public void test4_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<PointToHwUnit> result = instance.findByPK(entity.getPointId());
        assertEquals(0, result.size());
    }


}
