/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.ApAlByPoint;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Mohd Juned Alam
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApAlByPointDAOTest {

    private static ApAlByPointDAO instance;
    private static ApAlByPoint entity;
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));

    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().apAlByPointDAO();

        entity = new ApAlByPoint();
        entity.setAlSetId(UUID.randomUUID());
        entity.setApSetId(UUID.randomUUID());
        entity.setAreaId(UUID.randomUUID());
        entity.setAssetId(UUID.randomUUID());
        entity.setCustomerAccount("77777");
        entity.setDemodEnabled(true);
        entity.setDemodHighPass(0.5f);
        entity.setDemodLowPass(0.1f);
        entity.setFreqUnits("freqUnits");
        entity.setHighAlert(2.0f);
        entity.setHighAlertActive(true);
        entity.setHighFault(2.5f);
        entity.setHighFaultActive(true);
        entity.setLowAlert(1.0f);
        entity.setLowFaultActive(true);
        entity.setMaxFreq(2.2f);
        entity.setMinFreq(1.0f);
        entity.setParamAmpFactor("paramAmpFactor");
        entity.setParamName("paramName");
        entity.setParamType("paramType");
        entity.setParamUnits("paramUnits");
        entity.setPeriod(2.0f);
        entity.setPointLocationId(UUID.randomUUID());
        entity.setPointId(UUID.randomUUID());
        entity.setResolution(100);
        entity.setSampleRate(2.2f);
        entity.setSensorType("sensorType");
        entity.setSiteId(UUID.randomUUID());
        entity.setDwell(20);
        entity.setfMax(22);
    }

    /**
     * Test of create method, of class ApAlByPointDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create method testing.");
        instance.create(entity);
        List<ApAlByPoint> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        System.out.println("result " + result);
        assertEquals(1, result.size());
        assertEquals(entity, result.get(0));
        System.out.println("create method complete.");
    }

    /**
     * Test of update method, of class ApAlByPointDAO.
     */
    @Test
    public void test1_Update() {
        System.out.println("update method testing.");
        entity.setParamType("paramType Updated");
        instance.update(entity);
        List<ApAlByPoint> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(entity, result.get(0));
        System.out.println("update method complete.");
    }
    
        /**
     * Test of findByCustomerSiteAreaAssetLocation method, of class PointsDAO.
     */
    @Test
    public void test2_FindByCustomerSiteAreaAssetLocationPointApSet() {
        System.out.println("findByCustomerSiteAreaAssetLocationPointApSet");
        List<ApAlByPoint> expResult = new ArrayList<>();
        expResult.add(entity);
        List<ApAlByPoint> result = instance.findByCustomerSiteAreaAssetLocationPointApSet(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId());
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }
    
    /**
     * Test of findByCustomerSiteAreaAssetLocationPointApSetAlSet method, of class PointsDAO.
     */
    @Test
    public void test3_FindByCustomerSiteAreaAssetLocationPointApSetAlSet() {
        System.out.println("findByCustomerSiteAreaAssetLocationPointApSetAlSet");
        List<ApAlByPoint> expResult = new ArrayList<>();
        expResult.add(entity);
        List<ApAlByPoint> result = instance.findByCustomerSiteAreaAssetLocationPointApSetAlSet(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getAlSetId());
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }
    
    /**
     * Test of findByPK method, of class PointsDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<ApAlByPoint> expResult = new ArrayList<>();
        expResult.add(entity);
        List<ApAlByPoint> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }

    /**
     * Test of delete method, of class ApAlByPointDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete method testing.");
        instance.delete(entity);
        List<ApAlByPoint> dbApAlByPointList = new ArrayList<>();
        List<ApAlByPoint> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getAlSetId(), entity.getParamName());
        assertEquals(0, result.size());
        assertEquals(dbApAlByPointList, result);
        System.out.println("delete method complete.");
    }

}
