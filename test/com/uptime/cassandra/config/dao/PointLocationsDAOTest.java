/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.config.dao;

import com.uptime.cassandra.config.entity.PointLocations;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PointLocationsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//point_locations.cql", true, true, "worldview_dev1"));

    static PointLocationsDAO instance;
    static PointLocations pointLocations;
    
    public PointLocationsDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointLocations = new PointLocations();
        pointLocations.setAreaId(UUID.randomUUID());
        pointLocations.setAssetId(UUID.randomUUID());
        pointLocations.setCustomerAccount("77777");
        pointLocations.setDescription("Test Point Location 1 Desc");
        pointLocations.setDeviceSerialNumber("Test Device Serial Num 1");
        pointLocations.setFfSetIds("UUID1,UUID2,UUID3");
        pointLocations.setPointLocationName("Test Point Location 1");
        pointLocations.setPointLocationId(UUID.randomUUID());
        pointLocations.setSiteId(UUID.randomUUID());
        pointLocations.setSpeedRatio(20);
        pointLocations.setTachId(UUID.randomUUID());
        pointLocations.setAlarmEnabled(true);
        pointLocations.setRollDiameter(3);
        pointLocations.setRollDiameterUnits("Inches");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class PointLocationsDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(pointLocations);
        System.out.println("pointLocations.getCustomerAccount() - " + pointLocations.getCustomerAccount());
        List<PointLocations> result = instance.findByCustomerSiteAreaAsset(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId(), pointLocations.getAssetId());
        System.out.println("result " + result);
        assertEquals(1, result.size());
        assertEquals(pointLocations, result.get(0));
    }

    /**
     * Test of update method, of class PointLocationsDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        pointLocations.setDescription("Test Point Location 1 Desc");
        instance.update(pointLocations);
        List<PointLocations> result = instance.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId(), pointLocations.getAssetId(), pointLocations.getPointLocationId());
        assertEquals(pointLocations, result.get(0));
    }

    /**
     * Test of findByCustomerSiteAreaAsset method, of class PointLocationsDAO.
     */
    @Test
    public void test3_FindByCustomerSiteArea() {
        System.out.println("findByCustomerSiteAreaAsset");
        String customerAccount = pointLocations.getCustomerAccount();
        UUID siteId = pointLocations.getSiteId();
        UUID areaId = pointLocations.getAreaId();
        UUID assetId = pointLocations.getAssetId();
        List<PointLocations> expResult = new ArrayList<>();
        expResult.add(pointLocations);
        List<PointLocations> result = instance.findByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerSiteAreaAsset method, of class PointLocationsDAO.
     */
    @Test
    public void test31_FindByCustomerSiteAreaAsset() {
        System.out.println("findByCustomerSiteAreaAsset");
        String customerAccount = pointLocations.getCustomerAccount();
        UUID siteId = pointLocations.getSiteId();
        UUID areaId = pointLocations.getAreaId();
        List<PointLocations> expResult = new ArrayList<>();
        expResult.add(pointLocations);
        List<PointLocations> result = instance.findByCustomerSiteArea(customerAccount, siteId, areaId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class PointLocationsDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        String customerAccount = pointLocations.getCustomerAccount();
        UUID siteId = pointLocations.getSiteId();
        UUID areaId = pointLocations.getAreaId();
        UUID assetId = pointLocations.getAssetId();
        UUID pointLocationId = pointLocations.getPointLocationId();
        List<PointLocations> expResult = new ArrayList<>();
        expResult.add(pointLocations);
        List<PointLocations> result = instance.findByPK(customerAccount, siteId, areaId, assetId, pointLocationId);
        assertEquals(1, result.size());
        assertEquals(expResult, result);

    }

    /**
     * Test of delete method, of class PointLocationsDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(pointLocations);
        List<PointLocations> result = instance.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId(), pointLocations.getAssetId(), pointLocations.getPointLocationId());
        List<PointLocations> dbAssetSiteList = new ArrayList<>();
        assertEquals(0, result.size());
        assertEquals(dbAssetSiteList, result);
    }

}
