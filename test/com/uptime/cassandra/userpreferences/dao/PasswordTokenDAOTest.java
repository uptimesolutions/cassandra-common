/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.PasswordToken;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PasswordTokenDAOTest {
    private static final List<PasswordToken> passwordTokenList = new ArrayList();
    private static PasswordTokenDAO instance;
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));
    
    @BeforeClass
    public static void setUpClass() {
        PasswordToken entity;
        
        instance = UserPreferencesMapperImpl.getInstance().passwordTokenDAO();
        
        entity = new PasswordToken();
        entity.setUserId("TEST_USER");
        entity.setPasswordToken(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        
        passwordTokenList.add(entity);
    }

    /**
     * Test of create method, of class PasswordTokenDAO.
     */
    @Test
    public void test0_Create() {
        System.out.println("create method testing.");
        instance.create(passwordTokenList.get(0), 900);
        System.out.println("create method complete.");
    }

    /**
     * Test of findByPK method, of class PasswordTokenDAO.
     */
    @Test
    public void test1_FindByPK() {
        System.out.println("findByPK method testing.");
        List<PasswordToken> result = instance.findByPK(passwordTokenList.get(0).getUserId(),passwordTokenList.get(0).getPasswordToken());
        
        assertEquals(passwordTokenList.size(), result.size());
        assertEquals(passwordTokenList.get(0).getUserId(), result.get(0).getUserId());
        assertEquals(passwordTokenList.get(0).getPasswordToken(), result.get(0).getPasswordToken());
        System.out.println("findByPK method complete.");
    }

    /**
     * Test of printCQL method, of class PasswordTokenDAO.
     */
    @Test
    public void test2_PrintCQL() {
        System.out.println("printCQL method testing.");
        assertEquals("INSERT INTO password_token(userid,pwd_token) values('TEST_USER',f35e2a60-67e1-11ec-a5da-8fb4174fa836) USING TTL 900;", instance.printCQL(passwordTokenList.get(0), 900));
        assertEquals("", instance.printCQL(null, 0));
        System.out.println("printCQL method complete.");
    }
}
