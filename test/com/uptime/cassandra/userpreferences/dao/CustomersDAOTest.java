/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.Customers;
import java.util.List;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomersDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));
    static CustomersDAO instance;
    static Customers entity = null;
    
    public CustomersDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().customersDAO();
        entity = new Customers();
        entity.setCustomerName("ABCD");
        entity.setCustomerAccount("1234");
        entity.setFqdn("worldview-dev.corp.uptime-solutions.us:8443");
        entity.setSkin("Test Skin");
        entity.setLogo("Test Logo");
        entity.setSloUrl("https://worldview-dev.corp.uptime-solutions.us:8443");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class CustomersDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<Customers> result = instance.findByPK("1234");
        assertEquals(entity, result.get(0));
    }


    /**
     * Test of findByPK method, of class CustomersDAO.
     */
    @Test
    public void test2_FindByPK() {
        System.out.println("findByPK");
        String customerAccnt = "1234";
        List<Customers> result = instance.findByPK(customerAccnt);
        assertEquals(entity, result.get(0));

    }
   
    /**
     * Test of findAllCustomers method, of class CustomersDAO.
     */
    @Test
    public void test3_FindAllCustomers() {
        System.out.println("findAllCustomers");
        List<Customers> result = instance.findAllCustomers();
        assertEquals(4, result.size());
    }
    
    /**
     * Test of update method, of class CustomersDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        entity.setCustomerName("XYZZ");
        instance.update(entity);
        List<Customers> result = instance.findByPK("1234");
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class CustomersDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<Customers> result = instance.findByPK("1234");
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class CustomersDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO customers(customer_acct,customer_name,fqdn,logo,skin,slo_url) values('1234','XYZZ','worldview-dev.corp.uptime-solutions.us:8443','Test Logo','Test Skin','https://worldview-dev.corp.uptime-solutions.us:8443');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of printCQL method, of class CustomersDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM customers WHERE customer_acct='1234';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
}
