/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserPreferencesDAOTest {
        
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));

    static UserPreferencesDAO instance;
    static UserPreferences userPreferences;
    
    public UserPreferencesDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().userPreferencesDAO();
        userPreferences = new UserPreferences();
        userPreferences.setUserId("user111");
        userPreferences.setCustomerAccount("cust-acct-111");
        userPreferences.setLanguage("English");
        userPreferences.setTimezone("EST");
        userPreferences.setDefaultSite(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        userPreferences.setDefaultUnits("Test unit 1");
    }

    /**
     * Test of create method, of class UserPreferencesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        List<UserPreferences> result;
        
        instance.create(userPreferences);
        
        result = instance.findByPK(userPreferences.getUserId());
        assertEquals(1, result.size());
        assertEquals(userPreferences.getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(userPreferences.getLanguage(), result.get(0).getLanguage());
        assertEquals(userPreferences.getTimezone(), result.get(0).getTimezone());
        assertEquals(userPreferences.getDefaultSite(), result.get(0).getDefaultSite());
        assertEquals(userPreferences.getDefaultUnits(), result.get(0).getDefaultUnits());
        System.out.println("create method complete.");
    }

    /**
     * Test of update method, of class UserPreferencesDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        List<UserPreferences> result;
        
        userPreferences.setLanguage("Spanish");
        userPreferences.setTimezone("PST");
        userPreferences.setDefaultSite(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
        userPreferences.setDefaultUnits("Test Units");
        instance.update(userPreferences);
        
        result = instance.findByPK(userPreferences.getUserId());
        assertEquals(1, result.size());
        assertEquals(userPreferences.getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(userPreferences.getLanguage(), result.get(0).getLanguage());
        assertEquals(userPreferences.getTimezone(), result.get(0).getTimezone());
        assertEquals(userPreferences.getDefaultSite(), result.get(0).getDefaultSite());
        assertEquals(userPreferences.getDefaultUnits(), result.get(0).getDefaultUnits());
        System.out.println("update method complete.");
    }

    /**
     * Test of findByUserId method, of class UserPreferencesDAO.
     */
    @Test
    public void test3_FindByUserId() {
        System.out.println("findByUserId");
        List<UserPreferences> result = instance.findByPK("user111");
        
        assertEquals(1, result.size());
        assertEquals(result.get(0).getCustomerAccount(), "cust-acct-111");
        assertEquals(result.get(0).getLanguage(), "Spanish");
        assertEquals(result.get(0).getTimezone(), "PST");
        assertEquals(result.get(0).getDefaultSite(), UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
        System.out.println("findByUserId method complete.");
    }

    /**
     * Test of delete method, of class UserPreferencesDAO.
     */
    @Test
    public void test4_Delete() {
        System.out.println("delete");
        instance.delete(userPreferences);
        assertEquals(new ArrayList(), instance.findByPK(userPreferences.getUserId()));
        System.out.println("delete method complete.");
    }

    /**
     * Test of printCQL method, of class UserPreferencesDAO.
     */
    @Test
    public void test5_PrintCQL() {
        System.out.println("printCQL");
        String result;
        
        result  = instance.printCQL(userPreferences, "INSERT");
        System.out.println("result - " + result);
        assertEquals("INSERT INTO user_preferences(userid,customer_acct,language,timezone,default_site,default_units) values('user111','cust-acct-111','Spanish','PST',f35e2a60-67e1-11ec-a5da-8fb4174fa837,'Test Units');", result);
        result  = instance.printCQL(userPreferences, "DELETE");
        assertEquals("DELETE FROM user_preferences WHERE userid='user111';", result);
        result  = instance.printCQL(null, null);
        assertEquals("", result);
        System.out.println("printCQL method complete.");
    }
}
