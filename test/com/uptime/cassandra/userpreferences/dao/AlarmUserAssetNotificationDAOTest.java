/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmUserAssetNotificationDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));

    static AlarmUserAssetNotificationDAO instance;
    static AlarmUserAssetNotification alarmUserAssetNotification;

    public AlarmUserAssetNotificationDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().alarmUserAssetNotificationDAO();
        alarmUserAssetNotification = new AlarmUserAssetNotification();
        alarmUserAssetNotification.setUserId("77777");
        alarmUserAssetNotification.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        alarmUserAssetNotification.setAssetId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        alarmUserAssetNotification.setEmailAddress("aswani.rami@gmail.com,ramisetti.k@gmail.com");
        alarmUserAssetNotification.setFault(true);
        alarmUserAssetNotification.setAlert(true);

    }

    /**
     * Test of create method, of class AlarmUserAssetNotificationDAO.
     */
    @Test
    public void test1_Create() {
        instance.create(alarmUserAssetNotification);
        List<AlarmUserAssetNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserAssetNotification);
        List<AlarmUserAssetNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"), UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of update method, of class AlarmUserAssetNotificationDAO.
     */
    @Test
    public void test2_Update() {
        List<AlarmUserAssetNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserAssetNotification);
        alarmUserAssetNotification.setAlert(false);
        alarmUserAssetNotification.setFault(false);
        instance.update(alarmUserAssetNotification);
        List<AlarmUserAssetNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"), UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of delete method, of class AlarmUserAssetNotificationDAO.
     */
    @Test
    public void test6_Delete() {
        instance.delete(alarmUserAssetNotification);
        List<AlarmUserAssetNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"), UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(0, result.size());
    }   

    /**
     * Test of findByUseridSite method, of class AlarmUserAssetNotificationDAO.
     */
    @Test
    public void test3_FindByUseridSite() {
        List<AlarmUserAssetNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserAssetNotification);
        List<AlarmUserAssetNotification> result = instance.findByUseridSite("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of findByPK method, of class AlarmUserAssetNotificationDAO.
     */
    @Test
    public void test5_FindByPK() {
        List<AlarmUserAssetNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserAssetNotification);
        List<AlarmUserAssetNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"), UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

}
