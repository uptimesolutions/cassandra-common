/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteUsersDAOTest {

    static SiteUsersDAO instance;
    static SiteUsers siteUsers = new SiteUsers();
    static List<SiteUsers> result1 = null;

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));

    public SiteUsersDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().siteUsersDAO();
        siteUsers = new SiteUsers();
        siteUsers.setUserId("sam.spade@uptime-solutions.us");
        siteUsers.setCustomerAccount("77777");
        siteUsers.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        siteUsers.setSiteRole("user");
        siteUsers.setFirstName("Sam");
        siteUsers.setLastName("Spade");
    }

    /**
     * Test of create method, of class SiteUsersDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(siteUsers);
        assertEquals(siteUsers, instance.findByPK(siteUsers.getCustomerAccount(),siteUsers.getSiteId(), siteUsers.getUserId()).get(0));
        System.out.println("create method complete.");
    }

    /**
     * Test of findByUserId method, of class SiteUsersDAO.
     */
    @Test
    public void test2_findByCustomer() {
        System.out.println("findByCustomer");
        assertEquals(siteUsers, instance.findByCustomer(siteUsers.getCustomerAccount()).get(0));
        System.out.println("findByCustomer method complete.");
    }

    /**
     * Test of findByUserIdCustomerAccount method, of class SiteUsersDAO.
     */
    @Test
    public void test3_findByUserIdCustomerAccount() {
        System.out.println("findByUserIdCustomerAccount");
        assertEquals(siteUsers, instance.findByCustomerSite(siteUsers.getCustomerAccount(), siteUsers.getSiteId()).get(0));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of findByPK method, of class SiteUsersDAO.
     */
    @Test
    public void test4_findByPK() {
        System.out.println("findByPK");
        assertEquals(siteUsers, instance.findByPK(siteUsers.getCustomerAccount(),siteUsers.getSiteId(), siteUsers.getUserId()).get(0));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of update method, of class SiteUsersDAO.
     */
    @Test
    public void test5_Update() {
        System.out.println("update");
        siteUsers.setSiteRole("viewer");
        instance.update(siteUsers);
        assertEquals(siteUsers, instance.findByPK(siteUsers.getCustomerAccount(), siteUsers.getSiteId(), siteUsers.getUserId()).get(0));
        System.out.println("update method complete.");
    }
    
    /**
     * Test of delete method, of class SiteUsersDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(siteUsers);
        assertEquals(instance.findByPK(siteUsers.getCustomerAccount(),siteUsers.getSiteId(), siteUsers.getUserId()).size(), 0);
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of printCQL method, of class UserPreferencesDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String result;
        
        result  = instance.printCQL(siteUsers, "INSERT");
        System.out.println("result-1" + result);
        assertEquals("INSERT INTO site_users(userid,customer_acct,site_id,site_role,first_name,last_name) values('sam.spade@uptime-solutions.us','77777',f35e2a60-67e1-11ec-a5da-8fb4174fa836,'viewer','Sam','Spade');", result);
        result  = instance.printCQL(siteUsers, "DELETE");
        System.out.println("result-2" + result);
        assertEquals("DELETE FROM site_users WHERE userid='sam.spade@uptime-solutions.us' AND customer_acct='77777' AND site_id=f35e2a60-67e1-11ec-a5da-8fb4174fa836;", result);
        result  = instance.printCQL(null, null);
        assertEquals("", result);
        System.out.println("printCQL method complete.");
    }
}
