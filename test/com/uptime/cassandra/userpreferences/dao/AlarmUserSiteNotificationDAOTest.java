/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmUserSiteNotificationDAOTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));

    static AlarmUserSiteNotificationDAO instance;
    static AlarmUserSiteNotification alarmUserSiteNotification;

    public AlarmUserSiteNotificationDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().alarmUserSiteNotificationDAO();
        alarmUserSiteNotification = new AlarmUserSiteNotification();
        alarmUserSiteNotification.setUserId("77777");
        alarmUserSiteNotification.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        alarmUserSiteNotification.setEmailAddress("aswani.rami@gmail.com,ramisetti.k@gmail.com");
        alarmUserSiteNotification.setFault(true);
        alarmUserSiteNotification.setAlert(true);
    }

    /**
     * Test of create method, of class AlarmUserSiteNotificationDAO.
     */
    @Test
    public void test1_Create() {
        instance.create(alarmUserSiteNotification);
        List<AlarmUserSiteNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserSiteNotification);
        List<AlarmUserSiteNotification> result =instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of update method, of class AlarmUserSiteNotificationDAO.
     */
    @Test
    public void test2_Update() {
        List<AlarmUserSiteNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserSiteNotification);
        alarmUserSiteNotification.setFault(false);
        alarmUserSiteNotification.setAlert(false);
        instance.update(alarmUserSiteNotification);
        List<AlarmUserSiteNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of delete method, of class AlarmUserSiteNotificationDAO.
     */
    @Test
    public void test5_Delete() {
        List<AlarmUserSiteNotification> expectedResult = new ArrayList<>();
        instance.delete(alarmUserSiteNotification);
        List<AlarmUserSiteNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of findByUseridSite method, of class AlarmUserSiteNotificationDAO.
     */
    @Test
    public void test3_FindByUseridSite() {
        List<AlarmUserSiteNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserSiteNotification);
        List<AlarmUserSiteNotification> result = instance.findByPK("77777", UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        Assert.assertEquals(expectedResult, result);
    }

    /**
     * Test of findByPK method, of class AlarmUserSiteNotificationDAO.
     */
    @Test
    public void test4_FindByPK() {
        List<AlarmUserSiteNotification> expectedResult = new ArrayList<>();
        expectedResult.add(alarmUserSiteNotification);
        List<AlarmUserSiteNotification> result = instance.findByUserId("77777");
        Assert.assertEquals(expectedResult, result);
    }

}
