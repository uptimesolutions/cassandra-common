/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.UserSites;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserSitesDAOTest {

    static UserSitesDAO instance;
    static UserSites userSites = new UserSites();
    static List<UserSites> result1 = null;

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));

    public UserSitesDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().userSitesDAO();
        userSites = new UserSites();
        userSites.setUserId("akumar");
        userSites.setCustomerAccount("12");
        userSites.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        userSites.setSiteRole("user");
    }

    /**
     * Test of create method, of class UserSitesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(userSites);
        assertEquals(userSites, instance.findByPK(userSites.getUserId(), userSites.getCustomerAccount(), userSites.getSiteId()).get(0));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of update method, of class UserSitesDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        userSites.setCustomerAccount("13");
        userSites.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
        userSites.setSiteRole("viewer");
        instance.update(userSites);
        assertEquals(userSites, instance.findByPK(userSites.getUserId(), userSites.getCustomerAccount(), userSites.getSiteId()).get(0));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of findByUserId method, of class UserSitesDAO.
     */
    @Test
    public void test3_findByUserId() {
        System.out.println("findByUserId");
        assertEquals(userSites, instance.findByUserId(userSites.getUserId()).get(1));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of findByUserIdCustomerAccount method, of class UserSitesDAO.
     */
    @Test
    public void test4_findByUserIdCustomerAccount() {
        System.out.println("findByUserIdCustomerAccount");
        assertEquals(userSites, instance.findByUserIdCustomerAccount(userSites.getUserId(), userSites.getCustomerAccount()).get(0));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of findByPK method, of class UserSitesDAO.
     */
    @Test
    public void test5_findByPK() {
        System.out.println("findByPK");
        assertEquals(userSites, instance.findByPK(userSites.getUserId(), userSites.getCustomerAccount(), userSites.getSiteId()).get(0));
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of delete method, of class UserSitesDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(userSites);
        assertEquals(instance.findByPK(userSites.getUserId(), userSites.getCustomerAccount(),userSites.getSiteId()).size(), 0);
        System.out.println("printCQL method complete.");
    }

    /**
     * Test of printCQL method, of class UserPreferencesDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String result;
        
        result  = instance.printCQL(userSites, "INSERT");
        assertEquals("INSERT INTO user_sites(userid,customer_acct,site_id,site_role) values('akumar','13',f35e2a60-67e1-11ec-a5da-8fb4174fa837,'viewer');", result);
        result  = instance.printCQL(userSites, "DELETE");
        assertEquals("DELETE FROM user_sites WHERE userid='akumar' AND customer_acct='13' AND site_id=f35e2a60-67e1-11ec-a5da-8fb4174fa837;", result);
        result  = instance.printCQL(null, null);
        assertEquals("", result);
        System.out.println("printCQL method complete.");
    }
}
