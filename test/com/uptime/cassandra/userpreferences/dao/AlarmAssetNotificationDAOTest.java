/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.userpreferences.dao;

import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmAssetNotificationDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//user.cql", true, true, "worldview_dev1"));
    static AlarmAssetNotificationDAO instance;
    static AlarmAssetNotification entity;
    
    public AlarmAssetNotificationDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = UserPreferencesMapperImpl.getInstance().alarmAssetNotificationDAO();
        entity = new AlarmAssetNotification();
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setUserId("sam.spade@uptime-solutions.us");
        entity.setAlert(true);
        entity.setFault(true);
        entity.setEmailAddress("sam.spade@uptime-solutions.us");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class AlarmAssetNotificationDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<AlarmAssetNotification> result = instance.findByPK(entity.getAssetId(), entity.getUserId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByAsset method, of class AlarmAssetNotificationDAO.
     */
    @Test
    public void test2_FindByAsset() {
        System.out.println("findByAsset");
        List<AlarmAssetNotification> result = instance.findByAsset(entity.getAssetId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class AlarmAssetNotificationDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<AlarmAssetNotification> result = instance.findByPK(entity.getAssetId(), entity.getUserId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class AlarmAssetNotificationDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        instance.update(entity);
        List<AlarmAssetNotification> result = instance.findByPK(entity.getAssetId(), entity.getUserId());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class AlarmAssetNotificationDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<AlarmAssetNotification> result = instance.findByPK(entity.getAssetId(), entity.getUserId());
        assertEquals(0, result.size());
    }    
}
