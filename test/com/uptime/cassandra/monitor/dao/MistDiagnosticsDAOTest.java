/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.uptime.cassandra.monitor.entity.MistDiagnostics;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MistDiagnosticsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));

    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    static MistDiagnosticsDAO instance;
    static MistDiagnostics entity;
    
    public MistDiagnosticsDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = BaseStationMapperImpl.getInstance().mistDiagnosticsDAO();
        
        entity = new MistDiagnostics();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("fe6d75c8-2f96-450b-ada9-2c6b329e565f"));
        entity.setEchoDeviceId("Test Echo Device Id");
        entity.setMistlxChan((byte)0);
        entity.setDeviceId("BB003003");
//        entity.setAlive(true);
        entity.setBaseStationHostName("Test Host");
        entity.setBaseStationMac("Test MAC");
        entity.setBaseStationPort((short) 120);
        entity.setAreaName("Test Aeea");
        entity.setAssetName("Test Asset");
        entity.setSiteName("Test Site");
        entity.setPointLocationName("Test PointLocation");
        entity.setPointName("Test Point");
        entity.setLastSample(LocalDateTime.parse("2022-11-08 02:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setMistlxRssi((byte) 1);
        entity.setMistlxFsamp((byte) 2);
        entity.setMistlxLength((short) 1);
        entity.setMistlxBattery(1f);
        entity.setMistlxTemp(2f);
        entity.setMistlxDelta((short) 2);
        entity.setMistlxPcorr((short) 3);
        entity.setMistlxDbg((byte) 3);
        entity.setMistlxBleon((byte) 4);
        entity.setMistlxFpgaon((byte) 5);
        entity.setMistlxTotalMin((short) 4);
        entity.setMistlxMincnt((short) 5);
        entity.setMistlxFailcnt((short) 6);
        entity.setMistlxCtime((short) 7);
        entity.setMistlxCtimeMod((short) 8);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<MistDiagnostics> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        System.out.println("findByCustomerSiteId result - " + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSite method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<MistDiagnostics> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        System.out.println("findByCustomerSite result - " + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByCustomerSiteDevice method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test3_FindByCustomerSiteDevice() {
        System.out.println("findByCustomerSiteDevice");
        List<MistDiagnostics> result = instance.findByCustomerSiteDevice(entity.getCustomerAccount(), entity.getSiteId(), entity.getEchoDeviceId(), entity.getDeviceId());
        System.out.println("findByCustomerSiteDevice result - " + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<MistDiagnostics> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getEchoDeviceId(), entity.getDeviceId(), entity.getMistlxChan());
        System.out.println("findByPK result - " + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of update method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test5_Update() {
        System.out.println("update");
        entity.setMistlxLength((short) 10);
        instance.update(entity);
        List<MistDiagnostics> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getEchoDeviceId(), entity.getDeviceId(), entity.getMistlxChan());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test6_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<MistDiagnostics> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId(), entity.getEchoDeviceId(), entity.getMistlxChan());
        assertEquals(0, result.size());
    }
    
    /**
     * Test of printCQL method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "INSERT INTO mist_diagnostics(customer_acct,site_id,echo_device_id,device_id,mistlx_chan,last_sample,mistlx_rssi,mistlx_fsamp,mistlx_length,mistlx_battery,mistlx_temp,mistlx_delta,mistlx_pcorr,mistlx_dbg,mistlx_bleon,mistlx_fpgaon,mistlx_total_min,mistlx_mincnt,mistlx_failcnt,mistlx_ctime,mistlx_ctime_mod,site_name,area_name,asset_name,point_location_name,point_name,base_station_port,base_station_mac,base_station_hostname) values('77777',fe6d75c8-2f96-450b-ada9-2c6b329e565f,'Test Echo Device Id','BB003003',0,'2022-11-08T02:00:00Z',1,2,10,1.0,2.0,2,3,3,4,5,4,5,6,7,8,'Test Site','Test Aeea','Test Asset','Test PointLocation','Test Point',120,'Test MAC','Test Host');";
        String result = instance.printCQL(entity, "INSERT");
        System.out.println("printCQL result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class MistDiagnosticsDAO.
     */
    @Test
    public void test8_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "DELETE FROM mist_diagnostics WHERE customer_acct='77777' AND site_id=fe6d75c8-2f96-450b-ada9-2c6b329e565f AND echo_device_id='Test Echo Device Id' AND device_id='BB003003' AND mistlx_chan=0;";
        String result = instance.printCQL(entity, "DELETE");
        System.out.println("printCQL result - " + result);
        assertEquals(expResult, result);
    }

}
