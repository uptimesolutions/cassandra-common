/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;


import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteBasestationDeviceDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    
    private static SiteBasestationDeviceDAO instance;
    private static SiteBasestationDevice entity;
    private static List<SiteBasestationDevice> entities;
    
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    public SiteBasestationDeviceDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        entity = new SiteBasestationDevice();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("47bfb62a-bbe6-4345-8a86-08fdefb299e5"));
        entity.setBaseStationPort((short)1111);
        entity.setChannelType("AC");
        entity.setDeviceId("BB002345");
        entity.setDisabled(true);
        entity.setFmax(23);
        entity.setLastSampled(LocalDateTime.parse("2023-04-10 01:20:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setPointId(UUID.fromString("9d217a12-8f2b-4317-877b-153687124d3b"));
        entity.setResolution(24);
        entity.setSampleInterval((short)25);
        entity.setSensorChannelNum((byte) 1);
        entities = new ArrayList<>();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test1_Create_SiteBasestationDevice() {
        System.out.println("create");
        instance.create(entity);
        List<SiteBasestationDevice> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteBasestationDevice> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationPort(), entity.getDeviceId(), entity.getPointId());
        assertEquals(expResult, result);
    }

    /**
     * Test of create method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test2_Create_List() {
        System.out.println("create");
        SiteBasestationDevice sbd = new SiteBasestationDevice();
        sbd.setCustomerAccount("77777");
        sbd.setSiteId(UUID.randomUUID());
        sbd.setBaseStationPort((short)2222);
        sbd.setChannelType("AC");
        sbd.setDeviceId("BB002346");
        sbd.setFmax(230);
        sbd.setLastSampled(LocalDateTime.parse("2023-04-10 01:30:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        sbd.setPointId(UUID.randomUUID());
        sbd.setResolution(240);
        sbd.setSampleInterval((short)250);
        sbd.setSensorChannelNum((byte) 2);
        
        SiteBasestationDevice sbd1 = new SiteBasestationDevice();
        sbd1.setCustomerAccount("77777");
        sbd1.setSiteId(sbd.getSiteId());
        sbd1.setBaseStationPort((short)2222);
        sbd1.setChannelType("AC");
        sbd1.setDeviceId("BB002347");
        sbd1.setFmax(231);
        sbd1.setLastSampled(LocalDateTime.parse("2023-04-10 01:40:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        sbd1.setPointId(UUID.randomUUID());
        sbd1.setResolution(241);
        sbd1.setSampleInterval((short)251);
        sbd1.setSensorChannelNum((byte) 2);
        entities.add(sbd);
        entities.add(sbd1);
        instance.create(entities);
        
        List<SiteBasestationDevice> result = instance.findByCustomerSiteBasestation(entities.get(0).getCustomerAccount(), entities.get(0).getSiteId(), entities.get(0).getBaseStationPort());
        System.out.println("entities - " + entities.toString());
        System.out.println("result - " + result.toString());
        
        assertEquals(entities, result);
    }

    /**
     * Test of findByCustomerSiteBasestation method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test3_FindByCustomerSiteBasestation() {
        System.out.println("findByCustomerSiteBasestation");
        List<SiteBasestationDevice> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteBasestationDevice> result = instance.findByCustomerSiteBasestation(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationPort());
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteBasestationDevice method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test4_FindByCustomerSiteBasestationDevice() {
        System.out.println("findByCustomerSiteBasestationDevice");
        List<SiteBasestationDevice> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteBasestationDevice> result = instance.findByCustomerSiteBasestationDevice(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationPort(), entity.getDeviceId());
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test5_FindByPK() {
        System.out.println("findByPK");
        List<SiteBasestationDevice> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteBasestationDevice> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationPort(), entity.getDeviceId(), entity.getPointId());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of update method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test6_Update_SiteBasestationDevice() {
        System.out.println("update");
        entity.setFmax(300);
        instance.update(entity);
        List<SiteBasestationDevice> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteBasestationDevice> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationPort(), entity.getDeviceId(), entity.getPointId());
        assertEquals(expResult, result);
    }

    /**
     * Test of update method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test7_Update_List() {
        System.out.println("update");
        entities.get(0).setResolution(500);
        instance.update(entities);
        List<SiteBasestationDevice> result = instance.findByCustomerSiteBasestation(entities.get(0).getCustomerAccount(), entities.get(0).getSiteId(), entities.get(0).getBaseStationPort());
        System.out.println("entities - " + entities.toString());
        System.out.println("result - " + result.toString());
        assertEquals(entities, result);
    }

    /**
     * Test of delete method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test8_Delete_SiteBasestationDevice() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteBasestationDevice> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationPort(), entity.getDeviceId(), entity.getPointId());
        assertEquals(0, result.size());
    }

    /**
     * Test of delete method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test9_Delete_List() {
        System.out.println("delete");
        instance.delete(entities);
        List<SiteBasestationDevice> result = instance.findByCustomerSiteBasestation(entities.get(0).getCustomerAccount(), entities.get(0).getSiteId(), entities.get(0).getBaseStationPort());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test10_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "INSERT INTO site_basestation_device(customer_acct,site_id,base_station_port,device_id,point_id,channel_type,fmax,is_disabled,last_sampled,resolution,sample_interval,sensor_channel_num) values('77777',47bfb62a-bbe6-4345-8a86-08fdefb299e5,1111,'BB002345',9d217a12-8f2b-4317-877b-153687124d3b,'AC',23,true,'2023-04-10T01:20:00Z',24,25,1);";
        String result = instance.printCQL(entity, "INSERT");
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class SiteBasestationDeviceDAO.
     */
    @Test
    public void test11_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "DELETE FROM site_basestation_device WHERE customer_acct='77777' AND site_id=47bfb62a-bbe6-4345-8a86-08fdefb299e5 AND base_station_port=1111 AND device_id='BB002345' AND point_id=9d217a12-8f2b-4317-877b-153687124d3b;";
        String result = instance.printCQL(entity, "DELETE");
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    
}
