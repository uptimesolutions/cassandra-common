/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.cassandra.utils.UUIDGen;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApprovedBaseStationsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));

    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    static ApprovedBaseStationsDAO instance;
    static ApprovedBaseStations approvedBaseStations;
    
    public ApprovedBaseStationsDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = BaseStationMapperImpl.getInstance().approvedBaseStationsDAO();
        
        approvedBaseStations = new ApprovedBaseStations();
        approvedBaseStations.setCustomerAccount("77777");
        approvedBaseStations.setSiteId(UUID.randomUUID());
        approvedBaseStations.setMacAddress("Test Mac 1");
        approvedBaseStations.setIssuedToken(UUIDGen.getTimeUUID());
        approvedBaseStations.setLastPasscode(LocalDateTime.parse("2022-03-18 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        approvedBaseStations.setPasscode(UUID.randomUUID());

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class ApprovedBaseStationsDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(approvedBaseStations);
        List<ApprovedBaseStations> expResult = new ArrayList<>();
        expResult.add(approvedBaseStations);
        List<ApprovedBaseStations> result = instance.findByPK(approvedBaseStations.getCustomerAccount(), approvedBaseStations.getSiteId(), approvedBaseStations.getMacAddress());
        assertEquals(expResult, result);
    }

    /**
     * Test of update method, of class ApprovedBaseStationsDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        approvedBaseStations.setIssuedToken(UUIDGen.getTimeUUID());
        instance.update(approvedBaseStations);
        List<ApprovedBaseStations> expResult = new ArrayList<>();
        expResult.add(approvedBaseStations);
        List<ApprovedBaseStations> result = instance.findByPK(approvedBaseStations.getCustomerAccount(), approvedBaseStations.getSiteId(), approvedBaseStations.getMacAddress());
        assertEquals(expResult, result);
        assertEquals(expResult.get(0).getIssuedToken(), result.get(0).getIssuedToken());
    }
    
    /**
     * Test of findByCustomerSiteId method, of class ApprovedBaseStationsDAO.
     */
    @Test
    public void test3_FindByCustomerSiteId() {
        System.out.println("findByCustomerSiteId");
        String customer = approvedBaseStations.getCustomerAccount();
        UUID site = approvedBaseStations.getSiteId();
        List<ApprovedBaseStations> expResult = new ArrayList<>();
        expResult.add(approvedBaseStations);
        List<ApprovedBaseStations> result = instance.findByCustomerSiteId(customer, site);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class ApprovedBaseStationsDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<ApprovedBaseStations> expResult = new ArrayList<>();
        expResult.add(approvedBaseStations);
        List<ApprovedBaseStations> result = instance.findByPK(approvedBaseStations.getCustomerAccount(), approvedBaseStations.getSiteId(), approvedBaseStations.getMacAddress());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of delete method, of class ApprovedBaseStationsDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(approvedBaseStations);
        List<ApprovedBaseStations> result = instance.findByPK(approvedBaseStations.getCustomerAccount(), approvedBaseStations.getSiteId(), approvedBaseStations.getMacAddress());
        assertEquals(0, result.size());
    }

}
