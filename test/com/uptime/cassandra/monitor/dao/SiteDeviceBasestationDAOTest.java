/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;


import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteDeviceBasestationDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    
    private static SiteDeviceBasestationDAO instance;
    private static SiteDeviceBasestation entity;
    
    public SiteDeviceBasestationDAOTest() {
    }
    
    
    @BeforeClass
    public static void setUpClass() {
        instance = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        entity = new SiteDeviceBasestation();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("47bfb62a-bbe6-4345-8a86-08fdefb299e5"));
        entity.setBaseStationPort((short)1111);
        entity.setDeviceId("BB002345");
        entity.setDisabled(true);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<SiteDeviceBasestation> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteDeviceBasestation> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSite method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<SiteDeviceBasestation> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteDeviceBasestation> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<SiteDeviceBasestation> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteDeviceBasestation> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of update method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        entity.setBaseStationPort((short)8888);
        instance.update(entity);
        List<SiteDeviceBasestation> expResult = new ArrayList<>();
        expResult.add(entity);
        List<SiteDeviceBasestation> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<SiteDeviceBasestation> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "INSERT INTO site_device_basestation(customer_acct,site_id,device_id,base_station_port,is_disabled) values('77777',47bfb62a-bbe6-4345-8a86-08fdefb299e5,'BB002345',8888,true);";
        String result = instance.printCQL(entity, "INSERT");
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of printCQL method, of class SiteDeviceBasestationDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "DELETE FROM site_device_basestation WHERE customer_acct='77777' AND site_id=47bfb62a-bbe6-4345-8a86-08fdefb299e5 AND device_id='BB002345';";
        String result = instance.printCQL(entity, "DELETE");
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

}
