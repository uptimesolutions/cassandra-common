/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.uptime.cassandra.monitor.entity.GatewayLastUpload;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.cassandra.utils.UUIDGen;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GatewayLastUploadDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));

    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    static GatewayLastUploadDAO instance;
    static GatewayLastUpload gatewayLastUpload;
    
    public GatewayLastUploadDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = BaseStationMapperImpl.getInstance().gatewayLastUploadDAO();
        
        gatewayLastUpload = new GatewayLastUpload();
        gatewayLastUpload.setCustomerAccount("77777");
        gatewayLastUpload.setSiteId(UUID.fromString("3ad62424-f047-4f6a-a86e-14584f17c370"));
        gatewayLastUpload.setDeviceId("BB000123");
        gatewayLastUpload.setSiteName("MEMPHIS");
        gatewayLastUpload.setLastUpload(LocalDateTime.parse("2024-09-23 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(gatewayLastUpload);
        List<GatewayLastUpload> expResult = new ArrayList<>();
        expResult.add(gatewayLastUpload);
        List<GatewayLastUpload> result = instance.findByPK(gatewayLastUpload.getCustomerAccount(), gatewayLastUpload.getSiteId(), gatewayLastUpload.getDeviceId());
        assertEquals(expResult, result);
    }

    /**
     * Test of update method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test2_Update() {
        System.out.println("update");
        gatewayLastUpload.setSiteName("MEMPHIS 1");
        instance.update(gatewayLastUpload);
        List<GatewayLastUpload> expResult = new ArrayList<>();
        expResult.add(gatewayLastUpload);
        List<GatewayLastUpload> result = instance.findByPK(gatewayLastUpload.getCustomerAccount(), gatewayLastUpload.getSiteId(), gatewayLastUpload.getDeviceId());
        assertEquals(expResult, result);
        assertEquals(expResult.get(0).getSiteName(), result.get(0).getSiteName());
    }
    
    /**
     * Test of findByCustomerSiteId method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test3_FindByCustomerSiteId() {
        System.out.println("findByCustomerSiteId");
        String customer = gatewayLastUpload.getCustomerAccount();
        UUID site = gatewayLastUpload.getSiteId();
        List<GatewayLastUpload> expResult = new ArrayList<>();
        expResult.add(gatewayLastUpload);
        List<GatewayLastUpload> result = instance.findByCustomerSiteId(customer, site);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test4_FindByPK() {
        System.out.println("findByPK");
        List<GatewayLastUpload> expResult = new ArrayList<>();
        expResult.add(gatewayLastUpload);
        List<GatewayLastUpload> result = instance.findByPK(gatewayLastUpload.getCustomerAccount(), gatewayLastUpload.getSiteId(), gatewayLastUpload.getDeviceId());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of delete method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(gatewayLastUpload);
        List<GatewayLastUpload> result = instance.findByPK(gatewayLastUpload.getCustomerAccount(), gatewayLastUpload.getSiteId(), gatewayLastUpload.getDeviceId());
        assertEquals(0, result.size());
    }


    /**
     * Test of printCQL method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "INSERT INTO gateway_last_upload(customer_acct,site_id,device_id,site_name ,last_upload ) values('77777',3ad62424-f047-4f6a-a86e-14584f17c370,'BB000123',MEMPHIS 1,2024-09-23T01:00:00Z);";
        String result = instance.printCQL(gatewayLastUpload, "INSERT");
        System.out.println("printCQL result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of printCQL method, of class GatewayLastUploadDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "DELETE FROM gateway_last_upload WHERE customer_acct='77777' AND site_id=3ad62424-f047-4f6a-a86e-14584f17c370 AND device_id='BB000123';";
        String result = instance.printCQL(gatewayLastUpload, "DELETE");
        System.out.println("printCQL result - " + result);
        assertEquals(expResult, result);
    }
}
