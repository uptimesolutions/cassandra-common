/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;

import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EchoDiagnosticsDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));

    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    static EchoDiagnosticsDAO instance;
    static EchoDiagnostics entity;
    
    public EchoDiagnosticsDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = BaseStationMapperImpl.getInstance().echoDiagnosticsDAO();
        
        entity = new EchoDiagnostics();
        entity.setCustomerAccount("77777");
        entity.setSiteId(UUID.fromString("fe6d75c8-2f96-450b-ada9-2c6b329e565f"));
        entity.setBaseStationHostname("Test Host");
        entity.setBaseStationMac("Test MAC");
        entity.setSiteName("Memphis");
        entity.setDeviceId("BB003003");
        entity.setEchoBacklog((short) 0);
        entity.setEchoDatasets((short) 1);
        entity.setEchoFw("Test 11");
        entity.setEchoPowLevel((byte) 0);
        entity.setEchoPowerfault((short) 2);
        entity.setEchoRadioFail((byte) 1);
        entity.setEchoReboot((short) 3);
        entity.setEchoRestart((short) 4);
        entity.setEchoTerr((byte) 2);
        entity.setLastUpdate(LocalDateTime.parse("2022-11-10 02:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<EchoDiagnostics> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        //assertEquals(entity, result.get(0));
        assertEquals(entity.getSiteId(), result.get(0).getSiteId());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());
    }

    /**
     * Test of findByCustomerSite method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test2_FindByCustomerSite() {
        System.out.println("findByCustomerSite");
        List<EchoDiagnostics> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
        System.out.println("findByCustomerSite result - " + result);
        //assertEquals(entity, result.get(0));
        assertEquals(entity.getSiteId(), result.get(0).getSiteId());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());
    }

    /**
     * Test of findByPK method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<EchoDiagnostics> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationHostname(), entity.getDeviceId());
        System.out.println("findByPK result - " + result);
        //assertEquals(entity, result.get(0));
        assertEquals(entity.getSiteId(), result.get(0).getSiteId());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());

    }

    /**
     * Test of update method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        entity.setEchoFw("Test 222");
        instance.update(entity);
        List<EchoDiagnostics> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationHostname(), entity.getDeviceId());
        //assertEquals(entity, result.get(0));
        assertEquals(entity.getSiteId(), result.get(0).getSiteId());
        assertEquals(entity.getCustomerAccount(), result.get(0).getCustomerAccount());
        assertEquals(entity.getDeviceId(), result.get(0).getDeviceId());
        assertEquals(entity.getBaseStationHostname(), result.get(0).getBaseStationHostname());
        assertEquals(entity.getEchoFw(), result.get(0).getEchoFw());
    }

    /**
     * Test of delete method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<EchoDiagnostics> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getBaseStationHostname(), entity.getDeviceId());
        assertEquals(0, result.size());
    }

    /**
     * Test of printCQL method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test6_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "INSERT INTO echo_diagnostics(customer_acct,site_id,base_station_hostname,device_id,base_station_mac,site_name,echo_fw,echo_datasets,echo_backlog,echo_radio_fail,echo_terr,echo_restart,echo_pow_level,echo_reboot,echo_powerfault,last_update) values('77777',fe6d75c8-2f96-450b-ada9-2c6b329e565f,'Test Host','BB003003','Test MAC','Memphis','Test 222',1,0,1,2,4,0,3,2,'2022-11-10T02:00:00Z');";
        String result = instance.printCQL(entity, "INSERT");
        System.out.println("printCQL result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of printCQL method, of class EchoDiagnosticsDAO.
     */
    @Test
    public void test7_PrintCQL() {
        System.out.println("printCQL");
        String expResult = "DELETE FROM echo_diagnostics WHERE customer_acct='77777' AND site_id=fe6d75c8-2f96-450b-ada9-2c6b329e565f AND base_station_hostname='Test Host' AND device_id='BB003003';";
        String result = instance.printCQL(entity, "DELETE");
        System.out.println("printCQL result - " + result);
        assertEquals(expResult, result);
    }

}

