/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.monitor.dao;


//import com.uptime.cassandra.monitor.entity.EchoStatus;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EchoStatusDAOTest {
    
//    @ClassRule
//    public static CassandraCQLUnit cassandraCQLUnit
//            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
//
//    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
//    static EchoStatusDAO instance;
//    static EchoStatus entity;
//    public EchoStatusDAOTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//        instance = BaseStationMapperImpl.getInstance().echoStatusDAO();
//        entity = new EchoStatus();
//        entity.setCustomerAccount("77777");
//        entity.setSiteId(UUID.fromString("fe6d75c8-2f96-450b-ada9-2c6b329e565f"));
//        entity.setDeviceId("BB003003");
//        entity.setAlive(true);
//        entity.setLastUpdate(LocalDateTime.parse("2022-11-11 02:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of create method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test1_Create() {
//        System.out.println("create");
//        instance.create(entity);
//        List<EchoStatus> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
//        System.out.println("findByCustomerSite result - " + result);
//        assertEquals(entity, result.get(0));
//
//    }
//
//    /**
//     * Test of findByCustomerSite method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test2_FindByCustomerSite() {
//        System.out.println("findByCustomerSite");
//        List<EchoStatus> result = instance.findByCustomerSite(entity.getCustomerAccount(), entity.getSiteId());
//        assertEquals(entity, result.get(0));
//    }
//
//    /**
//     * Test of findByPK method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test3_FindByPK() {
//        System.out.println("findByPK");
//        List<EchoStatus> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
//        assertEquals(entity, result.get(0));
//    }
//
//    /**
//     * Test of update method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test4_Update() {
//        System.out.println("update");
//        instance.update(entity);
//        List<EchoStatus> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
//        assertEquals(entity, result.get(0));
//    }
//
//    /**
//     * Test of delete method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test5_Delete() {
//        System.out.println("delete");
//        instance.delete(entity);
//        List<EchoStatus> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getDeviceId());
//        assertEquals(0, result.size());
//    }
//
//    /**
//     * Test of printCQL method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test6_PrintCQL() {
//        System.out.println("printCQL");
//        String expResult = "INSERT INTO echo_status(customer_acct,site_id,device_id,is_alive,last_update) values('77777',fe6d75c8-2f96-450b-ada9-2c6b329e565f,'BB003003',true,'2022-11-11T02:00:00Z');";
//        String result = instance.printCQL(entity, "INSERT");
//        System.out.println("printCQL result - " + result);
//        assertEquals(expResult, result);
//    }
//    
//    /**
//     * Test of printCQL method, of class EchoStatusDAO.
//     */
//    @Test
//    public void test7_PrintCQL() {
//        System.out.println("printCQL");
//        String expResult = "DELETE FROM echo_status WHERE customer_acct='77777' AND site_id=fe6d75c8-2f96-450b-ada9-2c6b329e565f AND device_id='BB003003';";
//        String result = instance.printCQL(entity, "DELETE");
//        System.out.println("printCQL result - " + result);
//        assertEquals(expResult, result);
//    }

      @Test
    public void test() {

    }
    
}
