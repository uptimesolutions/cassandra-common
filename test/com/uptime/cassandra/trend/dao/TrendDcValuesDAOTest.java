/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.dao;

import com.uptime.cassandra.trend.entity.TrendDcValues;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TrendDcValuesDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//trend.cql", true, true, "worldview_dev1"));
    private static TrendDcValuesDAO instance;
    private static TrendDcValues entity;
    
    public TrendDcValuesDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = TrendMapperImpl.getInstance().trendDcValuesDAO();
        entity = new TrendDcValues();
        entity.setAlSetId(UUID.randomUUID());
        entity.setApSetId(UUID.randomUUID());
        entity.setAreaId(UUID.randomUUID());
        entity.setAssetId(UUID.randomUUID());
        entity.setCustomerAccount("77777");
        entity.setPointId(UUID.randomUUID());
        entity.setPointLocationId(UUID.randomUUID());
        entity.setSiteId(UUID.randomUUID());
        entity.setLabel("Trend Test 1");
        entity.setUnits("Unit Test 1");
        entity.setNotes("Test Note 1");
        entity.setValue(0.9F);
        entity.setHighAlert((float) 0.145);
        entity.setHighFault((float) 0.145);
        entity.setLowAlert((float) 0.145);
        entity.setLowFault((float) 0.145);
        entity.setSampleTime(Instant.parse("2022-07-01T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        entity.setSensorType("Test Sensor Type 1");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class TrendDcValuesDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<TrendDcValues> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getSampleTime());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findTrend method, of class TrendDcValuesDAO.
     */
    @Test
    public void test2_FindTrend() {
        System.out.println("findTrend");
        List<TrendDcValues> result = instance.findTrend(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), 1);
        assertEquals(entity, result.get(0));

    }

    /**
     * Test of findByPK method, of class TrendDcValuesDAO.
     */
    @Test
    public void test3_FindByPK() {
        System.out.println("findByPK");
        List<TrendDcValues> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getSampleTime());
        assertEquals(entity, result.get(0));

    }

    /**
     * Test of update method, of class TrendDcValuesDAO.
     */
    @Test
    public void test4_Update() {
        System.out.println("update");
        entity.setLabel("Trent Test 1 updated");
        instance.update(entity);
        List<TrendDcValues> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getSampleTime());
        assertEquals(entity, result.get(0));
        assertEquals(entity.getLabel(), result.get(0).getLabel());
    }

    /**
     * Test of delete method, of class TrendDcValuesDAO.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<TrendDcValues> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId(), entity.getSampleTime());
        assertEquals(0, result.size());
    }
}
