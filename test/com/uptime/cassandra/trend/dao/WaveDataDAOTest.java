/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.cassandra.trend.dao;

import com.uptime.cassandra.trend.entity.WaveData;
import com.uptime.cassandra.trend.entity.Waveform;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WaveDataDAOTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//trend.cql", true, true, "worldview_dev1"));
    private static WaveDataDAO instance;
    private static WaveData entity;
    public WaveDataDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = TrendMapperImpl.getInstance().waveDataDAO();
        entity = new WaveData();
        entity.setCustomerAccount("77777");
        entity.setSampleTime(Instant.parse("2022-02-21T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        entity.setSampleYear((short) 2022);
        entity.setSampleMonth((byte) 12);
        entity.setSensitivityValue(0.9F);
        entity.setAreaId(UUID.fromString("76327bd8-924a-4ea7-93bb-027841b96c70"));
        entity.setAssetId(UUID.fromString("6d75d5f6-05cc-43fd-be87-1615408b60ac"));
        entity.setCustomerAccount("77777");
        entity.setPointLocationId(UUID.fromString("665611d8-382f-4c95-b5af-a23432e59dac"));
        entity.setPointId(UUID.fromString("60685b3e-758a-4295-a243-3d600d8d75b5"));
        entity.setSiteId(UUID.fromString("15676cd8-61f6-42af-ae85-f3cfd1442913"));
        Waveform wf = new Waveform();
        wf.setChannelNum((byte)1);
        wf.setDeviceId("Test Device Id");
        wf.setSampleRateHz(0.9F);
        wf.setSampleTime(Instant.parse("2022-02-21T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        wf.setSensitivityUnits("Test Sensitivity Units");
        wf.setTachSpeed(12.1f);
        List<Double> samples = new ArrayList<>();
        samples.add(12.2);
        samples.add(12.3);
        samples.add(12.4);
        wf.setSamples(samples);
        entity.setWave(wf);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class WaveDataDAO.
     */
    @Test
    public void test1_Create() {
        System.out.println("create");
        instance.create(entity);
        List<WaveData> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getSampleYear(), entity.getSampleMonth(), entity.getSampleTime());
        System.out.println("create result -" + result.get(0).toString());
        System.out.println("entity -" + entity.toString());
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of findByPK method, of class WaveDataDAO.
     */
    @Test
    public void test2_FindByPK() {
        System.out.println("findByPK");
        List<WaveData> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getSampleYear(), entity.getSampleMonth(), entity.getSampleTime());
        assertEquals(entity, result.get(0));
    }
    
    /**
     * Test of update method, of class WaveDataDAO.
     */
    @Test
    public void test3_Update() {
        System.out.println("update");
        entity.setSensitivityValue(23);
        instance.update(entity);
        List<WaveData> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getSampleYear(), entity.getSampleMonth(), entity.getSampleTime());
        System.out.println("update result -" + result);
        assertEquals(entity, result.get(0));
    }

    /**
     * Test of delete method, of class WaveDataDAO.
     */
    @Test
    public void test4_Delete() {
        System.out.println("delete");
        instance.delete(entity);
        List<WaveData> result = instance.findByPK(entity.getCustomerAccount(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getSampleYear(), entity.getSampleMonth(), entity.getSampleTime());
        assertEquals(0, result.size());
    }

  /**
     * Test of printCQL method, of class WaveDataDAO.
     */
    @Test
    public void test90_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "INSERT";
        String expResult = "INSERT INTO wave_data(customer_acct, site_id, area_id, asset_id, point_location_id, point_id, sample_year, sample_month, sample_time ,sensitivity_value,wave) values('77777',15676cd8-61f6-42af-ae85-f3cfd1442913,76327bd8-924a-4ea7-93bb-027841b96c70,6d75d5f6-05cc-43fd-be87-1615408b60ac,665611d8-382f-4c95-b5af-a23432e59dac,60685b3e-758a-4295-a243-3d600d8d75b5,2022,12,'2022-02-21T01:00:00Z',23.0,'Waveform{sampleTime : 2022-02-21T01:00:00Z, deviceId : Test Device Id, channelNum : 1, sampleRateHz : 0.9, sensitivityUnits : Test Sensitivity Units, tachSpeed : 12.1, samples : [12.2, 12.3, 12.4]}');";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result INSERT - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of printCQL method, of class WaveDataDAO.
     */
    @Test
    public void test91_PrintCQL() {
        System.out.println("printCQL");
        String statementType = "DELETE";
        String expResult = "DELETE FROM wave_data WHERE customer_acct='77777' AND site_id=15676cd8-61f6-42af-ae85-f3cfd1442913 AND area_id=76327bd8-924a-4ea7-93bb-027841b96c70 AND asset_id=6d75d5f6-05cc-43fd-be87-1615408b60ac AND point_location_id=665611d8-382f-4c95-b5af-a23432e59dac AND point_id=60685b3e-758a-4295-a243-3d600d8d75b5 AND sample_year=2022 AND sample_month=12 AND sample_time ='2022-02-21T01:00:00Z';";
        String result = instance.printCQL(entity, statementType);
        System.out.println("result DELETE - " + result);
        assertEquals(expResult, result);
    }
}
